<?php
/**
 * The template for displaying the footer
 * Contains footer content and the closing of the #main and #page div elements.
 * @package CrunchPress
 * @subpackage Neomag
 */
?>
	<?php 
		global $neomag_footer_style,$post;

		if(isset($post)){
			$neomag_footer_style = get_post_meta ( $post->ID, "page-option-bottom-footer-style", true );
		}else{
			$neomag_footer_style = '';
		}
		cp_footer_html($neomag_footer_style);
	?>
<!--Wrapper End--> 
<?php wp_footer(); ?>
<script src="http://minhacasasolar.com.br/blog/wp-content/themes/neomag/frontend/js/jquery.bxslider.min.js"></script>
<script>
jQuery('.bxslider').bxSlider({
  infiniteLoop: false,
  hideControlOnEnd: true
});
</script>
</body>
</html>