<?php
/*
 * This file is used to generate different page layouts set from backend.
 */
 
 	//Fetch the theme Option Values
	$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');
	$neomag_maintenace_title = neomag_cp_get_themeoption_value('maintenace_title','general_settings');
	$neomag_countdown_time = neomag_cp_get_themeoption_value('countdown_time','general_settings');
	$neomag_email_mainte = neomag_cp_get_themeoption_value('email_mainte','general_settings');
	$neomag_mainte_description = neomag_cp_get_themeoption_value('mainte_description','general_settings');
	$neomag_social_icons_mainte = neomag_cp_get_themeoption_value('social_icons_mainte','general_settings');
	
	if($neomag_maintenance_mode <> 'disable'){
		//If Logged in then Remove Maintenance Page
		if ( is_user_logged_in() ) {
			$neomag_maintenance_mode = 'disable';
		} else {
			if($neomag_maintenance_mode == ''){}else{$neomag_maintenance_mode = 'enable';}
		}
	}
	
	if($neomag_maintenance_mode == 'enable'){
		//Trigger the Maintenance Mode Function Here
		maintenance_mode_fun();
	}else{
 
	get_header ();

		$neomag_page_builder_full = get_post_meta ( $post->ID, "cp-show-full-layout", true );      
		if($neomag_page_builder_full == 'No'){
			$neomag_sidebar_class = '';
			$neomag_content_class = '';
			$neomag_sidebar = get_post_meta ( $post->ID, 'page-option-sidebar-template', true );
			$neomag_sidebar_class = cp_sidebar_func($neomag_sidebar);
			$neomag_left_sidebar = get_post_meta ( $post->ID, "page-option-choose-left-sidebar", true );
			$neomag_right_sidebar = get_post_meta ( $post->ID, "page-option-choose-right-sidebar", true );
		}else{
			$neomag_sidebar_class = array('0'=>'no-sidebar','1'=>'col-md-12',);
			$neomag_content_class = array();
			$neomag_sidebar = array();
			$neomag_left_sidebar = '';
			$neomag_right_sidebar = '';
		}	
		
		$neomag_slider_off = '';
		$neomag_slider_type = '';
		$neomag_slider_slide = '';
		$neomag_slider_height = '';
		
		//Fetch the data from page
		$neomag_slider_off = get_post_meta ( $post->ID, "page-option-top-slider-on", true ); 
		$neomag_slider_type = get_post_meta ( $post->ID, "page-option-top-slider-types", true );
		$neomag_slider_type_album = get_post_meta ( $post->ID, "page-option-top-slider-album", true );
		$neomag_page_builder_full = get_post_meta ( $post->ID, "cp-show-full-layout", true );
		$neomag_page_title_breadcrumb = get_post_meta ( $post->ID, "page-option-item-page-title", true );
		$neomag_banner_text = get_post_meta ( $post->ID, "page-option-top-banner-text", true );
		$neomag_cp_page_caption = get_post_meta ( $post->ID, "cp-show-page-caption-pageant", true );
		$neomag_selected_page_header = get_post_meta ( $post->ID, "page-option-top-header-style", true );
		$neomag_selected_page_footer = get_post_meta ( $post->ID, "page-option-bottom-footer-style", true );
		$neomag_optional_page_class = get_post_meta ( $post->ID, "page-option-item-class", true );
		$neomag_video_post = get_post_meta ( $post->ID, "page-option-item-video-post", true );
		$neomag_post_slider = get_post_meta ( $post->ID, "page-option-item-post-slider", true );
		
		$neomag_cp_class_sch = '';
		$neomag_cp_page_title = get_post_meta ( $post->ID, "page-option-schedule-title", true );
		$neomag_cp_schedule = get_post_meta ( $post->ID, "page-option-top-schedule-mana", true );
		if($neomag_cp_schedule == 'No-Option'){
			$neomag_cp_class_sch = '';
		}else{
			$neomag_cp_class_sch = 'hide_caption';
		}
		
		$neomag_schedule_title = get_post_meta ( $post->ID, "page-option-schedule-title", true );
	
		$neomag_resv_button = neomag_cp_get_themeoption_value('resv_button','general_settings');
		$neomag_resv_text = neomag_cp_get_themeoption_value('resv_text','general_settings');
		$neomag_resv_short = neomag_cp_get_themeoption_value('resv_short','general_settings');
		
		//Video Banner Settings
		$neomag_slider_settings = get_option('slider_settings');
		
		if($neomag_slider_settings <> ''){
			$neomag_cp_slider = new DOMDocument ();
			$neomag_cp_slider->loadXML ( $neomag_slider_settings );
			//Video Banner Values
			$neomag_video_slider_on_off = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_slider_on_off');
			$neomag_video_banner_url = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_banner_url');
			$neomag_video_banner_title = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_banner_title');
			$neomag_video_banner_caption = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_banner_caption');
			$neomag_safari_banner_link = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','safari_banner_link');
		}

		//Video Banner Is Enabled
		
		if($neomag_slider_off == 'No'){
			if($neomag_video_post <> ''){ ?>
				
				<div class="post-main-slider">
                  <div class="container">
                     <div class="row">
                         <div class="col-md-12">
                          	<div class="video-header">
						<?php
							$neomag_agent = '';
							$neomag_browser = '';
							if(isset($_SERVER['HTTP_USER_AGENT'])){
								 $neomag_agent = $_SERVER['HTTP_USER_AGENT'];
							}

							if(strlen(strstr($neomag_agent,'Safari')) > 0 ){
								$neomag_browser = 'safari';
							   if(strstr($neomag_agent,'Chrome')){
								 $neomag_browser= 'chrome';
							   }
							}

							if($neomag_browser=='safari'){ ?>
								<div class="video-block" style="width:100%; height:900px;" data-vide-bg="" data-vide-options="position: 0% 50%"><img src = "<?php echo esc_url($neomag_safari_banner_link); ?>"></div>
							<?php }else{ 
							
							$neomag_video_post_data = get_post( $neomag_video_post ); 
								$neomag_video_title = $neomag_video_post_data->post_title; 
								$neomag_video_author_id = get_post_field( 'post_author', $neomag_video_post );
								$neomag_video_author = get_the_author_meta( 'user_nicename', $neomag_video_author_id );
								$neomag_video_date = get_the_date( 'F j, Y', $neomag_video_post );
								$neomag_video_content =  $neomag_video_post_data->post_content;
								
								$neomag_post_detail_xml = get_post_meta($neomag_video_post, 'post_detail_xml', true);
								if($neomag_post_detail_xml <> ''){
									$neomag_cp_post_xml = new DOMDocument ();
									$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml );
									$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
								}
								$neomag_video_post_url = $neomag_video_url_type;
								if($neomag_video_post_url <> ''){ neomag_video_banner_script($neomag_video_post_url); }
							?>
                              <div class="slider-caption">
                                  <h3><?php echo esc_attr($neomag_video_title); ?></h3>
                                  
                                  <ul class="post-tools">
                                  <li><i class="fa fa-user"></i> <?php echo esc_attr($neomag_video_author); ?></li>
                                  <li><i class="fa fa-clock-o"></i> <?php echo esc_attr($neomag_video_date); ?></li>
                                  </ul>
                                  
                                  <p><?php echo esc_attr($neomag_video_content); ?></p>
                                  
                                  <a class="shopping-button" href="<?php echo esc_url(site_url('/?post_type=product')); ?>"><?php esc_html_e('Shop Now','neomag');?></a>
                                  
                                  </div>
                              
							<?php }
					echo '</div></div></div></div></div>'; 
			}
		}else{ 	//Video Banner Is Disabled
			if(class_exists('cp_slider_class')){
				//Condition for Box Layout
				if($neomag_slider_off == 'Yes'){ 
					if($neomag_post_slider <> '' && ($neomag_post_slider != '78612')){ 
						$neomag_page_slider_id = 'cp-banner-1';
						echo '<div id="cp-banner-style-1">';
                  		echo cp_post_slider($neomag_post_slider, $neomag_page_slider_id);
						echo '</div>';
				} else {
					echo '<div class="main-banner">'; 
					echo cp_page_slider();
					echo '</div>';				
					} 
				}			
			}
		}
		if(is_search() || is_404()){
			$neomag_header_style = '';
		}else{
			$neomag_header_style = get_post_meta ( $post->ID, "page-option-top-header-style", true );
		}
		$neomag_html_class = cp_print_header_class($neomag_header_style);
		//Print Style 7
		if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
			cp_print_header_html($neomag_header_style);
		}
		$neomag_class_bread_margin = '';
		$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
		if($neomag_breadcrumbs == 'disable'){
			$neomag_class_bread_margin = 'margin_top_cp';
		}else{
			$neomag_class_bread_margin = '';
		}
		 if($neomag_slider_off <> 'Yes'){ 
				if(($neomag_video_post == '') && ($neomag_optional_page_class != 'gallery_full_width')){ 
		?>
            <!--PAGE TITLE SECTION START-->
                <div class="cp-page-title">
                  <h2>
                    <?php 
						if($neomag_page_title_breadcrumb <> ''){
							echo esc_attr($neomag_page_title_breadcrumb);
						}else{ 
							if($neomag_cp_page_title <> ''){ 
								echo esc_attr($neomag_cp_page_title);
							} else {
								echo get_the_title(); 
							}
						}
					?>
                  </h2>
                      <?php
						if(!is_front_page()){
							echo cp_breadcrumbs();
						}
					  ?>
                </div>
            <!--PAGE TITLE SECTION START--> 
		<?php } else { 
					if (! empty ( $neomag_cp_page_xml )) {
						$neomag_page_xml_val = new DOMDocument ();
						$neomag_page_xml_val->loadXML ( $neomag_cp_page_xml ); 
						foreach ( $neomag_page_xml_val->documentElement->childNodes as $neomag_item_xml ) { }
						}
		
		$value = cp_find_xml_value($neomag_item_xml,'column-text'); 
					$shrtcode = strtok($value, " "); 
					$yes_no = '';
					if(($neomag_item_xml->nodeName == 'Gallery ') && ($neomag_optional_page_class == 'gallery_full_width')){ $yes_no = 'yes'; } else { $yes_no = 'no'; }
					if(($shrtcode == '[map')||($yes_no = 'yes')){ 
						if($shrtcode == '[coming_soon'){  } else {
		?>
			
            <!--PAGE TITLE SECTION START-->
                <div class="cp-page-title">
                  <h2>
                    <?php 
						if($neomag_page_title_breadcrumb <> ''){
							echo esc_attr($neomag_page_title_breadcrumb);
						}else{ 
							if($neomag_cp_page_title <> ''){ 
							echo esc_attr($neomag_cp_page_title);
							} else {
								echo get_the_title(); 
							}
						}
					?>
                  </h2>
                      <?php
						if(!is_front_page()){
							echo cp_breadcrumbs();
						}
					  ?>
                </div>
            <!--PAGE TITLE SECTION START--> 


		<?php	} } } } ?>
		<!--BREADCRUMS END--> 
	<?php 
      $neomag_class = '';
      $neomag_class_sidebar = '';
      $neomag_class_gallery = 'cp-content-wrap';
          if($neomag_sidebar == 'no-sidebar'){ $neomag_class = 'col-md-12'; }
            if($neomag_sidebar == 'both-sidebar'){ $neomag_class = 'col-md-6'; $neomag_class_sidebar = 'col-md-3'; }
                if(($neomag_sidebar == 'left-sidebar')||($neomag_sidebar == 'right-sidebar')){ $neomag_class = 'col-md-9'; $neomag_class_sidebar = 'col-md-3'; }
                    elseif (($neomag_sidebar == 'both-sidebar-left')||($neomag_sidebar == 'both-sidebar-right')){ $neomag_class = 'col-md-6'; $neomag_class_sidebar = 'col-md-3'; }
                        if(($neomag_optional_page_class == 'gallery_full_width')||($neomag_optional_page_class == 'gallery_full_width about_us')){ $neomag_class_gallery = ''; }
      $neomag_optional_page_class = explode('gallery_full_width', $neomag_optional_page_class);
    ?>
  <!--CONTENT START-->
  <div id="cp-content-wrap" class="<?php echo esc_attr($neomag_class_gallery).' '; if($neomag_optional_page_class <> ''){ 
  if(isset($neomag_optional_page_class[1])){ echo esc_attr($neomag_optional_page_class[1]); } } ?>">

		<div class="<?php if($neomag_page_builder_full <> 'Yes'){echo 'container';}else{echo 'full-width-content margin-top-bottom-cp ';}?>">
			<?php if($neomag_slider_off == 'No'){ ?>
				<?php if($neomag_page_builder_full == 'Yes'){echo '<div class="container">';}?>
				
				<?php if($neomag_page_builder_full == 'Yes'){echo '</div>';}?>
			<?php } ?>    
			<!--MAIN CONTANT ARTICLE START-->
			<div class="<?php if($neomag_page_builder_full <> 'Yes'){echo 'margin-top-bottom-cp';}?>">
            	<?php $row = ''; if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){ $row = ''; } else { $row = '';} ?>
				<div class="<?php echo esc_attr($row); ?>">
					<?php
					if($neomag_sidebar == "left-sidebar" || $neomag_sidebar == "both-sidebar" || $neomag_sidebar == "both-sidebar-left"){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_left_sidebar ); ?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php
					}
					if($neomag_sidebar == 'both-sidebar-left'){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_right_sidebar );?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php }  ?>
                    
					<div id="block_content_first" >
						<div class="container-res <?php echo esc_attr($neomag_class); ?>">
							<div class="">
								<?php
									global $neomag_item_row_size, $post, $post_id; 
									$neomag_cp_page_xml = get_post_meta($post->ID,'page-option-item-xml', true);		
									$neomag_item_row_size = 0;	
									$neomag_counter = 0;
									// Page Item Part
									if (! empty ( $neomag_cp_page_xml )) {
										$neomag_page_xml_val = new DOMDocument ();
										$neomag_page_xml_val->loadXML ( $neomag_cp_page_xml ); 
										foreach ( $neomag_page_xml_val->documentElement->childNodes as $neomag_item_xml ) { 
											$neomag_counter++; 
											switch ($neomag_item_xml->nodeName) { 
												case 'Accordion' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm' );
													cp_print_accordion_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Blog' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'blog-posts');
													cp_print_blog_item ( $neomag_item_xml );
													echo '</div></div>';
												break;
												case 'News' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'blog-post');
													cp_print_news_item ( $neomag_item_xml );
													echo '</div>';
												break;
												case 'Latest-Products' :
													if(class_exists('cp_function_library')){
														if(class_exists("Woocommerce")){
															cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'');
																$cp_products_class = new cp_products_class;
																$cp_products_class->cp_print_latest_products ( $neomag_item_xml ); 
														}	
													}
													echo '</div>';
												break;
												case 'Hot-Deal' :
													if(class_exists('cp_function_library')){
														if(class_exists("Woocommerce")){
															cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'');
																$cp_products_class = new cp_products_class;
																$cp_products_class->cp_print_hot_deal ( $neomag_item_xml ); 
														}	
													}
													echo '</div>';
												break;
												case 'Blog_Slider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'');
													cp_print_blog_slider_item ( $neomag_item_xml );
													echo '</div>';
												break;
												case 'Heading-Banner' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'');
													cp_heading_style ( $neomag_item_xml );
													echo '</div>';
												break;
												case 'Offers' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'');
													$cp_service_class = new cp_service_class;
													$cp_service_class->cp_offers_element ( $neomag_item_xml );
													echo '</div>';
												break;
												case 'Timeline' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm timeline-posts');
													if(class_exists('cp_timeline_class')){
														$cp_timeline_class = new cp_timeline_class;
														$cp_timeline_class->cp_print_timeline_item($neomag_item_xml);
													}													
													echo '</div>';
													break;
												case 'Events-Slider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm timeline-posts');
													if(class_exists('EM_Events')){
														$cp_events_class = new cp_events_class;
														$cp_events_class->cp_print_events_slider($neomag_item_xml);
													}											
													echo '</div>';
													break;
												case 'Fixtures' :
												cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm timeline-posts');
													$cp_fixtures_class = new cp_fixtures_class;
													$cp_fixtures_class->cp_print_fixtures_item($neomag_item_xml);
												echo '</div>';
												break;
												case 'Service' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm timeline-posts');
													if(class_exists('cp_service_class')){
														$cp_service_class = new cp_service_class;
														$cp_service_class->cp_print_service_item($neomag_item_xml);
													}													
													echo '</div>';
													break;
												case 'Classes' :
												cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm timeline-posts');
												if(class_exists('cp_classes_class')){
													$cp_classes_class = new cp_classes_class;
													$cp_classes_class->cp_print_classes_item($neomag_item_xml);
												}													
												echo '</div>';
												break;
												case 'Attractions' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm attractions');
													if(class_exists('cp_attractions_class')){
														$cp_attractions_class = new cp_attractions_class;
														$cp_attractions_class->cp_print_attractions_item($neomag_item_xml);
													}													
													echo '</div>';
												break;
												case 'Destinations' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm attrac-slider');
													if(class_exists('cp_attractions_class')){
														$cp_attractions_class = new cp_attractions_class;
														$cp_attractions_class->cp_destinations_slider($neomag_item_xml);
													}													
													echo '</div>';
												break;
												case 'Division_Start' :
													if($neomag_page_builder_full == 'Yes'){
														cp_print_div_item ( $neomag_item_xml );
													}	
													break;	
												case 'Division_End' :
													if($neomag_page_builder_full == 'Yes'){
														cp_print_div_end_item ( $neomag_item_xml );
													}	
													break;		
												case 'Events' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'event_calendar-cp mbtm');
													if(class_exists('EM_Events')){
														$cp_events_class = new cp_events_class;
														$cp_events_class->cp_page_event_manager_plugin($neomag_item_xml);
													}
													echo '</div>';
													break;
												case 'Woo-Products' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm woo-produ-cp');
													if(class_exists('cp_function_library')){
														if(class_exists("Woocommerce")){
															cp_print_wooproduct_item ( $neomag_item_xml );
														}	
													}
													echo '</div>'; 
												break;
												case 'Store' :
													if(class_exists('cp_function_library')){
														if(class_exists("Woocommerce")){
															cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm woo-produ-cp');
																$cp_products_class = new cp_products_class;
																$cp_products_class->cp_print_store_item ( $neomag_item_xml ); 
														}	
													}
													echo '</div>';
												break;
												case 'Products_Slider' :
													if(class_exists('cp_function_library')){
														if(class_exists("Woocommerce")){
															cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm home-blog');
																$cp_products_class = new cp_products_class;
																$cp_products_class->cp_print_products_slider_item ( $neomag_item_xml ); 
														}	
													}
													echo '</div>';
												break;
												case 'Sermons-Gallery' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm sermons-gallery-cp');
													if(class_exists('cp_album_class')){
														$cp_album_class = new cp_album_class;
														$cp_album_class->cp_print_gallery_sermons_item ( $neomag_item_xml );
													}
													echo '</div>';
												break;
												case 'Latest-News' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm latest-news-box');
													cp_print_latest_news_item ( $neomag_item_xml );
													echo '</div>';
												break;
												case 'Sermons' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm sermons-cp');
													if(class_exists('cp_album_class')){
														$cp_album_class = new cp_album_class;
														$cp_album_class->cp_print_sermons_listing_item ( $neomag_item_xml );
													}
													echo '</div>';
												break;
												case 'Events-Counter' :
													if(class_exists('cp_function_library')){
														if(class_exists('EM_Events')){
															cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm');
															$cp_events_class = new cp_events_class;
															$cp_events_class->cp_print_count_events_item ( $neomag_item_xml );
															echo '</div>';
														}
													}
												break;
												case 'Event-Slider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm upcoming-events-box');
													if(class_exists('cp_function_library')){
														if(class_exists('EM_Events')){
															$cp_events_class = new cp_events_class;
															$cp_events_class->cp_print_upcomming_event ( $neomag_item_xml );
														}
													}
													echo '</div>';
												break;
												case 'Single-Sermons' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm single-sermons-cp');
													if(class_exists('cp_album_class')){
														$cp_album_class = new cp_album_class;
														$cp_album_class->cp_print_sermons_ofweek_item ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'Newest-Sermons' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm newest-sermons-cp');
													if(class_exists('cp_album_class')){
														$cp_album_class = new cp_album_class;
														$cp_album_class->cp_print_newest_sermons_item ( $neomag_item_xml );
													}
													echo '</div>';
													break;													
												case 'Modern-Blog' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													cp_print_blog_modern_item ( $neomag_item_xml );
													echo '</div>';
													break;	
												case 'Sidebar' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													cp_print_sidebar_item ( $neomag_item_xml );
													echo '</div>';
													break;		
												case 'Pastors' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'pastors fadeIn cp_load mbtm');
													if(class_exists('cp_album_class')){
														$cp_album_class = new cp_album_class;
														$cp_album_class->cp_print_pastor_item_item ( $neomag_item_xml );
													}
													echo '</div>';
													break;		
												case 'News' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm latest-news-cp');
													cp_print_news_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Members' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm our-team-cp');
													if(class_exists('cp_members_class')){
													$cp_members_class = new cp_members_class;
														$cp_members_class->cp_print_cp_members_item ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'Our-Team' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm our-team-cp');
													if(class_exists('cp_team_class')){
													$cp_team_class = new cp_team_class;
														$cp_team_class->cp_print_team_item ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'Team-Slider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm staff-wrapp');
													if(class_exists('cp_team_class')){
													$cp_team_class = new cp_team_class;
														$cp_team_class->cp_print_team_item_slider ( $neomag_item_xml );
													}
													echo '</div>';
													break;	
												case 'Contact-Form' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ), 'mt0 signup mbtm' );
													cp_print_contact_form ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Column' : 
													$value = cp_find_xml_value($neomag_item_xml,'column-text'); 
													$shrtcode = strtok($value, " ");
													if($shrtcode == '[coming_soon' || $shrtcode == '[two_col_parallax' || $shrtcode == '[images'){ } else {
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm column' );}														
													cp_print_column_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Features' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'feature fadeIn cp_load mbtm' );
													echo cp_print_column_service ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Content' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ) ,'fadeIn cp_load mbtm');
													cp_print_content_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Divider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ), 'wrapper fadeIn cp_load' );
													cp_print_divider ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Gallery' :
													cp_print_gallery_item ( $neomag_item_xml );
													break;
												case 'Message-Box' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm' );
													cp_print_message_box ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Slider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ), 'containter_slider fadeIn cp_load mbtm' );
													cp_print_slider_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Tab' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'fadeIn cp_load mbtm about' );
													cp_print_tab_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'Testimonial' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													if(class_exists('cp_testi_class')){
													$cp_testi_class = new cp_testi_class;
														$cp_testi_class->cp_print_testimonial ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'Client-Slider' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm quote-bg');
													if(class_exists('cp_testi_class')){
													$cp_testi_class = new cp_testi_class;
														$cp_testi_class->cp_print_testimonial_slider ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'faq' :
													if(class_exists('cp_faq_class')){
													$cp_faq_class = new cp_faq_class;
														$cp_faq_class->cp_print_faq ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'Portfolio' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm portfolio-gallery-cp');
													if(class_exists('cp_portfolio_class')){
													$cp_portfolio_class = new cp_portfolio_class;
														$cp_portfolio_class->cp_print_port_item ( $neomag_item_xml );
													}
													echo '</div>';
													break;	
												case 'Portfolio-Gallery' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													if(class_exists('cp_portfolio_class')){
													$cp_portfolio_class = new cp_portfolio_class;
														$cp_portfolio_class->cp_portfolio_gallery ( $neomag_item_xml );
													}
													echo '</div>';
													break;
												case 'Carousel' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													if(class_exists('cp_timeline_class')){
													$cp_timeline_class = new cp_timeline_class;
														$cp_timeline_class->cp_timeline_slider ( $neomag_item_xml );
													}
													echo '</div>';
													break;	
												case 'Crowd-Funding' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													cp_print_ignition_item($neomag_item_xml);
													echo '</div>';
													break;	
												case 'Feature-Projects' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm');
													echo '</div>';
													break;								
												case 'Toggle-Box' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm' );
													cp_print_toggle_box_item ( $neomag_item_xml );
													echo '</div>';
													break;
												case 'DonateNow' :
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm' );
													cp_print_donate_item ( $neomag_item_xml );
													echo '</div>';
													break;	
												default : 
													cp_print_item_size ( cp_find_xml_value ( $neomag_item_xml, 'size' ),'mbtm' );
													echo '</div>';
													break;
											} 
										}
										//Content Area
										if($neomag_page_xml_val->documentElement->childNodes->length == 0){ 
											echo '<div class="col-md-12">'; 
												cp_print_default_content_item();
											echo '</div>';
										}										
									}else{ 
										echo '<div class="col-md-12">'; 
										
										if(is_page('cart')||is_page('checkout')){ cp_print_cart_content_item(); } else {	cp_print_default_content_item(); }
										
										echo '</div>';
									}
							   ?>
							</div>
					   </div>
                       </div></div>
        <?php if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){  } else { echo '</div>'; } ?>
					<?php
					$neomag_page_builder_full = get_post_meta ( $post->ID, "cp-show-full-layout", true );      
					if($neomag_page_builder_full == 'No'){
						$neomag_sidebar_class = '';
						$neomag_content_class = '';
						$neomag_sidebar = get_post_meta ( $post->ID, 'page-option-sidebar-template', true );
						$neomag_sidebar_class = cp_sidebar_func($neomag_sidebar);
						$neomag_left_sidebar = get_post_meta ( $post->ID, "page-option-choose-left-sidebar", true );
						$neomag_right_sidebar = get_post_meta ( $post->ID, "page-option-choose-right-sidebar", true );
					}else{
						$neomag_sidebar_class = array('0'=>'no-sidebar','1'=>'col-md-12',);
						$neomag_content_class = array();
						$neomag_sidebar = array();
						$neomag_left_sidebar = '';
						$neomag_right_sidebar = '';
					}
					if($neomag_sidebar == "both-sidebar-right"){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_left_sidebar ); ?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php
					}
					if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_right_sidebar );?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php } ?>
                    <?php if(is_page('contact-us')||is_page('coming-soon')||is_page('about-us')||(is_page('about-me'))){ } else { ?>
                    </div>
                 </div>
                 <?php } if(is_page('about-us')||('about-me')){ } else { echo '</div></div></div>'; }?>
  
  <!--CONTENT END--> 
<?php get_footer(); } ?>