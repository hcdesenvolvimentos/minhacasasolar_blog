<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Minha_Casa_Solar
 */

	$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoBlog = $fotoBlog[0];

	global $post;
	$categories = get_the_category();
	$idPostAtual = $post->ID;
	$postsRelacionados = rwmb_meta('MinhaCasaSolar_post_produto');
get_header();
?>
<!-- PAGINA DE POST	-->
<div class="pg pg-post">
	
	<!-- BANNER DO POST	-->
	<figure class="bannerPost" style="background: url(<?php echo $fotoBlog  ?>)">
	</figure>

	<!-- DEFININDO CONTAINER -->
	<div class="containerLagura">

		<!-- DEFININDO COLUNAS -->
		<div class="row">

			<!-- DEFININDO A COLUNA E O TAMANHO DA COLUNA -->
			<div class="col-sm-8">
				
				<!-- ARTICLE PARA DEFINIR A ESTRUTURA DO POST -->
				<article class="estruturaPost">

					<!-- CATEGORIA DO POST -->
					<?php 
						foreach ($categories as $categories){
							if ($categories->name != "Destaque"){
								$nomeCategoria = $categories->name;
								$idCategoria = $categories->cat_ID;
							}
						} 
					?>
					<h2><?php echo $nomeCategoria ?></h2>

					<!-- TITULO DO POST -->
					<h1><?php echo get_the_title() ?></h1>

					<!--DATA DO POST -->
					<strong><?php echo  get_the_date('j F, Y'); ?></strong>

					<!-- TEXTO DO POST -->
					<div class="textoDoPost">

						<?php 
						if ( have_posts() ) : while( have_posts() ) : the_post();
							
							echo the_content();

						endwhile;endif; wp_reset_query(); ?>
					</div>

				</article>

				<div class="disqus">
					<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						    comments_template();
						endif; 
					?>
				</div>

				<!-- LISTA DE POSTS RELACIONADOS -->
				<div class="postsRelacionados">
					<span>Você pode gostar também de...</span>

					<?php 
				        //FILTRO CATEGORIA POST            
				        $filtroDePostPorCategoria = new WP_Query(array(
				            // TIPO DE POST
				            'post_type'     => 'post',
				            
				            // POST POR PAGINA
				            'posts_per_page'   => 3,
				            // FILTRANDO PELA CATEGORIA DESTAQUE
				            'tax_query'     => array(
				                array(
				                    // TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
				                    'taxonomy' => 'category',
				                    // PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
				                    'field'    => 'term_id',
				                    // SLUG DA CATEGORIA
				                    'terms'    => $idCategoria,
				                    )
				                )
				            )
				        );    
				    
				    ?>
					<ul>
						<?php 
			        	// LOOP DE POST
							while ( $filtroDePostPorCategoria->have_posts() ) : $filtroDePostPorCategoria->the_post();
								$fotoDestaquePostCategoria = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoDestaquePostCategoria = $fotoDestaquePostCategoria[0];
									global $post;
			                        $idpostLoop = $post;
			                        if ($idPostAtual != $idpostLoop->ID):
					 	?>	
					
						<li>
							<a href="<?php echo get_permalink(); ?>" class="linkPost">
							<article class="estruturaPostRelacionado">
								<figure class="imagemDestaquePost" style="background: url(<?php echo $fotoDestaquePostCategoria	?>)">
								</figure>
								<h2><?php echo $nomeCategoria; ?></h2>
								<h1><?php echo get_the_title(); ?></h1>
								<span><?php the_time('j F, Y');?></span>
								<p><?php customExcerpt(120); ?></p>
							</article>
							</a>	
							<?php disqus_count('myshortcode'); ?>
						</li>

					<?php endif; endwhile; ?>
					</ul>
				</div>

				<?php //LOOP DE POST CATEGORIA DESTAQUE				
					$postsProdutos = new WP_Query(array(
						'post_type'     => 'produtos',
						'posts_per_page'   => -1, 
						)
					); 
				if($postsProdutos):
				?>
					<!-- LISTA DE PRODUTOS RELACIONADOS -->
					<div class="produtosRelacionados">
						<div id="carosselProdutos" class="carrosselProdutos">
							<?php 
							
							
								while ($postsProdutos->have_posts()) : $postsProdutos->the_post();
									$fotoProduto = wp_get_attachment_image_src( get_post_thumbnail_id($postsProdutos->ID), 'full' );
									$fotoProduto = $fotoProduto[0];	
									$metaPrefixo = "MinhaCasaSolar_";
									$precoSemDesconto = rwmb_meta($metaPrefixo . 'preco_sem_desconto'); 
									$desconto = rwmb_meta($metaPrefixo . 'desconto');
									$precoComDesconto = rwmb_meta($metaPrefixo . 'preco_com_desconto');
									$parcelamento = rwmb_meta($metaPrefixo . 'parcelamento');
									$precoNoBoleto = rwmb_meta($metaPrefixo . 'preco_boleto');
									$avaliacaoProduto = rwmb_meta($metaPrefixo . 'avaliacao_produto');
									$linkProduto = rwmb_meta($metaPrefixo . 'link_produto');
									$precoParcela = rwmb_meta($metaPrefixo . 'preco_parcela');
									global $post;
									
							
									$verificacao = in_array($post->ID,$postsRelacionados);

										
							?>	
							<?php
								if ($verificacao):
									
								
							?>
							<div class="item">
								
								<div class="produto">
									<a href="<?php echo $linkProduto; ?>" class="linkProduto" target="_blank">
										<figure class="imagemDestaque">
											<img src="<?php echo $fotoProduto ?>" alt="<?php echo get_the_title(); ?>">
										</figure>
										<?php if($desconto != 0): ?><span class="porcentagemDesconto"><?php echo ($desconto . '%'); ?> OFF</span><?php endif; ?>
										<h4 class="nomeProduto"><?php echo get_the_title(); ?></h4>
										<p class="precoSemDesconto">De R$ <?php echo $precoSemDesconto; ?></p>
										<p class="precoComDesconto">R$ <?php echo $precoComDesconto; ?></p>
										<p class="precoParcelado">ou <?php echo $parcelamento; ?> x de R$ <?php echo $precoParcela; ?></p>
										<p class="precoNoBoleto">ou <strong>R$ <?php echo $precoNoBoleto; ?></strong> no boleto</p>
										<span class="avaliacaoProduto">
											<?php for($i=0; $i<$avaliacaoProduto; $i++): ?>
											<img src="http://localhost:8888/projetos/minhacasasolar_blog/wp-content/uploads/2018/08/star.svg" alt="star">
											<?php endfor; ?>
										</span>
									</a>
								</div>
							</div>
							<?php endif;endwhile; wp_reset_query(); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>

			<!-- DEFININDO A COLUNA E O TAMANHO DA COLUNA -->
			<div class="col-sm-4">
				<!-- SIDEBAR-->
				<?php include (TEMPLATEPATH . "/sidebar-single.php");  ?>
			</div>

		</div>


	</div>
	
</div>
<?php
get_footer();
