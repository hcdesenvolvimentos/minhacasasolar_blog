<?php

/**
 * Plugin Name: Base do Minha Casa Solar
 * Description: Controle base do Minha Casa Solar.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseMinhaCasaSolar () {

		// TIPOS DE CONTEÚDO
		conteudosMinhaCasaSolar();

		taxonomiaMinhaCasaSolar();

		metaboxesMinhaCasaSolar();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosMinhaCasaSolar (){

		// TIPOS DE CONTEÚDO
		tipolinhaDoTempo();

		// TIPOS DE PRODUTOS
		tipoProdutos();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				case 'produtos':
					$titulo = 'Nome do produto';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipolinhaDoTempo() {

		$rotuloslinhaDoTempo= array(
								'name'               => 'Linha do tempo',
								'singular_name'      => 'linha',
								'menu_name'          => 'Linha do tempo',
								'name_admin_bar'     => 'Linha do tempo',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova linha',
								'new_item'           => 'Nova linha',
								'edit_item'          => 'Editar linha do tempo',
								'view_item'          => 'Ver linha',
								'all_items'          => 'Todas as linhas',
								'search_items'       => 'Buscar linha',
								'parent_item_colon'  => 'Das linhas',
								'not_found'          => 'Nenhuma linha cadastrado.',
								'not_found_in_trash' => 'Nenhuma linha na lixeira.'
							);

		$argslinhaDoTempo 	= array(
								'labels'             => $rotuloslinhaDoTempo,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'linha-do-tempo' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail', 'editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('linhadotempo', $argslinhaDoTempo);

	}
	function tipoProdutos() {
		$rotulosProdutos = array(
			'name' 				=> 'Produtos',
			'singular_name' 	=> 'Produto',
			'menu_name' 		=> 'Produtos ',
			'name_admin_bar' 	=> 'Produtos',
			'add_new'           => 'Adicionar novo',
			'add_new_item'      => 'Adicionar novo produto',
			'new_item'          => 'Novo produto',
			'edit_item'         => 'Editar produto',
			'view_item'         => 'Ver produto',
			'all_items'         => 'Todos os produtos',
			'search_items'      => 'Buscar produto',
			'parent_item_colon' => 'Dos produtos',
			'not_found'         => 'Nenhum produto cadastrado.',
			'not_found_in_trash'=> 'Nenhum produto na lixeira.'
		);
		$argsProdutos	= array(
			'labels'			 => $rotulosProdutos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-cart',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'produtos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail', 'editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('produtos', $argsProdutos);
	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaMinhaCasaSolar () {		
		//taxonomiaCategoriaDestaque();
		taxonomiaCategoriaProdutos();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}		
		
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaProdutos() {

			$rotulosCategoriaProdutos = array(
				'name'              => 'Categorias de produtos',
				'singular_name'     => 'Categoria de produto',
				'search_items'      => 'Buscar categorias de produtos',
				'all_items'         => 'Todas as categorias de produtos',
				'parent_item'       => 'Categoria de produtos pai',
				'parent_item_colon' => 'Categoria de produtos pai:',
				'edit_item'         => 'Editar categoria de produtos',
				'update_item'       => 'Atualizar categoria de produtos',
				'add_new_item'      => 'Nova categoria de produtos',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de produtos',
				);

			$argsCategoriaProdutos 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaProdutos,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-produto' ),
				);

			register_taxonomy( 'categoriaProduto', array( 'produtos' ), $argsCategoriaProdutos );

		}		

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesMinhaCasaSolar(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'MinhaCasaSolar_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLinhadotempo',
				'title'			=> 'Detalhes da linha',
				'pages' 		=> array( 'linhadotempo' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Data: ',
						'id'    => "{$prefix}data_linha",
						'desc'  => '',
						'type'  => 'text',
					),	
					
									
				),
			);

			// METABOX DE PRODUTOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxProdutos',
				'title'			=> 'Detalhes do produto',
				'pages' 		=> array( 'produtos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Preço sem desconto: ',
						'id'    => "{$prefix}preco_sem_desconto",
						'desc'  => 'Insira aqui o preço sem desconto do produto.',
						'type'  => 'text',
					),
					array(
						'name'  => 'Desconto: ',
						'id'    => "{$prefix}desconto",
						'desc'  => 'Insira aqui a porcentagem de desconto do produto.',
						'type'  => 'number',
					),
					array(
						'name'  => 'Preço com desconto: ',
						'id'    => "{$prefix}preco_com_desconto",
						'desc'  => 'Insira aqui o preço sem desconto do produto.',
						'type'  => 'text',
					),
					array(
						'name'  => 'Parcelamento: ',
						'id'    => "{$prefix}parcelamento",
						'desc'  => 'Insira aqui o numero de vezes para parcelamento',
						'type'  => 'number',
					),
					array(
						'name'  => 'Preço de cada parcela: ',
						'id'    => "{$prefix}preco_parcela",
						'desc'  => 'Insira aqui preço de cada parcela',
						'type'  => 'text',
					),
					array(
						'name'  => 'Preço no boleto: ',
						'id'    => "{$prefix}preco_boleto",
						'desc'  => 'Insira aqui o preço do produto no boleto.',
						'type'  => 'text',
					),
					array(
						'name'  => 'Avaliação do produto: ',
						'id'    => "{$prefix}avaliacao_produto",
						'desc'  => 'Insira aqui o numero da avaliação do produto.',
						'type'  => 'number',
					),
					array(
						'name'  => 'Link do produto: ',
						'id'    => "{$prefix}link_produto",
						'desc'  => 'Insira aqui o link do produto.',
						'type'  => 'text',
					),
					
									
				),
			);//'post_type'  => 'servico',


			// METABOX DE POST
			$metaboxes[] = array(

				'id'			=> 'detalhesPost',
				'title'			=> 'Detalhes do post',
				'pages' 		=> array( 'post' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  	 => 'Produtos: ',
						'id'    	 => "{$prefix}post_produto",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'produtos',
						'field_type' => 'select_advanced',
						'multiple'	 => 'true'	
					),
				)
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesMinhaCasaSolar(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerMinhaCasaSolar(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseMinhaCasaSolar');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseMinhaCasaSolar();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );