

    Item Name : NeoMag Magazine Theme
	
    Item Version : Version 1.0
	
    Minumum Required : WordPress Version 4.0
	
    Author by : CrunchPress
	
    Support via Email : support@crunchpress.com
	
    Support via Forum : http://crunchpress.com/support



	NeoMag Blog & Magazine WordPress Theme is suitable for magazines, blogs, writers, personal sites and store fronts those require clean style. 
	We highly recommend the NeoMag Blog & Magazine WordPress Theme because we think it is one of the best Magazine WordPress themes available today. 
	NeoMag have all the features required by a content rich site content blocks, blog layouts, About Us template, Contact Page Layouts, Dozens of 
	Short-codes, and multiple Gallery Layouts. 
	
	NeoMag is fully-responsive theme based on Twitter Bootstrap 3.3.5.It is easy to use. It is very flexible 
	and customizable.It is beautiful, mobile-friendly responsive design.You can customize your fonts,and colour. All this can be done quickly from the 
	WordPress dashboard.

	Thank you for purchasing our theme,
		
	We provide technical support through our ticket system at http://crunchpress.com/support
		
	For any inquery please open a ticket, our support team will help you out. 