<?php
	/*
	 * This file will generate 404 error page.
	 */	
get_header(); 

//Get Theme Options for Page Layout
$neomag_select_layout_cp = '';
$neomag_cp_general_settings = get_option('general_settings');
if($neomag_cp_general_settings <> ''){
	$neomag_cp_logo = new DOMDocument ();
	$neomag_cp_logo->loadXML ( $neomag_cp_general_settings );
	$neomag_select_layout_cp = cp_find_xml_value($neomag_cp_logo->documentElement,'select_layout_cp');
	$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
}
?>
<!--Inner Pages Heading Area Start-->
  <div class="cp-page-title">
        <h2><?php esc_html_e('Page 404','neomag');?></h2>
        <?php if($neomag_breadcrumbs == 'enable'){?>
          <?php
				if(!is_front_page()){
					echo cp_breadcrumbs();
				}
			?>
        <?php } ?>
  </div>
  <!--Inner Pages Heading Area End-->  
    <!--CONTENT START-->
  <div id="cp-content-wrap" class="page404">
      <div class="container">
          <div class="row">
              <div class="col-md-12">  
              
                  <h1><?php esc_html_e('404','neomag');?></h1>
                 <h3><?php esc_html_e('The request page cannot be found','neomag');?> </h3>
                 <p><?php esc_html_e('Go back,or return to Home Page to choose a new page.','neomag');?><br>
                <?php esc_html_e('Please report any broken links to our team.','neomag');?>
                  </p>
                  
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="cp-back-button"><?php esc_html_e('Back to Homepage','neomag');?></a>
              
              </div>
          </div>  
      </div>
  </div>
  <!--CONTENT END--> 
<?php get_footer();?>