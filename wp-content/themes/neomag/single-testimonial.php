<?php get_header(); 
 if ( have_posts() ){ while (have_posts()){ the_post();
	
	global $post;
	
	$neomag_team_social = '';
	$neomag_sidebar = '';
	$neomag_left_sidebar = '';
	$neomag_right_sidebar = '';
	$neomag_team_designation = '';
	// Get Post Meta Elements detail 
	$neomag_team_detail_xml = get_post_meta($post->ID, 'team_detail_xml', true);
	if($neomag_team_detail_xml <> ''){
		$neomag_team_xml = new DOMDocument ();
		$neomag_team_xml->loadXML ( preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $neomag_team_detail_xml) );
		$neomag_team_social = cp_find_xml_value($neomag_team_xml->documentElement,'team_social');
		$neomag_sidebar = cp_find_xml_value($neomag_team_xml->documentElement,'sidebar_team');
		$neomag_left_sidebar = cp_find_xml_value($neomag_team_xml->documentElement,'left_sidebar_team');
		$neomag_right_sidebar = cp_find_xml_value($neomag_team_xml->documentElement,'right_sidebar_team');
		$neomag_team_designation = cp_find_xml_value($neomag_team_xml->documentElement,'team_designation');
		$neomag_team_facebook = cp_find_xml_value($neomag_team_xml->documentElement,'team_facebook');
		$neomag_team_caption = cp_find_xml_value($neomag_team_xml->documentElement,'team_caption');
		
		$neomag_team_linkedin = cp_find_xml_value($neomag_team_xml->documentElement,'team_linkedin');
		$neomag_google_plus = cp_find_xml_value($neomag_team_xml->documentElement,'team_google');
		$neomag_team_twitter = cp_find_xml_value($neomag_team_xml->documentElement,'team_twitter');
	}
	
	$neomag_select_layout_cp = '';
	$neomag_cp_general_settings = get_option('general_settings');
	if($neomag_cp_general_settings <> ''){
		$neomag_cp_logo = new DOMDocument ();
		$neomag_cp_logo->loadXML ( $neomag_cp_general_settings );
		$neomag_select_layout_cp = cp_find_xml_value($neomag_cp_logo->documentElement,'select_layout_cp');
	}
	$neomag_cp_page_caption = '';
	//Get Sidebar for page
	$neomag_sidebar_class = cp_sidebar_func($neomag_sidebar);
	$neomag_header_style = '';
	$neomag_html_class_banner = '';
	$neomag_html_class = cp_print_header_class($neomag_header_style);
	if($neomag_html_class <> ''){$neomag_html_class_banner = 'banner';}
	$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
	//Print Style 6
	if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
		cp_print_header_html($neomag_header_style);
	}
	?>
		<!--Inner Pages Heading Area Start-->
          <div class="inner-title">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <?php if($neomag_cp_page_caption <> ''){ ?><h2><?php echo esc_attr($neomag_cp_page_caption);?></h2><?php } else { 
                  		if(get_the_title() <> ''){ ?>
							<h2><?php if(strlen(get_the_title()) < 30 ) { echo get_the_title();} else {echo substr(get_the_title(),0 ,30) . '...';}?></h2>
				<?php } } ?>
                </div>
                <div class="col-md-6">
                <?php if($neomag_breadcrumbs == 'enable'){
					if(!is_front_page()){
						echo cp_breadcrumbs(); 
					}
                 } ?>
                </div>
              </div>
            </div>
          </div>
		<!--Inner Pages Heading Area End--> 		
			<?php if($neomag_breadcrumbs == 'disable'){
				$neomag_class_margin='margin_top_cp_team';
			}else {
				$neomag_class_margin='team_bottom_margin';
			}
			?>
<div class="cp-page-content inner-page-content blog-posts blog-with-sidebar <?php echo esc_attr($neomag_class_margin);?>">
			<div class="container">
			<!--MAIN CONTANT ARTICLE START-->
            <!--<div class="main-content">-->
				<div class="row">
					<?php
					if($neomag_sidebar == "left-sidebar" || $neomag_sidebar == "both-sidebar" || $neomag_sidebar == "both-sidebar-left"){?>
						<div id="block_first" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
							<?php dynamic_sidebar( $neomag_left_sidebar ); ?>
						</div>
						<?php
					}
					if($neomag_sidebar == 'both-sidebar-left'){?>
						<div id="block_first_left" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
							<?php dynamic_sidebar( $neomag_right_sidebar );?>
						</div>
					<?php } ?>
					<?php $neomag_image_size = array(555,355);?>
					<!--Blog Detail Page Page Start-->
					<div id="post-<?php the_ID(); ?>" class="<?php echo esc_attr($neomag_sidebar_class[1]);?> blog-single ">
                         <!--Blog Post -->
					  <div class="blog-post"> 
                        <div class="post-thumb"> 
                         <?php if(get_the_post_thumbnail($post->ID, $neomag_image_size) <> '') { ?>
							  <a href="<?php echo get_permalink();?>">
							  <?php echo get_the_post_thumbnail($post->ID, $neomag_image_size); ?>
                              </a>
							  <?php } ?>
                            </div>
						<div class="testimonial-right">
                         <div class="post-tools">
                          <ul>
                            <li>
                              <h4 class="user">
                              <?php if($neomag_team_designation <> ''){ 
									  echo '<i class="fa fa-cog"></i>'.$neomag_team_designation;
								} ?>
							  </h4>
                            </li>
                            <li> <i class="fa share-square-o"></i> 
								<?php if(isset($neomag_team_facebook) AND $neomag_team_facebook <> ''){?>
									<li><a title="Facebook Sharing" href="<?php echo esc_url($neomag_team_facebook)?>" class="social_active" id="fb_hr">
										<i class="fa fa-facebook"></i>
									</a></li>
								<?php }?>
								<?php if(isset($neomag_team_twitter) AND $neomag_team_twitter <> ''){?>
									<li><a title="Twitter Sharing" href="<?php echo esc_url($neomag_team_twitter)?>" class="social_active" id="twitter_hr">
										<i class="fa fa-twitter"></i>
									</a></li>
								<?php }?>
								<?php if(isset($neomag_team_linkedin) AND $neomag_team_linkedin <> ''){?>
									<li><a title="Linked In Sharing" href="<?php echo esc_url($neomag_team_linkedin)?>" class="social_active" id="linked_hr">
										<i class="fa fa-linkedin"></i>
									</a></li>
								<?php }?>
								<?php if(isset($neomag_google_plus) AND $neomag_google_plus <> ''){?>
									<li><a title="Google In Sharing" href="<?php echo esc_url($neomag_google_plus)?>" class="social_active" id="google_hr">
										<i class="fa fa-google-plus"></i>
									</a></li>
								<?php }?>
                            </li>
                          </ul>
                        </div>
                        <h3><a ><?php echo get_the_title(); ?></a></h3>      
							<?php the_content();?>
                        </div>
					  </div>
					  <!--Blog Post End --> 
					</div>
							<!--Blog Detail Page Page End--> 
							<?php
							if($neomag_sidebar == "both-sidebar-right"){?>
								<div class="<?php echo esc_attr($neomag_sidebar_class[0]);?> side-bar">
									<?php dynamic_sidebar( $neomag_left_sidebar ); ?>
								</div>
								<?php
							}
							if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){?>
								<div class="<?php echo esc_attr($neomag_sidebar_class[0]);?> side-bar">
									<?php dynamic_sidebar( $neomag_right_sidebar );?>
								</div>
							<?php } ?>				
					</div>
				</div>
			<!--</div>-->
		</div>
<?php 
	}
}
?>
<div class="clear"></div>
<?php get_footer(); ?>