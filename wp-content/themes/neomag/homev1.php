<?php session_start(); 
/**
 * Template Name: Home v1
 *
 * @package NeoMag Magazine Theme
 */
 
 	//Fetch the theme Option Values
	$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');
	$neomag_maintenace_title = neomag_cp_get_themeoption_value('maintenace_title','general_settings');
	$neomag_countdown_time = neomag_cp_get_themeoption_value('countdown_time','general_settings');
	$neomag_email_mainte = neomag_cp_get_themeoption_value('email_mainte','general_settings');
	$neomag_mainte_description = neomag_cp_get_themeoption_value('mainte_description','general_settings');
	$neomag_social_icons_mainte = neomag_cp_get_themeoption_value('social_icons_mainte','general_settings');
	
	if($neomag_maintenance_mode <> 'disable'){
		//If Logged in then Remove Maintenance Page
		if ( is_user_logged_in() ) {
			$neomag_maintenance_mode = 'disable';
		} else {
			$neomag_maintenance_mode = 'enable';
		}
	}
	
	if($neomag_maintenance_mode == 'enable'){
		//Trigger the Maintenance Mode Function Here
		maintenance_mode_fun();
	}else{
 
	get_header ();

		$neomag_page_builder_full = get_post_meta ( $post->ID, "cp-show-full-layout", true );      
		if($neomag_page_builder_full == 'No'){
			$neomag_sidebar_class = '';
			$neomag_content_class = '';
			$neomag_sidebar = get_post_meta ( $post->ID, 'page-option-sidebar-template', true );
			$neomag_sidebar_class = cp_sidebar_func($neomag_sidebar);
			$neomag_left_sidebar = get_post_meta ( $post->ID, "page-option-choose-left-sidebar", true );
			$neomag_right_sidebar = get_post_meta ( $post->ID, "page-option-choose-right-sidebar", true );
		}else{
			$neomag_sidebar_class = array('0'=>'no-sidebar','1'=>'col-md-12','2'=>'col-md-9','3'=>'col-md-6');
			$neomag_content_class = array();
			$neomag_sidebar = array();
			$neomag_left_sidebar = '';
			$neomag_right_sidebar = '';
		}	
		
		$neomag_slider_off = '';
		$neomag_slider_type = '';
		$neomag_slider_slide = '';
		$neomag_slider_height = '';
		
		//Fetch the data from page
		
		$neomag_slider_off = get_post_meta ( $post->ID, "page-option-top-slider-on", true ); 
		$neomag_slider_type = get_post_meta ( $post->ID, "page-option-top-slider-types", true );
		$neomag_slider_type_album = get_post_meta ( $post->ID, "page-option-top-slider-album", true );
		$neomag_page_builder_full = get_post_meta ( $post->ID, "cp-show-full-layout", true );
		$neomag_page_title_breadcrumb = get_post_meta ( $post->ID, "page-option-item-page-title", true );
		$neomag_banner_text = get_post_meta ( $post->ID, "page-option-top-banner-text", true );
		$neomag_cp_page_caption = get_post_meta ( $post->ID, "cp-show-page-caption-pageant", true );
		$neomag_selected_page_header = get_post_meta ( $post->ID, "page-option-top-header-style", true );
		$neomag_selected_page_footer = get_post_meta ( $post->ID, "page-option-bottom-footer-style", true );
		$neomag_optional_page_class = get_post_meta ( $post->ID, "page-option-item-class", true );
		$neomag_video_post = get_post_meta ( $post->ID, "page-option-item-video-post", true );
		$neomag_post_slider = get_post_meta ( $post->ID, "page-option-item-post-slider", true );
		$neomag_pagination_style = get_post_meta ( $post->ID, "page-option-pagination-styles", true );
		
		$neomag_cp_class_sch = '';
		$neomag_cp_page_title = get_post_meta ( $post->ID, "page-option-schedule-title", true );
		$neomag_cp_schedule = get_post_meta ( $post->ID, "page-option-top-schedule-mana", true );
		if($neomag_cp_schedule == 'No-Option'){
			$neomag_cp_class_sch = '';
		}else{
			$neomag_cp_class_sch = 'hide_caption';
		}
		
		$neomag_schedule_title = get_post_meta ( $post->ID, "page-option-schedule-title", true );
	
		$neomag_resv_button = neomag_cp_get_themeoption_value('resv_button','general_settings');
		$neomag_resv_text = neomag_cp_get_themeoption_value('resv_text','general_settings');
		$neomag_resv_short = neomag_cp_get_themeoption_value('resv_short','general_settings');
		
		//Video Banner Settings
		$neomag_slider_settings = get_option('slider_settings');
		
		if($neomag_slider_settings <> ''){
			$neomag_cp_slider = new DOMDocument ();
			$neomag_cp_slider->loadXML ( $neomag_slider_settings );
			//Video Banner Values
			$neomag_video_slider_on_off = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_slider_on_off');
			$neomag_video_banner_url = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_banner_url');
			$neomag_video_banner_title = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_banner_title');
			$neomag_video_banner_caption = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','video_banner_caption');
			$neomag_safari_banner_link = find_xml_child_nodes($neomag_slider_settings,'bx_slider_settings','safari_banner_link');
		}

		//Video Banner Is Enabled
		
		if($neomag_slider_off == 'No'){
			if($neomag_video_post <> ''){ ?>
				
				<div class="post-main-slider">
                  <div class="container">
                     <div class="row">
                         <div class="col-md-12">
                          	<div class="video-header">
						<?php
							$neomag_agent = '';
							$neomag_browser = '';
							if(isset($_SERVER['HTTP_USER_AGENT'])){
								 $neomag_agent = $_SERVER['HTTP_USER_AGENT'];
							}

							if(strlen(strstr($neomag_agent,'Safari')) > 0 ){
								$neomag_browser = 'safari';
							   if(strstr($neomag_agent,'Chrome')){
								 $neomag_browser= 'chrome';
							   }
							}

							if($neomag_browser=='safari'){ ?>
								<div class="video-block" style="width:100%; height:900px;" data-vide-bg="" data-vide-options="position: 0% 50%">
                                <img src="<?php echo esc_url($neomag_safari_banner_link); ?>"></div>
							<?php }else{ 
							
							$neomag_video_post_data = get_post( $neomag_video_post ); 
								$neomag_video_title = $neomag_video_post_data->post_title; 
								$neomag_video_author_id = get_post_field( 'post_author', $neomag_video_post );
								$neomag_video_author = get_the_author_meta( 'user_nicename', $neomag_video_author_id );
								$neomag_video_date = get_the_date( 'F j, Y', $neomag_video_post );
								$neomag_video_content =  $neomag_video_post_data->post_content;
								
								$neomag_post_detail_xml = get_post_meta($neomag_video_post, 'post_detail_xml', true);
								if($neomag_post_detail_xml <> ''){
									$neomag_cp_post_xml = new DOMDocument ();
									$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml );
									$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
								}
								$neomag_video_post_url = $neomag_video_url_type;
								if($neomag_video_post_url <> ''){ neomag_video_banner_script($neomag_video_post_url); }
							?>
                              <div class="slider-caption">
                                  <h3><?php echo esc_attr($neomag_video_title); ?></h3>
                                  
                                  <ul class="post-tools">
                                  <li><i class="fa fa-user"></i> <?php echo esc_attr($neomag_video_author); ?></li>
                                  <li><i class="fa fa-clock-o"></i> <?php echo esc_attr($neomag_video_date); ?></li>
                                  </ul>
                                  
                                  <p><?php echo esc_attr($neomag_video_content); ?></p>
                                  
                                  <a class="shopping-button" href="<?php echo esc_url(site_url('/?post_type=product')); ?>"><?php esc_html_e('Shop Now','neomag');?></a>
                                  
                                  </div>
                              
							<?php }
					echo '</div></div></div></div></div>'; 
			}
		}else{ 	//Video Banner Is Disabled
			if(class_exists('cp_slider_class')){
				//Condition for Box Layout
				$neomag_slider_off = 'Yes';
				if($neomag_slider_off == 'Yes'){ 
					if($neomag_post_slider <> '' && ($neomag_post_slider != '78612')){ 
						$neomag_page_slider_id = 'cp-banner-1';
						echo '<div id="cp-banner-style-1">';
						echo cp_post_slider($neomag_post_slider, $neomag_page_slider_id);
						echo '</div>';
				} else {
					echo '<div class="main-banner">'; 
					echo cp_page_slider();
					echo '</div>';				
					} 
				}			
			}
		}
		
		if(is_search() || is_404()){
			$neomag_header_style = '';
		}else{
			$neomag_header_style = get_post_meta ( $post->ID, "page-option-top-header-style", true );
		}
		$neomag_html_class = cp_print_header_class($neomag_header_style);
		//Print Style 7
		if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
			cp_print_header_html($neomag_header_style);
		}
		$neomag_class_bread_margin = '';
		$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
		if($neomag_breadcrumbs == 'disable'){
			$neomag_class_bread_margin = 'margin_top_cp';
		}else{
			$neomag_class_bread_margin = '';
		}
	?>
		<?php if($neomag_slider_off <> 'Yes'){ 
				if($neomag_video_post == ''){
		?>
        	 <!--  <div class="cp-page-title">
                      <h2>
                      <?php 
							if($neomag_page_title_breadcrumb <> ''){
								echo esc_attr($neomag_page_title_breadcrumb);
							}else{ 
							 if($neomag_cp_page_title <> ''){ 
								echo esc_attr($neomag_cp_page_title);
								} else {
								echo get_the_title();
								}
							}
						?>
                      </h2>
                      <?php if($neomag_cp_page_caption <> ''){ ?><h4><?php echo esc_attr($neomag_cp_page_caption);?></h4><?php }?>
                    </div> -->
		<?php } } ?>
		<!--BREADCRUMS END--> 
  <?php 
  $neomag_class = '';
  $neomag_class_sidebar = '';
  if($neomag_sidebar == 'no-sidebar'){ $neomag_class = 'col-md-12'; }
  if($neomag_sidebar == 'both-sidebar'){ $neomag_class = 'col-md-6'; $neomag_class_sidebar = 'col-md-3'; }
  if(($neomag_sidebar == 'left-sidebar')||($neomag_sidebar == 'right-sidebar')){ $neomag_class = 'col-md-9'; $neomag_class_sidebar = 'col-md-3'; }
  elseif (($neomag_sidebar == 'both-sidebar-left')||($neomag_sidebar == 'both-sidebar-right')){ $neomag_class = 'col-md-6'; $neomag_class_sidebar = 'col-md-3'; }
  ?>
  <!--CONTENT START-->
  <div id="cp-content-wrap" class="cp-content-wrap">
    <div class="container" id="gg">
      <div class=""> 
					<?php
					if($neomag_sidebar == "left-sidebar" || $neomag_sidebar == "both-sidebar" || $neomag_sidebar == "both-sidebar-left"){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_left_sidebar ); ?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php
					}
					if($neomag_sidebar == 'both-sidebar-left'){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_right_sidebar );?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php } 
					$neomag_fetch_category_id = get_post_meta( get_the_ID(), 'select_name' ); 
					if($neomag_fetch_category_id <> ''){
					$neomag_post_category_id = $neomag_fetch_category_id[0]; 
					}
					?>
        <!-- CONTENT START -->
        <div class="<?php echo esc_attr($neomag_class); ?>">
          <div class="cp-posts-style-1" id="cp-posts-style-1">
            <ul class="cp-posts-list">
              <?php
			  $paged = (get_query_var('page')) ? get_query_var('page') : 1; 
			  if(($neomag_post_category_id == '0')||($neomag_post_category_id == '')){ 
				//Popular Post 
				query_posts(
					array( 
					'post_type' => 'post',
					'paged'				=> $paged,
					'posts_per_page' => -1,
					'orderby' => 'date',
					'order' => 'DESC' )
				);
			} else {
              	query_posts(
					array( 
					'post_type' => 'post',
					'posts_per_page' => 7,
					'paged'				=> $paged,
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'terms' => $neomag_post_category_id,
							'field' => 'term_id',
						)
					),
					'orderby' => 'date',
					'order' => 'DESC' )
				);
			}
				if(have_posts()){
					while( have_posts() ){
					the_post();
					global $allowedposttags;
					// Get Post Meta Elements detail 
					$neomag_thumbnail_types = $neomag_video_url_type = $neomag_select_slider_type = $neomag_audio_url_type = '';
					$neomag_post_format = get_post_meta($post->ID, 'post_format', true);
					$neomag_post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
					if($neomag_post_detail_xml <> ''){ 
						$neomag_cp_post_xml = new DOMDocument ();
						$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml ); 
						$neomag_thumbnail_types = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_thumbnail');
						$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
						$neomag_select_slider_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'select_slider_type');	
						$neomag_audio_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'audio_url_type');
					}
					$neomag_thumbnail_id = get_post_thumbnail_id( $post->ID );
					$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, 'neomag_std_post');
					$neomag_format = get_post_format(); 
					if ( false === $neomag_format ) {
						$neomag_format = 'standard';
					}
					?>
					<?php if ( shortcode_exists( 'photo_set' ) ) { ?>
                        <?php
						// The [photo_set] short code exists.
						$neomag_content = get_post( $post->ID ); 
						 if ( has_shortcode( $neomag_content->post_content, 'photo_set' ) ) { 
						 echo '<li class="cp-post cp-photoset-post">';
						 $neomag_shortcode_content = $neomag_content->post_content;
						echo do_shortcode($neomag_shortcode_content);
						 
					 ?>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('atrás','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id );
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Leia Mais','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Escreva um Comentário</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comentário desativado','neomag') );
                              ?>
                           </div>
                        </div>
                      </li>
					<?php } } ?>
					<?php if ( $neomag_format === 'standard' && !( has_shortcode( $neomag_content->post_content, 'photo_set' )) &&  !($neomag_image_thumb[0] == '')) { ?>
                      
                      <li class="cp-post">
                        <div class="cp-thumb"> <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                          <div class="cp-post-hover"> <a href="<?php echo get_permalink();?>"><i class="fa fa-link"></i></a> 
                          <a data-rel="prettyPhoto" class="cp-zoom" rel="prettyPhoto" href="<?php echo esc_url($neomag_image_thumb[0]); ?>"><i class="fa fa-search"></i></a> </div>
                        </div>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('atrás','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id );
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                            <?php $neomag_post_content_image = neo_mag_catch_that_image(); 
								if($neomag_post_content_image <> ''){
									echo '<div class="content-img"><img src="'.esc_url($neomag_post_content_image).'" alt="'.$post->ID.'"></div>';
									} else {
							?>
                            <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 400));?></p>
                            <?php } ?>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Leia Mais','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Escreva um Comentário</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comentário desativado','neomag') );
                              ?>
                            </div>
                        </div>
                      </li>
                      
					<?php }	?>
					<?php if( $neomag_format === 'standard' && ($neomag_image_thumb[0] == '') && !( has_shortcode( $neomag_content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-text-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('atrás','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id );
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                            <p><?php echo strip_tags(mb_substr(get_the_content(),0, 400));?></p>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Leia Mais','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Escreva um Comentário</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comentário desativado','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
					<?php if( $neomag_format === 'quote' && ($neomag_image_thumb[0] == '') && !( has_shortcode( $neomag_content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-quote-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('atrás','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id );
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                            <blockquote><?php echo strip_tags(mb_substr(get_the_content(),0, 400));?></blockquote>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Leia Mais','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Escreva um Comentário</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comentário desativado','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
					<?php if( $neomag_format === 'video' && ($neomag_video_url_type <> '') && !( has_shortcode( $neomag_content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-text-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('atrás','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id );
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                    			<iframe src="<?php echo esc_url($neomag_video_url_type); ?>"></iframe>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Leia Mais','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Escreva um Comentário</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comentário desativado','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
				<?php	
					}//end while
					if($neomag_pagination_style == 'Old-Posts'){ echo '<li>'; neo_mag_pagination(); echo '</div></li>'; } 
					elseif($neomag_pagination_style == 'Number-Pagination'){ echo '<li>'; pagination_crunch(); echo '</li>'; }
				wp_reset_query();					
					} 
			  ?>
            </ul>
          </div>
        </div>
        <!-- CONTENT END --> 

					<?php  
					if($neomag_sidebar == "both-sidebar-right"){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_left_sidebar ); ?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php
					}
					if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){ ?>
                        <!--SIDEBAR START-->
                        <div class="<?php echo esc_attr($neomag_class_sidebar); ?>">
                          <div class="sidebar"> 
						  <?php dynamic_sidebar( $neomag_right_sidebar );?>
                          </div>
                        </div>
                        <!--SIDEBAR END--> 
					<?php } ?>

      </div>
    </div>
  </div>
  <!--CONTENT END--> 
<?php get_footer(); } ?>