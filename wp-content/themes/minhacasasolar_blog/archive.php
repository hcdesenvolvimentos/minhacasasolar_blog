<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Minha_Casa_Solar
 */

get_header();
?>
<style>
	.archive .pg-inicial .sessaoPosts{
		padding-top:0 ;
	}
	.archive .pg .col-sm-4 aside{
	    padding-top: 28px;
	}
</style>
	<!-- PG INICIAL -->
<div class="pg pg-inicial">
	<!-- DEFININDO CONTAINER -->
	<div class="containerLagura">
		
		<!-- DEFININDO COLUNAS -->
		<div class="row">

			<div class="col-sm-8">

				<!-- SESSÃO DE POSTS -->
				<section class="sessaoPosts">
					<h6 class="hidden">Sessão de Posts </h6>
					
					<!-- LISTA DE POSTS -->
					<ul>
						<?php 
					            	
							if ( have_posts() ) : while( have_posts() ) : the_post();
							$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoBlog = $fotoBlog[0];
							global $post;
							$categories = get_the_category();
						?>
						<!-- POST -->
						<li>
							<a href="<?php echo get_permalink() ?>" class="linkPost">
								<div class="row">

									<!-- DESCRÇÃO DE POST -->
									<div class="col-sm-7">
										<!-- CATEGORIA DO POST -->
										<?php 
											foreach ($categories as $categories){
												if ($categories->name != "Destaque"){
													$nomeCategoria = $categories->name;
												}
											} 
										?>
										<!-- CATEGORIA -->
										<h3><?php echo $nomeCategoria;  ?></h3>
										<!-- TÍTULO DO POST -->
										<h2><?php echo get_the_title() ?></h2>
										<!-- DATA -->
										<strong><?php echo  get_the_date('j F, Y'); ?></strong>
										<!-- BREVE DESCRIÇÃO -->
										<p><?php customExcerpt(130); ?></p>

									</div>

									<!-- IMAGEM DESTACADA -->
									<div class="col-sm-5">
										<figure>
											<img src="<?php echo $fotoBlog  ?>" alt="<?php echo get_the_title() ?> ">
											<span style="background: url(<?php echo $fotoBlog  ?>)"></span>
										</figure>
									</div>

								</div>
							</a>	
							<?php disqus_count('myshortcode'); ?>
						</li>
						<?php endwhile;endif; wp_reset_query(); ?>
					</ul>

				</section>

				<!-- PÁGINADOR -->
				<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			</div>
			
			<div class="col-sm-4">
				<?php echo get_sidebar(); ?>
			</div>
		
		</div>

	</div>	
</div>

<?php

get_footer();
