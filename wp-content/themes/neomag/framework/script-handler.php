<?php
	/*	
	*	CrunchPress Include Script File
	*	---------------------------------------------------------------------
	* 	@version	1.0
	*   @ Package   Fine Food Theme
	* 	@author		CrunchPress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) CrunchPress
	*	---------------------------------------------------------------------
	*	This file manage to embed the stylesheet and javascript to each page
	*	based on the content of that page.
	*	---------------------------------------------------------------------
	*/
	//Add Scripts in Theme
	if(is_admin()){
		add_action('admin_enqueue_scripts', 'cp_register_meta_script');
		add_action('admin_enqueue_scripts','cp_register_crunchpress_panel_scripts');
		add_action('admin_enqueue_scripts','cp_register_crunchpress_panel_styles');
	}else{
		add_action('wp_enqueue_scripts','cp_register_non_admin_styles');
		add_action('wp_enqueue_scripts','cp_register_non_admin_scripts');
	}
	/* 	---------------------------------------------------------------------
	*	This section include the back-end script
	*	---------------------------------------------------------------------
	*/ 
	function cp_register_meta_script(){
		global $post_type;
		wp_enqueue_style('bootstrap', NEOMAG_PATH_URL.'/framework/stylesheet/bootstrap.css');
		wp_enqueue_style('thickbox');
		//Font Awesome
		wp_enqueue_style('cp-fontAW',NEOMAG_PATH_URL.'/frontend/cp_font/css/font-awesome.css');
		wp_enqueue_style('cp-fontAW',NEOMAG_PATH_URL.'/frontend/cp_font/css/font-awesome-ie7.css');
		wp_enqueue_style('admin-css',NEOMAG_PATH_URL.'/framework/stylesheet/admin-css.css');
		// register style and script when access to the "page" post_type page
		if( $post_type == 'page' ){
			wp_enqueue_style('meta-css',NEOMAG_PATH_URL.'/framework/stylesheet/meta-css.css');
			wp_enqueue_style('page-dragging',NEOMAG_PATH_URL.'/framework/stylesheet/page-dragging.css');
			wp_enqueue_style('image-picker',NEOMAG_PATH_URL.'/framework/stylesheet/image-picker.css');
			wp_register_script('image-picker', NEOMAG_PATH_URL.'/framework/javascript/image-picker.js', false, '1.0', true);
			wp_enqueue_script('image-picker');
			wp_register_script('page-dragging', NEOMAG_PATH_URL.'/framework/javascript/page-dragging.js', false, '1.0', true);
			wp_enqueue_script('page-dragging');
			wp_register_script('edit-box', NEOMAG_PATH_URL.'/framework/javascript/edit-box.js', false, '1.0', true);
			wp_enqueue_script('edit-box');
			wp_register_script('confirm-dialog', NEOMAG_PATH_URL.'/framework/javascript/jquery.confirm.js', false, '1.0', true);
			wp_enqueue_script('confirm-dialog');
		// register style and script when access to the "post" post_type page
		}else if( $post_type == 'event' || $post_type == 'post' || $post_type == 'team'  || $post_type == 'portfolio' || $post_type == 'cp_slider' || $post_type == 'gallery' || $post_type == 'product' ){
			wp_deregister_style('admin-css');
			wp_enqueue_style('meta-css',NEOMAG_PATH_URL.'/framework/stylesheet/meta-css.css');
			wp_enqueue_style('image-picker',NEOMAG_PATH_URL.'/framework/stylesheet/image-picker.css');
			wp_enqueue_style('confirm-dialog',NEOMAG_PATH_URL.'/framework/stylesheet/jquery.confirm.css');
			wp_register_script('post-effects', NEOMAG_PATH_URL.'/framework/javascript/post-effects.js', false, '1.0', true);
			wp_enqueue_script('post-effects');
			wp_register_script('image-picker', NEOMAG_PATH_URL.'/framework/javascript/image-picker.js', false, '1.0', true);
			wp_localize_script('image-picker', 'URL', array('neomag' => NEOMAG_PATH_URL ));
			wp_enqueue_script('image-picker');
			wp_register_script('confirm-dialog', NEOMAG_PATH_URL.'/framework/javascript/jquery.confirm.js', false, '1.0', true);
			wp_enqueue_script('confirm-dialog');
		// register style and script when access to the "testimonial" post_type page		
		}else if( $post_type == 'testimonial' ){
			wp_enqueue_style('meta-css',NEOMAG_PATH_URL.'/framework/stylesheet/meta-css.css');
		}else if($post_type == 'albums'){
			wp_register_script('contact-validation', NEOMAG_PATH_URL.'/frontend/js/jquery.validate.js', false, '1.0', true);
			wp_enqueue_script('contact-validation');
		}
	}
	// register script in CrunchPress panel
	function cp_register_crunchpress_panel_scripts(){
		global $post_type;
		if($post_type == 'page'){
		}else{
			wp_enqueue_style('bootstrap',NEOMAG_PATH_URL.'/framework/stylesheet/bootstrap.css');
			$cp_script_url = NEOMAG_PATH_URL.'/framework/javascript/cp-panel.js';
			wp_enqueue_script('cp_scripts_admin', $cp_script_url, array('jquery','media-upload','cp-bootstrap','thickbox', 'jquery-ui-droppable','jquery-ui-datepicker','jquery-ui-tabs', 'jquery-ui-slider','jquery-timepicker','jquery-ui-position','mini-color','confirm-dialog','dummy_content'));
			wp_register_script('cp-bootstrap', NEOMAG_PATH_URL.'/framework/javascript/bootstrap.js', false, '1.0', true);
			//Font Awesome
			wp_enqueue_style('cp-fontAW',NEOMAG_PATH_URL.'/frontend/cp_font/css/font-awesome.css');
			wp_enqueue_style('cp-fontAW',NEOMAG_PATH_URL.'/frontend/cp_font/css/font-awesome-ie7.css');
			wp_register_script('mini-color', NEOMAG_PATH_URL.'/framework/javascript/jquery.miniColors.js', false, '1.0', true);
			wp_register_script('confirm-dialog', NEOMAG_PATH_URL.'/framework/javascript/jquery.confirm.js', false, '1.0', true);
			wp_register_script('jquery-timepicker', NEOMAG_PATH_URL.'/framework/javascript/jquery.ui.timepicker.js', false, '1.0', true);
			wp_register_script('dummy_content', NEOMAG_PATH_URL.'/framework/javascript/dummy_content.js', false, '1.0', true);
		}		
	}
	// register style in CrunchPress panel
	function cp_register_crunchpress_panel_styles(){
		wp_enqueue_style('jquery-ui',NEOMAG_PATH_URL.'/framework/stylesheet/jquery-ui.css');
		wp_enqueue_style('cp-panel',NEOMAG_PATH_URL.'/framework/stylesheet/cp-panel.css');
		wp_enqueue_style('mini-color',NEOMAG_PATH_URL.'/framework/stylesheet/jquery.miniColors.css');
		wp_enqueue_style('confirm-dialog',NEOMAG_PATH_URL.'/framework/stylesheet/jquery.confirm.css');
		wp_enqueue_style('jquery-timepicker',NEOMAG_PATH_URL.'/framework/stylesheet/jquery.ui.timepicker.css');
	}
	/* 	---------------------------------------------------------------------
	*	this section include the front-end script
	*	---------------------------------------------------------------------
	*/ 
	// Register all stylesheet
	function cp_register_non_admin_styles(){
		$neomag_cp_page_xml = '';
		$neomag_slider_type = '';
		global $post,$post_id,$neomag_cp_page_xml,$neomag_slider_type;
		$neomag_cp_page_xml = get_post_meta($post_id,'page-option-item-xml', true);
		$neomag_slider_type = get_post_meta ( $post_id, "page-option-top-slider-types", true );
		$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');
		if($neomag_maintenance_mode <> 'disable'){
				wp_register_script('jquery-counter', NEOMAG_PATH_URL.'/frontend/js/jquery.plugin.min.js', false, '1.0', true);
				wp_enqueue_script('jquery-counter');
			}
		//Masonary script
		if(is_page_template( 'homev2.php' )){ 
				wp_register_script('isotope-js', NEOMAG_PATH_URL.'/frontend/js/isotope.pkgd.min.js', false, '1.0', true);
				wp_enqueue_script('isotope-js');
		}
		wp_enqueue_style( 'default-style', get_stylesheet_uri() );  //Default Stylesheet
		wp_register_style( 'content_slider_css', NEOMAG_PATH_URL.( '/frontend/css/content_slider_style.css' ) );
		wp_enqueue_style('content_slider_css'); //content slider		
		//Video Slider JS
		wp_register_script('cp-video-js', NEOMAG_PATH_URL.'/frontend/js/jquery.videobackground.js', false, '1.0', true);
		wp_enqueue_script('cp-video-js');
		wp_register_script('custom-js', NEOMAG_PATH_URL.'/frontend/js/custom.js', false, '1.0', true);
			wp_enqueue_script('custom-js');
		//Pretty Photo Scripts
		wp_enqueue_style('prettyPhoto',NEOMAG_PATH_URL.'/frontend/css/prettyphoto.css');
		wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
		wp_enqueue_script('prettyPhoto');
		wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
		wp_enqueue_script('cp-pscript');
		//NeoMag Travel Version CSS
		wp_enqueue_style('cp-bootstrap',NEOMAG_PATH_URL.'/frontend/css/bootstrap.css'); //Bootstrap Grid
		wp_enqueue_style('cp-wp-cp_default',NEOMAG_PATH_URL.'/frontend/css/cp_widgets.css'); //Wordpress Default Widget Style
		wp_enqueue_style('cp-responsive',NEOMAG_PATH_URL.'/frontend/css/responsive.css'); //Responsive style
		wp_enqueue_style('cp-color-css',NEOMAG_PATH_URL.'/frontend/css/color.css'); //Bootstrap responsive
		//header
		wp_enqueue_style('cp-header',NEOMAG_PATH_URL.'/frontend/css/header.css'); //Bootstrap responsive
		wp_enqueue_style('cp-fontAW',NEOMAG_PATH_URL.'/frontend/cp_font/css/font-awesome.css');
		wp_enqueue_style('cp-fontAW',NEOMAG_PATH_URL.'/frontend/cp_font/css/font-awesome-ie7.css');
		$rtl_layout = '';
		$site_loader = '';
		$element_loader = '';
		//General Settings Values
		$cp_general_settings = get_option('general_settings');
		if($cp_general_settings <> ''){
			$cp_logo = new DOMDocument ();
			$cp_logo->loadXML ( $cp_general_settings );
			$rtl_layout = cp_find_xml_value($cp_logo->documentElement,'rtl_layout');
			$site_loader = cp_find_xml_value($cp_logo->documentElement,'site_loader');
			$element_loader = cp_find_xml_value($cp_logo->documentElement,'element_loader');
		}
		//Responsive stylesheet
		wp_deregister_style('woocommerce-general');
		wp_deregister_style('ls-google-fonts-css');
		wp_deregister_style('woocommerce-layout');
		wp_deregister_style('woocommerce_frontend_styles');		
		wp_deregister_style('events-manager');		
		wp_deregister_style('mm_font-awesome');		
		//RTL Layouts
		if($rtl_layout == 'enable'){
			wp_enqueue_style('cp-rtl',NEOMAG_PATH_URL.'/rtl.css');
		}		
		//Facebook Fan Page Script
		if(isset($post->ID)){
			$facebook_fan = '';
			$facebook_fan = get_post_meta ( $post->ID, "page-option-item-facebook-selection", true );
			if($facebook_fan == 'Yes'){$facebook_fan = 'facebook_fan';
				wp_enqueue_style('style_810',NEOMAG_PATH_URL.'/frontend/css/style810.css');
			}
		}	
		$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');				
		if($neomag_maintenance_mode == 'enable'){		
			wp_enqueue_style('cp-countdown',NEOMAG_PATH_URL.'/frontend/css/jquery.countdown.css');
			wp_enqueue_style('cp-comming_soon',NEOMAG_PATH_URL.'/frontend/css/comming_soon.css');
		}
		$twitter_feed = neomag_cp_get_themeoption_value('twitter_feed','general_settings');
		if($twitter_feed == 'enable'){ 
			wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
		}
		if(isset($post)){
			$content = strip_tags(get_the_content());
			if ( has_shortcode( $post->post_content, 'event_counter_box' ) ) { 		
				wp_enqueue_style('cp-countdown',NEOMAG_PATH_URL.'/frontend/css/jquery.countdown.css');
			}
			if ( has_shortcode( $post->post_content, 'person' ) ) { 		
				wp_enqueue_style('prettyPhoto',NEOMAG_PATH_URL.'/frontend/css/prettyphoto.css');
			}
			if ( has_shortcode( $post->post_content, 'slider' ) ) { 		
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/shortcodes/bxslider.css');
			}
			if ( has_shortcode( $post->post_content, 'counter_circle' ) ) { 		
				wp_enqueue_style('cp-easy-chart',NEOMAG_PATH_URL.'/frontend/shortcodes/chart.css');
			}
			if ( has_shortcode( $post->post_content, 'counters_circle' ) ) { 		
				wp_enqueue_style('cp-easy-chart',NEOMAG_PATH_URL.'/frontend/shortcodes/chart.css');
			}			
		}
		//Widget Active
		if(is_active_widget( '', '', 'twitter_widget')){			
			wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/shortcodes/bxslider.css');
		}
		//Widget Latest News Active
		if(is_active_widget( '', '', 'recent_news_show')){			
			wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
			wp_enqueue_script('cp-bx-slider');	
			wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
		}
		//Widget Product Slider Widget
		if(is_active_widget( '', '', 'product_slider_widget')){			
			wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
			wp_enqueue_script('cp-bx-slider');	
			wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
		}
		if( is_search() || is_archive() ){
			wp_enqueue_style('cp-anything-slider',NEOMAG_PATH_URL.'/frontend/anythingslider/css/anythingslider.css');
		// Post post_type
		}else if( isset($post) && $post->post_type == 'post' || 
			isset($post) && $post->post_type == 'event' ){
				// If using slider (flex slider)	
				if(!is_home()){
					$thumbnail_types = '';
					$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
					if($post_detail_xml <> ''){
						$cp_post_xml = new DOMDocument ();
						$cp_post_xml->loadXML ( $post_detail_xml );
						$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
						if( $thumbnail_types == 'Slider'){
							wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
						}
					}
					$event_detail_xml = get_post_meta($post->ID, 'event_detail_xml', true);
					if($event_detail_xml <> ''){
						$cp_event_xml = new DOMDocument ();
						$cp_event_xml->loadXML ( $event_detail_xml );
						$event_thumbnail = cp_find_xml_value($cp_event_xml->documentElement,'event_thumbnail');
						//Call the CountDown Style
						wp_enqueue_style('cp-countdown',NEOMAG_PATH_URL.'/frontend/css/jquery.countdown.css'); //Load Style		
					}
				}
		// Page post_type
		}else if( isset($post) && $post->post_type == 'page' ){
			global $post,$neomag_cp_page_xml, $neomag_slider_type, $cp_top_slider_type;
			$neomag_cp_page_xml = get_post_meta($post->ID,'page-option-item-xml', true);
			$cp_top_slider_switch = get_post_meta($post->ID,'page-option-top-slider-on', true);
			$neomag_slider_type = get_post_meta ( $post->ID, "page-option-top-slider-types", true );
			$cp_top_slider_type = get_post_meta($post->ID,'page-option-top-slider-types', true);
			//Team Slider and Scroller
			if(strpos($neomag_cp_page_xml,'<Team-Slider>') > -1){			
				wp_enqueue_style('cp-horizontal',NEOMAG_PATH_URL.'/frontend/css/horizontal.css'); //Horizontal Scroll Team
			}	
			//Layer Slider
			if(strpos($neomag_cp_page_xml,'<slider-type>Layer-Slider</slider-type>') > -1 || $neomag_slider_type == 'Layer-Slider'){
			}
			// If using carousel slider
			if(	strpos($neomag_cp_page_xml,'<slider-type>Flex-Slider</slider-type>') > -1 || $neomag_slider_type == 'Flex-Slider'){
				wp_enqueue_style('cp-flex-slider',NEOMAG_PATH_URL.'/frontend/css/flexslider.css');
			}			
			//Bx Slider Condition
			if(strpos($neomag_cp_page_xml,'<slider-type>Bx-Slider</slider-type>') > -1 || $neomag_slider_type == 'Bx-Slider' ){
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
			}
			//Latest News
			if(strpos($neomag_cp_page_xml,'<Latest-News>') > -1 ){
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
			}
			if(strpos($neomag_cp_page_xml,'<Column>') > -1){
			}
			//Recipe or Services offers
			if(strpos($neomag_cp_page_xml,'<Offers>') > -1){
				wp_enqueue_style('cp-mCustomScrollbar',NEOMAG_PATH_URL.'/frontend/css/jquery.mCustomScrollbar.css'); //Scroll Bar
			}
			//Calender View
			if( strpos($neomag_cp_page_xml,'<eventview>Calendar View</eventview>') > -1 ){
				wp_enqueue_style('cp-calender-view', NEOMAG_PATH_URL.'/framework/javascript/fullcalendar/fullcalendar.css');
			}
			// If using filterable plugin
			if( strpos($neomag_cp_page_xml,'<show-filterable>') > -1 ){
				wp_enqueue_style('cp-style-view', NEOMAG_PATH_URL.'/frontend/css/style_animate.css');
			}
			// If using Services
			if( strpos($neomag_cp_page_xml,'<service-widget-style>Circle-Icon</service-widget-style>') > -1 ){
				wp_enqueue_style('circle-hover',NEOMAG_PATH_URL.'/frontend/css/circle-hover.css');
			}
			// If using Events
			if( strpos($neomag_cp_page_xml,'<Events>') > -1 ){
				wp_enqueue_style('cp-countdown',NEOMAG_PATH_URL.'/frontend/css/jquery.countdown.css'); //Load Style				
			}
			// If using NewsSlider
			if( strpos($neomag_cp_page_xml,'<News-Slider>') > -1 ){
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
			}
			// If using Blog Slider
			if( strpos($neomag_cp_page_xml,'<Blog_Slider>') > -1 ){
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
			}
			// If using NewsSlider
			if( strpos($neomag_cp_page_xml,'<Client-Slider>') > -1 ){
			}
			if( strpos($neomag_cp_page_xml,'<Portfolio>') > -1 || strpos($neomag_cp_page_xml,'<Gallery>') > -1){
				wp_enqueue_style('prettyPhoto',NEOMAG_PATH_URL.'/frontend/css/prettyphoto.css');
			}
			if( strpos($neomag_cp_page_xml,'<Service>') > -1){
				wp_enqueue_style('prettyPhoto',NEOMAG_PATH_URL.'/frontend/css/prettyphoto.css');
			}
			// if using timeline
			if( strpos($neomag_cp_page_xml,'<Timeline>') > -1 ){
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/jquery.bxslider.css');
			}
			// if using Hot Deal Element
			if( strpos($neomag_cp_page_xml,'<Hot-Deal>') > -1 ){
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
			}
			if( strpos($neomag_cp_page_xml,'<Blog>') > -1 || strpos($neomag_cp_page_xml,'<Gallery>') > -1 || strpos($neomag_cp_page_xml,'<News>') > -1 || strpos($neomag_cp_page_xml,'<Events>') > -1){
				wp_enqueue_style('prettyPhoto',NEOMAG_PATH_URL.'/frontend/css/prettyphoto.css');				
			}
			if( strpos($neomag_cp_page_xml,'<Woo-Products>') > -1 ){
				//WooCommerce Style
				wp_enqueue_style('cp-wp-commerce',NEOMAG_PATH_URL.'/frontend/css/wp-commerce.css'); //WooCommerce Default
				wp_enqueue_style('prettyPhoto',NEOMAG_PATH_URL.'/frontend/css/prettyphoto.css');
			}
		}
		$font_google = '';
		$font_size_normal = '';
		$menu_font_google = '';
		$fonts_array = '';
		$font_google_heading = '';
		$heading_h1 = '';
		$heading_h2 = '';
		$heading_h3 = '';
		$heading_h4 = '';
		$heading_h5 = '';
		$heading_h6 = '';
		$embed_typekit_code = '';
		$cp_typography_settings = get_option('typography_settings');
		if($cp_typography_settings <> ''){
			$cp_typo = new DOMDocument ();
			$cp_typo->loadXML ( $cp_typography_settings );
			$font_google = cp_find_xml_value($cp_typo->documentElement,'font_google');
			$font_size_normal = cp_find_xml_value($cp_typo->documentElement,'font_size_normal');
			$menu_font_google = cp_find_xml_value($cp_typo->documentElement,'menu_font_google');
			$font_google_heading = cp_find_xml_value($cp_typo->documentElement,'font_google_heading');
			$heading_h1 = cp_find_xml_value($cp_typo->documentElement,'heading_h1');
			$heading_h2 = cp_find_xml_value($cp_typo->documentElement,'heading_h2');
			$heading_h3 = cp_find_xml_value($cp_typo->documentElement,'heading_h3');
			$heading_h4 = cp_find_xml_value($cp_typo->documentElement,'heading_h4');
			$heading_h5 = cp_find_xml_value($cp_typo->documentElement,'heading_h5');
			$heading_h6 = cp_find_xml_value($cp_typo->documentElement,'heading_h6');
			$embed_typekit_code = cp_find_xml_value($cp_typo->documentElement,'embed_typekit_code');
		}
		//Body Font Installing
		if(cp_get_font_type($font_google) == 'Google_Font'){
			//Google Font Body
			if($font_google <> ''){
				wp_enqueue_style('googleFonts', get_google_font_url($font_google));
			}	
		} else{
			//Adobe Edge Font (TypeKit) 
			if($font_google <> ''){
				wp_register_script( 'adobe-edge-font', "http://use.edgefonts.net/".$font_google.".js", false, '1.0', false);
				wp_enqueue_script('adobe-edge-font');	
			}
		}
		if(cp_get_font_type($font_google_heading) == 'Google_Font'){
			if($font_google_heading <> ''){				
				wp_enqueue_style('googleFonts-heading', get_google_font_url($font_google_heading) );
			}
		}else{
			if($font_google_heading <> ''){
				wp_register_script( 'adobe-edge-heading', "http://use.edgefonts.net/".$font_google_heading.".js", false, '1.0', false);
				wp_enqueue_script('adobe-edge-heading');	
			}
		}
		//Menu Font Installing	
		if(cp_get_font_type($menu_font_google) == 'Google_Font'){
			if($menu_font_google <> ''){
				wp_enqueue_style('menu-googleFonts-heading', get_google_font_url($menu_font_google));
			}
		}else{
			if($menu_font_google <> ''){
				wp_register_script( 'menu-edge-heading', "http://use.edgefonts.net/".$menu_font_google.".js", false, '1.0', false);
				wp_enqueue_script('menu-edge-heading');	
			}
		}
	}
        // Register all scripts
	function cp_register_non_admin_scripts(){
		global $post,$post_id;
		global $neomag_cp_is_responsive;
		global $crunchpress_element;		
		global $wp_scripts;
		$neomag_cp_page_xml = get_post_meta($post_id,'page-option-item-xml', true);
		$neomag_slider_type = get_post_meta ( $post_id, "page-option-top-slider-types", true );
		$social_networking = '';
		$site_loader = '';
		$element_loader = '';
		$cp_general_settings = get_option('general_settings');
		if($cp_general_settings <> ''){
			$cp_logo = new DOMDocument ();
			$cp_logo->loadXML ( $cp_general_settings );
			$social_networking = cp_find_xml_value($cp_logo->documentElement,'social_networking');
			$site_loader = cp_find_xml_value($cp_logo->documentElement,'site_loader');
			$element_loader = cp_find_xml_value($cp_logo->documentElement,'element_loader');
			$topweather_icon = cp_find_xml_value($cp_logo->documentElement,'topweather_icon');
		}
		wp_enqueue_script('jquery');
		wp_register_script('cp-modernizr', NEOMAG_PATH_URL.'/frontend/js/modernizr.js', false, '1.0', true);
		wp_enqueue_script('cp-modernizr');
		wp_register_script('cp-cp_search_classie', NEOMAG_PATH_URL.'/frontend/js/cp_search_classie.js', false, '1.0', true);
		wp_enqueue_script('cp-cp_search_classie');
		$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');	
		$cp_comming_soon = neomag_cp_get_themeoption_value('cp_comming_soon','general_settings');	
		if($neomag_maintenance_mode == 'enable'){
			//Style 1
				//Final Count Down SCript
				wp_register_script('cp-final-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery.final-countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-final-countdown');			
				//Equal Height Script
				wp_register_script('cp-final-kinetic', NEOMAG_PATH_URL.'/frontend/js/default/kinetic.js', false, '1.0', true);
				wp_enqueue_script('cp-final-kinetic');		
		}
		$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');				
		if($neomag_maintenance_mode == 'enable'){
			if($cp_comming_soon == 'Style 2'){			
				wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-countdown');
			}
		}
		//Masonary js
		if(is_single()){
				wp_register_script('masonary-js', NEOMAG_PATH_URL.'/frontend/js/masonry.pkgd.min.js', false, '1.0', true);
				wp_enqueue_script('masonary-js');
			}
		$topcounter_circle = neomag_cp_get_themeoption_value('topcounter_circle','general_settings');
		$countd_event_category = neomag_cp_get_themeoption_value('countd_event_category','general_settings');
		$color_scheme = neomag_cp_get_themeoption_value('color_scheme','general_settings');
		if(class_exists('CP_Shortcodes')){
			if($topcounter_circle == 'enable'){
				wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-countdown');
			}
		}	
		if ( is_singular() && get_option( 'thread_comments' ) ) 	wp_enqueue_script( 'comment-reply' );
		//BootStrap Script Loaded
		wp_register_script('cp-bootstrap', NEOMAG_PATH_URL.'/frontend/js/default/bootstrap.js', array('jquery'), '1.0', true);
		wp_localize_script('cp-bootstrap', 'ajax_var', array('url' => admin_url('admin-ajax.php'),'nonce' => wp_create_nonce('ajax-nonce')));
		wp_enqueue_script('cp-bootstrap');
		//Custom Script Loaded
		wp_register_script('cp-scripts', NEOMAG_PATH_URL.'/frontend/js/frontend_scripts.js', false, '1.0', true);
		wp_enqueue_script('cp-scripts');
		wp_register_script('owl-scripts', NEOMAG_PATH_URL.'/frontend/js/owl.carousel.min.js', false, '1.0', true);
		wp_enqueue_script('owl-scripts');
		wp_enqueue_style('owl-css',NEOMAG_PATH_URL.'/frontend/css/owl.carousel.css');
		wp_register_script('migrate-js', NEOMAG_PATH_URL.'/frontend/js/jquery-migrate-1.2.1.min.js', false, '1.0', true);
		wp_enqueue_script('migrate-js');
		wp_register_script('cp-easing', NEOMAG_PATH_URL.'/frontend/js/jquery-easing-1.3.js', false, '1.0', true);
		wp_enqueue_script('cp-easing');
		$twitter_feed = neomag_cp_get_themeoption_value('twitter_feed','general_settings');
		if($twitter_feed == 'enable'){ 			
			wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
			wp_enqueue_script('cp-bx-slider');
		}
		if(isset($post)){
			$content = strip_tags(get_the_content($post_id));
			if ( has_shortcode( $post->post_content, 'event_counter_box' ) ) { 		
				wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-countdown');
			}
			if ( has_shortcode( $post->post_content, 'person' ) ) { 		
				wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('prettyPhoto');
				wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
				wp_enqueue_script('cp-pscript');
			}
			if ( has_shortcode( $post->post_content, 'slider' ) ) { 		
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');
				wp_register_script('cp-fitvids-slider', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
				wp_enqueue_script('cp-fitvids-slider');	
			}
			if ( has_shortcode( $post->post_content, 'counter_circle' ) ) {
				wp_register_script('cp-easy-chart', NEOMAG_PATH_URL.'/frontend/shortcodes/easy-pie-chart.js', false, '1.0', true);
				wp_enqueue_script('cp-easy-chart');
				wp_register_script('cp-excanvas', NEOMAG_PATH_URL.'/frontend/shortcodes/excanvas.js', false, '1.0', true);
				wp_enqueue_script('cp-excanvas');
			}
			if ( has_shortcode( $post->post_content, 'counters_circle' ) ) {
				wp_register_script('cp-easy-chart', NEOMAG_PATH_URL.'/frontend/shortcodes/easy-pie-chart.js', false, '1.0', true);
				wp_enqueue_script('cp-easy-chart');
				wp_register_script('cp-excanvas', NEOMAG_PATH_URL.'/frontend/shortcodes/excanvas.js', false, '1.0', true);
				wp_enqueue_script('cp-excanvas');
			}
		}
		//calling all the scripts for progress circle
		$topcounter_circle = neomag_cp_get_themeoption_value('topcounter_circle','general_settings');
		$countd_event_category = neomag_cp_get_themeoption_value('countd_event_category','general_settings');
		$color_scheme = neomag_cp_get_themeoption_value('color_scheme','general_settings');
		if(class_exists('CP_Shortcodes')){
			if($topcounter_circle == 'enable'){
				wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-countdown');
			}
		}	
		global $wp_scripts,$post;
		wp_register_script('html5shiv',NEOMAG_PATH_URL.'/frontend/js/html5shive.js',array(),'1.5.1',false);
		wp_enqueue_script('html5shiv');
		$wp_scripts->add_data( 'html5shiv', 'conditional', 'lt IE 9' );		
		//Widget Active
		if(is_active_widget( '', '', 'twitter_widget')){
			wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
			wp_enqueue_script('cp-bx-slider');
			wp_register_script('cp-bx-fitdiv', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
			wp_enqueue_script('cp-bx-fitdiv');			
		}
		// Search and archive page
		if( is_search() || is_archive() ){
		// Post post_type
		}else if(isset($post) && $post->post_type == 'timeline' ){
		}else if( isset($post) &&  $post->post_type == 'sermons' && !is_home()){
			//Jplayer Music Started	
			wp_register_script('cp-jplayer', NEOMAG_PATH_URL.'/frontend/js/jquery.jplayer.min.js', false, '1.0', true);
			wp_enqueue_script('cp-jplayer');
			wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
			wp_enqueue_script('prettyPhoto');
			wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
			wp_enqueue_script('cp-pscript');	
		}else if(isset($post) &&  $post->post_type == 'event' && !is_home()){
			wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
			wp_enqueue_script('cp-countdown');
		}else if(isset($post) &&  $post->post_type == 'service' && !is_home()){
			wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
			wp_enqueue_script('prettyPhoto');
		}else if(isset($post) &&  $post->post_type == 'post' && !is_home() ){
			if(!is_home()){
				$cp_post_thumbnail = '';
				$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
				if($post_detail_xml <> ''){
					$cp_post_xml = new DOMDocument ();
					$cp_post_xml->loadXML ( $post_detail_xml );
					$cp_post_thumbnail = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');						
					if( $cp_post_thumbnail == 'Slider'){
						wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
						wp_enqueue_script('cp-bx-slider');
						wp_register_script('cp-bx-fitdiv', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
						wp_enqueue_script('cp-bx-fitdiv');
					}
				}
			}
		// Page post_type
		}else if( isset($post) &&  $post->post_type == 'page' ){
			global $post,$neomag_cp_page_xml, $neomag_slider_type, $cp_top_slider_type;
			$neomag_cp_page_xml = get_post_meta($post->ID,'page-option-item-xml', true);
			$cp_top_slider_switch = get_post_meta($post->ID,'page-option-top-slider-on', true);
			$neomag_slider_type = get_post_meta ( $post->ID, "page-option-top-slider-types", true );
			$cp_top_slider_type = get_post_meta($post->ID,'page-option-top-slider-types', true);
			$paraluxx = get_post_meta($post->ID,'page-option-attachment-bg-cp', true);
			//Team Slider and Scroller
			if(strpos($neomag_cp_page_xml,'<Team-Slider>') > -1){			
				//Team Scroll Bar Plugin
				wp_register_script('cp-sly', NEOMAG_PATH_URL.'/frontend/js/sly.min.js', false, '1.0', true);
				wp_enqueue_script('cp-sly');
				wp_register_script('cp-plugins_sly', NEOMAG_PATH_URL.'/frontend/js/plugins_sly.js', false, '1.0', true);
				wp_enqueue_script('cp-plugins_sly');
				wp_register_script('cp-horizontal', NEOMAG_PATH_URL.'/frontend/js/horizontal.js', false, '1.0', true);
				wp_enqueue_script('cp-horizontal');
			}		
			//Recipe or Services offers
			if(strpos($neomag_cp_page_xml,'<Offers>') > -1){
				wp_register_script('cp-mCustomScrollbar', NEOMAG_PATH_URL.'/frontend/js/jquery.mCustomScrollbar.concat.min.js', false, '1.0', true);
				wp_enqueue_script('cp-mCustomScrollbar');
			}
			// if using Accordions
			if( strpos($neomag_cp_page_xml,'<Accordion>') > -1 ){
				wp_register_script('cp-accordian-script', NEOMAG_PATH_URL.'/frontend/js/default/jquery.accordion.js', false, '1.0', true);
				wp_enqueue_script('cp-accordian-script');
			}
			// if using tabs
			if( strpos($neomag_cp_page_xml,'<Tab>') > -1 ){
				wp_enqueue_script('jquery-ui-tabs');
				wp_register_script('cp-tabs-script', NEOMAG_PATH_URL.'/frontend/js/tabs_script.js', false, '1.0', true);
				wp_enqueue_script('cp-tabs-script');
			}
			// if using Testimonial
			if( strpos($neomag_cp_page_xml,'<Client-Slider>') > -1 ){
			}
			// if using event slider
			if( strpos($neomag_cp_page_xml,'<Event-Slider>') > -1 ){
				wp_register_script('cp-flexisel-slider', NEOMAG_PATH_URL.'/frontend/js/default/jquery.flexisel.js', false, '1.0', true);
				wp_enqueue_script('cp-flexisel-slider');			
			}
			// if using Destination
			if( strpos($neomag_cp_page_xml,'<Destinations>') > -1 ){
				wp_register_script('cp-owl-slider', NEOMAG_PATH_URL.'/frontend/js/owl.carousel.js', false, '1.0', true);
				wp_enqueue_script('cp-owl-slider');
			}
			// if using timeline
			if( strpos($neomag_cp_page_xml,'<Timeline>') > -1 ){
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');	
				wp_register_script('cp-bx-fitdiv', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-fitdiv');
			}
			// if using Hot Deal Element
			if( strpos($neomag_cp_page_xml,'<Hot-Deal>') > -1 ){
					wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
					wp_enqueue_script('cp-bx-slider');	
			}
			//Parallax effect
			if( strpos($neomag_cp_page_xml,'<Division_Start>') > -1){
				if($paraluxx == 'Parallax'){
					wp_register_script('cp-skrollr', NEOMAG_PATH_URL.'/frontend/js/default/skrollr.min.js', false, '1.0', true);
					wp_enqueue_script('cp-skrollr');
				}
			}
			//Sermons
			if( strpos($neomag_cp_page_xml,'<Sermons>') > -1 ){
				wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('prettyPhoto');
				wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
				wp_enqueue_script('cp-pscript');	
				//Jplayer Music Started	
				wp_register_script('cp-jplayer', NEOMAG_PATH_URL.'/frontend/js/jquery.jplayer.min.js', false, '1.0', true);
				wp_enqueue_script('cp-jplayer');
				//Playlist Script
				wp_register_script('cp-jplayer-playlist', NEOMAG_PATH_URL.'/frontend/js/jplayer.playlist.min.js', false, '1.0', true);
				wp_enqueue_script('cp-jplayer-playlist');
			}
			//Blog Listing
			if( strpos($neomag_cp_page_xml,'<Blog>') > -1 ){
				wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('prettyPhoto');
				wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
				wp_enqueue_script('cp-pscript');	
			}
			if( strpos($neomag_cp_page_xml,'<Service>') > -1 ){
				wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('prettyPhoto');
				wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
				wp_enqueue_script('cp-pscript');	
			}
			if( strpos($neomag_cp_page_xml,'<Gallery>') > -1 ||
				strpos($neomag_cp_page_xml,'<Portfolio>') > -1 
				|| strpos($neomag_cp_page_xml,'<Portfolio-Gallery>') > -1
				|| strpos($neomag_cp_page_xml,'<News>') > -1
				|| strpos($neomag_cp_page_xml,'<Events>') > -1
				){
				wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('prettyPhoto');
				wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
				wp_enqueue_script('cp-pscript');	
			}
			// If using Flex Slider
			if( strpos($neomag_cp_page_xml,'<slider-type>Flex-Slider</slider-type>') > -1 || $neomag_slider_type == 'Flex-Slider' AND $cp_top_slider_switch == 'Yes'){				
				wp_register_script('cp-flex-slider', NEOMAG_PATH_URL.'/frontend/js/jquery.flexslider.js', false, '1.0', true);
				wp_enqueue_script('cp-flex-slider');
			}
			// Contact Form
			if( strpos($neomag_cp_page_xml,'<Contact-Form>') > -1){
				wp_register_script('contact-validation', NEOMAG_PATH_URL.'/frontend/js/jquery.validate.js', false, '1.0', true);
				wp_enqueue_script('contact-validation');
			}
			//Layer Slider Scripts
			if(strpos($neomag_cp_page_xml,'<slider-type>Layer-Slider</slider-type>') > -1 || $neomag_slider_type == 'Layer-Slider' AND $cp_top_slider_switch == 'Yes'){
				if(class_exists('LS_Sliders')){
				// Include in the footer?
					$footer = get_option('ls_include_at_footer', false) ? true : false;
					// Register LayerSlider resources
					wp_register_script('layerslider', LS_ROOT_URL.'/static/js/layerslider.kreaturamedia.jquery.js', array('jquery'), LS_PLUGIN_VERSION, $footer );
					wp_register_script('greensock', LS_ROOT_URL.'/static/js/greensock.js', false, '1.11.2', $footer );
					wp_register_script('layerslider-transitions', LS_ROOT_URL.'/static/js/layerslider.transitions.js', false, LS_PLUGIN_VERSION, $footer );
					wp_enqueue_style('layerslider', LS_ROOT_URL.'/static/css/layerslider.css', false, LS_PLUGIN_VERSION );
					// User resources
					$uploads = wp_upload_dir();
					if(file_exists($uploads['basedir'].'/layerslider.custom.transitions.js')) {
						wp_register_script('ls-user-transitions', $uploads['baseurl'].'/layerslider.custom.transitions.js', false, LS_PLUGIN_VERSION, $footer );
					}
					if(file_exists($uploads['basedir'].'/layerslider.custom.css')) {
						wp_enqueue_style('ls-user-css', $uploads['baseurl'].'/layerslider.custom.css', false, LS_PLUGIN_VERSION );
					}
					if(get_option('ls_conditional_script_loading', false) == false) {
						wp_enqueue_script('layerslider');
						wp_enqueue_script('greensock');
						wp_enqueue_script('layerslider-transitions');
						wp_enqueue_script('ls-user-transitions');
					}
				}
			}
			//Bx Slider Scripts
			if(strpos($neomag_cp_page_xml,'<slider-type>Bx-Slider</slider-type>') > -1 || strpos($neomag_cp_page_xml,'<Latest-News>') > -1){
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');		
				wp_register_script('cp-bx-fitdiv', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-fitdiv');
			}
			//Bx Slider Scripts
			if($neomag_slider_type == 'Bx-Slider' AND $cp_top_slider_switch == 'Yes'){
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');			
				wp_register_script('cp-bx-fitdiv', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-fitdiv');
			}
			// If using Events
			if( strpos($neomag_cp_page_xml,'<Events>') > -1 ){
				wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-countdown');
			}
			if( strpos($neomag_cp_page_xml,'<Event-Slider>') > -1 ){
				wp_register_script('cp-countdown', NEOMAG_PATH_URL.'/frontend/js/default/jquery_countdown.js', false, '1.0', true);
				wp_enqueue_script('cp-countdown');
			}
			// If using Anything Slider
			if( strpos($neomag_cp_page_xml, '<slider-type>Anything</slider-type>') == 233 || $neomag_slider_type == 'Anything' AND $cp_top_slider_switch == 'Yes'){
				wp_register_script('cp-anything-slider', NEOMAG_PATH_URL.'/frontend/anythingslider/js/jquery.anythingslider.js', false, '1.0', true);
				wp_enqueue_script('cp-anything-slider');	
				
				wp_register_script('cp-anything-slider-fx', NEOMAG_PATH_URL.'/frontend/anythingslider/js/jquery.anythingslider.fx.js', false, '1.0', true);
				wp_enqueue_script('cp-anything-slider-fx');	
			}
			// If using NewsSlider
			if( strpos($neomag_cp_page_xml,'<News-Slider>') > -1 ){
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');		
				wp_register_script('cp-bx-fitdiv', NEOMAG_PATH_URL.'/frontend/js/default/jquery.fitvids.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-fitdiv');		
			}
			// If using Blog Slider
			if( strpos($neomag_cp_page_xml,'<Blog_Slider>') > -1 ){
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');		
			}
			// If using Event Slider
			if( strpos($neomag_cp_page_xml,'<Event-Slider>') > -1 ){
				wp_register_script('cp-flex-slider', NEOMAG_PATH_URL.'/frontend/js/default/jquery.flexisel.js', false, '1.0', true);
				wp_enqueue_script('cp-flex-slider');	
			}
			if( strpos($neomag_cp_page_xml,'<Products_Slider>') > -1 ){
				wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('cp-bx-slider');	
				wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css'); 
			}
			// If using filterable plugin
			if( strpos($neomag_cp_page_xml,'<filterable>Yes</filterable>') > -1 ){
				wp_register_script('filterable', NEOMAG_PATH_URL.'/frontend/shortcodes/jquery-filterable.js', false, '1.0', true);
				wp_enqueue_script('filterable');
				wp_register_script('jquery-easing-1.3', NEOMAG_PATH_URL.'/frontend/js/jquery-easing-1.3.js', false, '1.0', true);
				wp_enqueue_script('jquery-easing-1.3');
			}
			// If using filterable plugin
			if( strpos($neomag_cp_page_xml,'<Woo-Products>') > -1 || strpos($neomag_cp_page_xml,'<Portfolio-Gallery>') > -1 ){
				wp_register_script('jquery-easing-1.3', NEOMAG_PATH_URL.'/frontend/js/jquery-easing-1.3.js', false, '1.0', true);
				wp_enqueue_script('jquery-easing-1.3');
				wp_register_script('prettyPhoto', NEOMAG_PATH_URL.'/frontend/js/default/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('prettyPhoto');
				wp_register_script('cp-pscript', NEOMAG_PATH_URL.'/frontend/js/default/pretty_script.js', false, '1.0', true);
				wp_enqueue_script('cp-pscript');
			}
			if( strpos($neomag_cp_page_xml,'<eventview>Calendar View</eventview>') > -1 ){
				wp_register_script('cp-calender-view', NEOMAG_PATH_URL.'/framework/javascript/fullcalendar/fullcalendar.js', false, '1.0', true);
				wp_enqueue_script('cp-calender-view');
			}
		}
	}
?>