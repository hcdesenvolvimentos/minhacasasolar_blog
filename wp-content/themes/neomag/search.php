<?php
/*
 * This file is used to generate WordPress standard archive/category pages.
 */ 
get_header ();
	
	//Get Default Option for Archives, Category, Search.
	$neomag_num_excerpt = '';
	$neomag_cp_default_settings = get_option('default_pages_settings');
	if($neomag_cp_default_settings != ''){
		$neomag_cp_default = new DOMDocument ();
		$neomag_cp_default->loadXML ( $neomag_cp_default_settings );
		$neomag_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'sidebar_default');
		$neomag_right_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'right_sidebar_default');
		$neomag_left_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'left_sidebar_default');
		$neomag_num_excerpt = cp_find_xml_value($neomag_cp_default->documentElement,'default_excerpt');
		
	}	
	//Get Default Excerpt
	$neomag_num_excerpt = 250;
	if(empty($paged)){
		$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
	}
	global $paged,$post,$neomag_sidebar,$blog_div_size_num_class,$counter,$wp_query;	
		
		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}
		$neomag_select_layout_cp = '';
		$neomag_cp_general_settings = get_option('general_settings');
		if($neomag_cp_general_settings <> ''){
			$neomag_cp_logo = new DOMDocument ();
			$neomag_cp_logo->loadXML ( $neomag_cp_general_settings );
			$neomag_select_layout_cp = cp_find_xml_value($neomag_cp_logo->documentElement,'select_layout_cp');
		}
		$neomag_sidebar_class = '';
		$neomag_content_class = '';
		
		//Get Sidebar for page
		$neomag_sidebar_class = array('0'=>'col-md-3','1'=>'col-md-12','2'=>'col-md-9');
		$neomag_header_style = '';
	$neomag_html_class_banner = '';
	$neomag_html_class = cp_print_header_class($neomag_header_style);
	if($neomag_html_class <> ''){$neomag_html_class_banner = 'banner';}
	$neomag_breadcrumbs = '';
	$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
	$neomag_cp_page_caption = '';
	$neomag_header_style = '';
	//Print Style 6
	if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
		cp_print_header_html($neomag_header_style);
	}
	?>
		<!--Inner Pages Heading Area Start-->
          <div class="cp-page-title">
			<?php if (is_category()) { ?>
				<h2><?php esc_html_e('Categorias','neomag'); ?> <?php echo single_cat_title(); ?></h2>
				<?php } elseif (is_day()) { ?>
					<h2><?php esc_html_e('Busca sobre','neomag'); ?> 
					<?php echo get_the_date(get_option("date_format")); ?></h2>
						<?php } elseif (is_month()) { ?>
							<h2><?php esc_html_e('Busca sobre','neomag'); ?> <?php echo get_the_date(get_option("date_format")); ?></h2>
								<?php } elseif (is_year()) { ?>
									<h2><?php esc_html_e('Busca sobre','neomag'); ?> <?php echo get_the_date(get_option("date_format")); ?></h2>
									<?php }elseif (is_search()) { ?>
								<h2><?php esc_html_e('Resultado da busca sobre:','neomag'); ?> : <?php echo get_search_query() ?></h2>
							<?php } elseif (is_tag()) { ?>
						<h2><?php esc_html_e('Tag da Busca','neomag'); ?> : <?php echo single_tag_title('', true); ?></h2>
					<?php }elseif (is_author()) { ?>
				<h2><?php esc_html_e('Busca sobre o Autor','neomag'); ?></h2>
			<?php }?> 
          </div>
		<!--Inner Pages Heading Area End-->    
    
            <!--CONTENT START-->
            <div id="cp-content-wrap" class="cp-content-wrap">
            <div class="container">
              <div class="row"> 
                <!--SIDEBAR START-->
					<?php
					if($neomag_sidebar == "left-sidebar" || $neomag_sidebar == "both-sidebar" || $neomag_sidebar == "both-sidebar-left"){?>
						<div id="block_first" class="sidebar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
							<?php dynamic_sidebar( $neomag_left_sidebar ); ?>
						</div>
						<?php
					}
					if($neomag_sidebar == 'both-sidebar-left'){?>
						<div id="block_first_left" class="sidebar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
							<?php dynamic_sidebar( $neomag_right_sidebar );?>
						</div>
					<?php } ?>
					<!-- <div id="search-<?php the_ID(); ?>" class="<?php echo esc_attr($neomag_sidebar_class[2]);?> blog_listing row blog-home">	 -->	
					<div id="search-<?php the_ID(); ?>" class="col-md-12 blog_listing row blog-home">				
                  <!--LEFT CONTENT START-->
                  <div >
                    <div class="cp-posts-style-1">
                      <ul class="cp-posts-list cp-post-details">
							<?php if (is_author()) { ?>
								<?php 
								if ( have_posts() ) {
									the_post();?>
									<div class="clear"></div>
									<!--DETAILED TEXT END-->
									<div class="about-admin">
										<div class="thumb">
											<?php echo get_avatar(get_the_author_meta( 'ID' ));?>
										</div>
										<div class="text">
											<h4><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )); ?>">
													<?php echo get_the_author(); ?>
												</a>
											</h4>
											<p><?php echo mb_substr(get_the_author_meta( 'description' ),0,360);?></p>
											<div class="share-it">
												<h5><?php esc_html_e('Follow Me','neomag');?></h5>
												<?php 
													$neomag_facebook = get_the_author_meta('facebook');
													$neomag_twitter = get_the_author_meta('twitter');
													$neomag_gplus = get_the_author_meta('gplus');
													$neomag_linked = get_the_author_meta('linked');
													$neomag_skype = get_the_author_meta('skype');
												?>
												<?php if($neomag_facebook <> ''){ ?><a title="" data-toggle="tooltip" href="<?php echo esc_url($neomag_facebook);?>" data-original-title="facebook"><i class="fa fa-facebook"></i></a><?php }?>
												<?php if($neomag_twitter <> ''){ ?><a title="" data-toggle="tooltip" href="<?php echo esc_url($neomag_twitter);?>" data-original-title="Twitter"><i class="fa fa-twitter"></i></a><?php }?>
												<?php if($neomag_gplus <> ''){ ?><a title="" data-toggle="tooltip" href="<?php echo esc_url($neomag_gplus);?>" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a><?php }?>
												<?php if($neomag_linked <> ''){ ?><a title="" data-toggle="tooltip" href="<?php echo esc_url($neomag_linked);?>" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a><?php }?>
												<?php if($neomag_skype <> ''){ ?><a title="" data-toggle="tooltip" href="<?php echo esc_url($neomag_skype);?>" data-original-title="skype"><i class="fa fa-skype"></i></a><?php }?>
											</div>
										</div>
									</div>
									<div class="clear"></div>
								<?php
								} 
							}
							if ( have_posts() ) { while ( have_posts() ) : the_post();
								//Image dimenstion
							global $post, $allowedposttags;
							$neomag_thumbnail_types = '';
							$neomag_post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
							if($neomag_post_detail_xml <> ''){
								$neomag_cp_post_xml = new DOMDocument ();
								$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml );
								$neomag_post_social = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_social');
								$neomag_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'sidebar_post');
								$neomag_right_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'right_sidebar_post');
								$neomag_left_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'left_sidebar_post');
								$neomag_thumbnail_types = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_thumbnail');
								$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
								$neomag_select_slider_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'select_slider_type');	
							}
						$neomag_post_format = get_post_format($post->ID);
						$neomag_check_post_std = get_post_format();
						if ( false === $neomag_check_post_std ) {
					if($neomag_thumbnail_types == 'Slider'){ $neomag_post_format = 'slider post'; } else
						{ $neomag_post_format = 'standard post'; }
					}
							$neomag_mask_html = '';
							$neomag_no_image_class = 'no-image';
							if(get_the_post_thumbnail($post_id, array(1170,350)) <> ''){
								$neomag_mask_html = '<div class="mask">
									<a href="'.get_permalink().'#comments" class="anchor"><span> </span> <i class="fa fa-comment"></i></a>
									<a href="'.get_permalink().'" class="anchor"> <i class="fa fa-link"></i></a>
								</div>';
								$neomag_no_image_class = 'image-exists';
							}	
							$neomag_arc_div_archive_listing_class = array("Full-Image" => array("index"=>"1", "class"=>"sixteen ", "size"=>array(1170,420), "size2"=>array(770, 400), "size3"=>array(570,300)));
							$neomag_item_type = 'Full-Image';
							$neomag_item_class = $neomag_arc_div_archive_listing_class[$neomag_item_type]['class'];
							$neomag_item_index = $neomag_arc_div_archive_listing_class[$neomag_item_type]['index'];		
							if( $neomag_sidebar == "no-sidebar" ){
								$neomag_item_size = $neomag_arc_div_archive_listing_class[$neomag_item_type]['size'];
							}else if ( $neomag_sidebar == "left-sidebar" || $neomag_sidebar == "right-sidebar" ){
								$neomag_item_size = $neomag_arc_div_archive_listing_class[$neomag_item_type]['size2'];
							}else{
								$neomag_item_size = $neomag_arc_div_archive_listing_class[$neomag_item_type]['size3'];
							}										
							$neomag_get_post_cp = get_post($post_id);
							$neomag_thumbnail_id = get_post_thumbnail_id( $post->ID );
							$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, 'full');
							?>
                            
            <li class="cp-post">
              <?php 
				$neomag_link_text = '';
				if($neomag_post_format == 'link'){ 
                // Get the text & url from the first link in the content
                $neomag_content = get_the_content();
                $neomag_link_string = neo_mag_extract_link('<a href=', '/a>', $neomag_content);
                $neomag_link_bits = explode('"', $neomag_link_string);
                foreach( $neomag_link_bits as $neomag_bit ) {
                    if( substr($neomag_bit, 0, 1) == '>') $neomag_link_text = substr($neomag_bit, 1, strlen($neomag_bit)-2);
                    if( substr($neomag_bit, 0, 4) == 'http') $neomag_link_url = $neomag_bit;
                }  ?>
              <?php if(cp_print_blog_thumbnail($post->ID,array(1170,350)) <> ''){ ?>
              <div class="cp-thumb link-post"> <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                <div class="cp-link-post-link"> <i class="fa fa-link"></i> <a href="<?php echo esc_url($neomag_link_url); ?>" target="_blank"><?php echo esc_attr($neomag_link_text); ?></a> </div>
              </div>
              <?php } } ?>
              <?php if(($neomag_post_format == 'standard post')||($neomag_post_format == 'chat')){ 
				  			if($neomag_image_thumb[0] <> ''){
				  ?>
              <div class="cp-thumb"> <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>"> </div>
              <?php } } ?>
              <?php if($neomag_post_format == 'video'){ ?>
              <div class="cp-thumb">
                <iframe src="<?php echo esc_url($neomag_video_url_type); ?>"></iframe>
              </div>
              <?php } ?>
              <?php if($neomag_post_format == 'photo_set'){ 
                  echo do_shortcode($neomag_shortcode_content);
                   } ?>
              <?php if($neomag_post_format == 'slider post'){ ?>
              <div class="cp-thumb"> <?php echo cp_print_blog_thumbnail($post->ID, 849); ?> </div>
              <?php } ?>
              <?php if($neomag_post_format == 'audio'){ ?>
              <div class="cp-thumb"> <?php echo cp_print_blog_thumbnail($post->ID, 849); ?> </div>
              <?php } ?>
              <div class="cp-post-base"> 
                
                <!--Post Content Start-->
                <div class="cp-post-content">
                  <a href="<?php echo get_permalink();?>"><h2><?php echo get_the_title(); ?></h2></a>
                  <ul class="cp-post-meta">
                    <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?>
                      <?php esc_html_e('ago','neomag'); ?>
                      </a></li>
                    <?php
                           $neomag_get_category_obj = get_the_category ( $post->ID ); 
                           $neomag_category_name = array_shift( $neomag_get_category_obj );
							if($neomag_category_name <> ''){
								echo '<li><a>'.$neomag_category_name->name.'</a></li>';
							}
                              ?>
                    <li>
                      <?php
                         //Get Post Comment 
                         comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                           esc_html__('1 Comment','neomag'),
                           esc_html__('% Comments','neomag'), '',
                           esc_html__('Comments are off','neomag') );
                       ?>
                    </li>
                  </ul>
                  <?php 
					if($neomag_post_format == 'photo_set'){ 
                  echo do_shortcode($neomag_shortcode_content);
                   } else { ?>
					<p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 400));?></p>
				  <?php } ?>
                </div>
                <!--Post Content End--> 
              </div>
            </li>
            <!--Post End-->
             <li>
                   <?php 
                    //End while
                    endwhile; 
                    //Condition Ends ?>
             <li>       
             <?php   }else{ ?>
						<div class="help-404">
							<h2><?php echo esc_html__('Nenhum resultado da busca encontrado ! Por favor, use uma outra palavra-chave.','neomag')?></h2>
							<form method="get" id="searchform" class="form-404" action="<?php  echo esc_url(home_url('/')); ?>/">
								<div id="search-text">
									<input type="text" placeholder="Buscar" value="<?php the_search_query(); ?>" name="s"  autocomplete="off" />
								</div>
								<input type="submit" id="searchsubmit" value="Enviar" />
								<span class="overflow"></span>
								<br class="clear">
							</form>
						</div>
						<?php
					}
                    //Pagination
                    cp_pagination();
                ?>						
            </div>	
        </div>
      </div>
					<?php
					if($neomag_sidebar == "both-sidebar-right"){?>
						<div class="<?php echo esc_attr($neomag_sidebar_class[0]);?>">
							<?php dynamic_sidebar( $neomag_left_sidebar ); ?>
						</div>
						<?php
					}
					if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){?>
					<div class="<?php echo esc_attr($neomag_sidebar_class[0]);?>">
						<?php dynamic_sidebar( $neomag_right_sidebar );?>
					</div>
					<?php } ?>						   
				</div>
			</div>
		</div>
	</div>	
<div class="clear"></div>
<?php get_footer(); ?>