<?php

	/*
	*	CrunchPress Page Item File
	*	---------------------------------------------------------------------
	* 	@version	1.0
	* 	@author		CrunchPress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) CrunchPress
	*	---------------------------------------------------------------------
	*	This file contains the function that can print each page item in 
	*	different conditions.
	*	---------------------------------------------------------------------
	*/

	
	// Print the item size <div> with it's class
	function cp_print_item_size($item_size, $addition_class=''){
		global $neomag_item_row_size;
		
		$neomag_item_row_size = (empty($neomag_item_row_size))? 'first': $neomag_item_row_size;
		if($neomag_item_row_size >= 1){
			$neomag_item_row_size = 'first';
		}
				
		switch($item_size){
			case 'element1-4':
				echo '<div class="col-md-3 mbtm ' . $addition_class .' ' .$neomag_item_row_size. '">';
				$neomag_item_row_size += 1/4; 
				break;
			case 'element1-3':
				echo '<div class="col-md-4 mbtm ' . $addition_class .' ' .$neomag_item_row_size. '">';
				$neomag_item_row_size += 1/3; 
				break;
			case 'element1-2':
				echo '<div class="col-md-6 mbtm ' . $addition_class .' ' .$neomag_item_row_size. '">';
				$neomag_item_row_size += 1/2; 
				break;
			case 'element2-3':
				echo '<div class="col-md-8 mbtm ' . $addition_class .' ' .$neomag_item_row_size. '">';
				$neomag_item_row_size += 2/3; 
				break;
			case 'element3-4':
				echo '<div class="col-md-9 mbtm ' . $addition_class .' ' .$neomag_item_row_size. '">';
				$neomag_item_row_size += 3/4; 
				break;
			case 'element1-1':
				echo '<div class="col-md-12 mbtm ' . $addition_class .' ' .$neomag_item_row_size. '">';
				$neomag_item_row_size += 1; 
				break;	
		}
	}
	// Print column 
	function cp_print_column_item($item_xml){
		echo do_shortcode(html_entity_decode(cp_find_xml_value($item_xml,'column-text')));
	}
	//Print Sidebar
	function cp_print_sidebar_item($item_xml){ 
	
		$select_layout = cp_find_xml_value($item_xml, 'sidebar-layout-select'); 
		dynamic_sidebar( $select_layout );
	}
	
	//Division Ends Here
	function cp_print_div_end_item ( $item_xml ){
		echo '</div></div></div></div></div>';
	}
	
	//Division Ends Here
	function cp_print_div_end_item_old ( $item_xml ){
		echo '</div></div>';
	}
	//Division of sections
	function cp_print_div_item ( $item_xml ){
		//Fetch Data from Theme Options
		
		$select_type = cp_find_xml_value($item_xml, 'select-type'); 
		$bgimage = cp_find_xml_value($item_xml, 'image'); 
		$bgcolor = cp_find_xml_value($item_xml, 'color'); 
		$opacity = cp_find_xml_value($item_xml, 'opacity'); 
		$repeat = cp_find_xml_value($item_xml, 'repeat'); 
		$bgattachment = cp_find_xml_value($item_xml, 'background-attachment'); 
		$bg_position = cp_find_xml_value($item_xml, 'bg_position'); 
		$paddingtop = cp_find_xml_value($item_xml, 'padding-top'); 
		$paddingbottom = cp_find_xml_value($item_xml, 'padding-bottom'); 
		$one_pos = cp_find_xml_value($item_xml, 'image-parallax-one-pos'); 
		$img_id_one = cp_find_xml_value($item_xml, 'image-parallax-one'); 
		$two_pos = cp_find_xml_value($item_xml, 'image-parallax-two-pos'); 
		$img_id_two = cp_find_xml_value($item_xml, 'image-parallax-two'); 
		$cp_class = cp_find_xml_value($item_xml, 'add-section-class'); 

		//Empty Array in Case Image not set
		$one_image = array();
		$two_image = array();
		$neomag_image_url = wp_get_attachment_image_src($bgimage, 'full');
		
		//Condition for Image if Found
		if(!empty($img_id_one)){
			$one_image = wp_get_attachment_image_src($img_id_one, 'full');
		}
		
		//Condition for Image if Found
		if(!empty($img_id_two)){
			$two_image = wp_get_attachment_image_src($img_id_two, 'full');
		}
		
		if($select_type == 'Plain'){
			echo '<div style="padding-top:'.($paddingtop).';padding-bottom:'.($paddingbottom).'; float:left; width: 100%;" class="color_class_cp cp_division_width'.esc_attr($cp_class).'">
			<div class="full-width cp_division_width">
			<div class="container">
			<div class="row">';
		}else if($select_type == 'Background Color'){
			
			echo '<div style="padding-top:'.($paddingtop).';padding-bottom:'.($paddingbottom).';background-color:'.($bgcolor).';" class=" cp_division_width color_class_cp student-profile '.esc_attr($cp_class).'">
			<div class="bg_full_transparency" style="float:left;width:100%; opacity:'.($opacity).';background-image:url('.esc_url($neomag_image_url[0]).');background-size:cover;background-attachment:'.($bgattachment).';background-repeat:'.($repeat).';"></div>
			<div class="full-width cp_division_width">
			<div class="container">
			<div class="row">';
		}else{
			if($bgattachment == 'Parallax'){
				$html_one_img = '';
				$html_two_img = '';
				if(!empty($one_image)){
					
					$html_one_img = '<div class="bg1 parallax_class" style="background-image:url('.($one_image[0]).')" data-0="background-position:0px 0px;" data-end="background-position:'.esc_attr($one_pos).';">&nbsp;</div>';
				
				}
				
				if(!empty($two_image)){
					
					$html_two_img = '<div class="bg2 parallax_class" style="background-image:url('.($two_image[0]).')" data-0="background-position:0px 0px;" data-end="background-position:'.esc_attr($two_pos).';">&nbsp;</div>';
				}
				
				
				echo '<div style="padding-top:'.esc_attr($paddingtop).';padding-bottom:'.($paddingbottom).';background-color:'.($bgcolor).';" class=" cp_division_width color_class_cp '.esc_attr($cp_class).'">
					'.($html_one_img).'
					'.($html_two_img).'
					<div class="bg_full_transparency" data-0="background-position:0px 0px;" data-end="background-position:'.($bg_position).';" style="float:left;width:100%;opacity:'.esc_attr($opacity).';background-image:url('.esc_url($neomag_image_url[0]).');background-size:cover;background-repeat:'.esc_url($repeat).';">
					
				<div style="float:left;width:100%;" class="full-width"><div class="container"><div class="row">';
			}else{
				
				echo '<div style="  background-color:'.($bgcolor).';" class=" cp_division_width color_class_cp '.($cp_class).'">
				<div class="bg_full_transparency" style=" padding-top:'.($paddingtop).';padding-bottom:'.($paddingbottom).'; float:left;width:100%; opacity:'.($opacity).';background:url('.esc_url($neomag_image_url[0]).') '.($repeat).' ;background-attachment:'.($bgattachment).';background-repeat:'.($repeat).';">
				<div class="full-width cp_division_width">
				<div class="container">
				<div class="row">';
			}
		}
	}
	
	//Division of sections
	function cp_print_div_item_old ( $item_xml ){
		//Fetch Data from Theme Options
		
		$select_type = cp_find_xml_value($item_xml, 'select-type'); 
		$bgimage = cp_find_xml_value($item_xml, 'image'); 
		$bgcolor = cp_find_xml_value($item_xml, 'color'); 
		$bgattachment = cp_find_xml_value($item_xml, 'background-attachment'); 
		$paddingtop = cp_find_xml_value($item_xml, 'padding-top'); 
		$paddingbottom = cp_find_xml_value($item_xml, 'padding-bottom'); 
		$neomag_image_url = wp_get_attachment_image_src($bgimage, 'full');
		
		
		if($select_type == 'Plain'){
			echo '<div class="container inner-container-cp" ><div class="row" style="padding-top:'.($paddingtop).';padding-bottom:'.($paddingbottom).';float:left;">';
		}else{
		//return '<div style="border-size:'.$bordersize.';border-color:'.$bordercolor.';padding-top:'.$paddingtop.';padding-bottom:'.$paddingbottom.';background-color:'.$bgcolor.';background-image:'.$bgimage.';background-repeat:'.$bgrepeat.';background-attachment:'.$bgattachment.';background-position:'.$bgposition.'" class="full-width"><div class="container">'.do_shortcode($content).'</div></div>';
			echo '<div style="float:left;width:100%;padding-top:'.($paddingtop).';padding-bottom:'.($paddingbottom).';background-color:'.($bgcolor).';background-image:url('.esc_url($neomag_image_url[0]).');background-attachment:'.($bgattachment).';" class="full-width"><div class="container">';
		}
	
	}

	$gallery_div_size_listing_class = array(
		'Masonary' => array( 'class'=>'col-md-3', 'class2'=>'col4_gallery_one_sidebar','class3'=>'col4_gallery_two_sidebar','size'=>array(614,614),'size2'=>array(614,614),'size3'=>array(614,614)),
		'4 Column' => array( 'class'=>'col-md-3', 'class2'=>'col4_gallery_one_sidebar','class3'=>'col4_gallery_two_sidebar','size'=>'Gallery_4','size2'=>'Gallery_5','size3'=>'Gallery_4'),
		'3 Column' => array( 'class'=>'col-md-4', 'class2'=>'col3_gallery_one_sidebar','class3'=>'col3_gallery_two_sidebar','size'=>'Gallery_3','size2'=>'Gallery_5','size3'=>'Gallery_3'),
		'2 Column' => array( 'class'=>'col-md-6', 'class2'=>'col-md-3','class3'=>'col-md-3','size'=>'Gallery_2','size2'=>'Gallery_5','size3'=>'Gallery_2'),
	); 	
	
	// Print gallery
	function cp_print_gallery_item($item_xml){
	
		global $gallery_div_size_listing_class;
		global $paged,$sidebar,$post_id,$wp_query;		

		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}
		$gal_counter = '';
		$masonary_counter = 0;
		
		//Fetch Elements Data from database
		$header = cp_find_xml_value($item_xml, 'header');
		$gallery_page = cp_find_xml_value($item_xml, 'page');
		$gallery_size = cp_find_xml_value($item_xml, 'item-size');
		$num_size = cp_find_xml_value($item_xml, 'num-size');
		$show_pagination = cp_find_xml_value($item_xml, 'show-pagination');
		
		//Count Images per row
		if($gallery_size == '2 Column'){$gal_counter = 2;}else if($gallery_size == '3 Column'){$gal_counter = 3;}else if($gallery_size == '4 Column'){$gal_counter = 4;}else if($gallery_size == 'Masonary'){$gal_counter = 1;}else{}		
		
		$gallery_class = $gallery_div_size_listing_class[$gallery_size]['class'];
		if( $sidebar == "no-sidebar" || $sidebar == ''){
			$gallery_class = $gallery_div_size_listing_class[$gallery_size]['class'];
			$item_size = $gallery_div_size_listing_class[$gallery_size]['size'];
		}else if ( $sidebar == "left-sidebar" || $sidebar == "right-sidebar" ){
			$gallery_class = $gallery_div_size_listing_class[$gallery_size]['class']; 
			$item_size = $gallery_div_size_listing_class[$gallery_size]['size2'];
		}else{
			$gallery_class = $gallery_div_size_listing_class[$gallery_size]['class'];
			$item_size = $gallery_div_size_listing_class[$gallery_size]['size3'];
		}
		
		
		if(!empty($header)){
			echo '<h2 class="heading-brands">' . esc_attr($header) . '</h2>';
		}

		if($gallery_page <> ''){
		$slider_xml_string = get_post_meta($gallery_page,'post-option-gallery-xml', true);
			if($gallery_size == 'Masonry View'){ 
				echo '<div class="gallery-cp">
                    <div id="container" class="gallery">';						
							wp_register_script('cp-isotop-min', NEOMAG_PATH_URL.'/frontend/js/blocksit.min.js', false, '1.0', true);
							wp_enqueue_script('cp-isotop-min');
							echo '<script>
								jQuery(document).ready(function($) {
									use strict;
									if ($("#container").length) {
										$("#container").BlocksIt({
											numOfCol: 4,
											offsetX: 15,
											offsetY: 15
										});
									}
								});
							</script>';
							if($slider_xml_string <> ''){
								$slider_xml_dom = new DOMDocument();
								if( !empty( $slider_xml_string ) ){
									$slider_xml_dom->loadXML($slider_xml_string);
										$children = $slider_xml_dom->documentElement->childNodes;
										if ( empty($wp_query->query['paged']) ) $wp_query->query['paged'] = 1;
											$total_page = '';
											if($num_size > 0){
													$limit_start = $num_size * ($wp_query->query['paged']-1);
													$limit_end = $limit_start + $num_size;
													if ( $limit_end > $slider_xml_dom->documentElement->childNodes->length ) {
														$limit_end = $slider_xml_dom->documentElement->childNodes->length;
													}
													
													if($num_size < $slider_xml_dom->documentElement->childNodes->length){
														$total_page = ceil($slider_xml_dom->documentElement->childNodes->length/$num_size);
													}else{
														$total_page = 1;
													}
											}
											else {
												$limit_start = 0;
												$limit_end = $slider_xml_dom->documentElement->childNodes->length;
											}
											$counter_gal_element = 0;
											for($i=$limit_start;$i<$limit_end;$i++) { 
												$thumbnail_id = cp_find_xml_value($children->item($i), 'image');
												$title = cp_find_xml_value($children->item($i), 'title');
												$caption = cp_find_xml_value($children->item($i), 'caption');
												$link_type = cp_find_xml_value($children->item($i), 'linktype');
												$video = cp_find_xml_value($children->item($i), 'video');
												$neomag_image_url = wp_get_attachment_image_src($thumbnail_id, $item_size); 
												$alt_text = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true);
												
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'full');
												$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(614,614));
												$link = cp_find_xml_value( $children->item($i), 'link');
												//Condition for Width and Height for each Masonry Element
												if($counter_gal_element % 4 == 0){$gal_class= 'item-w2 item-h3';}else if($counter_gal_element % 4 == 1){$gal_class= 'item-h2';}else if($counter_gal_element % 4 == 2){$gal_class= 'item-h2';}else if($counter_gal_element % 4 == 3){$gal_class= '';}else{}?>
												<div class="grid">
													<div class="caption"><a href="<?php echo esc_url($image_full[0])?>" class="zoom" data-gal="prettyPhoto[gallery1]"><i class="fa fa-plus"></i></a></div>
													<div class="imgholder"> <img src="<?php echo esc_url($image_full[0])?>" alt="<?php echo esc_attr($title);?>"> </div>
												</div>
										<?php $counter_gal_element++;
											} //Foreach loop
								} //Empty Condition check 
							}	//Empty Condition check 
						
                    echo '</div>
                </div>';
			}elseif($gallery_size == 'Carousel'){
				global $counter;
				echo '<div class="partner-logos">';						
							//Bx Slider Script Calling
							wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
							wp_enqueue_script('cp-bx-slider');	
							wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');?>
							<script type="text/javascript">
							jQuery(document).ready(function ($) {
								"use strict";
								if ($('#logo-slider-<?php echo esc_attr($counter)?>').length) {
									$('#logo-slider-<?php echo esc_attr($counter)?>').bxSlider({
										minSlides: 3,
										maxSlides: 6,
										slideWidth: 170,
										pager:false,
										slideMargin: 24,
										auto:true
									});
								}
							});
							</script>
							<?php
							echo '<ul id="logo-slider-'.esc_attr($counter).'" class="logo-slider">';
							if($slider_xml_string <> ''){
								$slider_xml_dom = new DOMDocument();
								if( !empty( $slider_xml_string ) ){
									$slider_xml_dom->loadXML($slider_xml_string);
										$children = $slider_xml_dom->documentElement->childNodes;
										if ( empty($wp_query->query['paged']) ) $wp_query->query['paged'] = 1;
											$total_page = '';
											if($num_size > 0){
													$limit_start = $num_size * ($wp_query->query['paged']-1);
													$limit_end = $limit_start + $num_size;
													if ( $limit_end > $slider_xml_dom->documentElement->childNodes->length ) {
														$limit_end = $slider_xml_dom->documentElement->childNodes->length;
													}
													
													if($num_size < $slider_xml_dom->documentElement->childNodes->length){
														$total_page = ceil($slider_xml_dom->documentElement->childNodes->length/$num_size);
													}else{
														$total_page = 1;
													}
											}
											else {
												$limit_start = 0;
												$limit_end = $slider_xml_dom->documentElement->childNodes->length;
											}
											$counter_gal_element = 0;
											for($i=$limit_start;$i<$limit_end;$i++) { 
												$thumbnail_id = cp_find_xml_value($children->item($i), 'image');
												$title = cp_find_xml_value($children->item($i), 'title');
												$caption = cp_find_xml_value($children->item($i), 'caption');
												$link_type = cp_find_xml_value($children->item($i), 'linktype');
												$video = cp_find_xml_value($children->item($i), 'video');
												$neomag_image_url = wp_get_attachment_image_src($thumbnail_id, $item_size);
												$alt_text = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true);
												
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'full');
												$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(614,614));
												$link = cp_find_xml_value( $children->item($i), 'link');
												//Condition for Width and Height for each Masonry Element
												if($counter_gal_element % 4 == 0){$gal_class= 'item-w2 item-h3';}else if($counter_gal_element % 4 == 1){$gal_class= 'item-h2';}else if($counter_gal_element % 4 == 2){$gal_class= 'item-h2';}else if($counter_gal_element % 4 == 3){$gal_class= '';}else{}?>
												<li><img src="<?php echo esc_url($image_full[0])?>" alt="<?php echo esc_attr($title);?>"></li>
										<?php $counter_gal_element++;
											} //Foreach loop
								} //Empty Condition check 
							}	//Empty Condition check 
						
                    echo '</ul>
                </div>';
			}else{
				if($slider_xml_string <> ''){
				$slider_xml_dom = new DOMDocument();
					if( !empty( $slider_xml_string ) ){
						$slider_xml_dom->loadXML($slider_xml_string);
						
						$ulclass = '';
						if($gallery_size == '1 Column'){ $ulclass = 'gallery-full'; }
						if(($gallery_size == '2 Column')){ $ulclass = 'cp-extended-gallery'; }
						if(($gallery_size == '3 Column')){ $ulclass = 'cp-grid-gallery'; }
						if($gallery_size == '4 Column'){ $ulclass = 'cp-full-gallery'; }
						if($gallery_size == 'Masonary'){ $ulclass = 'cp-grid-isotope'; 
						echo '<div class="container"><div class="row"><div class="col-md-12"><div class="'.$ulclass.' gallery"><div class="isotope items">';
						} else {
						echo '<ul class="'.$ulclass.' gallery cp_'.strtolower(str_replace(' ','_',$gallery_size)).'">';
					}
							$children = $slider_xml_dom->documentElement->childNodes;
							if ( empty($wp_query->query['paged']) ) $wp_query->query['paged'] = 1;
										$total_page = '';
										if($num_size > 0){
											$limit_start = $num_size * ($wp_query->query['paged']-1);
											$limit_end = $limit_start + $num_size;
											if ( $limit_end > $slider_xml_dom->documentElement->childNodes->length ) {
												$limit_end = $slider_xml_dom->documentElement->childNodes->length;
											}
											
											if($num_size < $slider_xml_dom->documentElement->childNodes->length){
												$total_page = ceil($slider_xml_dom->documentElement->childNodes->length/$num_size);
											}else{
												$total_page = 1;
											}
									}
									else {
										$limit_start = 0;
										$limit_end = $slider_xml_dom->documentElement->childNodes->length;
									}
							$counter_gal_element = 0;
							$single_col = 0;
							for($i=$limit_start;$i<$limit_end;$i++) { 
								$thumbnail_id = cp_find_xml_value($children->item($i), 'image');
								$title = cp_find_xml_value($children->item($i), 'title');
								$caption = cp_find_xml_value($children->item($i), 'caption');
								$link_type = cp_find_xml_value($children->item($i), 'linktype');
								$video = cp_find_xml_value($children->item($i), 'video');
								$neomag_image_url = wp_get_attachment_image_src($thumbnail_id, $item_size);
								$alt_text = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true);	
								
								if($gallery_size == '4 Column'){ ?>

										<!--LIST ITEM START-->
										<li class="cp-25pw">
                                        
                                        	<figure class="cp-gallery-thumb">
											<?php 
											$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full');
											$image_thumb = aq_resize( $image_full[0], 450, 323, true );
											$link = cp_find_xml_value( $children->item($i), 'link');
											if( $link_type == 'Link to URL' ){
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="' . esc_url($image_thumb) . '" alt="' . esc_attr($alt_text) . '" />'; ?>
                                                	<a href="<?php echo esc_url($link); ?>" data-rel="prettyPhoto[gallery1]" class="hicon"><i class="fa fa-search"></i></a> 
											<?php }else if( $link_type == 'Lightbox' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full'); 
												$image_thumb = aq_resize( $image_full[0], 450, 323, true );
												$link = cp_find_xml_value( $children->item($i), 'link'); ?>
                                                <img src="<?php echo esc_url($image_thumb);?>" alt="<?php echo esc_attr($alt_text);?>" />
                                              <figcaption><h2 class="corbranca">
                                              <?php if($title <> ''){ ?>
                                                <?php echo esc_attr($title);?>
                                              <?php } ?> 
                                              </h2>
											  <?php if($caption <> ''){ ?>
                                              <p><?php echo esc_attr($caption); ?></p>
                                              <?php } ?>
                                                <a class="cp-zoom" href="<?php echo esc_url($image_full[0]);?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a> 
                                              </figcaption>
                                                
												<?php
											}else if( $link_type == 'Video' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'full');
												$image_thumb = wp_get_attachment_image_src($thumbnail_id, $item_size);
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo get_video($video,700,700);
											}else{
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="'.esc_url($image_thumb[0]).'" alt="'.$alt_text.'" />';
											}
										?>
                                        </figure>
										</li>
                                        <!--LIST ITEM START-->	
									
							<?php		
								}else{ 
									if($gallery_size == '2 Column'){ ?>		
										
										<!--LIST ITEM START-->
										<li class="<?php if($gallery_size != '1 Column'){ echo esc_attr($gallery_div_size_listing_class[$gallery_size]['class']); } ?>">
                                        
                                        	<figure class="cp-gallery-thumb">
											<?php 
											$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full');
											$image_thumb = aq_resize( $image_full[0], 560, 320, true );
											$link = cp_find_xml_value( $children->item($i), 'link');
											if( $link_type == 'Link to URL' ){
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="' . esc_url($image_thumb) . '" alt="' . esc_attr($alt_text) . '" />'; ?>
                                                	<a href="<?php echo esc_url($link); ?>" data-rel="prettyPhoto[gallery1]" class="hicon"><i class="fa fa-search"></i></a> 
											<?php }else if( $link_type == 'Lightbox' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full');
												$image_thumb = aq_resize( $image_full[0], 560, 320, true );
												$link = cp_find_xml_value( $children->item($i), 'link'); ?>
                                                <img src="<?php echo esc_url($image_thumb);?>" alt="<?php echo esc_attr($alt_text);?>" />
                                              <figcaption><h2>
                                              <?php if($title <> ''){ ?>
                                                <?php echo esc_attr($title);?>
                                              <?php } ?> 
                                              </h2> 
                                                <a class="cp-zoom" href="<?php echo esc_url($image_full[0]);?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a> 
                                              </figcaption>
                                                
												<?php
											}else if( $link_type == 'Video' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'full');
												$image_thumb = wp_get_attachment_image_src($thumbnail_id, $item_size);
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo get_video($video,700,700);
											}else{
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="'.esc_url($image_thumb).'" alt="'.$alt_text.'" />';
											}
										?>
                                        </figure>
                                        <div class="cp-gallery-content">
                                        <?php if($title <> ''){ ?>
                                          <h3><?php echo esc_attr($title);?></h3>
                                        <?php } ?>
                                          <ul class="cp-post-meta">
                                            <li><a><?php echo human_time_diff( get_post_time(), current_time('timestamp') ); ?>
                                            <?php esc_html_e(' ago','neomag'); ?></a></li>
                                          </ul>
                                          <?php if($caption <> ''){ ?>
                                          <p><?php echo esc_attr($caption); ?></p>
                                          <?php } ?>
                                        </div>
										</li>
                                        <!--LIST ITEM START-->	
									<?php 
									 }  $counter_gal_element++;
								}	
									if($gallery_size == '3 Column'){ ?>		

										<!--LIST ITEM START-->
										<li class="<?php if($gallery_size != '1 Column'){ echo esc_attr($gallery_div_size_listing_class[$gallery_size]['class']); } ?>">
                                        
                                        	<figure class="cp-gallery-thumb">
											<?php 
											$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full');
											$image_thumb = aq_resize( $image_full[0], 265, 230, true );
											$link = cp_find_xml_value( $children->item($i), 'link');
											if( $link_type == 'Link to URL' ){
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="' . esc_url($image_thumb) . '" alt="' . esc_attr($alt_text) . '" />'; ?>
                                                	<a href="<?php echo esc_url($link); ?>" data-rel="prettyPhoto[gallery1]" class="hicon"><i class="fa fa-search"></i></a> 
											<?php }else if( $link_type == 'Lightbox' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full');
												$image_thumb = aq_resize( $image_full[0], 265, 230, true );
												$link = cp_find_xml_value( $children->item($i), 'link'); ?>
                                                <img src="<?php echo esc_url($image_thumb);?>" alt="<?php echo esc_attr($alt_text);?>" />
                                              <figcaption><h2>
                                              <?php if($title <> ''){ ?>
                                                <?php echo esc_attr($title);?>
                                              <?php } ?>
                                              </h2>  
                                                <a class="cp-zoom" href="<?php echo esc_url($image_full[0]);?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a> 
                                              </figcaption>
                                                
												<?php
											}else if( $link_type == 'Video' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'full');
												$image_thumb = wp_get_attachment_image_src($thumbnail_id, $item_size);
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo get_video($video,700,700);
											}else{
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="'.esc_url($image_thumb[0]).'" alt="'.$alt_text.'" />';
											}
										?>
                                        </figure>
										</li>
                                        <!--LIST ITEM START-->	
									<?php 
									 }  $counter_gal_element++;
									if($gallery_size == 'Masonary'){ 
										wp_register_script('isotope-js', NEOMAG_PATH_URL.'/frontend/js/isotope.pkgd.min.js', false, '1.0', true);
										wp_enqueue_script('isotope-js');
										$image_full = wp_get_attachment_image_src($thumbnail_id, 'Full'); 
									?>		
										<!--LIST ITEM START-->
										<div class="<?php 
										if($masonary_counter==0||$masonary_counter==2||$masonary_counter==5){ 
										$image_thumb = aq_resize( $image_full[0], 380, 514, true );
										echo 'item height2'; 
										} else { 
										$image_thumb = aq_resize( $image_full[0], 380, 257, true );
										echo 'item'; }?>
                                        ">
                                        	<figure class="cp-hover-eff">
											<?php 
											$link = cp_find_xml_value( $children->item($i), 'link');
											if( $link_type == 'Link to URL' ){
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="' . esc_url($image_thumb) . '" alt="' . esc_attr($alt_text) . '" />'; ?>
                                                	<a href="<?php echo esc_url($link); ?>" data-rel="prettyPhoto[gallery1]" class="hicon"><i class="fa fa-search"></i></a> 
											<?php }else if( $link_type == 'Lightbox' ){
												$link = cp_find_xml_value( $children->item($i), 'link'); ?>
                                                <img src="<?php echo esc_url($image_thumb);?>" alt="<?php echo esc_attr($alt_text);?>" />
                                              <figcaption>
                                              <?php if($title <> ''){ ?>
                                                <h3><?php echo esc_attr($title);?></h3>
                                              <?php } ?>  
                                                <a class="cp-zoom" href="<?php echo esc_url($image_full[0]);?>" data-rel="prettyPhoto"><i class="fa fa-search"></i> 
                                                <?php esc_html_e('View Large','neomag'); ?></a> 
                                              </figcaption>
                                                
												<?php
											}else if( $link_type == 'Video' ){
												$image_full = wp_get_attachment_image_src($thumbnail_id, 'full');
												$image_thumb = wp_get_attachment_image_src($thumbnail_id, $item_size);
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo get_video($video,700,700);
											}else{
												$link = cp_find_xml_value( $children->item($i), 'link');
												echo '<img src="'.esc_url($image_thumb[0]).'" alt="'.$alt_text.'" />';
											}
										?>
                                        </figure>
										</div>
                                        <!--LIST ITEM START-->	
									<?php 
									 }  $counter_gal_element++;$masonary_counter++;

							} // End Foreach Loop
							if($gallery_size == 'Masonary'){ echo '</div></div></div></div>'; } else { echo '</ul>'; }
						if($show_pagination == 'Yes'){
							pagination_crunch($pages = $total_page);
						}						
					}
				}
			} //Masonry Condition Ends
		} //Gallery page if not empty ends	
	} // Gallery element function ends
	
	
	
	function cp_heading_style($item_xml,$echo=''){
	
		$heading = cp_find_xml_value($item_xml, 'heading');
		$sub_heading = cp_find_xml_value($item_xml, 'sub_heading');	
		$caption = cp_find_xml_value($item_xml, 'caption');	
		$upload_image = cp_find_xml_value($item_xml, 'upload_image');	
		if(isset($upload_image)){
			$bg_image = wp_get_attachment_image_src($upload_image, 'full');
		}
		
		
		if($echo == 'false'){
			return $html_heading = '
			<div class="thumb-style">
				<img src="'.esc_url($bg_image[0]).'" alt="">
				<div class="caption">
					<h2>'.esc_attr($heading).'</h2>
					<strong>'.esc_attr($sub_heading).'</strong>
					<p>'.esc_attr($caption).'</p>
				</div>
			</div>';
		}else{
			return $html_heading = '
			<div class="thumb-style">
				<img src="'.esc_url($bg_image[0]).'" alt="">
				<div class="caption">
					<h2>'.esc_attr($heading).'</h2>
					<strong>'.esc_attr($sub_heading).'</strong>
					<p>'.esc_attr($caption).'</p>
				</div>
			</div>';
		}	
	
	}
	
	function get_gallery_image_one($post_id, $item_size){
		$thumbnail_id = get_post_thumbnail_id( $post_id );
		$thumbnail = wp_get_attachment_image_src( $thumbnail_id , $item_size );
		
		if($thumbnail[1].'x'.$thumbnail[2] == $item_size){
			echo get_the_post_thumbnail($post_id, $item_size);
		}else{
			echo get_the_post_thumbnail($post_id, 'full');
		}
	}
	
	
	
	//Newest Album Section
	function cp_print_newest_album_item($item_xml){
		
		global $counter,$post,$post_id;
		
		$header = cp_find_xml_value($item_xml, 'header');
		$category_album = cp_find_xml_value($item_xml, 'category');		
		
		//Bx Slider Script Calling
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');?>
		<script type="text/javascript">
		jQuery(document).ready(function ($) {
			"use strict";
			if ($('#newest-<?php echo esc_js($counter)?>').length) {
				$('#newest-<?php echo esc_js($counter)?>').bxSlider({
					minSlides: 1,
					maxSlides: 1,
					auto:true
				});
			}
		});
		</script>
		<?php if($header <> ''){ ?><h2 class="h-style"><?php echo esc_attr($header);?></h2><?php }?>
		<div class="accordian-list">
			<Section id="newest-<?php echo esc_attr($counter);?>">
				<?php
				query_posts(array(
				'posts_per_page' 			=> 5,
				'post_type'					=> 'albums',
				'album-category'			=> $category_album,
				'post_status'				=> 'publish',
				'order'						=> 'DESC',
				));
				$counter_one = 0;
				if(have_posts()){
				while( have_posts() ){
					the_post();	?>
					<div class="slide">
						<a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_post_thumbnail($post_id, array(350,350));?></a>
						<div class="img-cap">
							<h3><?php echo esc_html(get_the_title());?></h3>
						</div>
					</div>
				<?php }//End While loop
				wp_reset_postdata();
				} wp_reset_query(); 
				 ?>
			</Section>
		</div>
		<?php
	}
	
	//Newest Album Section
	function cp_print_footer_shop_item($category_product="",$bg_img=""){
		global $counter,$post,$post_id;
		
		$select_layout_cp = '';
		$cp_general_settings = get_option('general_settings');
		if($cp_general_settings <> ''){
			$cp_logo = new DOMDocument ();
			$cp_logo->loadXML ( $cp_general_settings );
			$select_layout_cp = cp_find_xml_value($cp_logo->documentElement,'select_layout_cp');
		}
		
		//Condition to Fetch All Categories
		if(!empty($category_product)){
			$category_term = get_term_by( 'term_id', $category_product , 'product_cat');
			$category_product = $category_term->slug;
		}			
		//Bx Slider Script Calling
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
		?>
				<script type="text/javascript">
				jQuery(document).ready(function ($) {
					"use strict";
					if ($('#product-<?php echo esc_js($counter)?>').length) {
						$('#product-<?php echo esc_js($counter)?>').bxSlider({
							slideWidth: <?php if($select_layout_cp == 'boxed_layout'){echo esc_js('278');}else{echo esc_js('278');}?>,
							minSlides: 1,
							maxSlides: <?php if($category_product == '' || $category_product == ' '){echo esc_js('4');}else{echo esc_js('3');}?>,
							slideMargin: 20,
							pager:false, 
							auto:true
						});
					}
				});
				</script>
				<div <?php if($bg_img[0] <> ''){ echo 'style="background:url('.$bg_img[0].') no-repeat;"';}?> data-type="background" data-speed="12"  class="new-album-list">
					<div class="<?php if($select_layout_cp == 'boxed_layout'){echo esc_attr('container-boxed container-fluid');}else{echo esc_attr('container');}?>">
						<div class="row-fluid">
							<?php if($category_product <> ''){ ?>
								<div class="span3 new-album-cap fadeInLeft cp_load">
									<h2><?php echo esc_attr($category_term->name);?></h2>
									<p><?php echo esc_attr(substr(($category_term->description),0,200));?></p>
								</div>
							<?php }else{ ?>
								<div class="span12">
									<h2><?php esc_html_e('All Categories','neomag');?></h2>
								</div>	
							<?php } ?>
							<div class="<?php if($category_product == '' || $category_product == ' '){echo 'span12 first';}else{echo 'span9';}?> album-slider fadeInRight cp_load">
								<div id="product-<?php echo esc_attr($counter)?>" class="slider7">
								<?php
									//Reset Query for Post
									//Default Query For Grid
									query_posts(array(
										'posts_per_page'			=> 10,
										'post_type'					=> 'product',
										'product_cat'				=> $category_product,
										'post_status'				=> 'publish',
										'order'						=> 'DESC',
									));
									$counter_product = 0;
									if(have_posts()){
										while(have_posts()){
										the_post();	
										global $post,$post_id,$product,$product_url,$woocommerce;
										$permalink_structure = get_option('permalink_structure');
										if($permalink_structure <> ''){
											$permalink_structure = '?';
										}else{
											$permalink_structure = '&';
										}
										$regular_price = get_post_meta($post->ID, '_regular_price', true);
										if($regular_price == ''){
											$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
										}
										$sale_price = get_post_meta($post->ID, '_sale_price', true);
										$sku_num = get_post_meta($post->ID, '_sku', true);
										
										if($sale_price == ''){
											$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
										}
										$currency = get_woocommerce_currency_symbol(); ?>
										<!-- Start New Album Slider -->							
										<div class="slide"> 
											<a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_post_thumbnail($post_id, array(270,290));?></a>
											<div class="mask-overlay">
												<a href="<?php echo esc_url(get_permalink());?>" class="anchor"><span> </span> <i class="fa fa-shopping-cart"></i></a>
												<a href="<?php echo esc_url(get_permalink());?>" class="anchor"> <i class="fa fa-link"></i></a>
											</div>
											<div class="item-cap"> <strong class="item-title"><a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(get_the_title());?></a></strong> <span class="price-rating"> <span class="price"><?php echo esc_attr($currency);?><?php if($sale_price <> ''){echo esc_attr($sale_price);}else{echo esc_attr($regular_price);}?></span></span> 
												<?php
												echo apply_filters( 'woocommerce_loop_add_to_cart_link',
													sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s">%s</a>',
														esc_url( $product->add_to_cart_url() ),
														esc_attr( $product->id ),
														esc_attr( $product->get_sku() ),
														$product->is_purchasable() ? 'add_to_cart_button' : '',
														esc_attr( $product->product_type ),
														esc_html( $product->add_to_cart_text() )
													),
												$product );
												?>
											</div>
										</div>
										<!-- End New Album Slider --> 					
									<?php
										} // End while 
										wp_reset_postdata();
									} wp_reset_query();  ?>
								</div>
							</div>
						</div>
					</div>
				</div>
		<?php	
	}
	
	//Blog Item
	function cp_print_blog_item_item($item_xml){
		global $counter;
		$num_excerpt = 250;
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt');
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css'); 
		?>
		<script type="text/javascript">
		jQuery(document).ready(function ($) {
			if ($('#blog_slider-<?php echo esc_js($counter);?>').length) {
				$('#blog_slider-<?php echo esc_js($counter);?>').bxSlider({
					minSlides: 1,
					maxSlides: 1
				});
			}
		});
		</script>
		<div class="blog_class" id="blog_store">
			<figure id="blog" class="span12 first">
				<?php if($header <> ''){?><h2 class="title"><?php echo esc_attr($header);?><span class="h-line"></span></h2><?php }?>
				<div id="slider_blog">
					<ul id="blog_slider-<?php echo esc_attr($counter);?>">
					<?php
					query_posts(array(
						'posts_per_page'			=> $num_fetch,
						'post_type'					=> 'post',
						'category'					=> $category,
						'post_status'				=> 'publish',
						'order'						=> 'DESC',
					));
					$event_counter = 0;
					if(have_posts()){
					while( have_posts() ){
					the_post();	
					global $post,$post_id;
					?>
						<li>
							<div class="img span4">
								<?php echo get_the_post_thumbnail($post_id, array(175,155));;?>
							</div>
							<div class="content span8">
								<div class="icon_date"> 
									<i class="fa fa-picture"></i>
									<span class="date"><?php echo esc_attr(get_the_date(get_option('date_format')))?></span>
								</div>
								<div class="post_excerpt">
									<h4><a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(get_the_title());?></a></h4>
									<p><?php 
									if($num_excerpt <> ''){
										echo esc_attr(strip_tags(substr(get_the_content(),0,$num_excerpt)));
									}else{
										echo esc_attr(strip_tags(substr(get_the_content(),0,250)));
									}
									
									?></p>
									<a class="readmore" href="<?php echo esc_url(get_permalink());?>"><?php esc_html_e('Read More','neomag');?><i class="icon-plus"></i> </a>
								</div>
							</div>
						</li>
					<?php } wp_reset_postdata();
					} wp_reset_query();
					?>	
					</ul>
				</div>
			</figure>
		</div>	
	<?php
	}
	
	//WooProduct Slider
	function cp_print_woo_product_slider_item($item_xml){ 
		wp_register_script('cp-caroufredsel-slider', NEOMAG_PATH_URL.'/frontend/js/caroufredsel.js', false, '1.0', true);
		wp_enqueue_script('cp-caroufredsel-slider');	
		
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		
	
		global $post;
		$facebook_class = '';
		if($post <> ''){
			$facebook_class = get_post_meta ( $post->ID, "page-option-item-facebook-selection", true );
		}
	?>
			
		<script type="text/javascript">
			jQuery(document).ready(function ($) {
				'use strict';
				<?php if($facebook_class == 'Yes'){?>
				var _visible = 4;
				<?php }else{?>
				var _visible = 6;
				<?php }?>
				var $pagers = $('#pager a');
				var _onBefore = function() {
					$(this).find('img').stop().fadeTo( 300, 1 );
					$pagers.removeClass( 'selected' );
				};

				$('#carousel').carouFredSel({
					items: _visible,
					width: '100%',
					auto: false,
					scroll: {
						duration: 750
					},
					prev: {
						button: '#prev',
						items: 2,
						onBefore: _onBefore
					},
					next: {
						button: '#next',
						items: 2,
						onBefore: _onBefore
					},
				});

				$pagers.click(function( e ) {
					e.preventDefault();
					
					var group = $(this).attr( 'href' ).slice( 1 );
					var slides = $('#carousel div.' + group);
					var deviation = Math.floor( ( _visible - slides.length ) / 2 );
					if ( deviation < 0 ) {
						deviation = 0;
					}

					$('#carousel').trigger( 'slideTo', [ $('#' + group), -deviation ] );
					$('#carousel div img').stop().fadeTo( 300, 1 );
					slides.find('img').stop().fadeTo( 300, 1 );

					$(this).addClass( 'selected' );
				});
			});
		</script>
			<div id="inner">
				<div id="carousel">
				<?php
					query_posts(array(
						'posts_per_page'			=> $num_fetch,
						'post_type'					=> 'product',
						'category'					=> $category,
						'post_status'				=> 'publish',
						'order'						=> 'DESC',
					));
					if(have_posts()){
						while( have_posts() ){
						the_post();	
						global $post,$post_id;
						$categories = '';
						$currency = '';
						//Price of Product
						$regular_price = get_post_meta($post->ID, '_regular_price', true);
						if($regular_price == ''){
							$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
						}
						$sale_price = get_post_meta($post->ID, '_sale_price', true);
						if($sale_price == ''){
							$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
						}
						if(function_exists('get_woocommerce_currency_symbol')){
							$currency = get_woocommerce_currency_symbol();
						}
						?>
						<div class="cp_product" id="<?php 
						if(class_exists("Woocommerce")){
							$categories = get_the_terms( $post->ID, 'product_cat' );
								if($categories <> ''){
									foreach ( $categories as $category ) {
										echo esc_attr($category->term_id);
									}
								}
						}	
						?>">
							<?php echo get_the_post_thumbnail($post_id, array(140,200));;?>
							<em><?php echo esc_attr(get_the_title());?></em>
							<span class="cp_price"><sup><?php echo esc_attr($currency);?></sup><?php echo esc_attr($regular_price);?></span>
							<a class="view_detail" href="<?php echo esc_url(get_permalink());?>"><?php esc_html_e('View Detail','neomag');?></a>
						</div>
					<?php } 
						wp_reset_postdata();
					}wp_reset_query(); 
				?>
				</div>
				<div id="pager">
				<?php
				$category = cp_find_xml_value($item_xml, 'category');
				$category = ( $category == '786512' )? '': $category;
				if( !empty($category) ){
					$category_term = get_term_by( 'name', $category , 'product_cat');
					$category = $category_term->slug;
				}
				if(class_exists("Woocommerce")){
					$categories = get_categories( array('child_of' => $category, 'taxonomy' => 'product_cat', 'hide_empty' => 0) );
					if($categories <> ""){
						foreach($categories as $values){?>
						<a href="#<?php echo esc_attr($values->term_id);?>"><?php echo esc_attr($values->name);?></a>
					<?php
						}
					}
				}
				?>
				</div>
				<a href="#" id="prev"><span class="font_aw"><i class="fa fa-chevron-left"></i></span></a>
				<a href="#" id="next"><span class="font_aw"><i class="fa fa-chevron-right"></i></span></a>
			</div>
	<?php }	
	
	// Print the slider item
	function cp_print_slider_item($item_xml){
		
		global $counter;
		$xml_size = cp_find_xml_value($item_xml, 'size');
		if( $xml_size == 'full-width' ){
			echo '<div class="Full-Image"><div class="thumbnail_image">';
		}else{
			echo '<div class="Full-Image"><div class="thumbnail_image">';
		}
		$slider_xml_dom  = new DOMDocument ();
		$neomag_slider_type= cp_find_xml_value($item_xml,'slider-type');
		$slider_width = cp_find_xml_value($item_xml, 'width');
		$neomag_slider_height = cp_find_xml_value($item_xml, 'height');
		$neomag_slider_slide = cp_find_xml_value($item_xml, 'slider-slide');
		$neomag_slider_slide_layer = cp_find_xml_value($item_xml, 'slider-slide-layer');
		if(!empty($neomag_slider_slide)){
		$slider_xml = get_post_meta( intval($neomag_slider_slide), 'cp-slider-xml', true);
			if($slider_xml <> ''){
				$slider_xml_dom = new DOMDocument ();
				$slider_xml_dom->loadXML ( $slider_xml );
			}
		}
		//Determine the width of slider
		if( !empty($slider_width) && !empty($neomag_slider_height) ){
			$xml_size = array($slider_width, $neomag_slider_height);
		} else if(!empty($neomag_slider_height)){
			$xml_size = array(980, $neomag_slider_height);
		}else{
			$xml_size = array(980,360);
		}
		//Slider Name
		$slider_name = 'slider'.$counter;
		switch(cp_find_xml_value($item_xml,'slider-type')){
			
			case 'Anything': 
				wp_register_script('cp-anything-slider', NEOMAG_PATH_URL.'/frontend/anythingslider/js/jquery.anythingslider.js', false, '1.0', true);
				wp_enqueue_script('cp-anything-slider');	

				wp_register_script('cp-anything-slider-fx', NEOMAG_PATH_URL.'/frontend/anythingslider/js/jquery.anythingslider.fx.js', false, '1.0', true);
				wp_enqueue_script('cp-anything-slider-fx');	
				echo cp_print_anything_slider($slider_name,$slider_xml_dom->documentElement,$size=$xml_size);
				break;
			case 'Flex-Slider': 
				cp_print_flex_slider($slider_xml_dom->documentElement,$size=$xml_size);
				break;
			case 'Default-Slider': 
				cp_print_fine_slider($slider_xml_dom->documentElement,$size=$xml_size);
				break;
			case 'Bx-Slider': 
				echo cp_print_bx_slider($slider_xml_dom->documentElement,$size=$xml_size,$slider_name);				
				break;
			case 'Layer-Slider': 
				if(class_exists('LS_Sliders')){
					echo do_shortcode('[layerslider id="' . esc_attr($neomag_slider_slide_layer) . '"]');
				}else{
					echo '<h2>Please install the LayerSlider plugin first.</h2>';
				}	
			break;	
				
		}
		?>
		
		<?php
		if( cp_find_xml_value($item_xml, 'size') == 'full-width' ){
			echo "</div></div>";
		}else{
		      echo "</div></div>";
		}
	}
	
	// Print Content Item
	function cp_print_content_item($item_xml){
		
		$title = cp_find_xml_value($item_xml, 'title');
		$description = cp_find_xml_value($item_xml, 'description');
		
		//Loop for Content Area
		if(have_posts()){
			while(have_posts()){
				the_post();
				global $post;
				if($title == 'Yes'){
					echo '<h2 class="h-style">' . esc_attr(get_the_title()) . '</h2>';
				}
				
				if($description == 'Yes'){
					the_content();	
				}
			}
		}
	}
	
	// Print Content Item
	function cp_print_default_content_item(){ 
		
		global $post;	
		$thumbnail_id = get_post_thumbnail_id( $post->ID );
		$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(1170,350));
		$image_thumb = wp_get_attachment_image_src($thumbnail_id, 'full');

	?>

        <div class="cp-posts-style-1" id="cp-posts-style-default">
            <ul class="cp-posts-list">
		
		<?php while ( have_posts() ) : the_post(); ?>
                      <li class="cp-post <?php if($image_thumb[0] == ''){ echo 'with-no-thumb'; } ?>">
                        <div class="cp-thumb">
                        <?php if($image_thumb[0] <> ''){ ?>
                        <img src="<?php echo esc_url($image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                          <div class="cp-post-hover"> <a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-link"></i></a> 
                          <a href="<?php echo esc_url($image_thumb[0]); ?>"><i class="fa fa-search"></i></a> </div>
                        <?php } ?>
	                     </div>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <a href="<?php echo esc_url(get_permalink());?>"><?php the_title( '<h2>', '</h2>' ); ?></a>
				<?php
					the_content();
					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:','neomag' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
					) );

					edit_post_link( esc_html__( 'Edit','neomag' ), '<span class="edit-link">', '</span>' );
				?>
                            </div>
                        </div>
		<?php
		echo '<div class="cp-post-comments-form">';
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		echo '</div></li>';
		endwhile; ?>
            </ul>
          </div>

<?php	}

	// Print Cart Item
	function cp_print_cart_content_item(){
		
		while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content-cp">
				<?php
					the_content();
					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:','neomag' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
					) );

					edit_post_link( esc_html__( 'Edit','neomag' ), '<span class="edit-link">', '</span>' );
				?>
			</div><!-- .entry-content -->
		</div><!-- #post-## -->
		
		<?php
		echo '<div class="cp-post-comments-form">';
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		echo '</div>';
		endwhile;
	}
	
	// Print Accordion
	function cp_print_accordion_item($item_xml){
		global $counter;
		$tab_xml = find_xml_node($item_xml, 'tab-item');
		$header = cp_find_xml_value($item_xml, 'header');
		echo '<script type="text/javascript">
			   jQuery(document).ready(function($) {
				//custom animation for open/close
				$.fn.slideFadeToggle = function(speed, easing, callback) {
					return this.animate({opacity: "toggle", height: "toggle"}, speed, easing, callback);
				};
			});
		</script>';
		
		if(!empty($header)){
			echo '<div class="accor-testimonials">
            <div class="">
              <h2>' . esc_attr($header) . '</h2>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
		}
		
		$counter_accordion = '';
		$counter_accor = 0;
		foreach($tab_xml->childNodes as $accordion){
		if($counter_accor == 0){$counter_accordion = 'open_first_1';}else{$counter_accordion = 'accordion-'.$counter_accor;};
		echo '<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading'.esc_attr($counter_accordion).'">
                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.esc_attr($counter_accordion).'" aria-expanded="true" aria-controls="collapse'.esc_attr($counter_accordion).'">'.cp_find_xml_value($accordion, 'title').'</a> </h4>
                  </div>
                  <div id="collapse'.esc_attr($counter_accordion).'" class="panel-collapse collapse ';if($counter_accor == 0){ echo 'in';}
				 echo '" role="tabpanel" aria-labelledby="heading'.esc_attr($counter_accordion).'">
                    <div class="panel-body">'.do_shortcode(html_entity_decode(cp_find_xml_value($accordion, 'caption'))).'</div>
                  </div>
                </div>';
				$counter_accor++;
		}
		echo '</div></div></div>';
	}
	
	// Print Divider
	function cp_print_divider($item_xml){
		//Hide me button
		$hide_button = cp_find_xml_value($item_xml, 'hide-bottom-top');
		$margin_top = cp_find_xml_value($item_xml, 'margin-top');
		$margin_bottom = cp_find_xml_value($item_xml, 'margin-bottom');
		
		
		if($hide_button == 'Yes'){
			wp_register_script('cp-easing', NEOMAG_PATH_URL.'/frontend/js/jquery-easing-1.3.js', false, '1.0', true);
			wp_enqueue_script('cp-easing');
			wp_register_script('cp-top-script', NEOMAG_PATH_URL.'/frontend/js/jquery.scrollTo-min.js', false, '1.0', true);
			wp_enqueue_script('cp-top-script');
			echo '<div style="margin-top:'.$margin_top.';margin-bottom:'.$margin_bottom.'" class="clear"></div><div class="divider mr10"><div class="scroll-top"><a class="scroll-topp">'.esc_html__('Back to Top','neomag').'</a>';
			echo cp_find_xml_value($item_xml, 'text');
			echo '</div></div>';
		}else{
			echo '<div style="float:left;width:100%;margin-top:'.$margin_top.';margin-bottom:'.$margin_bottom.'" class="clear"></div>';
		}
	}
	
	// Print Message Box
	function cp_print_message_box($item_xml){
		$box_color = cp_find_xml_value($item_xml, 'color');
		$box_title = cp_find_xml_value($item_xml, 'title');
		$box_content = html_entity_decode(cp_find_xml_value($item_xml, 'content'));
		echo '<div class="message-box-wrapper ' . esc_attr($box_color) . '">';
		echo '<div class="message-box-title">' . esc_attr($box_title) . '</div>';
		echo '<div class="message-box-content">' . esc_attr($box_content) . '</div>';
		echo '</div>';
	}
	
	// Print Toggle Box
	function cp_print_toggle_box_item($item_xml){
		$tab_xml = find_xml_node($item_xml, 'tab-item');
		$header = cp_find_xml_value($item_xml, 'header');
		if(!empty($header)){
			echo '<h3 class="toggle-box-header-title title-color cp-title">' . esc_attr($header) . '</h3>';
		}
		echo "<ul class='toggle-view'>";
		foreach($tab_xml->childNodes as $toggle_box){
			$active = cp_find_xml_value($toggle_box, 'active');
			echo "<li>";
			
			echo "<span class='link";
			echo ($active == 'Yes')? ' active':'';
			echo "' ></span>";
			echo "<h3 class='color'>". cp_find_xml_value($toggle_box, 'title') . "</h3>";
			echo "<div class='panel"; 
			echo ($active == 'Yes')? ' active': '';
			echo "' id='toggle-box-content' >";
			echo do_shortcode(html_entity_decode(cp_find_xml_value($toggle_box, 'caption'))) . '</div>';
			echo "</li>";
		
		}
		echo "</ul>";
	}

	// Print Tab
	function cp_print_tab_item($item_xml){
	
		$tab_xml = find_xml_node($item_xml, 'tab-item');
		
		$tab_widget_title =  html_entity_decode(cp_find_xml_value($item_xml,'tab-widget'));
		$tab_style =  html_entity_decode(cp_find_xml_value($item_xml,'tab-layout-select'));
		$num = 0;
		$tab_title = array();
		$tab_content = array();
		$tab_title[$num] = cp_find_xml_value($item_xml, 'title');
		if( !empty($tab_widget_title) ){
			if(is_front_page()){
				echo '<h2>'.esc-attr($tab_widget_title).'</h2>';
			}else{
				echo '<h3 class="heading1 bg-div"><span class="inner"><strong>';
				echo  esc_attr($tab_widget_title);
				echo '</strong><span class="bgr1"></span></span></h3>';
			}
		}
		if($tab_style == 'Horizontal'){
			echo '<div id="horizontal-tabs" class="tabs tab-area">';
		}else{
			echo '<div id="vertical-tabs" class="tabs tab-area">';
		}
		foreach($tab_xml->childNodes as $toggle_box){
			$tab_title[$num] = cp_find_xml_value($toggle_box, 'title');
			$tab_content[$num] = html_entity_decode(cp_find_xml_value($toggle_box, 'caption'));
			$num++;
		}
		echo "<ul class='nav nav-tabs'>";
           for($i=0; $i<$num; $i++){
				echo '<li><a href="#' . str_replace(' ', '-', $tab_title[$i]) .$i. '" class=" ';
				echo ( $i == 0 )? 'active':'';
				echo '" >' . esc_attr($tab_title[$i]) . '</a></li>';
			}
           
         echo "</ul>";
			
			echo "<ul class='tab-content'>";
			for($i=0; $i<$num; $i++){
				echo '<li id="' . str_replace(' ', '-', $tab_title[$i]) .$i. '" class="tabscontent ';
				echo ( $i == 0 )? 'active':'';  
				echo '" >' . do_shortcode($tab_content[$i]) . '</li>';
			}
			echo "</ul>";	
			echo "</div>";	
	}
	
	// Print column service
	function cp_print_column_service($item_xml){		
		$feature_html = '';
		$html_readmore = '';
		$title = cp_find_xml_value($item_xml, 'title');
		$fontaw = cp_find_xml_value($item_xml, 'FontAwesome');
		$layout = cp_find_xml_value($item_xml, 'layout');
		
		$descrip = html_entity_decode(cp_find_xml_value($item_xml, 'text'));
		$service_layout = cp_find_xml_value($item_xml, 'service-layout');
		$morelink = cp_find_xml_value($item_xml, 'morelink');
		if($morelink <> ''){$html_readmore = '<a class="readmore" href="'.esc_url($morelink).'">'.esc_html__('Readmore','neomag').'</a>';}
		

		$feature_html = '
			<div class="service-grid">
				<ul>
					<li>
						<span>
							<a href="'.esc_url($morelink).'" class="inner"><i class="'.esc_attr($fontaw).'"></i></a>
						</span>
					</li>
					<li>
						<h3>'.esc_attr($title).'</h3>
						<p>'.do_shortcode($descrip).'</p>
					</li>
					<li>
						<a class="readmore" href="'.esc_url($morelink).'">'.esc_html__('Read More','neomag').'</a>
					</li>
				</ul>
            </div>';
		
		
		return $feature_html;
	}

	// Print contact form
	function cp_print_contact_form($item_xml){
		global $counter;
		foreach ($_REQUEST as $keys=>$values) {
			$$keys = $values;
		} ?>
		<script type="text/javascript">			
			jQuery(document).ready(function($) {
				'use strict';
				var $form = $(this);
				$('#form_contact').validate({
					submitHandler: function(form) {							
						jQuery.post(ajaxurl,
							$('form#form_contact').serialize() , 
							function(data){
								jQuery("#loading_div").html('');
								jQuery(".frm_area").hide();
								jQuery("#succ_mess").show('');
								jQuery("#succ_mess").html(data);
							}
						);
						return false;
					}
				});				
			});
		</script>
		<?php
		$header = cp_find_xml_value($item_xml, 'header');
		$text = cp_find_xml_value($item_xml, 'text');
		if(!empty($header)){
			echo '<div id="contact-us-form" class="cp-contact-form"><h3>' . esc_html($header) . '</h3>';
		}
		?>
			<form id="form_contact" method="POST">
            <?php if($text <> ''){ ?>
              <p> <?php echo esc_html($text); ?> </p>
              <?php } ?>
				<div id="succ_mess"></div>
              <ul>
              <li>
              	<ul class="cp-name">
                <li>
                  <input type="text" id="name_contact" name="name_contact" placeholder="<?php esc_html_e('Nome*','neomag');?>" class="required require-field form-control">
                </li>
                <li>
                  <input type="text" id="email_contact" name="email_contact" placeholder="<?php esc_html_e('E-mail*','neomag');?>" class="required email require-field form-control">
                </li>
                </ul>
                </li>
                <li>
                  <input type="text" id="website" name="website" placeholder="<?php esc_html_e('Assunto*','neomag');?>" class="required require-field form-control">
                </li>
                <li>
                  <textarea id="message_comment" name="message_comment" placeholder="<?php esc_html_e('Mensagem*','neomag');?>" class="required require-field form-control"></textarea>
                </li>
                <li class="cp-login-buttons">
                  <button type="submit" style="font-weight:bold;"><?php esc_html_e('ENVIAR','neomag');?></button>
                </li>
              </ul>
				<div id="loading_div" class=""></div>
				<div class="hide"><input type="hidden" id="receiver" name="receiver" value="<?php echo cp_find_xml_value($item_xml, 'email'); ?>" /></div>
				<div class="hide"><input type="hidden" name="successful_msg_contact" value="Your message has been submitted." /></div>
				<div class="hide"><input type="hidden" name="un_successful_msg_contact" value="Please Provide Correct Information!" /></div>
				<div class="hide"><input type="hidden"  name="form_submitted" value="form_submitted" /></div>
				<div class="hide"><input type="hidden"  name="action" value="cp_contact_submit" /></div>
			</form>
       </div>   
	<?php
	}
	
	//News Slider
	function cp_print_news_slider_box($item_xml){
		global $counter;
		$header = cp_find_xml_value($item_xml, 'header');
		$category = html_entity_decode(cp_find_xml_value($item_xml, 'category'));
		$num_fetch = html_entity_decode(cp_find_xml_value($item_xml, 'num-fetch'));
		if($category <> ''){
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
		?>
		<script type="text/javascript">
        jQuery(document).ready(function($) {
			'use strict';
			$('#news_slider-<?php echo esc_js($counter);?>').bxSlider({  minSlides: 1, maxSlides: 1, slideMargin: 18,  speed: 500, });
        });
        </script>
         <!-- Content -->
			<div id="news" class="blog_class">
			<?php if($header <> ''){?><h2 class="title"><?php echo esc_attr($header);?><span class="h-line"></span></h2><?php }?>
				<ul class="news_slider" id="news_slider-<?php echo esc_attr($counter);?>">
				<?php
			global $post;
				query_posts(array( 
				'post_type' => 'post',
				'showposts' => $num_fetch,
				'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'terms' => $category,
						'field' => 'term_id',
					)
				),
				'orderby' => 'title',
				'order' => 'DESC' )
			);
			$counter_team = 0; 
			if ( have_posts()  ) {
				while( have_posts() ){
					the_post();
					global $post; ?>
					<li> 
						<div class="span5 first" id="img_holder"> 
							<div class="img">
								<?php $size = array(260,220); echo cp_function_library::cp_thumb_size($post->ID,$size);?>
							</div>
							<div class="img_title"> 
							<a> <i class="fa fa-plus"></i> </a>
							<a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(get_the_title());?></a> 
							<p><?php echo esc_attr(strip_tags(substr(get_the_content(),0,10)));?></p>
							</div>
						</div>
						<div class="span7 ns_desc"> 
							<a href="<?php echo esc_url(get_permalink());?>" class="title"><?php echo esc_attr(get_the_title());?>  <span class="h-line"></span> </a> 
							<p><?php echo esc_attr(strip_tags(substr(get_the_content(),0,130)));?></p>
							<a href="<?php echo esc_url(get_permalink())?>" class="rm"><?php esc_html_e('View All News &nbsp;','neomag');?><i class="fa fa-plus"></i></a>
						</div> 
					</li>
			<?php } //End while loop
				wp_reset_postdata();
			} //Check Post Condition Ends
				wp_reset_query(); 
				?>
				</ul>
			</div>
        <?php
		} //if Category Empty
	}
	
	//News Headline Function Starts Here
	function cp_print_news_headline($item_xml){

		global $counter;
		//Fetch All Elements from Element
		$header = cp_find_xml_value($item_xml, 'header');
		$category = html_entity_decode(cp_find_xml_value($item_xml, 'category'));
		$num_fetch = html_entity_decode(cp_find_xml_value($item_xml, 'num-fetch'));
		
		//Condition For Category
		if($category <> ''){
			
			?>
			<!--Runs the Slider Script here -->
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					'use strict';
					$('#news_slider-<?php echo esc_js($counter);?>').bxSlider({  minSlides: 1, maxSlides: 1, slideMargin: 18,  speed: 500, });
				});
			</script>
			 <!-- News Content Start -->
				<div id="news" class="blog_class">
				<?php if($header <> ''){?><h2 class="title"><?php echo esc_attr($header);?><span class="h-line"></span></h2><?php }?>
					<ul class="news_slider" id="news_slider-<?php echo esc_attr($counter);?>">
					<?php
					
					//Arguments for Loop
					global $post;
					query_posts(array( 
						'post_type' => 'post',
						'showposts' => $num_fetch,
						'tax_query' => array(
							array(
								'taxonomy' => 'category',
								'terms' => $category,
								'field' => 'term_id',
							)
						),
						'orderby' => 'title',
						'order' => 'DESC' )
					);
					if ( have_posts() ) {
						while ( have_posts() ): the_post();?>
						<li> 
							<div class="span5 first" id="img_holder"> 
								<div class="img">
								<?php $size = array(260,220); echo cp_function_library::cp_thumb_size($post->ID,$size);?>
								</div>
								<div class="img_title"> 
								<a> <i class="fa fa-plus"></i> </a>
								<a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(get_the_title());?></a> 
								<p><?php echo esc_attr(strip_tags(substr(get_the_content(),0,10)));?></p>
								</div>
							</div>
							<div class="span7 ns_desc"> 
								<a href="<?php echo esc_url(get_permalink());?>" class="title"><?php echo esc_attr(get_the_title());?>  <span class="h-line"></span> </a> 
								<p><?php echo esc_attr(strip_tags(substr(get_the_content(),0,130)));?></p>
								<a href="<?php echo esc_url(get_permalink())?>" class="rm"><?php esc_html_e('View All News &nbsp;','neomag');?><i class="fa fa-plus"></i></a>
							</div> 
						</li>
						<?php endwhile;
						wp_reset_postdata();
					}
					wp_reset_query(); 
					?>
					</ul>
				</div>
			<?php
		} //if Category Empty
	} //News Headline Function Ends Here

	
	// Print text widget
	function cp_print_text_widget($item_xml){
		
		
		$title = cp_find_xml_value($item_xml, 'title');
		$caption = html_entity_decode(cp_find_xml_value($item_xml, 'caption'));
		$button_title =  cp_find_xml_value($item_xml, 'button-title');
		echo '<div class="text-widget-wrapper"><div class="text-widget-content-wrapper ';   
		echo empty($button_title)? 'sixteen columns': 'twelve columns';
		echo ' mt0"><h3 class="text-widget-title">' . $title . '</h3>';
		echo '<div class="text-widget-caption">' . do_shortcode($caption) . '</div>';
		echo '</div>';
		if( !empty($button_title) ){
			$button_margin = (int) cp_find_xml_value($item_xml, 'button-top-margin');
			echo '<div class="text-widget-button-wrapper three columns mt0" >';
			echo '<a class="text-widget-button" style="position:relative; top:' . $button_margin . 'px;" href="' . cp_find_xml_value($item_xml, 'button-link') . '" >';
			echo  esc_attr($button_title) . '</a>';
			echo '</div> '; 
			echo '<br class="clear">';
		}  echo '</div>';
		
	}
	
	global $neomag_cp_is_responsive;
	// size is when no sidebar, side2 is use when 1 sidebar, side 3 is use when 3 sidebar
	if( $neomag_cp_is_responsive ){
		$port_div_size_num_class = array(
			"1/4" => array("class"=>"four columns", "size"=>"390x224", "size2"=>"390x245", "size3"=>"390x247"), 
			"1/3" => array("class"=>"one-third column", "size"=>"390x242", "size2"=>"390x238", "size3"=>"390x247"), 
			"1/2" => array("class"=>"eight columns", "size"=>"450x290", "size2"=>"390x247", "size3"=>"390x247"), 
			"1/1" => array("class"=>"sixteen columns", "size"=>"620x225", "size2"=>"390x182", "size3"=>"390x292"));	
	}else{
		$port_div_size_num_class = array(
			"1/4" => array("class"=>"four columns", "size"=>"210x121", "size2"=>"135x85", "size3"=>"210x135"), 
			"1/3" => array("class"=>"one-third column", "size"=>"290x180", "size2"=>"190x116", "size3"=>"210x135"), 
			"1/2" => array("class"=>"eight columns", "size"=>"450x290", "size2"=>"300x190", "size3"=>"210x135"), 
			"1/1" => array("class"=>"sixteen columns", "size"=>"620x225", "size2"=>"320x150", "size3"=>"180x135"));
	}
	$neomag_class_to_num = array(
		"element1-4" => 0.25,
		"1/4"=>0.25,
		"element1-3" => 0.33,
		"1/3"=>0.33,
		"element1-2" => 0.5,
		"1/2"=>0.5,
		"element2-3" => 0.66,
		"2/3"=>0.66,
		"element3-4" => 0.75,
		"3/4"=>0.75,
		"element1-1" => 1,
		"1/1" => 1	
	);
	

	// Print nested page
	function cp_print_page_item($item_xml){		
		
		global $paged;
		global $sidebar;
		global $port_div_size_num_class;	
		global $neomag_class_to_num;
		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}
	
		// get the item class and size from array
		$port_size = cp_find_xml_value($item_xml, 'item-size');
		
		// get the item class and size from array
		$item_class = $port_div_size_num_class[$port_size]['class'];
		if( $sidebar == "no-sidebar" ){
			$item_size = $port_div_size_num_class[$port_size]['size'];
		}else if ( $sidebar == "left-sidebar" || $sidebar == "right-sidebar" ){
			$item_size = $port_div_size_num_class[$port_size]['size2'];
		}else{
			$item_size = $port_div_size_num_class[$port_size]['size3'];
		}

		// get the page meta value
		$header = cp_find_xml_value($item_xml, 'header');
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt');	

		// page header
		if(!empty($header)){
			echo '<h2><span class="txt-left">' . $header . '</span> <span class="bg-right"></span></h2>';
		}
		global $post;
		$post_temp = query_posts(array('post_type'=>'page', 'paged'=>$paged, 'post_parent'=>$post->ID, 'posts_per_page'=>$num_fetch ));
		// get the portfolio size
		$port_wrapper_size = $neomag_class_to_num[cp_find_xml_value($item_xml, 'size')];
		$port_current_size = 0;
		$port_size =  $neomag_class_to_num[$port_size];
		
		$port_num_have_bottom = sizeof($post_temp) % (int)($port_wrapper_size/$port_size);
		$port_num_have_bottom = ( $port_num_have_bottom == 0 )? (int)($port_wrapper_size/$port_size): $port_num_have_bottom;
		$port_num_have_bottom = sizeof($post_temp) - $port_num_have_bottom;
		
		echo '<section id="portfolio-item-holder" class="portfolio-item-holder">';
		while( have_posts() ){
			the_post();
			// start printing data
			echo '<figure class="' . $item_class . ' mt0 pt25 portfolio-item">'; 
			$image_type = get_post_meta( $post->ID, 'post-option-featured-image-type', true);
			$image_type = empty($image_type)? "Link to Current Post": $image_type; 
			$thumbnail_id = get_post_thumbnail_id();
			$thumbnail = wp_get_attachment_image_src( $thumbnail_id , $item_size );
			$alt_text = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true);
			
			$hover_thumb = "hover-link";
			$pretty_photo = "";
			$permalink = get_permalink();
			

			if( !empty($thumbnail[0]) ){
				echo '<div class="portfolio-thumbnail-image">';
				echo '<div class="overflow-hidden">';
				echo '<a href="' . esc_url($permalink) . '" ' . $pretty_photo . ' title="' . esc_attr(get_the_title()) . '">';
				echo '<span class="portfolio-thumbnail-image-hover">';
				echo '<span class="' . $hover_thumb . '"></span>';
				echo '</span>';
				echo '</a>';
				echo '<img src="' . $thumbnail[0] .'" alt="'. $alt_text .'"/>';
				echo '</div>'; //overflow hidden
				echo '</div>'; //portfolio thumbnail image						
			}
			
			
			echo '<div class="portfolio-thumbnail-context">';
			// page title
			if( cp_find_xml_value($item_xml, "show-title") == "Yes" ){
				echo '<h2 class="heading portfolio-thumbnail-title port-title-color cp-title"><a href="' . esc_url(get_permalink()) . '">' . esc_attr(get_the_title()) . '</a></h2>';
			}
			// page excerpt
			if( cp_find_xml_value($item_xml, "show-excerpt") == "Yes" ){			
				echo '<div class="portfolio-thumbnail-content">' . mb_substr( get_the_excerpt(), 0, $num_excerpt ) . '</div>';
			}
			// read more button
			if( cp_find_xml_value($item_xml, "read-more") == "Yes" ){
				echo '<a href="' . esc_url(get_permalink()) . '" class="portfolio-read-more cp-button">' . esc_html__('Read More','neomag') . '</a>';
			}
			echo '</div>';
			// print space if not last line
			if($port_current_size < $port_num_have_bottom){
				echo '<div class="portfolio-bottom"></div>';
				$port_current_size++;
			}
			echo '</figure>';

		}

		echo "</section>";
		echo '<div class="clear"></div>';
		if( cp_find_xml_value($item_xml, "pagination") == "Yes" ){	
			cp_pagination();
		}		
	}
	
	//Donation Box
	function cp_print_donate_item($item_xml){
		$header = cp_find_xml_value($item_xml, 'header');
		$description = cp_find_xml_value($item_xml, 'description');
		$donate_button = cp_find_xml_value($item_xml, 'donate_button_text');
		$button_link = cp_find_xml_value($item_xml, 'button-link');
	?>
	<section id="donation_box">	
		<div class="donation_box">
			<figure class="span10">
				<?php echo esc_attr($description);?>
			</figure>
			<figure class="span2">
					<a href="<?php echo esc_url($button_link);?>" class="donate-now btn btn-large dropdown-toggle" type="submit"><?php echo esc_attr($donate_button);?></a>
			</figure>
		</div>
	</section>
	<?php }
	
	//Blog Slider 
	function cp_print_blog_slider_item($item_xml){ 
	
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$number_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt'); 
		
		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}

		// Get Post From Database
		if($category == '0'){
			//Popular Post 
			query_posts(
				array( 
				'post_type'			 => 'post',
				'paged'				 => $paged,
				'posts_per_page' 	 => $number_fetch,
				'orderby'			 => 'date',
				'order' 			 => 'DESC' )
			);
		}else{
			//Popular Post 
			query_posts(
				array( 
				'post_type' 		=> 'post',
				'posts_per_page' 	=> $number_fetch,
				'paged'				=> $paged,
				'tax_query' 		=> array(
					array(
						'taxonomy'	=> 'category',
						'terms' 	=> $category,
						'field' 	=> 'term_id',
					)
				),
				'orderby' 			=> 'date',
				'order' 			=> 'DESC' )
			);
		}
		?>
			
			<div class="trave-home-blog">
			<script type="text/javascript">
				jQuery(document).ready(function ($) {
					"use strict";
					if ($(".travel-home-blog").length) {
						$(".travel-home-blog").bxSlider({
							auto:true
						});
					}
				});
			</script>
			<?php if($header <> ''){ ?><div class = "col-md-12"><h2 class="section-title"><?php echo esc_attr($header); ?></h2><?php }?>
				
				<ul class="travel-home-blog">
					<?php
					if(have_posts()){
					while( have_posts() ){
						the_post();
						global $post, $post_id, $allowedposttags;
					?>
					<li>
					  <div class="th-post">
						<div class="col-md-8"><?php echo cp_print_blog_thumbnail($post->ID,array(1600,900));?></div>
						<div class="col-md-4 thb-cbg">
						  <div class="hb-content">
							<div class="post-head">
							  <h3><?php echo esc_attr(substr(get_the_title(),0,20));?></h3>
							  <ul>
								<li><i class="fa fa-calendar"></i><?php echo esc_attr(get_the_date('d'))?> <?php echo esc_attr(get_the_date('M'))?>, <?php echo esc_attr(get_the_date('Y'))?></li>
								<li><i class="fa fa-user"></i> <?php echo esc_attr(get_the_author());?> </li>
								<li> 
								<?php
									//Get Post Comment 
									comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
									esc_html__('<i class="fa fa-comments-o"></i>	 1 ','neomag'),
									esc_html__('<i class="fa fa-comments-o"></i>	 %','neomag'), '',
									esc_html__('<i class="fa fa-comments-o"></i>	 0','neomag') );
									?>
								</li>
							  </ul>
							</div>
							<p class="cp-text-description"><?php echo esc_attr(strip_tags(mb_substr(get_the_content(),0,$num_excerpt)));?></p>
							<a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e ('Read More','neomag');?></a> </div>
						</div>
					  </div>
					</li>	
				<?php } //end while
				wp_reset_postdata();
				} wp_reset_query(); 
				?> 
				</ul>
			</div></div>
			<?php			
	 } //Function Ends Here
			
	
	//Crowd Funding Functions to Fetch Products
	function cp_print_funds_item_item($item_xml){ 
		$header = cp_find_xml_value($item_xml, 'header');
		$project = cp_find_xml_value($item_xml, 'project');
		
		//Condition to check Projects are not empty
		if($project <> ''){
			//Fetch All elements here
			$ign_fund_goal = get_post_meta($project, 'ign_fund_goal', true);
			$ign_project_id = get_post_meta($project, 'ign_project_id', true);
			$ign_product_image1 = get_post_meta($project, 'ign_product_image1', true);
			$ignition_date = get_post_meta($project, 'ign_fund_end', true);
			$ignition_datee = date('d-m-Y h:i:s',strtotime($ignition_date));
			
			$getPledge_cp = getPledge_cp($ign_project_id);
			$current_date = date('d-m-Y h:i:s');
			$project_date = new DateTime($ignition_datee);
			$current = new DateTime($current_date);
			
			$days = round(($project_date->format('U') - $current->format('U')) / (60*60*24));

		?>
		<div id="charity_progress">
			<h3><a href="<?php echo esc_url(get_permalink($project));?>"><?php echo esc_attr(get_the_title($project));?></a></h3>
			<div id="charity_process_inner">
				<div class="span4 img first">
					<img src="<?php echo esc_url($ign_product_image1);?>" alt="<?php echo esc_attr(get_the_title($project));?>"/>
				</div>
				<div class="span8 progress_report">
				<h2> <?php esc_html_e('$','neomag');?><?php echo esc_attr(getTotalProductFund_cp($ign_project_id));?> </h2>
				<h4><?php esc_html_e('Pledged of','neomag');?> <?php esc_html_e('$','neomag');?><?php echo esc_attr($ign_fund_goal);?> <?php esc_html_e('Goal','neomag');?></h4>
					<div class="progress progress-striped active">  
						<div style="width:<?php echo esc_attr(getPercentRaised_cp($ign_project_id));?>%;" class="bar p80"></div>    
					</div>
					  <div class="info"> 
							<div class="span6 first">
								<i class="fa fa-user"></i> <span> <?php echo esc_attr($getPledge_cp[0]->p_number);?></span> <?php esc_html_e('Pledgers','neomag');?>
							</div>
							<div class="span6 ntr">
								<i class="fa fa-calendar-empty"></i> <span> <?php echo esc_attr($days);?></span> <?php esc_html_e('Days Left','neomag');?>
							</div>
					  </div>
				</div>
			</div>
		</div>	
	<?php
		}// Condition Ends Here
	} // Function ends here
	
	//WooCommerece Feature Products
	function cp_print_woo_product_feature_item($item_xml){ 
		global $counter;
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		
		//BX Slider Scripts
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
	?>
		<script type="text/javascript">
		//Run Bx Slider
		jQuery(document).ready(function ($) {
			$('#shop_slider-<?php echo esc_js($counter);?>').bxSlider({
				slideWidth: 140,
				minSlides: 1,
				maxSlides: 3,
				slideMargin: 28
			});
		});	
		</script>
		<figure id="blog_store">
			<?php if($header <> ''){?><h2 class="title"> <?php echo esc_attr($header);?><span class="h-line"></span></h2><?php }?>
			<div class="slider_shop" id="slider_shop">
				<ul class="shop_slider" id="shop_slider-<?php echo esc_attr($counter);?>">
				<?php
		
			
					$counter_team = 0; 
					query_posts(array( 
						'post_type' => 'product',
						'showposts' => $num_fetch,
						'tax_query' => array(
							array(
								'taxonomy' => 'product_cat',
								'terms' => $category,
								'field' => 'term_id',
							)
						),
						'orderby' => 'title',
						'order' => 'DESC' )
					);
					if(have_posts()){
					while( have_posts() ){
					the_post();
					$currency = '';
					global $post,$product,$product_url;
						$regular_price = get_post_meta($post->ID, '_regular_price', true);
						if($regular_price == ''){
							$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
						}
						$sale_price = get_post_meta($post->ID, '_sale_price', true);
						if($sale_price == ''){
							$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
						}
						if(function_exists('get_woocommerce_currency_symbol')){
							$currency = get_woocommerce_currency_symbol();
						}
					?>
					<li> 
						<div class="img">
							<a href="<?php echo esc_url(get_permalink());?>">
								<?php $size = array(260,220); echo cp_function_library::cp_thumb_size($post->ID,$size);?>
							</a>
						</div>
						<div class="price_cart"><span class="price"><?php echo esc_attr($currency);?><?php echo esc_attr($sale_price);?></span><a href="<?php echo esc_url(get_permalink());?>&add-to-cart=<?php echo esc_attr($post->ID);?>"><i class="fa fa-shopping-cart"></i></a></div>
					</li>
					<?php } //end while
					wp_reset_postdata(); 
					} wp_reset_query(); 
					?>
				</ul>
			</div>
		</figure>
	<?php	
	}
	
	//News Bar Under Slider
	function cp_news_bar_frontpage($news_button,$news_title,$news_category){

		//BX Slider Scripts
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css'); ?>
		<script type="text/javascript">
		//Run Bx Slider
		jQuery(document).ready(function ($) {
			'use strict';
			$('#slider12').bxSlider({
				pager:false,
			});
		});	
		</script>
		<?php
		$args = array(
			'cat' => $news_category, 
			'posts_per_page'   => 5,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
		);
		// Retrieve posts
		$post_list = get_posts( $args );
		//News Title
		if($news_title <> ''){ ?><strong class="news-title"><?php echo esc_attr($news_title);?></strong><?php }else{ ?><strong class="news-title"><?php esc_html_e('Dont miss','neomag');?></strong><?php } ?>
		<div id="ticker" class="ticker ">
			<div id="slider12">
				<?php
				//Arguments for Loop
				foreach($post_list as $post){ ?>
						<div class="slide">
							<p><?php echo esc_attr($post->post_title);?> &#45; <em><?php echo htmlspecialchars(strip_tags(substr($post->post_content,0,120))); echo '...';?></em></p>
						</div>
					<?php
				} //If Posts Condition Ends
				?>
			</div>						
		</div> <!-- End News Ticker & Share-search bar -->
		<?php
	}	//Function ends Here


	function cp_top_meta_items(){
		return '<ul class="row form-box">
			<li class="col-md-4 col-sm-4 col-xs-12">
				<div class="cp-search-box">
					<form method="get" id="searchform" action="'.esc_url(home_url('/')).'/">
						<input type="text" placeholder="Search" value="'.the_search_query().'" name="s"  autocomplete="off" />
						<button class="submit" type="submit"><i class="fa fa-search"></i></button>
					</form>
				</div>
			</li>
			<li class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-group">
					<label>By Dish</label>
					<select>
						<option>Italian</option>
						<option>New Zelend</option>
						<option>Italian</option>
					</select>
				</div>
			</li>
			<li class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-group">
					<label>By price</label>
					<select>
						<option>Low to High</option>
						<option>High to Low</option>
						<option>Italian</option>
					</select>
				</div>
			</li>
		</ul>';
	}