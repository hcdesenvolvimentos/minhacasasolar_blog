

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Minha_Casa_Solar
 */
global $configuracao;
?>
 
	<!-- BOTÃO VOLTAR AO TOPO -->
	<div class="backToTop" style="display: none">
	    <span id="subir">
	        <i class="fas fa-chevron-up"></i>
	    </span>
	</div>

<!-- RODAPÉ -->
<footer class="rodape">
	<div class="containerLagura">
		<div class="row">
		
			<div class="col-sm-2">
				<figure class="logo">
					<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
				</figure>
			</div>
			
			<div class="col-sm-3">
				<div class="descricao">
					<h4><?php echo $configuracao['rodape_titulo_texto'] ?></h4>
					<p><?php echo $configuracao['rodape_texto'] ?></p>
				</div>
			</div>
			
			<div class="col-sm-2">
				<div class="menuRodape">
					<strong>MINHA CASA SOLAR</strong>
					<nav>
						<?php 
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu Minha Casa Solar',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'nav navbar-nav',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
								);
							wp_nav_menu( $menu );
						?>
					</nav>
				</div>
			</div>
			
			<div class="col-sm-2">
				<div class="menuRodape">
					<strong>Categorias</strong>
					<nav>
						<?php 
							// LISTA DE CATEGORIAS
							$arrayCategorias = array();
							$categorias=get_categories($args);
							foreach($categorias as $categoria):
								$arrayCategorias[$categoria->cat_ID] = $categoria->name;
								$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
								if ($nomeCategoria != "Destaque"):
									$nomeCategoria = $nomeCategoria;
								
						?>
						<a href="<?php echo get_category_link($categoria->cat_ID); ?>" title="Energia Solar"><?php echo $nomeCategoria ?></a>
						<?php endif; endforeach; ?>
						
					</nav>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="redesSociais">
					<a href="<?php echo $configuracao['opt_facebook'] ?>" target="_blank" class="facebook">
						<i class="fab fa-facebook-f"></i>
					</a>			
					<a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank" class="instagram">
						<i class="fab fa-instagram"></i>
					</a>
					<a href="<?php echo $configuracao['opt_linkedin'] ?>" target="_blank" class="linkedin">
						<i class="fab fa-linkedin-in"></i>
					</a>
					<a href="<?php echo $configuracao['opt_google'] ?>" target="_blank" class="google">
						<i class="fab fa-google-plus-g"></i>
					</a>
					<a href="<?php echo $configuracao['opt_youtube'] ?>" target="_blank" class="youtube">
						<i class="fab fa-youtube"></i>
					</a>
				</div>
			</div>

		</div>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
