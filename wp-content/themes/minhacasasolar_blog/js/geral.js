$(function(){


    /*****************************************
        SCRIPTS PÁGINA INICIAL
    *******************************************/
    //CARROSSEL DE DESTAQUE
    $("#carrosselDestaque").owlCarousel({
        items : 1,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        //responsiveClass:true,             
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
                                    
 //        }                                
        
    });
    //CARROSSEL DE PRODUTOS
    $("#carosselProdutos").owlCarousel({
        items : 3,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        responsiveClass:true,             
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:3
            },
                                    
        }                                
        
    });

    //CARROSSEL DE DESTAQUE
    $("#carrossellLinhadoTempo").owlCarousel({
        items : 10,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,
        margin: 120,

        //CARROSSEL RESPONSIVO
        responsiveClass:true,             
        responsive:{
            320:{
                items:3
            },
            600:{
                items:5,
                margin: 0
            },
           
            991:{
                items:10
            },
            1024:{
                items:10
            },
            1440:{
                items:10
            },
                                    
        }                                
        
    });
    var carrossellLinhadoTempo = $("#carrossellLinhadoTempo").data('owlCarousel');
    $('#btnMudarCarrossel').click(function(){ carrossellLinhadoTempo.next(); });
    $('#btnMudarCarrosselright').click(function(){ carrossellLinhadoTempo.prev(); });

    let descricaoPadrao = "Conheça nossa história!";

    let descricaroAtivo = $( ".carrossellLinhadoTempo .item").attr('data-descricao');
    $(".textoLinhadoTempo p").text(descricaoPadrao);

    $( ".carrossellLinhadoTempo .item").mouseover(function(e) {
        let descrcao = $(this).attr('data-descricao');
       
        $(".textoLinhadoTempo p").text(descrcao);
        $(".carrossellLinhadoTempo .item").removeClass('ativo');
    }).mouseout(function() {
        let descricaroAtivo = $( ".carrossellLinhadoTempo .item").attr('data-descricao');
        $(".textoLinhadoTempo p").text(descricaoPadrao);
        $(".carrossellLinhadoTempo .item.primeiro").addClass('ativo');

    });

    $( ".topo .pesquisar").mouseover(function(e) {
        $(this).addClass('abrirAreaPesquisar');
    }).mouseout(function() {
        $(this).removeClass('abrirAreaPesquisar');
    });

    $(window).bind('scroll', function () {
       var alturaScroll = $(window).scrollTop()
       if (alturaScroll > 200) {
            $(".topo.topoFixo").fadeIn();
       }else{
            $(".topo.topoFixo").fadeOut();
       }
    });

     //SCRIPT APARECER BOTAO VOLTAR AO TOPO
    $(window).bind('scroll', function () {
       
       var alturaScroll = $(window).scrollTop()
       if (alturaScroll > 200) {
            $(".backToTop").fadeIn();
       }else{
            $(".backToTop").fadeOut();

       }
    });

    //SCRIPT VOLTAR AO TOPO
    $(document).ready(function() {
    $('#subir').click(function(){
        $('html, body').animate({scrollTop:0}, 'fast');
        
        });
    });
          
    // REMOVE O BOX QUE CONTÉM O ATTR TITLE
    $(document).ready(function() {
        $('a').removeAttr("title")
    });

    


});