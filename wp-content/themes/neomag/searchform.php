<form method="get" id="cp-top-search" class="searchform" action="<?php  echo esc_url(home_url('/')); ?>/">
	<input type="text" class="form-control" placeholder="<?php  echo esc_html_e('Buscar','neomag'); ?>" value="<?php the_search_query(); ?>" name="s"  autocomplete="off" />
    <button type="submit"><i class="fa fa-search"></i></button>
</form>