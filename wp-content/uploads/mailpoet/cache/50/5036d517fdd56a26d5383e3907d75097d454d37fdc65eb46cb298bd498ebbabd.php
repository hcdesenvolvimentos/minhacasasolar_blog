<?php

/* form/iframe.html */
class __TwigTemplate_f853ba9c5ed9ed013335268c69c1b5fa6253b46678c4a00fa206d85854563cb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 7]>
<html class=\"ie ie7\" ";
        // line 3
        echo (isset($context["language_attributes"]) ? $context["language_attributes"] : null);
        echo ">
<![endif]-->
<!--[if IE 8]>
<html class=\"ie ie8\" ";
        // line 6
        echo (isset($context["language_attributes"]) ? $context["language_attributes"] : null);
        echo ">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html ";
        // line 9
        echo (isset($context["language_attributes"]) ? $context["language_attributes"] : null);
        echo ">
<!--<![endif]-->
  <head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width\">
    <meta name=\"robots\" content=\"noindex, nofollow\">
    <title>";
        // line 15
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("MailPoet Subscription Form");
        echo "</title>
    ";
        // line 16
        echo $this->env->getExtension('MailPoet\Twig\Assets')->generateStylesheet("public.css");
        echo "
    ";
        // line 17
        echo (isset($context["scripts"]) ? $context["scripts"] : null);
        echo "
  </head>
  <body>
    ";
        // line 20
        echo (isset($context["form"]) ? $context["form"] : null);
        echo "
    <script type=\"text/javascript\">
      var MailPoetForm = ";
        // line 22
        echo json_encode((isset($context["mailpoet_form"]) ? $context["mailpoet_form"] : null));
        echo ";
    </script>
  </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "form/iframe.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 22,  58 => 20,  52 => 17,  48 => 16,  44 => 15,  35 => 9,  29 => 6,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<!--[if IE 7]>
<html class=\"ie ie7\" <%= language_attributes | raw %>>
<![endif]-->
<!--[if IE 8]>
<html class=\"ie ie8\" <%= language_attributes | raw %>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <%= language_attributes | raw %>>
<!--<![endif]-->
  <head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width\">
    <meta name=\"robots\" content=\"noindex, nofollow\">
    <title><%= __('MailPoet Subscription Form') %></title>
    <%= stylesheet('public.css') %>
    <%= scripts | raw %>
  </head>
  <body>
    <%= form | raw %>
    <script type=\"text/javascript\">
      var MailPoetForm = <%= json_encode(mailpoet_form) %>;
    </script>
  </body>
</html>
", "form/iframe.html", "/home/hgcom/public_html/minhacasasolar/wp-content/plugins/mailpoet/views/form/iframe.html");
    }
}
