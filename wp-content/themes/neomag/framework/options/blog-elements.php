<?php

	/*
	*	CrunchPress Blog Item File
	*	---------------------------------------------------------------------
	* 	@version	1.0
	* 	@author		CrunchPress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) CrunchPress
	*	---------------------------------------------------------------------
	*	This file contains the function that can print each blog item in 
	*	different conditions.
	*	---------------------------------------------------------------------
	*/
	global $neomag_cp_is_responsive;
	// size is when no sidebar, side2 is use when 1 sidebar, side 3 is use when 3 sidebar
	if( $neomag_cp_is_responsive ){
		$blog_div_listing_num_class = array(
			"Full-Image" => array("index"=>"1", "class"=>"sixteen ", "size"=>array(1170,350), "size2"=>array(770, 265), "size3"=>array(570,300)),
			"Small-Thumbnail" => array("index"=>"2", "class"=>"sixteen", "size"=>array(175,155), "size2"=>array(175,155), "size3"=>array(175,155)));
	}	
	
	// Print blog item
	function cp_print_blog_item($item_xml){ 
	global $paged,$post,$sidebar,$blog_div_listing_num_class,$counter,$post_id; ?>
		<div class="blog blog-full" id="content-<?php echo esc_attr($counter);?>">
			<?php
			if(empty($paged)){
				$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
			}
			
			// Post Per Page Default
			$get_default_nop = get_option('posts_per_page');
			
			// get the blog meta value		
			$header = cp_find_xml_value($item_xml, 'header');
			$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
			$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt');
			$category = cp_find_xml_value($item_xml, 'category');
			$layout_select = cp_find_xml_value($item_xml, 'layout_select'); 
			
			//Pagination default wordpress
			if(cp_find_xml_value($item_xml, "pagination") == 'Wp-Default'){
				$num_fetch = get_option('posts_per_page');
			}else if(cp_find_xml_value($item_xml, "pagination") == 'Theme-Custom'){
				$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
			}else{}

			if($layout_select == '2 Column'){ 
			echo '<div class="about-development"><div class=""><div class="row"><div class="col-md-12">';
            echo '<h2 class="sec-title2">'.($header).'</h2></div><div class="row"><div class="col-md-12">';
			} elseif($layout_select == '3 Column'){ 
			echo '<div class="col-md-12">';
			if($header <> ''){ echo '<h2>'.($header).'</h2>'; }
			echo '</div>';
			} elseif($layout_select == 'Time Line'){ 
						$timeline_counter = 0;
						echo '<div class="cp-timeline-section">';
						if(empty($sidebar)){ echo '<div class="container"><div class="row"><div class="col-md-12">'; }
                        echo '<div class="our-timeline">';
			} else {
			// print header
			if(!empty($header)){ ?>
					<h2 class="h-style"><?php echo esc_attr($header);?></h2>
			<?php
			} }
			if ($layout_select == 'List'){ echo '<div class="cp_blog-section"><ul class="blog-list">';} 
			elseif($layout_select == '2 Column'){}  
			elseif($layout_select == '3 Column'){}  
			elseif($layout_select == 'Time Line'){
			} else { 
			echo '<div class="cp-posts-style-1 ';
			if($layout_select == 'Half Width'){ echo 'blog-with-sidebar'; }
			echo  '">'; 
			}
			if($layout_select == '3 Column'){ echo '<div class="cp-news-isotope"><div class="isotope items">'; 
				wp_register_script('isotope-js', NEOMAG_PATH_URL.'/frontend/js/isotope.pkgd.min.js', false, '1.0', true);
				wp_enqueue_script('isotope-js');

			}
			$counter_blog = 0;
			// Get Post From Database
			if($category == '0'){
				//Popular Post 
				query_posts(
					array( 
					'post_type' => 'post',
					'paged'				=> $paged,
					'posts_per_page' => $num_fetch,
					'orderby' => 'date',
					'order' => 'DESC' )
				);
			}else{ 
				//Popular Post 
				query_posts(
					array( 
					'post_type' => 'post',
					'posts_per_page' => $num_fetch,
					'paged'				=> $paged,
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'terms' => $category,
							'field' => 'term_id',
						)
					),
					'orderby' => 'date',
					'order' => 'DESC' )
				);
			}
			$counter_blog = 0; 
			//Have Posts
			if(have_posts()){
				while( have_posts() ){
					the_post();
					global $post, $post_id, $allowedposttags;
					
					// Get Post Meta Elements detail 
					$post_social = '';
					$sidebar = '';
					$right_sidebar = '';
					$left_sidebar = '';
					$thumbnail_types = '';
					$post_caption = '';
					
					$post_format = get_post_meta($post->ID, 'post_format', true);
					$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
					if($post_detail_xml <> ''){ 
						$cp_post_xml = new DOMDocument ();
						$cp_post_xml->loadXML ( $post_detail_xml ); 
						$post_social = cp_find_xml_value($cp_post_xml->documentElement,'post_social');
						$sidebar = cp_find_xml_value($cp_post_xml->documentElement,'sidebar_post');
						$right_sidebar = cp_find_xml_value($cp_post_xml->documentElement,'right_sidebar_post');
						$left_sidebar = cp_find_xml_value($cp_post_xml->documentElement,'left_sidebar_post');
						$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
						$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
						$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');	
						$neomag_audio_url_type = cp_find_xml_value($cp_post_xml->documentElement,'audio_url_type');
						$post_caption = cp_find_xml_value($cp_post_xml->documentElement,'page_caption');	
						
					}
					
					// get the item class and size from array
					$item_type = 'Full-Image';
					$item_class = $blog_div_listing_num_class[$item_type]['class'];
					$item_index = $blog_div_listing_num_class[$item_type]['index'];
					if( $sidebar == "no-sidebar" ){
						$item_size = $blog_div_listing_num_class[$item_type]['size'];
					}else if ( $sidebar == "left-sidebar" || $sidebar == "right-sidebar" ){
						$item_size = $blog_div_listing_num_class[$item_type]['size2'];
					}else{
						$item_size = $blog_div_listing_num_class[$item_type]['size3'];
						$item_class = 'both_sidebar_class';
					} 
					
					
					if($thumbnail_types == 'Image'){
						$mask_html = '';
						$no_image_class = 'no-image';
						$image_size = array(1170,350);
					}else if($thumbnail_types == 'Slider'){
						$mask_html = '';
						$no_image_class = '';
						$image_size = array(1170,350);
					}else if($thumbnail_types == 'Video'){
						$mask_html = '';
						$no_image_class = '';
						$image_size = array(1170,350);
					}else{
						$mask_html = '';
						$no_image_class = 'no-image';
						$image_size = array(1170,350);
					}
					
					$thumbnail_id = get_post_thumbnail_id( $post->ID );
					$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(1170,350));
					$image_thumb = wp_get_attachment_image_src($thumbnail_id, 'full');
					if($image_thumb[1] == '1600'){
						$mask_html = '<div class="mask">
							<a href="'.esc_url(get_permalink()).'#comments" class="anchor"><span> </span> <i class="fa fa-comment"></i></a>
							<a href="'.esc_url(get_permalink()).'" class="anchor"> <i class="fa fa-link"></i></a>
						</div>';
						$no_image_class = 'image-exists';
					}
					$item_class = 'col-md-12';
					$get_post_cp = get_post($post);
					$counter_track = $counter.$post->ID;
					
		if($layout_select == 'Half Width'){

					$neomag_format = get_post_format(); 
					if ( false === $neomag_format ) {
						$neomag_format = 'standard';
					}



 					 if ( shortcode_exists( 'photo_set' ) ) { ?>
                        <?php
						// The [photo_set] short code exists.
						$content = get_post( $post->ID ); 
						 if ( has_shortcode( $content->post_content, 'photo_set' ) ) { 
						 echo '<li class="cp-post cp-photoset-post">';
						 $shortcode_content = $content->post_content;
						echo do_shortcode($shortcode_content);
						 
					 ?>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                           </div>
                        </div>
                      </li>
					<?php } } ?>
					<?php if ( $neomag_format === 'standard' && !( has_shortcode( $content->post_content, 'photo_set' )) &&  !($image_thumb[0] == '')) { ?>
                      
                      <li class="cp-post">
                        <div class="cp-thumb"> <img src="<?php echo esc_url($image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                          <div class="cp-post-hover"> <a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-link"></i></a> 
                          <a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-search"></i></a> </div>
                        </div>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <?php $post_content_image = neo_mag_catch_that_image(); 
								if($post_content_image <> ''){
									echo '<div class="content-img"><img src="'.esc_url($post_content_image).'" alt="'.esc_attr($post->ID).'">';
									} else {
							?>
                            <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 400));?></p>
                            <?php } ?>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
                      
					<?php }	?>
					<?php if( $neomag_format === 'standard' && ($image_thumb[0] == '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-text-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <p><?php echo strip_tags(mb_substr(get_the_content(),0, 400));?></p>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					
					<?php }?>
					<?php if( $neomag_format === 'quote' && ($image_thumb[0] == '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-quote-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <blockquote><?php echo strip_tags(mb_substr(get_the_content(),0, 400));?></blockquote>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
					<?php if( $neomag_format === 'video' && ($video_url_type <> '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-text-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                    			<iframe src="<?php echo esc_url($video_url_type); ?>"></iframe>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
					
				<?php	
					
				echo '</li>';  

				?>
					<!--BLOG LIST ITEM END-->
	<?php }else if($layout_select == 'Grid'){ ?>
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="grid-blog">
						<ul class="bdata">
							<li> 
								<?php if(get_the_post_thumbnail($post_id, array(570,300)) <> ''){ ?>
									<a href="<?php echo esc_url(get_permalink());?>">
										<div class="block-image background-scale">
											<?php echo get_the_post_thumbnail($post_id, array(570,300));?>
											<div class="img-overlay pat-override"></div>
										</div>
									</a>
								<?php } ?>
								<?php if(get_the_author_meta('ID') <> ''){ ?>
									<div class="hpost-img">
										<span>
										<?php echo get_avatar(get_the_author_meta('ID'));?>
										</span>
									</div>
								<?php }?>
							</li>
							<li class="bp-data">
								<h4><a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(substr(get_the_title(),0,20));?></a></h4>
								<p><?php echo strip_tags(mb_substr(get_the_content(),0, 75));?></p>
							</li>
							<li>
								<ul class="flinks">
								<?php $archive_year  = get_the_time('Y'); $archive_month = get_the_time('m'); $archive_day   = get_the_time('d'); ?>
									<li><a href="<?php echo esc_url(get_day_link( $archive_year, $archive_month, $archive_day)); ?>"><i class="fa fa-calendar"></i> <?php echo get_the_date('d')?> <?php echo get_the_date('M')?></a></li>
									<li><a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-user"></i> <?php echo get_the_author();?></a></li>
									<li>
										<?php
										//Get Post Comment 
										comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
										esc_html__('<i class="fa fa-comments-o"></i>	 1 ','neomag'),
										esc_html__('<i class="fa fa-comments-o"></i>	 %','neomag'), '',
										esc_html__('<i class="fa fa-comments-o"></i>	 0','neomag') );
										?>
									</li>
								</ul>
							</li>
						</ul>
						</div>
					</div>		
					<!-- ingenio -->
       <?php }else if($layout_select == '2 Column'){?>
                        
                          <div class="col-md-6 col-sm-6">
                            <div class="content-box"> <?php echo get_the_post_thumbnail($post_id, array(380,200));?>
                              <h3><?php echo esc_attr(substr(get_the_title(),0,20));?></h3>
                              <p><?php echo strip_tags(mb_substr(get_the_content(),0, $num_excerpt));?></p>
                              <a href="<?php echo esc_url(get_permalink());?>" class="readmore-blue"><?php esc_html_e('Read More','neomag');?></a> </div>
                          </div>
                          
					<!--3 Column grid start-->
	<?php }else if($layout_select == '3 Column'){ 
					$neomag_format = get_post_format(); 
					if ( false === $neomag_format ) {
						$neomag_format = 'standard';
					}
					?>
					<?php if ( shortcode_exists( 'photo_set' ) ) { ?>
                        <?php
						// The [photo_set] short code exists.
						$content = get_post( $post->ID ); 
						 if ( has_shortcode( $content->post_content, 'photo_set' ) ) { continue;
						 echo '<div>';
						 $shortcode_content = $content->post_content;
						echo do_shortcode($shortcode_content);
					 ?>

                      <div class="cp-post-content cp-post-content2">
                        <h3><?php echo esc_attr(get_the_title()); ?></h3>
                        <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                        </div>
                    </div>

					<?php } } ?>
					<?php if ( $neomag_format === 'standard' && !( has_shortcode( $content->post_content, 'photo_set' )) &&  !($image_thumb[0] == '')) { 
					if($thumbnail_types != 'Slider'){
					?>
                      
		            <div class="item cp-post col-md-4">
                      <figure class="cp-thumb"><img src="<?php echo esc_url($image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>"></figure>
                      <div class="cp-post-content">
                        <a href="<?php echo esc_url(get_permalink());?>"><h3><?php echo get_the_title(); ?></h3></a>
                        <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                        </ul>
                        <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 185));?></p>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                        </div>
                    </div>
                    
					<?php } }	?>
					<?php if( $neomag_format === 'standard' && ($image_thumb[0] == '') && !( has_shortcode( $content->post_content, 'photo_set' )) && $thumbnail_types != 'Slider'){ ?>

                        <div class="item cp-post col-md-4">
                          <div class="cp-post-content">
                        <a href="<?php echo esc_url(get_permalink());?>"><h3><?php echo get_the_title(); ?></h3></a>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                        <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 185));?></p>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            </div>
                        </div>

					<?php } ?>
					<?php if( $neomag_format === 'quote' && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                        <div class="item cp-post col-md-4">
                        <blockquote>
                        <figure class="cp-thumb"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/h2-m6.jpg" alt=""></figure>
                        <div class="cp-post-content">
                            <a href="<?php echo esc_url(get_permalink());?>"><p>"<?php echo strip_tags(mb_substr(get_the_content(),0, 152));?>"</p></a>
                         <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        
                        
                        </blockquote>
                          
                        </div>

					<?php } ?>
					<?php if( $neomag_format === 'video' && ($video_url_type <> '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                        <div class="item cp-post col-md-4">
                          <div class="video-holder">
                            <iframe src="<?php echo esc_url($video_url_type); ?>"></iframe>
                          </div>
                          <div class="cp-post-content">
                            <h3><?php echo get_the_title(); ?></h3>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 185));?></p>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            </div>
                        </div>

					<?php } ?>
					<?php if( $neomag_format === 'standard' && ($select_slider_type <> '') && !( has_shortcode( $content->post_content, 'photo_set' ))){
						if($thumbnail_types == 'Slider'){ 
						?>

                        <div class="item cp-post col-md-4">
                          <figure class="cp-thumb">   
                          <?php echo cp_print_blog_thumbnail($post->ID,'neomag_std_post'); ?>     
                          </figure>
                          <div class="cp-post-content">
                            <h3><?php echo esc_attr(get_the_title()); ?></h3>
                            <ul>
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 185));?></p>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        </div>

					<?php  } } ?>

					<!--Time Line grid start-->
					<?php } else if($layout_select == 'Time Line'){ ?>
                    
                    <!--Time line section Start-->

                          <div class="cp-timeline-block-<?php if($timeline_counter % 2 == 0){ echo 'left'; } else { echo 'right'; } ?>">
                             <div class="content-box"> <?php echo get_the_post_thumbnail($post_id, array(500,300)); ?>
                               <h3><?php echo substr(get_the_title(),0,75);?></h3>
                                <strong><?php echo get_the_author();?> . <?php echo get_the_date('j ');?> <?php echo get_the_date('F Y');?></strong>
                               <p><?php echo strip_tags(mb_substr(get_the_content(),0, $num_excerpt));?></p>
                               <a href="<?php echo esc_url(get_permalink());?>" class="readmore"><?php esc_html_e('Read More','neomag');?></a> </div>
                          </div>

                    <!--Time line section End--> 

					<!--Time Line grid end -->
                    
<?php $timeline_counter++; }else if($layout_select == 'List'){ ?> 

						<li>
							<div class="thumb"> <a class = "link_detail" href="<?php echo esc_url(get_permalink());?>"> <i class="fa fa-link"></i></a> <?php if(get_the_post_thumbnail($post_id, array(570,300)) <> '') {echo get_the_post_thumbnail($post_id, array(570,300));}?></div>
							<div class="text"> <strong class="cp_strong"><a href="<?php echo esc_url(get_permalink());?>"><?php echo substr(get_the_title(),0,30);?></a></strong>
								<ul class="listd">
								<?php $archive_year  = get_the_time('Y'); $archive_month = get_the_time('m'); $archive_day   = get_the_time('d'); ?>
									<li><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><i class="fa fa-user"></i><?php echo esc_attr(get_the_author()); ?></a></li>
									<li><a href="<?php echo esc_url(get_day_link( $archive_year, $archive_month, $archive_day)); ?>"><i class="fa fa-calendar"></i> <?php echo get_the_date('d')?> <?php echo get_the_date('F')?> <?php echo get_the_date('Y')?></a></li>
									<li>
										<?php
											//Get Post Comment 
											comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
											esc_html__('<i class="fa fa-comments-o"></i>	 1 ','neomag'),
											esc_html__('<i class="fa fa-comments-o"></i>	 %','neomag'), '',
											esc_html__('<i class="fa fa-comments-o"></i>	 0','neomag') );
										?>
									</li>
								</ul>
								<p><?php echo strip_tags(mb_substr(get_the_content(),0, $num_excerpt));?></p>
								<a href="<?php echo esc_url(get_permalink());?>" class="more-info"><?php esc_html_e('Read More','neomag'); ?></a>
							</div>
						</li>
					<?php }else{ ?>
                    
                    
   <!--Layout fullwidth-->                 
					<!--BLOG LIST ITEM START-->
					<?php 
					
					$neomag_format = get_post_format(); 
					if ( false === $neomag_format ) {
						$neomag_format = 'standard';
					}

					if ( shortcode_exists( 'photo_set' ) ) { ?>
                        <?php
						// The [photo_set] short code exists.
						$content = get_post( $post->ID ); 
						 if ( has_shortcode( $content->post_content, 'photo_set' ) ) { 
						 echo '<li class="cp-post cp-photoset-post">';
						 $shortcode_content = $content->post_content;
						echo do_shortcode($shortcode_content);
						 
					 ?>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                           </div>
                        </div>
                      </li>
					<?php } } ?>
					<?php if ( $neomag_format === 'standard' && !( has_shortcode( $content->post_content, 'photo_set' )) &&  !($image_thumb[0] == '')) { ?>
                      
                      <li class="cp-post">
                        <div class="cp-thumb"> <img src="<?php echo esc_url($image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>" >
                          <div class="cp-post-hover"> <a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-link"></i></a> 
                          <a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-search"></i></a> </div>
                        </div>
                        <div class="">
                           <div class="cp-post-base">
                            <div class="cp-post-content">
                              <h2><?php echo get_the_title(); ?></h2>
                              <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                              </ul>
                            <?php $post_content_image = neo_mag_catch_that_image(); 
								if($post_content_image <> ''){
									echo '<div class="content-img"><img src="'.esc_url($post_content_image).'" alt="'.$post->ID.'">';
									} else {
							?>
                            <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 400));?></p>
                            <?php } ?>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                              </div>
                          </div>
                        </div>
                       
                      </li>

					<?php }	?>
					<?php if( $neomag_format === 'standard' && ($image_thumb[0] == '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-text-post">
                      <div class="container">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <p><?php echo strip_tags(mb_substr(get_the_content(),0, 400));?></p>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            </div>
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
					<?php if( $neomag_format === 'quote' && ($image_thumb[0] == '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-quote-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                            <blockquote><?php echo strip_tags(mb_substr(get_the_content(),0, 400));?></blockquote>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            </div>
                        </div>
                      </li>
					
					<?php } ?>
					<?php if( $neomag_format === 'video' && ($video_url_type <> '') && !( has_shortcode( $content->post_content, 'photo_set' ))){?>

                      <li class="cp-post cp-text-post">
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <h2><?php echo get_the_title(); ?></h2>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo esc_url(get_permalink());?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
                              ?>
                              <li><a><?php echo esc_attr($category_name->name); ?></a></li>
                            </ul>
                    			<iframe src="<?php echo esc_url($video_url_type); ?>"></iframe>
                            <a href="<?php echo esc_url(get_permalink());?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            
                            </div>
                        </div>
                      </li>
					<?php } ?>
					<!--BLOG LIST ITEM ENDs-->
                    
				<?php
					} 
					if($layout_select == 'Time Line'){}
						elseif($layout_select == '2 Column'){}
							elseif($layout_select == '3 Column'){}
								elseif($layout_select == 'Half Width'){}
									else{  echo ""; 
					} 
				}//end while
				wp_reset_postdata();
			}		
			if($layout_select == 'List'){ 
				echo '</ul></div>';
			}elseif($layout_select == '3 Column'){
				echo '</div></div>';
			}elseif($layout_select == 'Time Line'){
				if(empty($sidebar)){ echo '</div></div></div>'; }
				echo '</div></div>';
			}elseif($layout_select == '2 Column'){
				if(!empty($sidebar)){ echo '</div></div></div></div>'; }
			}else{
				}		
			if( cp_find_xml_value($item_xml, "pagination") == "Theme-Custom" ){	
					pagination_crunch();
			}
			 wp_reset_query();
			 if($layout_select == '3 Column'){ } else { echo '</div>'; } 
			?> 
		<?php
	}	
	
	
	// Print blog item
	function print_blog_modern_item($item_xml){
		global $paged,$post,$sidebar,$blog_div_listing_num_class,$counter,$post_id;
		
		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}
		
		// Post Per Page Default
		$get_default_nop = get_option('posts_per_page');
		
		// get the blog meta value		
		$header = cp_find_xml_value($item_xml, 'header');
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		$select_feature = cp_find_xml_value($item_xml, 'select_feature');
		$category = cp_find_xml_value($item_xml, 'category');
		
		//Pagination default wordpress
		if(cp_find_xml_value($item_xml, "pagination") == 'Wp-Default'){
			$num_fetch = get_option('posts_per_page');
		}else if(cp_find_xml_value($item_xml, "pagination") == 'Theme-Custom'){
			$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		}else{}
		
		
		// print header
		if(!empty($header)){ ?>
		<figure class="page_titlen feature_story">
			<div class="span12 first">
				<h2><?php echo esc_attr($header);?></h2>
			</div>
		</figure>
		<?php
		}
		

		//If feature Post selected
		if($select_feature <> '786512'){ 
			$thumbnail_types = '';
			$post_detail_xml = get_post_meta($select_feature, 'post_detail_xml', true);
			if($post_detail_xml <> ''){
				$cp_post_xml = new DOMDocument ();
				$cp_post_xml->loadXML ( $post_detail_xml );
				$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
				$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
				$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');				
			}
			if($thumbnail_types == 'Image'){ ?>
				<ul class="featured-story">
					<li class="span12 featured-slider">
						<?php echo get_the_post_thumbnail($select_feature, array(1170,350));?>
						<div class="post-slide-cap">
							<span class="post-type"><?php get_the_date(get_option('date_format'));?><i class="fa fa-camera"></i></span>
							<strong class="f-post-title"><a href="<?php echo esc_url(get_permalink($select_feature));?>"><mark><?php echo esc_attr(get_the_title($select_feature))?></mark></a></strong>
						</div>
					</li>
				</ul>
			<?php
			}else{ ?>
				<ul class="featured-story">
					<li class="span12 featured-slider">
						<?php echo cp_print_blog_modern_thumbnail($select_feature,array(1170,350));?>
						<div class="post-slide-cap">
							<span class="post-type"><?php get_the_date(get_option('date_format'));?><i class="fa fa-camera"></i></span>
							<strong class="f-post-title"><a href="<?php echo esc_url(get_permalink($select_feature));?>"><mark><?php echo esc_attr(get_the_title($select_feature))?></mark></a></strong>
						</div>
					</li>
				</ul>
			<?php
			}
			
			//Arguments for loop
			query_posts(array(
				'posts_per_page'			=> $num_fetch,
				'paged'						=> $paged,
				'category_name'				=> $category,
				'post_type'					=> 'post',
				'post_status'				=> 'publish',
				'order'						=> 'DESC',
				'post__not_in' => array($select_feature)
			));
		}else{
			query_posts(array(
				'posts_per_page'			=> $num_fetch,
				'paged'						=> $paged,
				'category_name'				=> $category,
				'post_type'					=> 'post',
				'post_status'				=> 'publish',
				'order'						=> 'DESC',
			));
		}
		echo '<ul class="featured-story">';
		$counter_post = 0;
		if(have_posts()){
			while( have_posts() ){
				the_post();
				global $post, $post_id;
				
				//Get post parameters
				$thumbnail_types = '';
				$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
				if($post_detail_xml <> ''){
					$cp_post_xml = new DOMDocument ();
					$cp_post_xml->loadXML ( $post_detail_xml );
					$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
					$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
					$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');				
				}
				// get the item class and size from array
				$item_type = 'Full-Image';
				$item_class = $blog_div_listing_num_class[$item_type]['class'];
				$item_index = $blog_div_listing_num_class[$item_type]['index'];
				if( $sidebar == "no-sidebar" ){
					$item_size = array(150,150);
				}else if ( $sidebar == "left-sidebar" || $sidebar == "right-sidebar" ){
					$item_size = array(150,150);
				}else{
					$item_size = array(150,150);
					$item_class = 'both_sidebar_class';
				} 
				//Slider Settings
				if($select_slider_type <> 'Slider'){
					//Every Third
					if($counter_post % 3 == 0 ){ ?>	 
						<li class="span4 first"> 
							<?php echo cp_print_blog_modern_thumbnail($post->ID, $item_size);?>
							<div class="post-slide-cap modern-dec">
								<span class="post-type"><?php get_the_date(get_option('date_format'));?><i class="fa fa-camera"></i></span>
								<strong class="f-post-title"><a href="<?php echo esc_url(get_permalink());?>"><mark><?php echo get_the_title()?></mark></a></strong>
							</div>
						</li>
					<?php }else{ ?>
						<li class="span4"> 
							<?php echo cp_print_blog_modern_thumbnail($post->ID, $item_size);?>
							<div class="post-slide-cap modern-dec">
								<span class="post-type"><?php get_the_date(get_option('date_format'));?><i class="fa fa-camera"></i></span>
								<strong class="f-post-title"><a href="<?php echo esc_url(get_permalink());?>"><mark><?php echo get_the_title()?></mark></a></strong>
							</div>
						</li>
					<?php } $counter_post++;
				}
				
			}//end while
			wp_reset_postdata(); 		
		}wp_reset_query(); 
		echo '</ul>';		
	}	
	
	
	function cp_print_blog_thumbnail( $postid, $item_size ){
		global $counter;
		
		//Get Post Meta Options
		$img_html = '';
		$thumbnail_types = '';
		$video_url_type = '';
		$select_slider_type = '';
		$post_detail_xml = get_post_meta($postid, 'post_detail_xml', true);
		if($post_detail_xml <> ''){
			$cp_post_xml = new DOMDocument ();
			$cp_post_xml->loadXML ( $post_detail_xml );
			$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
			$neomag_audio_url_type = cp_find_xml_value($cp_post_xml->documentElement,'audio_url_type');
			$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
			$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');			
			//Print Image
			
			if( $thumbnail_types == "Image"){
				if(get_the_post_thumbnail($postid, $item_size) <> ''){
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . get_the_post_thumbnail($postid, $item_size);
					$img_html = $img_html . '</div>';
				}
			}else if( $thumbnail_types == "Video" ){
				//Print Video
				if($video_url_type <> ''){
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . '<div class="blog-thumbnail-video">';
					if($item_size == 'Blog'){
						$img_html = $img_html . cp_get_video($video_url_type, '835', '337');
					}else{
						$img_html = $img_html . cp_get_video($video_url_type, cp_get_width($item_size), cp_get_height($item_size));  
					}
					$img_html = $img_html . '</div></div>';
				}
			}else if ( $thumbnail_types == "Slider" ){
				//Print Slider
				$slider_xml = get_post_meta( intval($select_slider_type), 'cp-slider-xml', true); 				
				if($slider_xml <> ''){
					$slider_xml_dom = new DOMDocument();
					$slider_xml_dom->loadXML($slider_xml);
					$slider_name='bxslider'.$counter.$postid;
					$audio_counter = $counter.$postid;
					//Included Anything Slider Script/Style
					wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/default/bxslider.min.js', false, '1.0', true);
					wp_enqueue_script('cp-bx-slider');	
					wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
					//Inline Style for Slider Width
					if(cp_get_width($item_size) == '175'){
						$img_html = "<style>#'". $slider_name."'{width:'".cp_get_width($item_size)."'px;height:'".cp_get_height($item_size)."'px;float:left;}</style>";
					}else{
						$img_html = "<style>#'". $slider_name."'{width:100%;height:350px;float:left;}</style>";
					}
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . cp_print_post_bx_slider($slider_xml_dom->documentElement, $item_size,$slider_name);
					$img_html = $img_html . '</div>';
				}
			}else if($thumbnail_types == "Audio"){ 
				if($neomag_audio_url_type <> '' ){
				$audio_counter = $counter.$postid;
					//Jplayer Music Started	
					$img_html =  '<div class="audio_player song-list">';
					$audio_html = '';
					if(strpos($neomag_audio_url_type,'soundcloud')){
						$img_html = $img_html . cp_get_audio_track($neomag_audio_url_type,$audio_counter);
					}else{
						$img_html = $img_html . cp_get_audio_track($neomag_audio_url_type,$audio_counter) . get_the_post_thumbnail($postid, $item_size);
					}
					$img_html = $img_html . '</div>';
				} // No MP3 Song
			}else{				
				if(get_the_post_thumbnail($postid, $item_size) <> ''){
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . get_the_post_thumbnail($postid, $item_size);
					$img_html = $img_html . '</div>';
				}
			}
		}
		return $img_html;
	}
	
	
	// print the blog thumbnail
	function cp_print_blog_modern_thumbnail( $post_id, $item_size ){
		global $counter;
		//Get Post Meta Options
		$img_html = '';
		$thumbnail_types = '';
		$video_url_type = '';
		$select_slider_type = '';
		$post_detail_xml = get_post_meta($post_id, 'post_detail_xml', true);
		if($post_detail_xml <> ''){
			$cp_post_xml = new DOMDocument ();
			$cp_post_xml->loadXML ( $post_detail_xml );
			$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
			$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
			$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');			
			//Print Image
			if( $thumbnail_types == "Image" || empty($thumbnail_types) ){
				if(get_the_post_thumbnail($post_id, $item_size) <> ''){
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . get_the_post_thumbnail($post_id, $item_size);
					$img_html = $img_html . '</div>';
				}
			}else if( $thumbnail_types == "Video" ){
				//Print Video
				if($video_url_type <> ''){
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . '<div class="blog-thumbnail-video">';
					//echo cp_get_width($item_size);
					if(cp_get_width($item_size) == '175'){
						$img_html = $img_html . get_video($video_url_type, cp_get_width($item_size), cp_get_height($item_size));
					}else{
						$img_html = $img_html . get_video($video_url_type, '100%', cp_get_height($item_size));
					}
					$img_html = $img_html . '</div></div>';
				}
			}else if ( $thumbnail_types == "Slider" ){
				//Print Slider
				$slider_xml = get_post_meta( intval($select_slider_type), 'cp-slider-xml', true); 				
				if($slider_xml <> ''){
					$slider_xml_dom = new DOMDocument();
					$slider_xml_dom->loadXML($slider_xml);
					$slider_name='bxslider'.$counter.$post_id;				
					//Included Anything Slider Script/Style
					wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
					wp_enqueue_script('cp-bx-slider');	
					wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');
					if(cp_get_width($item_size) == '175'){
						$img_html = "<style>#'". $slider_name."'{width:'".cp_get_width($item_size)."'px;height:'".cp_get_height($item_size)."'px;float:left;}</style>";
					}else{
						$img_html = "<style>#'". $slider_name."'{width:100%;height:350px;float:left;}</style>";
					}
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . cp_print_bx_post_slider($slider_xml_dom->documentElement, $item_size,$slider_name);
					$img_html = $img_html . '</div>';
				}
			}else if($thumbnail_types == "Audio"){ 
				if(get_the_post_thumbnail($post_id, $item_size) <> ''){
					$img_html = '<div class="post_featured_image thumbnail_image">';
					$img_html = $img_html . cp_get_audio_track($audio);;
					$img_html = $img_html . '</div>';
				}
			}
		}
		return $img_html;
	}
	
	 
	//News Element
	function cp_print_news_item($item_xml){

		echo '<div id="content">';
		global $paged,$post,$sidebar,$blog_div_listing_num_class,$post_id;
		
		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}
		
		//Get Thumbnail Options
		$thumbnail_types = '';
		$post_detail_xml = get_post_meta($post_id, 'post_detail_xml', true);
		if($post_detail_xml <> ''){
			$cp_post_xml = new DOMDocument ();
			$cp_post_xml->loadXML ( $post_detail_xml );
			$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
		}
				
		// get the blog meta value		
		$header = cp_find_xml_value($item_xml, 'header');
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt');
		$news_layout = cp_find_xml_value($item_xml, 'news-layout');
		$category = cp_find_xml_value($item_xml, 'category');
		
		// print header
		if(!empty($header)){
			echo '<h2 class="h-style">' . ($header) . '</h2>';
		}
		
		//Pagination default wordpress
		if(cp_find_xml_value($item_xml, "pagination") == 'Wp-Default'){
			$num_fetch = get_option('posts_per_page');
		}else if(cp_find_xml_value($item_xml, "pagination") == 'Theme-Custom'){
			$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
		}else{}
		
		if($category == '0'){
			//Popular Post 
			query_posts(
				array( 
				'post_type' => 'post',
				'paged'				=> $paged,
				'posts_per_page' => $num_fetch,
				'orderby' => 'date',
				'order' => 'DESC' )
			);
		}else{
			//Popular Post 
			query_posts(
				array( 
				'post_type' => 'post',
				'posts_per_page' => $num_fetch,
				'paged'				=> $paged,
				'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'terms' => $category,
						'field' => 'term_id',
					)
				),
				'orderby' => 'date',
				'order' => 'DESC' )
			);
		}
		echo '<div class="news-page">';
		$counter_news = 0;
		if( have_posts() ){
			while( have_posts() ){
				the_post();
				$counter_news++;
				global $post, $post_id;
			//Print All post from News
			
				  // get the item class and size from array
				$item_type = 'Full-Image';
				$item_class = $blog_div_listing_num_class[$item_type]['class'];
				$item_index = $blog_div_listing_num_class[$item_type]['index'];
				if( $sidebar == "no-sidebar" ){
					$item_size = $blog_div_listing_num_class[$item_type]['size'];
				}else if ( $sidebar == "left-sidebar" || $sidebar == "right-sidebar" ){
					$item_size = $blog_div_listing_num_class[$item_type]['size2'];
				}else{
					$item_size = $blog_div_listing_num_class[$item_type]['size3'];
					$item_class = 'both_sidebar_class';
				} 
				$thumbnail_id = get_post_thumbnail_id( $post->ID );
				$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(1170,350));
				$image_thumb = wp_get_attachment_image_src($thumbnail_id, 'full');
				$item_class = '';
				if(cp_print_blog_thumbnail( $post->ID, array(1170,350) ) <> ''){ $item_class = '';}else{$item_class = 'no-image-found';}?>
					
					<div class="news-post">
						<?php if(cp_print_blog_thumbnail( $post->ID, array(1170,350) ) <> ''){ ?>
							<div class="iwrapper"> 
								<a class="thumbnail" href="<?php echo esc_url(get_permalink()); ?>"> 
									<?php echo cp_print_blog_thumbnail( $post->ID, array(1170,350) );?> 
								</a>
								<div class="gb-overlay"></div>
								<div class="picon"> 
									<span class="iconbg2">	
										<a data-gal="prettyPhoto[]" href="<?php echo esc_url($image_thumb[0]);?>"><i class="fa fa-search"></i></a> 
									</span> 
									<span class="iconbg2">
										<a href="<?php echo esc_url(get_permalink()); ?>"><i class="fa fa-link"></i></a> 
									</span> 
								</div>
							</div>
						<?php } ?>
						<div class="news-caption">
							<h4><a href="<?php echo esc_url(get_permalink()); ?>"><?php echo esc_attr(get_the_title());?></a></h4>
							<div class="news-post-text">
							  <p><?php echo strip_tags(mb_substr(get_the_content(),0,$num_excerpt)); ?></p>
							</div>
							<div class="news-tools">
								<ul>
									<li class="views">
										<?php the_tags('<i class="fa fa-tags"></i> ',', ',' ');?>
									</li>
									<li class="post-date">
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
									</li>
									<li class="readmore pull-right"> 
										<?php if(strlen(get_the_excerpt() > $num_excerpt)){?>
											<a href="<?php echo esc_url(get_permalink()); ?>" class="more-btn">
												<i class="fa fa-plus-circle"></i> 
												<?php esc_html_e('Read more','neomag'); ?>
											</a>
										<?php }?>
									</li>
								</ul>
							</div>
						</div>
					</div>
			<?php
				
			   
			}//end while 
			wp_reset_postdata();
		}wp_reset_query(); 
		echo '</div>';
			if( cp_find_xml_value($item_xml, "pagination") == "Theme-Custom" ){	
				cp_pagination();
			}
		echo '</div>';
		echo '<span id="loader"></span>';
		
	
	}	
	
	//Latest Show For DJ
	function cp_print_latest_show_item($item_xml){
		global $post,$counter;
		
		//Fetch elements data from database
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt');
		
		//Condition for Header
		if($header <> ''){ echo '<h2 class="h-style">'.($header).'</h2>';} ?>
		<?php
		//Bx Slider Script Calling
		wp_register_script('cp-bx-slider', NEOMAG_PATH_URL.'/frontend/js/bxslider.min.js', false, '1.0', true);
		wp_enqueue_script('cp-bx-slider');	
		wp_enqueue_style('cp-bx-slider',NEOMAG_PATH_URL.'/frontend/css/bxslider.css');?>
		<script type="text/javascript">
		jQuery(document).ready(function ($) {
			"use strict";
			if ($('#news-<?php echo esc_js($counter)?>').length) {
				$('#news-<?php echo esc_js($counter)?>').bxSlider({
					minSlides: 1,
					maxSlides: 1,
					auto:true,
					mode:'fade',
					pagerCustom: '#bx-pager'
				});
			}
		});
		</script>
			<div class="timelines-box">
			<?php
					if($category == '0'){
					//Popular Post 
						query_posts(
							array( 
							'post_type' => 'post',
							'posts_per_page' => 3,
							'orderby' => 'title',
							'order' => 'ASC' )
						);
					
					}else{
						//Popular Post 
						query_posts(
							array( 
							'post_type' => 'post',
							'posts_per_page' => 3,
							'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'terms' => $category,
									'field' => 'term_id',
								)
							),
							'orderby' => 'title',
							'order' => 'ASC' )
						);
					}
			?>
				<ul class="text-parent-cp" id="bx-pager">
				<?php 
					$counter_news = 0;
					if(have_posts()){
						while ( have_posts() ) { 
							the_post();
							global $post,$post_id;?>
								<li><a data-slide-index="<?php echo esc_attr($counter_news);?>"><?php echo get_the_title();?></a></li>
						<?php 
							$counter_news++;
						}
					}
						
					?>
				</ul>
				<ul id="news-<?php echo esc_attr($counter)?>" class="timelines-slider post-list">
					<?php
					if(have_posts()){
					while ( have_posts() ) { 
						the_post();
						global $post,$post_id;
						//Post Extra Information
						$thumbnail_types = '';
						$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
						if($post_detail_xml <> ''){
							$cp_post_xml = new DOMDocument ();
							$cp_post_xml->loadXML ( $post_detail_xml );
							$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
							$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
							$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');				
						}
						$width_class_first = '';
						$thumbnail_id = get_post_thumbnail_id( $post->ID );
						$thumbnail = wp_get_attachment_image_src( $thumbnail_id , array(1600,900) );?>
						<li>
							<?php if($thumbnail[1].'x'.$thumbnail[2] == '1600x900'){ ?><figure><a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_post_thumbnail($post->ID, array(1600,900));?></a></figure>
							<div class="caption">
								<p><?php echo esc_attr(strip_tags(mb_substr(get_the_content(),0,$num_excerpt)));?></p>
							</div>
							<?php }?>
						</li>
					<?php }
						wp_reset_postdata();
					}wp_reset_query();
					?>	
				</ul>
				
			</div>
		<?php
	}
	
	//Latest News For Site
	function cp_print_featured_item($item_xml){
		global $post,$counter;
		
		//Fetch elements data from database
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$number_posts = cp_find_xml_value($item_xml, 'number-of-posts');
		?>
		<div class="latest_posts acc-style">
		
		<?php 
		//Condition for Header
		if($header <> ''){ echo '<h3>'.($header).'</h3>';} ?>
		
			<div class="css3accordion">
				<ul id="feature-<?php echo esc_attr($counter)?>" class="css3accordion-cp">
					<?php
					if($category == '0'){
					//Popular Post 
						query_posts(
							array( 
							'post_type' => 'post',
							'posts_per_page' => 3,
							'orderby' => 'title',
							'order' => 'ASC' )
						);
					
					}else{
						//Popular Post 
						query_posts(
							array( 
							'post_type' => 'post',
							'posts_per_page' => 3,
							'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'terms' => $category,
									'field' => 'term_id',
								)
							),
							'orderby' => 'title',
							'order' => 'ASC' )
						);
					}
					if(have_posts()){
						while ( have_posts() ) { 
						the_post();
						global $post,$post_id;
						//Post Extra Information
						$thumbnail_types = '';
						$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
						if($post_detail_xml <> ''){
							$cp_post_xml = new DOMDocument ();
							$cp_post_xml->loadXML ( $post_detail_xml );
							$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
							$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
							$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');				
						}
						$width_class_first = '';
						$thumbnail_id = get_post_thumbnail_id( $post->ID );
						$thumbnail = wp_get_attachment_image_src( $thumbnail_id , array(570,300) );?>
						<li>
							<div class="inner-acc">
								<a href="<?php echo esc_url(get_permalink());?>" class="thumb hoverBorder"><?php echo get_the_post_thumbnail($post->ID, array(570,300));?></a>
								<div class="content">
									 <div class="top">
									 <?php $archive_year  = get_the_time('Y'); $archive_month = get_the_time('m'); $archive_day   = get_the_time('d'); ?>
										<a href="<?php echo esc_url(get_day_link( $archive_year, $archive_month, $archive_day)); ?>"><strong class="mnt"><i class="fa fa-calendar"></i> <?php echo get_the_date();?></strong></a>
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
									</div>
									<strong class="title"><?php echo esc_attr(substr(get_the_title(),0,26));?></strong>
									<p><?php echo strip_tags(mb_substr(get_the_content(),0,65));?></p>
									<a href="<?php echo esc_url(get_permalink());?>" class="readmore"><?php esc_html_e('Read More','neomag');?></a>
								</div>
							</div>
						</li>
					<?php } //end while 
						wp_reset_postdata();
					} wp_reset_query();
					?>	
				</ul>
			</div>
		</div>	
		<?php
	}
	
	
	//Latest News Element
	function cp_print_latest_news_item($item_xml){ 
		global $post,$counter;
		
		//Fetch elements data from database
		$header = cp_find_xml_value($item_xml, 'header');
		$category = cp_find_xml_value($item_xml, 'category');
		$number_posts = cp_find_xml_value($item_xml, 'number-of-posts');
		$news_style = cp_find_xml_value($item_xml, 'news_style');
		
		//Condition for slider
		if ($news_style == 'Slider'){?>
		<!-- Latest News HTML -->
		<div class="latest-news">
		<?php 
		//Condition for Header
		if($header <> ''){ echo '<h2>'.($header).'</h2>';} ?>
			<div class="news-section">
				<!-- Slider Code Here-->
				<script type="text/javascript">
				jQuery(document).ready(function ($){
					"use strict";
					if ($('#news-<?php echo esc_js($counter)?>').length) {
						$('#news-<?php echo esc_js($counter)?>').bxSlider({
							minSlides: 3,
							maxSlides: 8,
							mode:'vertical',
							pager:false,
							auto:false
						});
					}
				});
				</script>
				<!-- Slider Code Ends Here-->
				<ul id='news-<?php echo esc_attr($counter)?>' class="news-slider">
					<?php
					if($category == '0'){
						query_posts(
							array( 
							'post_type' => 'post',
							'posts_per_page' => $number_posts,
							'orderby' => 'date',
							'order' => 'DESC' )
						);
					}else{
						query_posts(
							array( 
							'post_type' => 'post',
							'posts_per_page' => $number_posts,
							'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'terms' => $category,
									'field' => 'term_id',
								)
							),
							'orderby' => 'date',
							'order' => 'DESC' )
						);
					}
					if(have_posts()){
						while ( have_posts() ) { 
							the_post();
							global $post,$post_id;
							//Post Extra Information
							$thumbnail_types = '';
							$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
							if($post_detail_xml <> ''){
								$cp_post_xml = new DOMDocument ();
								$cp_post_xml->loadXML ( $post_detail_xml );
								$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
								$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
								$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');				
							}
							$width_class_first = '';
							$thumbnail_id = get_post_thumbnail_id( $post->ID );
							$thumbnail = wp_get_attachment_image_src( $thumbnail_id , array(150,100) );?>
                            
							<!-- Repeatable Code Begins Here -->
							<li class="slide">
								<div class="thumbnail clearfix">
									 <a class="img" href = "<?php echo esc_url(get_permalink());?>"><?php echo get_the_post_thumbnail($post->ID, array(150,100));?></a>
										<div class="new-caption">
											<strong class="date"><?php echo get_the_date();?></strong>
											<strong class="ntitle"><a href = "<?php echo esc_url(get_permalink());?>">
											<?php 
												$title = get_the_title();
													if (strlen($title) < 35){ 
														echo get_the_title();
													}
													else {
														echo substr(get_the_title(),0,35);
														echo '...';
													}
											?>
											</a></strong>
											<p><?php echo strip_tags(mb_substr(get_the_content(),0,65));?><a href="<?php echo esc_url(get_permalink());?>" title="<?php echo get_the_title();?>"> <?php esc_html_e('Read More [+]','neomag');?></a></p>
										</div>
								</div>
							</li>
						<?php } //end while
						wp_reset_query();
					} wp_reset_postdata();
					?>			
				</ul>
			</div>		
		</div>
	<?php
	}else { ?>
			         
            	<div class="cp-home-latest-news gap-81">
                  <div class="container">
                    <div class="row">
                       <div class="col-md-12">
                          <h2 class="sec-title"><?php if($header <> ''){ echo esc_attr($header); } ?></h2>
                       </div>
                                      
					<div id='news-<?php echo esc_attr($counter)?>' class="news-slider">
						<?php
						if($category == '0'){
							query_posts(
								array( 
								'post_type' => 'post',
								'posts_per_page' => $number_posts,
								'orderby' => 'date',
								'order' => 'DESC' )
							);
						}else{
							query_posts(
								array( 
								'post_type' => 'post',
								'posts_per_page' => $number_posts,
								'tax_query' => array(
									array(
										'taxonomy' => 'category',
										'terms' => $category,
										'field' => 'term_id',
									)
								),
								'orderby' => 'date',
								'order' => 'DESC' )
							);
						}
						if(have_posts()){
							while ( have_posts() ) { 
								the_post();
								global $post,$post_id;
								//Post Extra Information
								$thumbnail_types = '';
								$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
								if($post_detail_xml <> ''){
									$cp_post_xml = new DOMDocument ();
									$cp_post_xml->loadXML ( $post_detail_xml );
									$thumbnail_types = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
									$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
									$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');				
								}
								$width_class_first = '';
								$thumbnail_id = get_post_thumbnail_id( $post->ID );
								$thumbnail = wp_get_attachment_image_src( $thumbnail_id , array(380,200) );?>
                               <!--Home Latest News Start-->
                                      <div class="col-md-4">
                                        <div class="news-box">
                                          <div class="thumb"><?php echo get_the_post_thumbnail($post->ID, array(380,200));?></div>
                                          <div class="news-caption">
                                            <div class="date"><?php echo get_the_time('d'); ?> <span> / <?php echo get_the_time('M'); ?></span></div>
                                            <h3><a href="<?php echo esc_url(get_permalink());?>">
                                            <?php 
													$title = get_the_title();
													if (strlen($title) < 28){ 
														echo get_the_title();
													}
													else {
														echo substr(get_the_title(),0,28);
														echo '...';
													}
												 ?>
                                            </a></h3>
                                            <p><?php echo strip_tags(mb_substr(get_the_content(),0,95));?></p>
                                          </div>
                                        </div>
                                      </div>
                                <!--Home Latest News End--> 
							<?php 
							} //end while
							wp_reset_postdata();
						} wp_reset_query(); 
						?>			
					</div>
                  </div>
                </div>
              </div>
		<?php
		}
	}// function ends
	
	 $archive_year  = get_the_time('Y'); $archive_month = get_the_time('m');