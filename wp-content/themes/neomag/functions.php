<?php 
	/*	
	*	CrunchPress function.php
	*	---------------------------------------------------------------------
	* 	@version	1.0
	*   @ Package   NeoMag Theme
	* 	@author		CrunchPress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) CrunchPress
	*	---------------------------------------------------------------------
	*	This file contains all important functions and features of the theme.
	*	---------------------------------------------------------------------
	*/	

	// constants
	define('NEOMAG_SMALL','cp');                                   // Short name of theme (used for various purpose in CP framework)
	define('NEOMAG_FULL','NeoMag Theme Panel');                    // Full name of theme (used for various purpose in CP framework)
	// logical location for CP framework
	if(!defined( 'NEOMAG_PATH_URL' )){ define('NEOMAG_PATH_URL', get_template_directory_uri());}
	// Physical location for CP framework
	if(!defined( 'NEOMAG_PATH_SER' )){define('NEOMAG_PATH_SER', get_template_directory() );}            
	// Define URL path of framework directory
	if(!defined( 'NEOMAG_FW_URL' )){define( 'NEOMAG_FW_URL', NEOMAG_PATH_URL . '/framework' );}
	// Define server path of framework directory             
	if(!defined( 'NEOMAG_FW' )){define( 'NEOMAG_FW', NEOMAG_PATH_SER . '/framework' );}
	// Define admin url
	if(!defined( 'AJAX_URL' )){define('AJAX_URL', admin_url( 'admin-ajax.php' ));}

	$neomag_date_format = get_option(NEOMAG_SMALL.'_default_date_format','F d, Y');                     // Get default date format
	$neomag_widget_date_format = get_option(NEOMAG_SMALL.'_default_widget_date_format','M d, Y');       // Get default date format for widgets
	define('GDL_DATE_FORMAT', $neomag_date_format);
	define('GDL_WIDGET_DATE_FORMAT', $neomag_widget_date_format);
	$neomag_cp_is_responsive = 'enable';
	$neomag_cp_is_responsive = ($neomag_cp_is_responsive == 'enable')? true: false;
	
	$neomag_default_post_sidebar = get_option(NEOMAG_SMALL.'_default_post_sidebar','post-no-sidebar');   // Get default post sidebar
	$neomag_default_post_sidebar = str_replace('post-', '', $neomag_default_post_sidebar);               
	$neomag_default_post_left_sidebar = get_option(NEOMAG_SMALL.'_default_post_left_sidebar','');        // Get option for left sidebar
	$neomag_default_post_right_sidebar = get_option(NEOMAG_SMALL.'_default_post_right_sidebar','');      // Get option for right sidebar


	//Blog Post Image Size
	add_image_size('neomag_slider',1800,528, true);
	add_image_size('neomag_std_post',849, 342, true);	
	add_image_size('neomag_gallery1',554, 323, true);	
	add_image_size('neomag_gallery2',450, 323, true);
	add_image_size('neomag_gallery3',380, 514, true);
	add_image_size('neomag_gallery4',380, 257, true);
	add_image_size('neomag_gallery5',326, 280, true);
	add_image_size('neomag_gallery6',218, 280, true);

	add_action( 'after_setup_theme', 'neomag_theme_setup', 20 );


	function neomag_theme_setup() {

	
	if ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443 ) {
		define('CP_HTTP', 'https://');
	}else{
		define('CP_HTTP', 'http://');
	}
	
    add_theme_support( 'post-formats', array( 'audio', 'quote', 'video', 'chat', 'link' ) );
	//Declare WooCommerce Support
	add_theme_support( 'woocommerce' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'neomag_cp_get_featured_posts',
		'max_posts' => 6,
	) );

	add_action('woocommerce_before_main_content', 'neomag_wrapper_start', 10);
	add_action('woocommerce_after_main_content', 'neomag_wrapper_end', 10);
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
	add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
	add_action('woocommerce_before_main_content', 'neomag_woocommerce_remove_breadcrumb');
	add_action( 'woo_custom_breadcrumb', 'neomag_woocommerce_custom_breadcrumb' );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	/*remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );*/
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	/*remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );*/
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	/*remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );*/
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 40 );
	add_action( 'init' , 'neomag_add_categories_to_attachments' ); 
 	// Register your custom function to override some LayerSlider data
    add_action('layerslider_ready', 'neomag_cp_layerslider_overrides');
	// Remove issues with prefetching adding extra views
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	add_action('the_content', 'neomag_single_content');
	
	add_filter('get_avatar','neomag_cp_add_avatar_css');
	add_filter('user_contactmethods', 'neomag_cp_modify_contact_methods');
	add_filter( 'wp_title', 'neomag_cp_wp_title', 10, 2 );
	add_filter('wp_generate_tag_cloud', 'neomag_tag_cloud',10,3);
	// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
	add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
	add_filter( 'registration_redirect', 'neo_mag_registration_redirect' );


	//register the location of menus
	register_nav_menus(array('header-menu'=>'Main Menu'));
	
	}

	if( !function_exists('neomag_get_root_directory') ){                                                 // Get file path ( to support child theme )
		function neomag_get_root_directory( $path ){
			if( file_exists( get_stylesheet_directory() . '/' . $path ) ){
				return get_stylesheet_directory() . '/';
			}else{
				return get_stylesheet_directory() . '/';
			}
		}
	}
	
	// include essential files to enhance framework functionality
	include_once(NEOMAG_FW.	'/script-handler.php');							// It includes all javacript and style in theme
	include_once(NEOMAG_FW.	'/extensions/super-object.php'); 				// Super object function
	include_once(NEOMAG_FW.	'/extensions/aq_resizer.php'); 				// Runtime image resizer script
	include_once(NEOMAG_FW.	'/cp-functions.php'); 							// Registered CP framework functions
	include_once(NEOMAG_FW.	'/cp-option.php');								// CP framework control panel
	include_once(NEOMAG_FW.	'/cp_options_typography.php');					// CP Typography control panel
	include_once(NEOMAG_FW.	'/cp_options_slider.php');						// CP Slider control panel
	include_once(NEOMAG_FW.	'/cp_options_social.php');						// CP Social Sharing
	include_once(NEOMAG_FW.	'/cp_options_sidebar.php');						// CP Sidebar Option Page
	include_once(NEOMAG_FW.	'/cp_options_default_pages.php');				// CP Default Options control panel
	include_once(NEOMAG_FW.	'/cp_options_newsletter.php');					// CP Newsletter control panel
	include_once(NEOMAG_FW.	'/cp_dummy_data_import.php');					// CP Dummy Data control panel
	// dashboard option
	include_once(NEOMAG_FW. '/options/meta-template.php'); 					// templates for post portfolio and gallery
	include_once(NEOMAG_FW. '/options/post-option.php');					// Register meta fields for post_type
	include_once(NEOMAG_FW. '/options/page-option.php'); 					// Register meta fields page post_type	
	include_once(NEOMAG_FW. '/options/product-option.php');					// WooCommerce Elements
	include_once(NEOMAG_FW. '/extensions/widgets/cp_popular_posts_widget.php'); // Custom Popular Posts
	include_once(NEOMAG_FW. '/extensions/widgets/cp_facebook_widget.php'); // Custom Facebook Widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_newsletter_widget.php'); // Custom NewsLetter
	include_once(NEOMAG_FW. '/extensions/widgets/cp_news_widget.php'); // Custom News Widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_flickr_widget.php'); // Custom Flicker Widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_about_theme_widget.php'); // Custom about theme widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_recent_posts_widget.php'); // Custom cp_recent_posts_widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_text_widget.php'); // Custom cp_text_widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_contact_us_widget.php'); // Custom contact us widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_author_widget.php'); // Custom Author widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_footer_newsletter_widget.php'); // Custom Footer newsletter widget
	include_once(NEOMAG_FW. '/extensions/widgets/cp_instagram_widget.php'); // Custom instagram widget
	include_once(NEOMAG_FW. '/extensions/contact_submit.php'); 				// Custom Or External Plugins
	include_once(NEOMAG_FW. '/extensions/plugins.php'); 				// Custom Or External Plugins

	if(!is_admin()){
		include_once(NEOMAG_FW. '/extensions/sliders.php');	                            // Functions to print sliders
		include_once(NEOMAG_FW. '/options/page-elements.php');	                        // Organize page item element
		include_once(NEOMAG_FW. '/options/blog-elements.php');							// Organize blog item element
	    include_once(NEOMAG_FW. '/extensions/comment.php'); 							// function to get list of comment
		include_once(NEOMAG_FW. '/extensions/pagination.php'); 							// Register pagination plugin
		include_once(NEOMAG_FW. '/extensions/social-shares.php'); 						// Register social shares 
		include_once(NEOMAG_FW.	'/extensions/loadstyle.php');                  // Register breadcrumbs navigation
		include_once(NEOMAG_FW.	'/extensions/breadcrumbs.php');                  // Register breadcrumbs navigation
		include_once(NEOMAG_FW.	'/extensions/featured-content.php');                  // Register Feature content
		include_once(NEOMAG_FW. '/extensions/cp-headers.php'); // Registered CP Header style
		include_once(NEOMAG_FW. '/extensions/cp-footers.php'); // Registered CP Header style
	}
	// Custom Function for avatar 
	function neomag_cp_add_avatar_css($neomag_class) {
		$neomag_class = str_replace("class='avatar", "class='avatar avatar-96 photo team-img margin", $neomag_class) ;
		return $neomag_class;		
	}

	function neomag_wrapper_start() {
		$neomag_select_layout_cp = '';	
		$neomag_cp_general_settings = get_option('general_settings');
		if($neomag_cp_general_settings <> ''){
			$neomag_cp_logo = new DOMDocument ();
			$neomag_cp_logo->loadXML ( $neomag_cp_general_settings );
			$neomag_select_layout_cp = cp_find_xml_value($neomag_cp_logo->documentElement,'select_layout_cp');
			
		}
		$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
		$neomag_header_style = '';
		//Print Style 6
		if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
			cp_print_header_html($neomag_header_style);
		}
		$neomag_header_style = '';
		$neomag_html_class = cp_print_header_class($neomag_header_style);
		?>
        <div class="inner-title">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2><?php if(is_single()){ echo get_the_title();}else{ woocommerce_page_title();};?></h2>
            </div>
            <div class="col-md-6">
            <?php if($neomag_breadcrumbs == 'enable'){
              echo do_action('woo_custom_breadcrumb');
             } ?>
            </div>
          </div>
        </div>
      </div>
	<div class="cp-page-content inner-page-content woocommerce">
        <div class="container">
	        <div class="<?php if(is_single()){ echo 'product-detail'; } else { echo 'product-listing'; } ?>">
<?php
	}
	function neomag_wrapper_end() {
	  echo '</div></div></div>';
	}

 if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
	function woocommerce_template_loop_product_thumbnail() {
		echo woocommerce_get_product_thumbnail();
	} 
 }
 if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	
	function woocommerce_get_product_thumbnail( $neomag_size = 'shop_catalog', $neomag_placeholder_width = 0, $neomag_placeholder_height = 0  ) {
		global $post, $woocommerce;
		if ( ! $neomag_placeholder_width )
			$neomag_placeholder_width = wc_get_image_size( 'shop_catalog_image_width' );
		if ( ! $neomag_placeholder_height )
			$neomag_placeholder_height = wc_get_image_size( 'shop_catalog_image_height' );
			
			if($neomag_placeholder_width == '' ||  $neomag_placeholder_height == '' || is_array($neomag_placeholder_width)){ $neomag_placeholder_width = 250; $neomag_placeholder_height = 264; } 
			$neomag_product_thumbnail_output = '';
			$neomag_product_thumbnail_output .= '<div class="thumb"><a href="'.get_the_permalink().'">';
			if ( has_post_thumbnail() ) {
				
				$neomag_product_thumbnail_output .= get_the_post_thumbnail( $post->ID, $neomag_size ); 
				
			} else {
				$neomag_product_thumbnail_output .= '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder" width="' . $neomag_placeholder_width . '" height="' . $neomag_placeholder_height . '" />';
			}
			
			$neomag_product_thumbnail_output .= '</a></div>';
			
			return $neomag_product_thumbnail_output;
	}
 }

	//Reposition WooCommerce breadcrumb 
	function neomag_woocommerce_remove_breadcrumb(){
		remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
	}

	function neomag_woocommerce_custom_breadcrumb(){
		woocommerce_breadcrumb();
	}

// add categories to attachments  
function neomag_add_categories_to_attachments() {
      register_taxonomy_for_object_type( 'category', 'attachment' );  
}  
	
	//Add Custom Field to user profile
	function neomag_cp_modify_contact_methods($profile_fields) {
		// Add new fields
		$profile_fields['twitter'] = 'Twitter URL';
		$profile_fields['facebook'] = 'Facebook URL';
		$profile_fields['gplus'] = 'Google+ URL';
		$profile_fields['linked'] = 'Linked in URL';
		$profile_fields['skype'] = 'Skype ID';
		return $profile_fields;
	}
	
	//Feature Post function
	function neomag_cp_get_featured_posts() {
		return apply_filters( 'neomag_cp_get_featured_posts', array() );
	}
	//Feature Post function
	function cp_has_featured_posts() {
		return ! is_paged() && (bool) neomag_cp_get_featured_posts();
	}
 
    function neomag_cp_layerslider_overrides() {
 
        // Disable auto-updates
        $GLOBALS['lsAutoUpdateBox'] = false;
    }
	
	function neomag_cp_admin_notice_framework() { ?>
		<div class="updated">
			<p><strong><?php esc_html_e( 'Please install theme required plug-ins to use all functionalities of theme','neomag' ); ?></strong> - <?php esc_html_e('in case of deactivating the theme required plug-ins you may not able to use theme extra functionality.','neomag');?></p>
		</div>
		<?php
	}
	
	//Maintenance Mode Admin Notice
	$mm = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');
	if($mm == 'enable'){
		add_action( 'admin_notices', 'neomag_cp_admin_notice_maintenance_mode' );
	}
	function neomag_cp_admin_notice_maintenance_mode() { ?>
		<div class="error">
			<p><strong><?php esc_html_e( 'Theme has activated maintenance mode!','neomag' ); ?></strong> - <?php esc_html_e('Please turn it off from Cp Theme Panel > General Settings > Maintenance Mode Settings > Maintenance Mode (On/Off).','neomag');?></p>
		</div>
		<?php
	}
	
	function neomag_cp_wp_title( $neomag_title, $neomag_sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $neomag_title;
		}
		// Add the site name.
		$neomag_title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$neomag_site_description = get_bloginfo( 'description', 'display' );
		if ( $neomag_site_description && ( is_home() || is_front_page() ) ) {
			$neomag_title = "$neomag_title $neomag_sep $neomag_site_description";
		}
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 ) {
			$neomag_title = "$neomag_title $neomag_sep " . sprintf( esc_html__( 'Page %s','neomag' ), max( $paged, $page ) );
		}
		return $neomag_title;
	}

function neomag_tag_cloud($neomag_tag_string){
   return preg_replace("/style='font-size:.+pt;'/", '', $neomag_tag_string);
}

function woocommerce_header_add_to_cart_fragment( $neomag_fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart','neomag' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ,'neomag'), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a> 
	<?php
	
	$neomag_fragments['a.cart-contents'] = ob_get_clean();
	
	return $neomag_fragments;
}

// Extract first occurance of text from a string
function neo_mag_extract_link($neomag_start, $neomag_end, $neomag_tring) {
	$neomag_tring = stristr($neomag_tring, $neomag_start);
	$neomag_trimmed = stristr($neomag_tring, $neomag_end);
	return substr($neomag_tring, strlen($neomag_start), -strlen($neomag_trimmed));
}
function neo_mag_registration_redirect()
{
return home_url( '/' );
}

function neo_mag_catch_that_image() {
	global $post, $posts;
	$neo_mag_first_img = '';
	ob_start();
	ob_end_clean();
	$neo_mag_catch_that_image_output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	if($neo_mag_catch_that_image_output <> ''){
	$neo_mag_first_img = $matches [1] [0];
	} else { $neo_mag_first_img = ''; }
	return $neo_mag_first_img;
}
function neomag_get_header_style(){
		global $post, $post_id;
		$neomag_header_style = get_post_meta ( $post->ID, "page-option-top-header-style", true );
		return $neomag_header_style;
	}

function neomag_single_content($neo_mag_content){

		if(is_single()){ ?>
		<script>
		 jQuery(document).ready(function($) {
		$('.cp-grid-gallery-post').masonry({
		  // options
		  itemSelector: '.grid',
		  columnWidth: 215
		});
		  });
		</script>
		<?php } return $neo_mag_content;	
}

// limit number of the tags cloud
add_filter('widget_tag_cloud_args', 'limit_tag_in_tag_cloud');
function limit_tag_in_tag_cloud($args){
    if(isset($args['taxonomy']) && $args['taxonomy'] == 'post_tag'){
        $args['number'] = 5; // number of tags to return
    }
    return $args;
}