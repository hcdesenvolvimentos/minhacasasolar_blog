<?php 
/*	
*	CrunchPress Headers File
*	---------------------------------------------------------------------
* 	@version	1.0
* 	@author		CrunchPress
* 	@link		http://crunchpress.com
* 	@copyright	Copyright (c) CrunchPress
*	---------------------------------------------------------------------
*	This file Contain all the custom Built in function 
*	Developer Note: do not update this file.
*	---------------------------------------------------------------------
*/
	
	function cp_footer_style_1(){ ?>
    
      <!--FOOTER START-->

  <footer class="cp-footer">
    <div class="footer-top">
      <div class="container">
        <div class="row"> 
			<?php dynamic_sidebar('sidebar-footer'); ?>
        </div>
      </div>
    </div>
    
    
    <div class="footer-mid">
    <?php dynamic_sidebar('sidebar-Upper-Footer'); ?>
    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/fotos_footer.jpg" >
    </div>
    
    
    <div class="cp-footer-bottom">
    
        <?php $copyright_text = neomag_cp_get_themeoption_value('copyright_code','general_settings'); echo '<p>'.esc_attr($copyright_text).'</p>'; ?>
    
    </div>
    
  </footer>
  
  <!--FOOTER END--> 
  <?php if(is_page('signup')||is_page('gallery-masonary')||is_page('about-us')||is_page(79)||is_page(80)||is_page(81)||is_single('296')){ } else{  ?>  
</div>
<?php } if(is_page('54')||is_page('60')||is_page('479')){ echo '</div>'; } ?>
<!--WRAPPER END--> 
	<?php }
	
	function cp_footer_style_2(){ ?>
	<footer id="footer" class = "cp_footer_2">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('sidebar-footer'); ?>
				</div>
			</div>
		</div>
		<div class="footer-mid">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php cp_footer_logo_int('social');?>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 pull-right">
						<?php $copyright_text = neomag_cp_get_themeoption_value('copyright_code','general_settings');
							echo esc_attr($copyright_text);
					  ?>
						
					</div>
				</div>
			</div>
            
            <a class="pull-right backtop" href="#" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
            
		</div>
	</footer>
	<?php }
	
	function cp_footer_style_3(){ ?>
	<footer id="footer" class = "cp_footer_3">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('sidebar-footer'); ?>
				</div>
			</div>
		</div>
		<div class="footer-mid text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php cp_footer_logo_int('social');?>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">
		  <div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php $copyright_text = neomag_cp_get_themeoption_value('copyright_code','general_settings');
							echo esc_attr($copyright_text);
					  ?>
				</div>
			</div>
		  </div>
          
          <a class="pull-right backtop" href="#" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
          
		</div>
	</footer>
		
	<?php }
	
	function cp_footer_style_4(){ ?>
	<footer id="footer" class="footer cp_footer_4">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('sidebar-footer'); ?>
				</div>
			</div>
		</div>
		<div class="footer-mid">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php // Footer Social Icons
							$topsocial_icon_footer = neomag_cp_get_themeoption_value('social_networking','general_settings');
							if(esc_attr($topsocial_icon_footer) == 'enable'){ 
								 cp_social_icons_list('social'); 
							} 
						?>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 pull-right">
						<?php $copyright_text = neomag_cp_get_themeoption_value('copyright_code','general_settings');
							echo esc_attr($copyright_text);
					  ?>
                        
					</div>
				</div>
			</div>
            
            <a class="pull-right backtop" href="#" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
            
		</div>
	</footer>
  
	<?php }
	
	function cp_footer_style_5(){ ?>
		<footer id="footer" class = "cp_footer_5">
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar('sidebar-footer'); ?>
					</div>
				</div>
			</div>
			<div class="footer-mid">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<?php cp_footer_logo_int();?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 social_icons_right">
						<?php 
						// Footer Social Icons
							$topsocial_icon_footer = neomag_cp_get_themeoption_value('social_networking','general_settings');
							if(esc_attr($topsocial_icon_footer) == 'enable'){ 
								cp_social_icons_list('social');
							} 
						?>
						</div>	
					</div>
				</div>
			</div>
			<!-- /.Copyright ./-->
			<div class="copyright-row">
			  <div class="container">
				<div class="row">
					<div class="col-lg-12">
					  <?php $copyright_text = neomag_cp_get_themeoption_value('copyright_code','general_settings');
							echo esc_attr($copyright_text);
					  ?>
					</div>
				</div>
			  </div>
              
              <a class="pull-right backtop" href="#" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
              
			</div>
		</footer>
		<!--FOOTER SECTION START-->
	<?php }
	
	
	function cp_footer_style_6(){ ?>
		<footer id="footer" class="footer cp_footer_6">
			<div class="footer-mid text-center">
				<div class="container">
					<div class="row">
						<div class="col-md-12">							
							<?php cp_footer_logo_int('social');?>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<?php $copyright_text = neomag_cp_get_themeoption_value('copyright_code','general_settings');
								echo esc_attr($copyright_text);
							?>
						</div>
					</div>
				</div>
                
                <a class="pull-right backtop" href="#" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
                
			</div>
		</footer>
	<?php }
	
	//page-option-bottom-footer-style
	
	function cp_footer_html($footer=""){
		
		$neomag_footer_style_apply = neomag_cp_get_themeoption_value('footer_style_apply','general_settings');
		if(esc_attr($neomag_footer_style_apply) == 'enable'){$footer = 'enable';}else{}
		
		if(($footer) == 'Style 1'){
			cp_footer_style_1();
		}else if(($footer) == 'Style 2'){
			cp_footer_style_2();
		}else if(($footer) == 'Style 3'){
			cp_footer_style_3();
		}else if(($footer) == 'Style 4'){
			cp_footer_style_4();
		}else if(($footer) == 'Style 5'){
			cp_footer_style_5();
		}else if(($footer) == 'Style 6'){
			cp_footer_style_6();
		}else{
			$select_footer_cp = neomag_cp_get_themeoption_value('select_footer_cp','general_settings');
			if(($select_footer_cp) == 'Style 1'){
				cp_footer_style_1();
			}else if(($select_footer_cp) == 'Style 2'){
				cp_footer_style_2();
			}else if(($select_footer_cp) == 'Style 3'){
				cp_footer_style_3();
			}else if(($select_footer_cp) == 'Style 4'){
				cp_footer_style_4();
			}else if(($select_footer_cp) == 'Style 5'){
				cp_footer_style_5();
			}else if(($select_footer_cp) == 'Style 6'){
				cp_footer_style_6();
			}else{
				cp_footer_style_1();
			}
		}
	}