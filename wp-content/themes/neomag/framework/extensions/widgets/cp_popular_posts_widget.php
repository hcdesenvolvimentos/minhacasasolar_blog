<?php
class popular_post extends WP_Widget
{
  function popular_post()
  {
    $widget_ops = array('classname' => 'featured-posts', 'description' => 'Select Category to show its Popular Posts' );
    parent::__construct('popular_post', 'CrunchPress : Show Popular Posts', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
	$get_cate_posts = isset( $instance['get_cate_posts'] ) ? ( $instance['get_cate_posts'] ) : '';
	$nop = isset( $instance['nop'] ) ? ( $instance['nop'] ) : '';
?>
  <p>
  <label for="<?php echo ($this->get_field_id('title')); ?>">
	 <?php esc_html_e('Title:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('title')); ?>" name="<?php echo ($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </label>
  </p>   
  <p>
  <label for="<?php echo ($this->get_field_id('nop')); ?>">
	 <?php esc_html_e('Number of Posts To Display:','neomag');?> 
	  <input class="widefat" size="2" id="<?php echo ($this->get_field_id('nop')); ?>" name="<?php echo ($this->get_field_name('nop')); ?>" type="text" value="<?php echo esc_attr($nop); ?>" />
  </label>
  </p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
  
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    $instance['get_cate_posts'] = $new_instance['get_cate_posts'];	
	$instance['nop'] = $new_instance['nop'];
    return $instance;
  }
 
	function widget($args, $instance)
	{
		
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$get_cate_posts = isset( $instance['get_cate_posts'] ) ? ( $instance['get_cate_posts'] ) : '';		
		$nop = isset( $instance['nop'] ) ? ( $instance['nop'] ) : '';	
	
		if($nop == ""){$nop = '-1';} 
		echo html_entity_decode($before_widget);	
		// WIDGET display CODE Start
		
		if (!empty($title)){
			echo html_entity_decode($before_title);
			echo esc_attr($title);
			echo html_entity_decode($after_title);
		}
			global $wpdb, $post;
			?>
			<div class="widget-content" id="featuredpost">
                <ul class="fpost-list">
			<?php 
				$category_array = get_term_by('id', esc_attr($get_cate_posts), 'recipe-category');
				$popularpost = new WP_Query( array( 'ignore_sticky_posts' => true,'posts_per_page' => $nop, 'post_type'=> 'post', 'meta_key' => 'popular_post_views_count', 'orderby' => 'popular_post_views_count meta_value_num', 'order' => 'DESC'  ) ); 
					while ( $popularpost->have_posts() ) : $popularpost->the_post();
					global $post;
					$post_social = '';
					$sidebars = '';
					$right_sidebar_post = '';
					$left_sidebar_post = '';
					$post_thumbnail = '';
					$video_url_type = '';
					$select_slider_type = '';
					$post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
					if($post_detail_xml <> ''){
						$cp_post_xml = new DOMDocument ();
						$cp_post_xml->loadXML ( $post_detail_xml );
						$post_social = cp_find_xml_value($cp_post_xml->documentElement,'post_social');
						$sidebars = cp_find_xml_value($cp_post_xml->documentElement,'sidebar_post');
						$right_sidebar_post = cp_find_xml_value($cp_post_xml->documentElement,'right_sidebar_post');
						$left_sidebar_post = cp_find_xml_value($cp_post_xml->documentElement,'left_sidebar_post');
						$post_thumbnail = cp_find_xml_value($cp_post_xml->documentElement,'post_thumbnail');
						$video_url_type = cp_find_xml_value($cp_post_xml->documentElement,'video_url_type');
						$select_slider_type = cp_find_xml_value($cp_post_xml->documentElement,'select_slider_type');			
					}
					?>
                              <?php
                                    $get_category_obj = get_the_category ( $post->ID ); 
                                    $category_name = array_shift( $get_category_obj );
								    $category_id = get_cat_ID( $category_name->name );
    								$category_link = get_category_link( $category_id );
                              ?>

				<!-- Widget Popular Post Code -->
                  <li> 
                  <?php echo get_the_post_thumbnail($post->ID, array(83,57));?>
                    <div class="cp-post-content">
                      <h5><a href="<?php echo esc_url(get_permalink());?>"><?php echo substr(get_the_title(),0,10);?>..</a></h5>
                      <ul class="cp-post-meta">
                        <li><a><?php echo (get_the_date('M'));?> <?php echo get_the_date('n');?></a></li>
                        <li><a href="<?php echo esc_url($category_link); ?>"><?php echo substr(($category_name->name),0,7); ?></a></li>
                      </ul>
                    </div>
                  </li>
				<!--Widget Code Popular Post END -->

				<?php endwhile; wp_reset_query(); ?>
			</ul>
          </div>
<?php 
	
	echo html_entity_decode($after_widget);
		}
	}
add_action( 'widgets_init', create_function('', 'return register_widget("popular_post");') );?>