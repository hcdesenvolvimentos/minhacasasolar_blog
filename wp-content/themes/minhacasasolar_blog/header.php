<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Minha_Casa_Solar
 */

global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- FAVICON -->
	<link rel="shortcut icon" type="ge/x-icon" href="https://recursos.minhacasasolar.com.br/i/favicon.ico" /> 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-12946981-3"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-12946981-3');
</script>
<!-- TOPO -->
<header class="topo">
	<div class="containerLagura containerMenu">
		<div class="row">
			
			<!-- LOGO -->
			<div class="col-md-3 logo">
				<a href="<?php echo home_url('/'); ?>">
					<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
				</a>
			</div>

			<!-- MENU  -->	
			<div class="col-md-9">
				<div class="navbar" role="navigation">	
									
					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
							<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Minha Casa Solar',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
							<div class="pesquisar">
										<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
											<input type="text" name="s" id="search" placeholder="Pesquise aqui!">
											<input type="submit">
										</form>
									</div>
						</nav>						
					</div>			
					
				</div>
			</div>
		</div>
	</div>

</header>

<!-- TOPO -->
<header class="topo topoFixo">
	<div class="containerLagura containerMenu">
		<div class="row">
			
			<!-- LOGO -->
			<div class="col-md-3 logo">
				<a href="<?php echo home_url('/'); ?>">
					<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
				</a>
			</div>

			<!-- MENU  -->	
			<div class="col-md-9">
				<div class="navbar" role="navigation">	
									
					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
							<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Minha Casa Solar',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
							<div class="pesquisar">
										<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
											<input type="text" name="s" id="search" placeholder="Pesquise aqui!">
											<input type="submit">
										</form>
									</div>
						</nav>						
					</div>			
					
				</div>
			</div>
		</div>
	</div>

</header>

<script>
	$(document).ready(function(){
		$('.sub-menu').addClass( "dropdown-menu" );
	});
</script>