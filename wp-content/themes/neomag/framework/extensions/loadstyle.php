<?php
	// Ajax to include font infomation
	add_action('wp_ajax_nopriv_cp_color_bg', 'cp_color_bg');
	add_action('wp_ajax_cp_color_bg','cp_color_bg');
	//function cp_color_bg($recieve_color='', $bg_texture='',$navi_color='',$head_text_color='',$color_font_body='',$select_layout_cp = '',$backend_on_off=''){
	function cp_color_bg($recieve_color = '' , $bg_texture='',$navi_color='', $heading_color = '', $body_color = '',$select_layout_cp = '', $backend_on_off = ''){
		
		global $html_new;
		
		/*
		================================================
						Create StyleSheet
		================================================
		*/
		$html_new .= '<style id="stylesheet">';
			
				/*
				================================================
							TEXT SELECTION COLOR 
				================================================
				*/
				$html_new .= '::selection {
					background: '.($recieve_color).'; /* Safari */
					color:#fff;
				}
				::-moz-selection {
					background: '.($recieve_color).'; /* Firefox */
					color:#fff;
				}';
				
				/*Main Red Background Color*/
				$html_new .= '.red-bg, .top-social, .footer-logo, .pro-bottom .pcart, .topbar, .home-latest .title, .sidebar-widget .btn-primary:hover, .comment-form input[type="submit"]:hover, .cart-coupon input[type="submit"]:hover, .drbox, .purchase, .main-nav ul.megamenu > li a:hover, .main-nav ul.megamenu > li.active, .slider-box li a:hover, .slider-box li.active, .home-testimonials .bx-wrapper .bx-pager.bx-default-pager a:hover, .home-testimonials .bx-wrapper .bx-pager.bx-default-pager a.active, ul.footer-links li a:hover, .megamenu > li:hover > a, .megamenu > li .active > a, .as-content .content-as .content .pro-area, .bs-hover .bs-content .content .pro-area, .grid-view li.pro-name, .readmore-button:hover, .top-social, .stuff-title > button:hover, .filter-form input[type="submit"]:hover, .csstransforms3d .megamenu a:hover span:before, .csstransforms3d .megamenu a:focus span:before, .csstransforms3d .megamenu a:hover span:before, .csstransforms3d .megamenu a:focus span:before, #loginForm input[type="submit"]:hover, #searchForm input[type="submit"]:hover, .footer-form input[type="button"]:hover, .submit_btn:hover, .register input[type="submit"]:hover, .login input[type="submit"]:hover, .sidebar-widget .btn-primary:hover, .comment-form input[type="submit"]:hover, .cart-coupon input[type="submit"]:hover, .cart-footer input[type="submit"]:hover, .top-search input[type="submit"]:hover, .latest-news .bx-wrapper .bx-prev:hover:after, .latest-news .bx-wrapper .bx-next:hover:after, .activemm, #header9 .topbar, #header9 .social, .btn-light:hover, .btn-light:focus, .thumb-style .caption strong:before, .services-list .icon span, .menu-list > li:hover .rate, .cp_our-menu .menu-tabs, .cp_facts-section, .latest-products .pro-bottom li.latest-pro-cart, .latest-pro-cart .add_to_cart_button, .latest-pro-cart .added_to_cart, .home-blog-container .blog-content h3, #footer .widget li:hover, #header9 .main-navbar .navbar-nav > li > a:hover, #header9 .main-navbar .navbar-nav > li.active > a, #header9 .main-navbar .navbar-default .navbar-nav > .open > a, #header9 .main-navbar .navbar-default .navbar-nav > .open > a:hover, #header9 .main-navbar .navbar-default .navbar-nav > .open > a:focus, #header9 .current-menu-item a, #header9 .main-navbar .navbar-nav li:hover, .content-block .captions, .simple-grid .product-box .bottom .title, .normal-grid .pro-box h4, .normal-grid .add_to_cart_button:after, .normal-grid .add_to_cart_button, .normal-grid .added_to_cart, .modern-grid .pro-content .rate, .fc.fc-ltr .fc-event-inner, .sidebar .widget_archive li:hover, .sidebar .widget_pages a:hover, .sidebar .widget_meta a:hover, .sidebar .widget_recent_entries a:hover, .sidebar .widget_nav_menu a:hover, .simple-grid .product-box .frame .bottom-row .like, .cp_search.open .cp_search-submit, .about-me-left h3, .testimonials .bx-wrapper .bx-pager.bx-default-pager a:hover, .testimonials .bx-wrapper .bx-pager.bx-default-pager a.active, .trainer-holder .caption, .match-detail .title, .match-detail h3.title, .em-bookings-cancel, #cp_header5 .navigation-row.zindex-item, #cp_header5 .navigation-row .logo, #cp_header_12 .home-menu .nav.navbar-nav li a:hover, 
				
				.pricing .btn-style:hover, .trainer-holder .frame em.title, #cp_header2 .bg-travel, .bg-travel-wrapper, 
				.event-post .listing_bookings, 
				.widget .tagcloud a:hover, #main-woo .onsale, .trave-home-blog .read-more:hover, .features-box a:hover, 
				.category_list_filterable a:hover, #cp_header7 .logo::before, #cp_header7 .logo::after, #cp_header7 .logo, .gallery-frame .caption .zoom:hover,
				#wrapper.wrapper .features-box a:hover, .page-numbers.current, .pagination a:hover, .css-events-list .em-pagination a.page-numbers:hover, 
.em-pagination .current, .pagination .page-numbers:hover, 
.pagination #pagination li a:hover, .em-pagination .page-numbers:hover, 
.woocommerce-pagination .page-numbers a.page-numbers:hover, 
.woocommerce-pagination .page-numbers span.current, #wrapper.wrapper .page-numbers.current, #wrapper.wrapper .woocommerce-pagination .page-numbers span.current,
.wrapper span.page-numbers.current
				 
				
				 {
					background-color:'.($recieve_color).'; 
					color:#fff;
				}';
				$html_new .= 'body p{ color: '.($recieve_color).'; }';
				/* Red Text Colours */
				$html_new .= '#loginButton span, .product-details .pro-head a:hover, .stuff-title h2 span, .testi-text span, .testimonials-left strong, .hot-deal .reviews, 
				.list-view .reviews, .brand-logo .bx-wrapper a.bx-prev:hover:after, .brand-logo .bx-wrapper a.bx-next:hover:after, .nbs-flexisel-ul li div.caption h4, 
				.iconbg2 > a, .share-post li a:hover, #header9 .topbar p a, .slide .thumbnail p a, .nbs-flexisel-item h4 a, .filter-links a, .blog-tools a:hover, 
				.content-block .captions .content button, .event-post .event-content button, .partner-logos .bx-wrapper a.bx-prev:hover:after, 
				.partner-logos .bx-wrapper a.bx-next:hover:after, #footer .widget .footer-social a:hover, .sidebar .footer-blog .thumbnail .caption a, 
				.modal-dialog.modal-sm > form .lost, #cp_header1 .navbar-default .navbar-nav li a:focus, 
				#cp_header2 .navigation-row .navbar-default .navbar-nav li a, a:hover, a:focus, .p404 a:hover, .nbs-flexisel-item .hover-links > a:hover,
				.blog-tools .readmore a:hover, .news-tools .readmore a:hover, .modern-grid-diagonal .fitem .cart a:hover, .frame-hover > a:hover,
				.modern-grid-diagonal .fitem .like a:hover, .modern-grid-diagonal .fitem .cart a:hover, .modern-grid-diagonal .pro-box .thumb .thumb-hover .cart a:hover,
				.modern-grid-diagonal .pro-box .thumb .thumb-hover .like a:hover, .blog-post-text a:hover, .summary.entry-summary .price ins
				
				
				 {
					color:'.($recieve_color).'; 
				}';
				/* Red Border Colours */
				$html_new .= '.slider #bx-pager > a:hover, .slider #bx-pager .active, .home-events, a.thumbnail:hover, a.thumbnail:focus, a.thumbnail.active
				 {
					border-color:'.($recieve_color).'; 
				}';
				
				/* Red Border Colours */
				$html_new .= '#header9 .social:before
				 {
					border-right-color:'.($recieve_color).'; 
				}';
				
				
				$html_new .= '.home-latest .title:before, .normal-grid .add_to_cart_button:before
				 {
					border-top-color:'.($recieve_color).';
				}';
				
				
					$html_new .= '.widget .tagcloud a:hover::before
				 {
					border-color: transparent transparent transparent '.($recieve_color).';
				}';
				
				/*=============================================
					Yellow Colors Start Secondary
				=============================================== */
				/* Text Color */
				$html_new .= '.wrapper .main-title h2, .wrapper .home-events-title h2 strong, ul#breadcrumb li.current, .woocommerce-breadcrumb, .blog-tools .readmore a, .news-tools .readmore a, .blog-post .blog-caption h4 a:hover, .news-post .news-caption h4 a:hover, .slide .thumbnail .date, .wrapper a:hover, .hot-deal .price, .list-view .price, .hot-deal .rating-stars, .list-view .rating-stars, .woocommerce .star-rating span, .woocommerce-page .star-rating span, .name, #footer .widget a:hover, ul#breadcrumb li.current, .woocommerce-breadcrumb, .iconbg2 > a:hover, .pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus, .wrapper a:hover, .main-title h2, .home-events-title h2, .hover-links a:hover, .content-block .captions .content h4 a:hover, .event-post .event-content h4 a:hover, .blog-post .blog-caption h4 a:hover, .news-post .news-caption h4 a:hover, .blog-tools .readmore a, .news-tools .readmore a, .auther-details .replay a:before, .iconbg2 > a:hover, .contact-icons a, .testimonial .icons .fa:hover, .about-pics strong, .about-pics span, .about-text .note span, #footer .social li a:hover, .copyright-row a, .name, .contact-form .fa, .inner-titlebg h4, .ticket-price > strong, .em-booking-login-form > a, #em-booking a, .yellow-text, .wrapper .inner-titlebg h4, ul#breadcrumb, .woocommerce-breadcrumb, .latest-product-slider .iwrapper:after, #footer .widget .footer-blog a, #header9 .topleft a:hover, .widget #calendar_wrap #today, .widget_em_widget ul li a, .slide .thumbnail .ntitle a:hover, a,
				.comments-list .text .post-time ul li a, .accordion-toggle > strong, .cart-collaterals .cart_totals .order-total, .shipping-calculator-button, .comments-list .comment-reply-link:before, .pagination > li > a:hover,
.pagination > li > span:hover, .p404 a, .topbar a:hover, .topbar a:active, .topbar a:focus, .pagination > li > a:focus, .slide .thumbnail p a:hover,
.pagination > li > span:focus, .time_circles > div, .time_circles > div > h4, .cp_wrapper1 .text h2 span, .cp_wrapper1 .text p span, .cp_wrapper1 .social-links li a:hover, .cp_wrapper2 .cp-cs-content span, .cp_wrapper2 .social-links li a:hover, #product_grid .woocommerce .star-rating, #product_grid .woocommerce-page .star-rating, #product_grid .pro-box .pro-footer .star-rating, .woocommerce .star-rating span, .woocommerce-page .star-rating span, .accordion-open p , .custom_accordion_cp.accordion-open, .wrapper .topbar a:hover
 				
				 {
					color: '.($body_color).';
				}';
				
				/* Background */
				$html_new .= '.nbs-flexisel-item .eventcd, #footer .wpcf7-form .wpcf7-submit, .events .eventcd, .event-content .eventcd, .fc.fc-ltr .fc-header, .wrapper .woocommerce-page #respond #submit.submit, .comment-form .form-submit .submit, .readmore-button, .side-banner .bx-wrapper .bx-pager.bx-default-pager a:hover, .side-banner .bx-wrapper .bx-pager.bx-default-pager a.active, .events .eventcd, .event-content .eventcd, .nbs-flexisel-item .eventcd, .wrapper .testimonial .title, .trave-home-blog .read-more, .hot-deal .like a, .hot-deal .cart a, .list-view .like a, .list-view .cart a, .custom_accordion_cp h3, .table th, .table .progress, .striker, .category_list_filterable .gdl-button.active, #signin .modal-content .btn-style, .modal-dialog.modal-sm #sing-up .btn-style, .full-fixture #booking_form #em-booking-submit, .full-fixture #booking_form #em_wp-submit, .btn-hover, .btn-dark-hover, #dbem-bookings-table thead, .widget .em-calendar-wrapper tbody td a, .value .reset_variations, .post-password-form input[type="submit"], .textwidget .wpcf7 .wpcf7-submit,
				#header9 .main-navbar .hcart, .widget #searchform input[type="submit"], .widget_product_search input[type="submit"], .pro-bottom .pcart:hover, .related.products ul.products li h3, #main-woo .products li h3, .woocommerce .shop_table.cart .actions .button, .woocommerce .shipping_calculator .button, #order_review #payment .button, .woocommerce .login .button, .entry-content-cp .checkout_coupon .button, .summary.entry-summary .button, .wrapper .woocommerce #respond input#submit.submit, .wrapper .woocommerce-page #respond #submit.submit, #place_order.button, .woocommerce .shop_table.cart thead, .em-search-submit, #horizontal-tabs .nav-tabs > li > a:hover, #horizontal-tabs .ui-tabs-active.ui-state-active a, #vertical-tabs .nav-tabs > li > a:hover, #vertical-tabs .ui-tabs-active.ui-state-active a, .sidebar .readmore, .sidebar .widget_shopping_cart_content .buttons .button, #header9 .topbar-dropdowns .dropdown-menu > li > a:hover, #header9 .topbar-dropdowns .dropdown-menu > li > a:focus, .related.products ul.products li h3, #main-woo .products li h3, .related.products .products .rel-box .add_to_cart_button, #main-woo .woo-cat-products .add_to_cart_button, 
				#main-woo .woo-cat-products .added_to_cart , .pricing .btn-style, .form-404 input#searchsubmit, .home-blog-container .post-meta .add_to_cart_button:hover,
				.simple-grid .product-box .frame .add_to_cart_button:hover, .simple-grid .product-box .frame .added_to_cart:hover
				
				
				 {
					background: '.($body_color).';
				}';
				
				$html_new .= '.blog-post-text blockquote, .news-post-text blockquote {
					border-bottom-color: '.($body_color).';
				}';
				
				$html_new .= '.side-banner .bx-wrapper .bx-pager.bx-default-pager a:hover, .side-banner .bx-wrapper .bx-pager.bx-default-pager a.active, .blog-post-text blockquote, .news-post-text blockquote, .blog-details > p:nth-last-child(1), .testimonial img, .team-box:hover, .about-text .quote, .cp_wrapper2 .social-links li a:hover {
					border-color: '.($body_color).';
				}';
				/* Secondary Colors Ends*/
				
				$html_new .= '.addtocart:before, .home .pro-box .add_to_cart_button:before, .addtocart:before, .normal-grid .pro-box .add_to_cart_button:before, .pro-box .thumb .sale,
				.classes-time .nav-tabs > li.active > a:after
				 {
					border-top-color:'.($recieve_color).';
				}';
				$html_new .= '.copyright-row:before{
					border-bottom:12px solid '.($recieve_color).';
				}';
				
				$html_new .= '.home-menu .navbar-nav > li > .sub-menu, .home-menu .menu ul > li .children, #header .nav > li > a:hover, #header .nav > li > a:focus, #header .navbar-nav > li > .sub-menu, #header .menu ul > li .children,
				.home-menu .navbar-nav > li ul.sub-menu li ul.sub-menu, .home-menu .menu > li ul.children li ul.children,
				.pricing-table-curve  .price-table:hover .pricing-footer
				
				{
					border-bottom-color:'.($recieve_color).';
				}';
				/* CSS Footer */
				$html_new .= '.ftop {
					background:#C0617D
				}';
				/*Background Color*/
				$html_new .= '#cp_header6 .navigation-row, #cp_header6 .search-box .search, #cp_header1 .navigation-row, #cp_header4 .navigation-row, .cp_search_box, #cp_header2 .navbar-nav li a:hover,#cp_header2 .navbar-nav li a:focus,#cp_header2 .navbar-nav li a.active,#cp_header2 .search,#cp_header3 .logo-row,#cp_header3 .navbar-nav li a:hover,#cp_header3 .navbar-nav li a:focus,#cp_header3 .navbar-nav li a.active, .pro-box button.add_to_cart_button, .pro-box .added_to_cart, product-box .bottom strong.price, .added_to_cart, .onsale, .widget_search #searchsubmit, .sidebar #searchform input[type="submit"], .sidebar .widget_product_search #searchform input[type="submit"], .sidebar .newsletter-box button,
				.about .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, #header-login, .about .nav-tabs > li a, .percentage, .progress span,
				.p404 .buttons, .service-grid span:hover, .blog .date, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button,  .category_list_filterable .active, input[type="submit"]:hover,
				.block-image li.white-rounded:hover, .ghover .pluss:hover, .home .pro-box .add_to_cart_button:after, .addtocart:after, .addtocart, .home .pro-box .add_to_cart_button ,
				.progress .percentage, .pagination #pagination a:hover, .pagination #pagination .active a{
					background:'.($recieve_color).'; 
				}';				
				$html_new .= '.pattren_cp_footer {
					background-color:#fff;
					background-image: 
					radial-gradient(circle at 100% 150%,'.$recieve_color.' 24%,'.$recieve_color.' 25%,'.$recieve_color.' 28%,'.$recieve_color.' 29%,'.$recieve_color.' 36%,'.$recieve_color.' 36%,'.$recieve_color.' 40%, transparent 40%, transparent),
					radial-gradient(circle at 0    150%,'.$recieve_color.' 24%,'.$recieve_color.' 25%,'.$recieve_color.' 28%,'.$recieve_color.' 29%,'.$recieve_color.' 36%,'.$recieve_color.' 36%,'.$recieve_color.' 40%, transparent 40%, transparent),
					radial-gradient(circle at 50%  100%,'.$recieve_color.' 10%,'.$recieve_color.' 11%,'.$recieve_color.' 23%,'.$recieve_color.' 24%,'.$recieve_color.' 30%,'.$recieve_color.' 31%,'.$recieve_color.' 43%,'.$recieve_color.' 44%,'.$recieve_color.' 50%,'.$recieve_color.' 51%,'.$recieve_color.' 63%,'.$recieve_color.' 64%,'.$recieve_color.' 71%, transparent 71%, transparent),
					radial-gradient(circle at 100% 50%,'.$recieve_color.' 5%,'.$recieve_color.' 6%,'.$recieve_color.' 15%,'.$recieve_color.' 16%,'.$recieve_color.' 20%,'.$recieve_color.' 21%,'.$recieve_color.' 30%,'.$recieve_color.' 31%,'.$recieve_color.' 35%,'.$recieve_color.' 36%,'.$recieve_color.' 45%,'.$recieve_color.' 46%,'.$recieve_color.' 49%, transparent 50%, transparent),
					radial-gradient(circle at 0    50%,'.$recieve_color.' 5%,'.$recieve_color.' 6%,'.$recieve_color.' 15%,'.$recieve_color.' 16%,'.$recieve_color.' 20%,'.$recieve_color.' 21%,'.$recieve_color.' 30%,'.$recieve_color.' 31%,'.$recieve_color.' 35%,'.$recieve_color.' 36%,'.$recieve_color.' 45%,'.$recieve_color.' 46%,'.$recieve_color.' 49%, transparent 50%, transparent);
					background-size:30px 60px;
					height:25px;
				}';
				$html_new .= '.footer-copy {
					background:rgba(0,0,0,0.5);
				}';
				
				if($select_layout_cp == 'boxed_layout'){
				$html_new .= '
				#custom_mega .mmm_fullwidth_container{
					width:auto !important;
				}
				.banner_edu{
					overflow:hidden;
				}
				#wrapper{
					width:1280px;
					margin:0 auto;
					background:#fff;
					float:none;
					box-shadow:0px 0px 10px 0px rgba(0,0,0,0.2);
					-moz-box-shadow:0px 0px 10px 0px rgba(0,0,0,0.2);
					-webkit-box-shadow:0px 0px 10px 0px rgba(0,0,0,0.2);
				}';
				}else{
				
				}
				/*Text Color End*/
				
				//Header Background Image
				$header_image = neomag_cp_get_themeoption_value('header_logo_bg','general_settings');
				
				$image_src = '';
					if(!empty($header_image)){ 
						$image_src = wp_get_attachment_image_src( $header_image, 'full' );
						$image_src = (empty($image_src))? '': $image_src[0];			
				}
				
				if($header_image <> ''){
					if($image_src <> ''){
							$html_new .= '.inner-titlebg {background: url('.esc_url($image_src).') no-repeat center center/cover}';
					}
				}else{
					$path =  NEOMAG_PATH_URL;
					$html_new .=  '.inner-titlebg {background: url('.esc_url($path).'/images/inner-pagebg.jpg) no-repeat center center/cover}';
				}
				
				//For Header 6 Only
				$header_style = '';
				
				if(cp_print_header_html_val($header_style) == 'Style 6'){
					$html_new .=  '.inner-titlebg {padding:0px; background:none;}';
				}
		$html_new .= '</style>';
		
		//Color Picker Is Installed 
		if($backend_on_off <> 1){
			die(json_encode($html_new));
		}else{
			return $html_new;
		}
	}
	// Add Style to Frontend
	function add_font_code(){
		global $pagenow;
		
		//Style tag Start
		echo '<style type="text/css">';
			
			//Attach Background
			$select_bg_pat = neomag_cp_get_themeoption_value('select_bg_pat','general_settings');
			$body_image = neomag_cp_get_themeoption_value('body_image','general_settings');
			$image_repeat_layout = neomag_cp_get_themeoption_value('image_repeat_layout','general_settings');
			$position_image_layout = neomag_cp_get_themeoption_value('position_image_layout','general_settings');
			$image_attachment_layout = neomag_cp_get_themeoption_value('image_attachment_layout','general_settings');
			
			 if($select_bg_pat == 'Background-Image'){
				$image_src_head = '';							
				if(!empty($body_image)){ 
					$image_src_head = wp_get_attachment_image_src( $body_image, 'full' );
					$image_src_head = (empty($image_src_head))? '': $image_src_head[0];
					$thumb_src_preview = wp_get_attachment_image_src( $body_image, 'full');
				}
				echo 'body{
				background-image:url('.esc_url($thumb_src_preview[0]).');
				background-repeat:'.esc_attr($image_repeat_layout).';
				background-position:'.esc_attr($position_image_layout).';
				background-attachment:'.esc_attr($image_attachment_layout).';
				background-size:cover; }';
			}else if($select_bg_pat == 'Background-Color'){ 
				$bg_scheme = neomag_cp_get_themeoption_value('bg_scheme','general_settings');
				echo '.inner-pages h2 .txt-left{background:'.esc_attr($bg_scheme).';}';
			}else if($select_bg_pat == 'Background-Patren'){
				$body_patren = neomag_cp_get_themeoption_value('body_patren','general_settings');
				$color_patren = neomag_cp_get_themeoption_value('color_patren','general_settings');
				//render Body Pattern
				if(!empty($body_patren)){
					$image_src_head = wp_get_attachment_image_src( $body_patren, 'full' );
					$image_src_head = (empty($image_src_head))? '': $image_src_head[0];
					$thumb_src_preview = wp_get_attachment_image_src( $body_patren, array(60,60));
					//Custom patterm
					if($thumb_src_preview[0] <> ''){ echo 'body{background:url('.esc_url($thumb_src_preview[0]).') repeat !important;}'; }
				}else{ 
					$bg_scheme = neomag_cp_get_themeoption_value('bg_scheme','general_settings');
					$color_patren = neomag_cp_get_themeoption_value('color_patren','general_settings');
					//Default patterns
					echo 
					'body{background:'.$bg_scheme.' url('.NEOMAG_PATH_URL.$color_patren.') repeat;} 
					.inner-pages h2 .txt-left{background:'.$bg_scheme.' url('.esc_url(NEOMAG_PATH_URL.$color_patren).') repeat;}'; 
				}
			}
			//Heading Variables
			$heading_h1 = neomag_cp_get_themeoption_value('heading_h1','typography_settings');
			$heading_h2 = neomag_cp_get_themeoption_value('heading_h2','typography_settings');
			$heading_h3 = neomag_cp_get_themeoption_value('heading_h3','typography_settings');
			$heading_h4 = neomag_cp_get_themeoption_value('heading_h4','typography_settings');
			$heading_h5 = neomag_cp_get_themeoption_value('heading_h5','typography_settings');
			$heading_h6 = neomag_cp_get_themeoption_value('heading_h6','typography_settings');
			if(is_page_template( 'homev5.php' )){ $heading_h2 = '40px';}
			if(is_page_template( 'homev3.php' ) || is_page_template( 'homev4.php' )){ $heading_h3 = '36px';}
			if(is_404()){ $heading_h1 = '200px';}
			//Render Heading sizes
			if($heading_h1 <> ''){ echo 'h1{ font-size:'.esc_attr($heading_h1).'px !important; }'; }
			if($heading_h2 <> ''){ echo 'h2{ font-size:'.esc_attr($heading_h2).'px !important; }'; }
			if($heading_h3 <> ''){ echo 'h3{ font-size:'.esc_attr($heading_h3).'px !important; }'; }
			if($heading_h4 <> ''){ echo 'h4{ font-size:'.esc_attr($heading_h4).'px !important; }'; }
			if($heading_h5 <> ''){ echo 'h5{ font-size:'.esc_attr($heading_h5).'px !important; }'; }
			if($heading_h6 <> ''){ echo 'h6{ font-size:'.esc_attr($heading_h6).'px !important; }'; }
			
			//Body Font Size
			$font_size_normal = neomag_cp_get_themeoption_value('font_size_normal','typography_settings');
			if($font_size_normal <> ''){ echo 'body{font-size:'.esc_attr($font_size_normal).'px !important;}'; }
			
			//Body Font Family
			$font_google = neomag_cp_get_themeoption_value('font_google','typography_settings');
			if($font_google <> 'Default'){ echo '.classes-page .skill-inner .label, body,.comments-list li .text p, .header-4-address strong.info,.header-4-address a.email,strong.copy,.widget-box-inner p,.blog-post-box .text p,.box-1 p, .box-1 .textwidget,.get-touch-form input,.get-touch-form strong.title,.footer-copyright strong.copy,#inner-banner p,.welcome-text-box p,.about-me-text p,.about-me-text blockquote q,.team-box .text p,.accordition-box .accordion-inner p,.facts-content-box p,.our-detail-box p,.our-detail-box ul li,.widget_em_widget ul li,.sidebar-recent-post ul li p,blockquote p,blockquote q,.author-box .text p,.contact-page address ul li strong.title,.contact-page address ul li strong.ph,.contact-page address ul li strong.mob,.contact-page address ul li a.email,a.comment-reply-link,.timeline-project-box > .text p,.comments .text p,.event-row .text p,.project-detail p,.news-box .text p,.error-page p,.cp-columns p,.cp-list-style ul li,.customization-options ul li,.cp-accordion .accordion-inner strong,.list-box ul li,.list-box2 ul li,.list-box3 ul li,.tab-content p, .tab-content-area p,.blockquote-1 q,.blockquote-2 q,.map h3,.even-box .caption p,.header-4-address strong.info,.header-4-address a.email,strong.copy,.widget-box-inner p { font-family:"'.esc_attr($font_google).'";}'; }else{ 
			echo '';
			}
			
			//Body Font Size
			$boxed_scheme = neomag_cp_get_themeoption_value('boxed_scheme','general_settings');
			$select_layout_cp = neomag_cp_get_themeoption_value('select_layout_cp','general_settings');
			if($select_layout_cp == 'box_layout'){ echo '.boxed{background:'.esc_attr($boxed_scheme).';}'; }
			
			//Heading Font Family
			$font_google_heading = neomag_cp_get_themeoption_value('font_google_heading','typography_settings');
			if($font_google_heading <> 'Default'){ echo '
			h1, h2, h3, h4, h5, h6, 
			.head-topbar .left ul li strong.number,
			.head-topbar .left ul li a,.navigation-area a.btn-donate-2,.footer-menu li a,.footer-menu2 li a,#nav-2 li a,#nav-2 li ul li a,.navigation-area a.btn-donate3,.top-search-input,a.btn-donate5,.navigation-area a.btn-donate,.top-search-input,#nav li ul li a,.cp-banner .caption h1,.cp-banner .caption h2,.cp-banner .caption strong.title,.cp-banner .caption a.view,.widget-box-inner h2,.entry-header > h1,.h-style,.latest-news-box h3,.css3accordion .content .top a,.css3accordion .content .top strong.mnt,.css3accordion .content .top a.comment,.css3accordion .content strong.title,.css3accordion .content p,.css3accordion .content a.readmore,.upcoming-heading h3,.upcoming-box .caption strong.title,.upcoming-box .caption strong.mnt,.upcoming-events-box a.btn-view,.countdown_holding span,.countdown_amount,.countdown_period,.our-project a.btn-view,.our-project h3,.portfolio-filter li a,.gallery-box .caption strong.title,.timeline-box h3,.timeline-head strong.mnt,.timeline-frame-outer .caption h4,.timeline-frame-outer .caption p,.blog-post h3,.blog-post-box .caption strong.date,.blog-post-box .caption strong.comment,.blog-post-box .text strong.title,.blog-post-box .text h4,.blog-post-box .text a.readmore,.blog-post-share strong.title1,.name-box strong.name,.name-box-inner strong,.text-row strong.title,.text-row strong.time,.twitter-info-box ul li strong.number,.twitter-info-box ul li a.tweet,.box-1 h4,.box-1 a.btn-readmore,.box-1 .text strong.title,.box-1 .text strong.mnt,#inner-banner h1,.welcome-text-box h2,.about-me-left .text ul li h3,.about-me-left .text ul li strong.title,.about-me-socila strong.title,.about-me-text h3,.team-member-box h3,.team-box .text h4,.team-box .text h4 a,.team-box .text strong.title,.heading h3,.our-facts-box strong.number,.our-facts-box a.detail,.our-detail-box h4,.accordition-box .accordion-heading .accordion-toggle strong,.facts-tab-box .nav-tabs > li > a, .nav-pills > li > a,.blog-box-1 strong.title,.bottom-row .left span,.bottom-row .left a,.bottom-row .left ul li a,.bottom-row .right strong.title,.blog-box-1 .text h2,.blog-box-1 .text a.readmore,.pagination-all.pagination ul > li > a, .pagination ul > li > span,.sidebar-input,.sidebar-member a.member-text,.sidebar-recent-post h3,.sidebar-recent-post ul li:hover .text strong.title,.widget_em_widget ul li a,.sidebar-recent-post ul li .text strong.title,.sidebar-recent-post ul li a.mnt,.sidebar-recent-post ul li a.readmore,.list-area ul li a,.archive-box ul li a,.tagcloud a,.share-socila strong.title,.author-box .text strong.title,.contact-me-row strong.title,.blog-detail-form h3,.form-area label,.detail-input,.detail-textarea,.detail-btn-sumbit,.post-password-form input[type="submit"],#searchsubmit,.detail-btn-sumbit2,a.comment-reply-link,.donate-page h2,.donate-form ul li a,.donate-form-area ul li label,.donate-input,.donate-btn-submit,.timeline-project-box .holder .heading-area,.timeline-project-box .blog-box-1 > .text h2,.comment-box h3,.comments .text strong.title,.comments .text a.date,.comments .text a.reply,.timer-area ul li a,.event-detail-timer .countdown-amount,.countdown-period,.contact-me-row2 strong.title,.contact-me-row2 ul li a,.related-event-box h3,.related-box .text strong.title,.related-box .text a.date,.member-input,.member-input-2,.member-input-3,.member-form label,.check-box strong.title,.member-btn-submit,.event-heading a,.event-row .text h2,.detail-row li a,.map-row a.location,.project-detail h2,.project-detail-list li .even,.project-detail-list li .odd,.other-project h3,.news-box .text-top-row span,.news-box .text-top-row a,.news-box .text-top-row a,.news-box .text h2,.news-box .text a.readmore,.slide-out-div h3,.error-page h2,.cp-columns h2,.cp-columns strong.title,.customization-options h2,.cp-highlighter h2,.cp-accordion .accordion-heading .accordion-toggle strong,.cp-testimonials h2,.frame-box strong.name,.frame-box strong.title,.testimonial-box-1 blockquote q,.single-testimonial blockquote q,.frame-box2 strong.name,.frame-box2 strong.title,.button-box a,.typography h1,h2.cp-heading-full,.typography h2,h3.cp-heading-full,.typography h3,h4.cp-heading-full,.typography h4,h5.cp-heading-full,.typography h5,h6.cp-heading-full,.typography h6,.tabs-box .nav-tabs > li > a, .nav-pills > li > a,#wp-calendar caption,.even-box .caption h2,.timeline-round strong.year,#search-text input[type="text"],.sidebar-recent-post select,.content_section .review-final-score h3,.content_section .review-final-score h4, #cp_header7 .navigation-row .navbar-default li a, 
			.thumb-style .caption h2, .thumb-style .caption strong, .services-box .cp_strong, .food-title h2, .food-title strong, .cp_special-menu .text h2, .cp_special-menu .btn-light,
			.cp_our-menu .nav-tabs > li > a, .food-title h2, .chef-info .text strong.title, .event-carousel-holder .event-info .cp_strong, .event-carousel-holder .countdown-amount,
			.our-facts-box strong, .cp_blog-section .blog-list .cp_strong, .cp_blog-section .more-info, .cp-few-words .cp-heading-full, .footer-top h2
			
			{ font-family:"'.esc_attr($font_google_heading).'";}'; }else{ echo 'h1, h2, h3, h4, h5, h6{}';}
			
			//Menu Font Family
			$menu_font_google = neomag_cp_get_themeoption_value('menu_font_google','typography_settings');
			if($menu_font_google <> 'Default'){ echo '#navbar.main-menu > .menu_holder > .menu_inner > .nav_logo > .mobile_toggle > .mobile_button, #navbar.main-menu > .menu_holder > .menu_inner > ul > li > .item_link, #navbar.main-menu > .menu_holder > .menu_inner > ul > li > .item_link .link_text, #navbar.main-menu > .menu_holder > .menu_inner > ul > li.nav_search_box *, #navbar.main-menu > .menu_holder > .menu_inner > ul > li .post_details > .post_title, #navbar.main-menu > .menu_holder > .menu_inner > ul > li .post_details > .post_title > .item_link, .navigation ul{font-family:"'.esc_attr($menu_font_google).' !important";}'; echo '#nav li a{font-family:"'.esc_attr($menu_font_google).' !important";} > ';}else{ echo '#nav{font-family:"Open Sans",sans-serif;}';}
			
		echo '</style>';
		//Style Tag End
		
		$color_scheme = neomag_cp_get_themeoption_value('color_scheme','general_settings');	
		$body_color = neomag_cp_get_themeoption_value('body_color','general_settings');
		$heading_color = neomag_cp_get_themeoption_value('heading_color','general_settings');
		$select_layout_cp = neomag_cp_get_themeoption_value('select_layout_cp','general_settings');
		
		$recieve_color = '';
		$recieve_an_color = '';
		$html_new = '';
		$backend_on_off = 1;
		//Color Scheme
		echo cp_color_bg($color_scheme,$bg_texture='',$navi_color='',$heading_color,$body_color,$select_layout_cp,$backend_on_off);
	}
	//Add Style in Footer
	global $pagenow;
	if( $GLOBALS['pagenow'] != 'wp-login.php' ){
		if(!is_admin()){
			//for Frontend only
			add_action('wp_head', 'add_font_code');
		}
	}