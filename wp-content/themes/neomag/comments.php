<?php 
	/*
	 * This file is used to generate comments form.
	 */	

	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
	if (post_password_required()){
		?> <p class="nopassword"><?php echo esc_html__('This post is password protected. Enter the password to view comments.','neomag'); ?></p> <?php
		return;
	}
if ( have_comments() ) : ?>
	<h3><?php comments_number(esc_html__('Sem Comentários ','neomag'), esc_html__('Um Comentário','neomag'), esc_html__('% Comentários','neomag') );?></h3>
	<ul id="comments" class="comments-list">
		<?php wp_list_comments(array('callback' => 'get_comment_list')); ?>
	</ul>
	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
		<br>
		<div class="comments-navigation">
			<div class="previous"> <?php previous_comments_link('Older Comments'); ?> </div>
			<div class="next"> <?php next_comments_link('Newer Comments'); ?> </div>
		</div>
	<?php endif; ?>
<?php endif; ?>
<?php 

	$neomag_comment_form = array( 
		'fields' => apply_filters( 'comment_form_default_fields', array(
			'author' => '<li class="col-md-6">' .
						'<input class="form-control" id="author" name="author" placeholder="' . esc_html__( 'Nome*','neomag' ) . '" type="text" value="' .
						 $commenter['comment_author'] . '" size="30" tabindex="1" />' .						
						'<div class="clear"></div>' .
						'</li><!-- #form-section-author .form-section -->',
			'email'  => '<li class="col-md-6">' .
						'<input id="email" class="form-control" name="email" placeholder="' . esc_html__( 'Email*','neomag' ) . '" type="text" value="' . $commenter['comment_author_email'] . '" size="30" tabindex="2" />' .						
						'</li><!-- #form-section-email .form-section -->',
			'url'    => '<li class="col-md-12">' .
						'<input id="url" class="form-control" name="url" placeholder="' . esc_html__( 'Website*','neomag' ) . '" type="text" value="' . $commenter['comment_author_url'] . '" size="30" tabindex="3" />' .						
						'<div class="clear"></div>' .
						'</li><li class="col-md-12"><!-- #form-section-url .form-section -->' ) ),
			'comment_field' => '' .
						'<div class="textarea-cp">' .
						'<textarea cols="85" rows="2" placeholder="' . esc_html__( 'Comments*','neomag' ) . '" class="comm-area form-control" id="comment" name="comment" aria-required="true"></textarea></div>' .
						'',
		'comment_notes_before' => '<ul class="comment-form">',
		'comment_notes_after' => '</li></ul>',
		'title_reply' => esc_html__('Escreva um comentário','neomag'),
		'label_submit'=>'Enviar'
	);

	$neomag_comment_form2 = array( 
		'fields' => apply_filters( 'comment_form_default_fields', array(
			'author' => '<li class="comment-form-author">' .
						'<input class="form-control" id="author" name="author" placeholder="' . esc_html__( 'Nome','neomag' ) . '" type="text" value="' .
						 $commenter['comment_author'] . '" size="30" tabindex="1" />' .						
						'<div class="clear"></div>' .
						'</li><!-- #form-section-author .form-section -->',
			'email'  => '<li class="comment-form-email">' .
						'<input id="email" class="form-control" name="email" placeholder="' . esc_html__( 'Email','neomag' ) . '" type="text" value="' . $commenter['comment_author_email'] . '" size="30" tabindex="2" />' .						
						'</li><!-- #form-section-email .form-section -->',
			'url'    => '<li>' .
						'<input id="url" class="form-control" name="url" placeholder="' . esc_html__( 'Website','neomag' ) . '" type="text" value="' . $commenter['comment_author_url'] . '" size="30" tabindex="3" />' .						
						'<div class="clear"></div>' .
						'</li><li class="comment-form-comment"><!-- #form-section-url .form-section -->' ) ),
			'comment_field' => '' .
						'</ul></div><div class="col-md-6">' .
						'<textarea placeholder="' . esc_html__( 'Comments','neomag' ) . '" class="comm-area" id="comment" name="comment" aria-required="true"></textarea></div>' .
						'',
		'comment_notes_before' => '<div class="row"><div class="col-md-6"><ul>',
		'comment_notes_after' => '</div><!-- #form-section-comment .form-section -->',
		'title_reply' => esc_html__('Escreva uma resposta','neomag'),
		'label_submit'=>'Enviar'
	);
	
	if ( 'event' == get_post_type() ){ 
		comment_form( $neomag_comment_form2, $post->ID); 
		} else {
		comment_form( $neomag_comment_form, $post->ID); 
		}
?>