<?php 
/*
 * This file is used to generate main index page.
 */
					
	//Fetch the theme Option Values
	$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings');
	$neomag_maintenace_title = neomag_cp_get_themeoption_value('maintenace_title','general_settings');
	$neomag_countdown_time = neomag_cp_get_themeoption_value('countdown_time','general_settings');
	$neomag_email_mainte = neomag_cp_get_themeoption_value('email_mainte','general_settings');
	$neomag_mainte_description = neomag_cp_get_themeoption_value('mainte_description','general_settings');
	$neomag_social_icons_mainte = neomag_cp_get_themeoption_value('social_icons_mainte','general_settings');
	$neomag_page_title_breadcrumb = get_post_meta ( $post->ID, "page-option-item-page-title", true );
	$neomag_cp_page_title = get_post_meta ( $post->ID, "page-option-schedule-title", true );
	$neomag_cp_page_caption = get_post_meta ( $post->ID, "cp-show-page-caption-pageant", true );
	$neomag_optional_page_class = get_post_meta ( $post->ID, "page-option-item-class", true );

	$neomag_num_excerpt = '';
	
	
	if($neomag_maintenance_mode <> 'disable'){
		//If Logged in then Remove Maintenance Page
		if ( is_user_logged_in() ) {
			$neomag_maintenance_mode = 'disable';
		} else {
			if($neomag_maintenance_mode == ''){}else{$neomag_maintenance_mode = 'enable';}
		}
	}
	
	if($neomag_maintenance_mode == 'enable'){
		//Trigger the Maintenance Mode Function Here
		maintenance_mode_fun();
	}else{

		@get_header();
		$neomag_header_style = '';
		//Print Style 6
		if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
			cp_print_header_html($neomag_header_style);
		}
		$neomag_item_class = '';
		$neomag_sidebar = '';
		// Fetch sidebar theme option 		
		$neomag_cp_default_settings = get_option('default_pages_settings');		
		if($neomag_cp_default_settings != ''){		
			$neomag_cp_default = new DOMDocument ();
			$neomag_cp_default->loadXML ( $neomag_cp_default_settings );
			$neomag_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'sidebar_default');
			$neomag_right_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'right_sidebar_default');
			$neomag_left_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'left_sidebar_default');
		}		
		//Get Sidebar for index
		$neomag_sidebar_class = cp_sidebar_func($neomag_sidebar);	
		
		 ?>	
        	  <div class="cp-page-title">
                      <h2>
                      <?php 
							if($neomag_page_title_breadcrumb <> ''){
								echo esc_attr($neomag_page_title_breadcrumb);
							}else{ 
							 if($neomag_cp_page_title <> ''){ 
								echo esc_attr($neomag_cp_page_title);
								} else {
								echo get_the_title();
								}
							}
						?>
                      </h2>
                      <?php if($neomag_cp_page_caption <> ''){ ?><h4><?php echo esc_attr($neomag_cp_page_caption);?></h4><?php }?>
                    </div>
	<?php 
		  $neomag_class = '';
		  $neomag_class_sidebar = '';
		  $neomag_class_gallery = 'cp-content-wrap';
		  if($neomag_sidebar == 'no-sidebar'){ $neomag_class = 'col-md-12'; }
			  if($neomag_sidebar == 'both-sidebar'){ $neomag_class = 'col-md-6'; $neomag_class_sidebar = 'col-md-3'; }
				  if(($neomag_sidebar == 'left-sidebar')||($neomag_sidebar == 'right-sidebar')){ $neomag_class = 'col-md-9'; $neomag_class_sidebar = 'col-md-3'; }
		  elseif (($neomag_sidebar == 'both-sidebar-left')||($neomag_sidebar == 'both-sidebar-right')){ $neomag_class = 'col-md-6'; $neomag_class_sidebar = 'col-md-3'; }
			  if(($neomag_optional_page_class == 'gallery_full_width')||($neomag_optional_page_class == 'gallery_full_width about_us')){ $neomag_class_gallery = ''; }
		  $neomag_optional_page_class = explode('gallery_full_width', $neomag_optional_page_class);
    ?>
      <!--CONTENT START-->
      <div id="cp-content-wrap" class="cp-content-wrap">
        <div class="container" id="gg">
          <div class="row"> 
                     <?php
					if($neomag_sidebar == "left-sidebar" || $neomag_sidebar == "both-sidebar" || $neomag_sidebar == "both-sidebar-left"){ ?>
						<div id="block_first" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
						  <?php dynamic_sidebar( $neomag_left_sidebar ); ?>
						</div>
					<?php
					}
					if($neomag_sidebar == 'both-sidebar-left'){ ?>
						<div id="block_first_left" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
						  <?php dynamic_sidebar( $neomag_right_sidebar );?>
						</div>
					<?php } ?>
        <!-- CONTENT START -->
        <div class="<?php echo esc_attr($neomag_class); ?>">
          <div class="cp-posts-style-1" id="cp-posts-style-1">
            <ul class="cp-posts-list">
							<?php
							//Feature Sticky Post	
								if ( is_front_page() && cp_has_featured_posts() ) {
									// Include the featured content template.
									get_template_part( 'featured-content' );
								}
								$neomag_mask_html = '';
								$neomag_no_image_class = 'no-image';
							?>
							<?php while ( have_posts() ) : the_post();
								//If image exists print its mask
								global $post,$allowedposttags;
								$neomag_thumbnail_types = '';
								$neomag_post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
								if($neomag_post_detail_xml <> ''){
									$neomag_cp_post_xml = new DOMDocument ();
									$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml );
									$neomag_post_social = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_social');
									$neomag_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'sidebar_post');
									$neomag_right_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'right_sidebar_post');
									$neomag_left_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'left_sidebar_post');
									$neomag_thumbnail_types = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_thumbnail');
									$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
									$neomag_select_slider_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'select_slider_type');	
								}
								$neomag_thumbnail_id = get_post_thumbnail_id( $post->ID );
								$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, array(1170,350));
								$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, 'full');
								$neomag_get_post_cp = get_post($post);
								$neomag_mask_html = '';
								$neomag_no_image_class = 'no-image';
								if(get_the_post_thumbnail($post_id, array(1170,350)) <> ''){
									$neomag_mask_html = '<div class="mask">
										<a href="'.get_permalink().'#comments" class="anchor"><span> </span> <i class="fa fa-comment"></i></a>
										<a href="'.get_permalink().'" class="anchor"> <i class="fa fa-link"></i></a>
									</div>';
									$neomag_no_image_class = 'image-exists';
								}	
							?>
								<!--BLOG LIST ITEM START-->
                      <li class="cp-post <?php if($neomag_image_thumb[0] == ''){ echo 'with-no-thumb'; } ?>">
                        <div class="cp-thumb"> 
                        <?php if($neomag_image_thumb[0] <> ''){ ?>
                        <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                          <div class="cp-post-hover"> <a href="<?php echo get_permalink();?>"><i class="fa fa-link"></i></a> 
                          <a href="<?php echo esc_url($neomag_image_thumb[0]); ?>"><i class="fa fa-search"></i></a> </div>
                        <?php } ?>
                        </div>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <a href="<?php echo get_permalink();?>"><h2><?php echo get_the_title(); ?></h2></a>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id ); 
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                            <?php $neomag_post_content_image = neo_mag_catch_that_image(); 
								if($neomag_post_content_image <> ''){
									echo '<div class="content-img"><img src="'.esc_url($neomag_post_content_image).'" alt="'.$post->ID.'">';
									} else {
							?>
                            <p><?php the_content(); ?></p>
                            <?php } ?>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<p class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</p>','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            </div>
                        </div>
                      </li>
							<?php endwhile; cp_pagination();?>
            </ul>
          </div>
        </div>
        <!-- CONTENT END --> 
			<?php
				$neomag_sidebar = '';
                $neomag_cp_default_settings = get_option('default_pages_settings');
                
                if($neomag_cp_default_settings != ''){
                   $neomag_cp_default = new DOMDocument ();
                   $neomag_cp_default->loadXML ( $neomag_cp_default_settings );
                   $neomag_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'sidebar_default');
                   $neomag_right_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'right_sidebar_default');
                   $neomag_left_sidebar = cp_find_xml_value($neomag_cp_default->documentElement,'left_sidebar_default');
                  }	
                if($neomag_sidebar == "both-sidebar-right"){ ?>
                   <div id="block_second" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
                        <?php dynamic_sidebar( $neomag_left_sidebar ); ?>
                   </div>
              <?php
                   }
                    if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){ ?>
                       <div id="block_second_right" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
                            <?php dynamic_sidebar( $neomag_right_sidebar );?>
                       </div>
              <?php }  ?>       
      </div>
    </div>
  </div>
  <!--CONTENT END--> 
<?php 	@get_footer(); } ?>