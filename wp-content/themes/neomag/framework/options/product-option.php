<?php

	/*	
	*	Crunchpress Portfolio Option File
	*	---------------------------------------------------------------------
	* 	@version	1.0
	* 	@author		Crunchpress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) Crunchpress
	*	---------------------------------------------------------------------
	*	This file create and contains the portfolio post_type meta elements
	*	---------------------------------------------------------------------
	*/
	
	//FRONT END RECIPE LAYOUT
	$wooproduct_class = array("Full-Image" => array("index"=>"1", "class"=>"sixteen ", "size"=>array(1170,350), "size2"=>array(614,614), "size3"=>array(350,350)));

	// Print Recipe item
	function cp_print_wooproduct_item($item_xml){
		global $paged,$sidebar,$wooproduct_class,$post,$wp_query,$counter;
		if(empty($paged)){
			$paged = (get_query_var('page')) ? get_query_var('page') : 1; 
		}
		$sidebar_class = '';
		$layout_set_ajax = '';
		$item_type = 'Full-Image';
		// get the item class and size from array
		$item_class = $wooproduct_class[$item_type]['class'];
		$item_index = $wooproduct_class[$item_type]['index'];
		$full_content = cp_find_xml_value($item_xml, 'show-full-news-post');
		if( $sidebar == "no-sidebar" ){
			$item_size = $wooproduct_class[$item_type]['size'];
			$sidebar_class = 'no_sidebar';
		}else if ( $sidebar == "left-sidebar" || $sidebar == "right-sidebar" ){
			$sidebar_class = 'one_sidebar';
			$item_size = $wooproduct_class[$item_type]['size2'];
		}else{
			$sidebar_class = 'two_sidebar';
			$item_size = $wooproduct_class[$item_type]['size3'];
		}
		
	// get the product meta value
	$header = cp_find_xml_value($item_xml, 'header');
	$category = cp_find_xml_value($item_xml, 'category');
	$num_fetch = cp_find_xml_value($item_xml, 'num-fetch'); 
	$num_excerpt = cp_find_xml_value($item_xml, 'num-excerpt');
	$column_size = cp_find_xml_value($item_xml, 'column-select');
	
	$select_layout_cp = '';
	$cp_general_settings = get_option('general_settings');
	if($cp_general_settings <> ''){
		$cp_logo = new DOMDocument ();
		$cp_logo->loadXML ( $cp_general_settings );
		$select_layout_cp = cp_find_xml_value($cp_logo->documentElement,'select_layout_cp');
	}
	
	$show_filterable = cp_find_xml_value($item_xml, 'filterable');
	$layout_select = cp_find_xml_value($item_xml, 'layout_select');
	$pagination = cp_find_xml_value($item_xml, 'pagination');
	$layout_class = strtolower(str_replace(' ','-',$layout_select));	
	
	if($show_filterable == 'Yes'){
		if($column_size == '3'){
			$column_size = 'column-4';
		}else{
			if($layout_select == 'Modern Grid Diagonal'){
				$column_size = 'column-4';
			}else{
				$column_size = 'column-3';
			}
		}
	}else{
		if($column_size == '3'){
			$column_size = 'col-md-4';
		}else{
			if($layout_select == 'Modern Grid Diagonal'){
				$column_size = 'col-md-4';
			}else{
				$column_size = 'col-md-3';
			}
		}
	}	
	//Theme Default Pagination
	if(cp_find_xml_value($item_xml, "pagination") == 'Wp-Default'){
		$num_fetch = get_option('posts_per_page');
	}else if(cp_find_xml_value($item_xml, "pagination") == 'Theme-Custom'){
		$num_fetch = cp_find_xml_value($item_xml, 'num-fetch');
	}else{}
	$cp_function_library = new cp_function_library;
	if(class_exists("Woocommerce")){
		
	$quan = array();
	$quantity = '';
	$total = '';
	$currency = '';
	if($show_filterable == 'Yes' AND $layout_select != 'Modern Grid Diagonal'){		
		$counter_portfolio = 0; ?>
    <!--CARING CHILDREN START-->
    <div class="cp-caring-product gap-80">
      <div class="products-tabs">
		<?php if(($header <> '') && ($layout_select != 'Normal Grid')){ ?>
		        <h2 class="sec-title"><?php echo esc_attr($header);?></h2>
		<?php } ?>			
      
        <div role="tabpanel"> 
          
          <!-- Nav tabs -->
          <div class="container">
            <ul class="nav nav-tabs" role="tablist">
            <?php if($layout_select == 'Normal Grid'){ echo '<li><h2>'.esc_attr($header).'</h2></li>'; } ?>
              <?php	
			  $tab_counter = 1;
			  $list_counter = 1;
				$categories = get_categories( array('child_of' => $category, 'taxonomy' => 'product_cat', 'hide_empty' => 0) );
				  if($categories <> ""){
					  $cat_list = array();
					foreach($categories as $values){ ?>
						<li role="presentation" <?php if($tab_counter == 1){ echo 'class="active"';  } ?>>
						<a href="#tab<?php echo esc_attr($tab_counter); ?>" aria-controls="tab<?php echo esc_attr($tab_counter); ?>" role="tab" data-toggle="tab">
						<?php echo esc_attr($values->name); ?></a></li>
			  <?php $cat_list[$tab_counter-1] = $values->name; $tab_counter++; } } ?>
            </ul>
          </div>
          <!-- Tab panes -->
          <div class="tab-content <?php if(($layout_select == 'Simple Grid')){ echo 'product-listing woocommerce'; } ?>">
			<?php foreach($cat_list as $tabs){ ?>
            <div role="tabpanel" class="tab-pane <?php if($list_counter == 1){ echo 'active'; }?>" id="tab<?php echo esc_attr($list_counter); ?>">
            	
				<?php  if(($layout_select == 'Normal Grid')){ $nop = 6; ?>
                <div class="tab-products">
                <?php } ?>
                
				<?php  if(($layout_select == 'Simple Grid')){ $nop = 4; ?>
              <div class="container">
                <ul class="cp-product-list row">
				<?php } ?>		
				
                	<?php 
					$args = array( 'post_type' => 'product', 'posts_per_page' => $nop, 'product_cat' => $tabs ); 

    				$loop = new WP_Query( $args );

    				while ( $loop->have_posts() ) : $loop->the_post(); 
    				global $post,$post_id,$product,$product_url;  
							$regular_price = get_post_meta($post->ID, '_regular_price', true);
							if($regular_price == ''){
								$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
							}
							$sale_price = get_post_meta($post->ID, '_sale_price', true);
							if($sale_price == ''){
								$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
							}
							$sku_num = get_post_meta($post->ID, '_sku', true);
							$currency = get_woocommerce_currency_symbol(); 
					
					 cp_selected_grid($layout_select,$product,$post); 
                    
				endwhile; 

					wp_reset_postdata();
    				wp_reset_query();
					?>							
				<?php  if(($layout_select == 'Simple Grid')){ ?>			
                </ul>
             </div>
             <?php } ?>
             <?php  if(($layout_select == 'Normal Grid')){ ?>
             </div>
             <?php } ?>
           </div>
			<?php $list_counter++; } ?>	
          </div>
        </div>
      </div>
    </div>
    <!--CARING CHILDREN End--> 
		<?php }else{
			if($category == '0'){
				query_posts(
					array( 
					'post_type' => 'product',
					'posts_per_page'			=> $num_fetch,
					'orderby' => 'title',
					'order' => 'ASC' )
				);
			}else{
				query_posts(
					array( 
					'post_type' => 'product',
					'posts_per_page'			=> $num_fetch,
					'paged'						=> $paged,
					'tax_query' => array(
						array(
							'taxonomy' => 'product_cat',
							'terms' => $category,
							'field' => 'term_id',
						)
					),
					'orderby' => 'title',
					'order' => 'ASC' )
				);
			} 
			if(have_posts()){ ?>
			<div class="cp_woo_grid <?php echo esc_attr($layout_class);?>">
				<div class="row">
				<?php
				while( have_posts() ){
				the_post();	
				global $post,$post_id,$product,$product_url;
				$regular_price = get_post_meta($post->ID, '_regular_price', true);
				if($regular_price == ''){
					$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
				}
				$sale_price = get_post_meta($post->ID, '_sale_price', true);
				if($sale_price == ''){
					$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
				}
				$sku_num = get_post_meta($post->ID, '_sku', true);
				$currency = get_woocommerce_currency_symbol(); 				
					if($layout_select == 'Modern Grid Diagonal'){ $span_col = 'col-md-4'; }else{ $span_col = 'col-md-3';}
					if ($layout_select == 'Normal Grid') { $column_size = $column_size . ' col-sm-6' ; }
					?>
					<div class="<?php echo esc_attr($column_size);?> product-box">
						<?php cp_selected_grid($layout_select,$product,$post);?>
					</div>
				<?php }
				wp_reset_postdata();
				?>	
				</div>
			</div>	
			<div class="clear"></div>
			<?php
			} wp_reset_query();
			if( cp_find_xml_value($item_xml, "pagination") == "Theme-Custom"){	
				cp_pagination();
			}
		}
	}		
}	

	function get_cart() {
		return array_filter( (array) $this->cart_contents );
	}
	
	function get_remove_url( $cart_item_key ) {
		global $woocommerce;
		$cart_page_id = woocommerce_get_page_id('cart');
		if ($cart_page_id)
			return apply_filters( 'woocommerce_get_remove_url', wp_nonce_url( 'cart', add_query_arg( 'remove_item', $cart_item_key, get_permalink($cart_page_id) ) ) );
	}
	
	function cp_normal_grid($product='',$post=''){
		$permalink_structure = get_option('permalink_structure');
		if($permalink_structure <> ''){
			$permalink_structure = '?';
		}else{
			$permalink_structure = '&';
		}
		$regular_price = get_post_meta($post->ID, '_regular_price', true);
		if($regular_price == ''){
			$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
		}
		$sale_price = get_post_meta($post->ID, '_sale_price', true);
		$sku_num = get_post_meta($post->ID, '_sku', true);
		
		if($sale_price == ''){
			$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
		}
		$currency = get_woocommerce_currency_symbol();
		
		$thumbnail_id = get_post_thumbnail_id( $post->ID );
		$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(350,350));
		$image_thumb = wp_get_attachment_image_src($thumbnail_id, 'full');
	?>
		<!--PRODUCT LIST ITEM START-->
        <div class="item">
             <div class="tab-pro-box"> 
			 <?php echo get_the_post_thumbnail($post->ID, array(300,300));?>
                 <div class="box-caption">
                      <h3><a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(get_the_title());?></a></h3>
                      <strong><?php echo esc_attr($currency);?><?php if($sale_price <> ''){echo esc_attr($sale_price);}else{echo esc_attr($regular_price);}?></strong> 
                      <?php
				echo apply_filters( 'woocommerce_loop_add_to_cart_link',
					sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="addtocart %s product_type_%s">Add To Cart</a>',
						esc_url( $product->add_to_cart_url() ),
						esc_attr( $product->id ),
						esc_attr( $product->get_sku() ),
						$product->is_purchasable() ? 'add_to_cart_button' : '',
						esc_attr( $product->product_type ),
						esc_html( $product->add_to_cart_text() )
					),
				$product );
				?> 
                 </div>
             </div>
        </div>
		<!--PRODUCT LIST ITEM START-->
<?php }

	function cp_normal_grid_loop(){ ?>
		<!--Grid View Start-->
		<section class="product_view" id="product_grid">  
			<div class="row grid-list-view product_image_holder grid-style">
				<?php
				$counter_product = 0;
				while( have_posts() ){
					the_post();	
					global $post,$post_id,$product,$product_url,$woocommerce;
					$permalink_structure = get_option('permalink_structure');
					if($permalink_structure <> ''){
						$permalink_structure = '?';
					}else{
						$permalink_structure = '&';
					}
					$regular_price = get_post_meta($post->ID, '_regular_price', true);
					if($regular_price == ''){
						$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
					}
					$sale_price = get_post_meta($post->ID, '_sale_price', true);
					$sku_num = get_post_meta($post->ID, '_sku', true);
					
					if($sale_price == ''){
						$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
					}
					$currency = get_woocommerce_currency_symbol();
					
					$thumbnail_id = get_post_thumbnail_id( $post->ID );
					$image_thumb = wp_get_attachment_image_src($thumbnail_id, array(350,350));
					$image_thumb = wp_get_attachment_image_src($thumbnail_id, 'full');
					if($counter_product % 4 == 0){ echo '<div class="clear clearfix"></div>';}else{}$counter_product++;
					echo '<div class="col-lg-3 col-md-3 col-sm-6">';
						cp_normal_grid($product,$post);
					echo '</div>';
				}//End While ?>
			</div>	
		</section>
	<?php 
	}
	
	//Fetch the Grid Selected by User
	function cp_selected_grid($grid_layout='',$product='',$post=''){
		if($grid_layout == 'Simple Grid'){
			cp_simple_grid($product,$post);		
		}else if($grid_layout == 'Normal Grid'){
			cp_normal_grid($product,$post);		
		}else if($grid_layout == 'Modern Grid'){
			cp_modern_grid_square($product,$post);
		}else{
			cp_modern_grid_diagonal($product,$post);
		}
	}
	
	//print Modern Grid Diagonal
	function cp_modern_grid_diagonal($product='',$post=''){
		$regular_price = get_post_meta($post->ID, '_regular_price', true);
		if($regular_price == ''){
			$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
		}
		$sale_price = get_post_meta($post->ID, '_sale_price', true);
		if($sale_price == ''){
			$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
		}
		$sku_num = get_post_meta($post->ID, '_sku', true);
		$currency = get_woocommerce_currency_symbol();
	?>
		<div class="fitem">
			<div class="cart">
				<?php
				echo apply_filters( 'woocommerce_loop_add_to_cart_link',
					sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="cart button %s product_type_%s"><i class="fa fa-shopping-cart"></i></a>',
						esc_url( $product->add_to_cart_url() ),
						esc_attr( $product->id ),
						esc_attr( $product->get_sku() ),
						$product->is_purchasable() ? 'add_to_cart_button' : '',
						esc_attr( $product->product_type ),
						esc_html( $product->add_to_cart_text() )
					),
				$product );
				?>
			</div>
			<div class="thumb">
				<div class="frame">
					<span class="frame-hover"><a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-link"></i></a></span>
						<div class="frame-caption">
							<h3><?php echo esc_attr(get_the_title());?></h3>
							<div class="bottom-row woocommerce">
								<?php if ( $rating_html = $product->get_rating_html() ) : ?>
									<?php echo html_entity_decode($rating_html); ?>
								<?php endif; ?>
							</div>
							<strong class="price"><?php echo esc_attr($currency);?><?php if($sale_price <> ''){echo esc_attr($sale_price);}else{echo esc_attr($regular_price);}?></strong> 
						</div>
					<?php echo get_the_post_thumbnail($post->ID, array(450,450));?>
				</div>
			</div>
			<div class="like"><a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-file-text-o"></i></a></div>
		</div>
	<?php
	}
	
	function cp_modern_grid_square($product='',$post=''){
		$regular_price = get_post_meta($post->ID, '_regular_price', true);
		if($regular_price == ''){
			$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
		}
		$sale_price = get_post_meta($post->ID, '_sale_price', true);
		if($sale_price == ''){
			$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
		}
		$sku_num = get_post_meta($post->ID, '_sku', true);
		$currency = get_woocommerce_currency_symbol();
	?>
	
		<div class="pro-box">
			<div class="thumb">
				<div class="thumb-hover"> 
					<span class="cart">						
						<?php
						echo apply_filters( 'woocommerce_loop_add_to_cart_link',
							sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="cart button %s product_type_%s"><i class="fa fa-shopping-cart"></i></a>',
								esc_url( $product->add_to_cart_url() ),
								esc_attr( $product->id ),
								esc_attr( $product->get_sku() ),
								$product->is_purchasable() ? 'add_to_cart_button' : '',
								esc_attr( $product->product_type ),
								esc_html( $product->add_to_cart_text() )
							),
						$product );
						?>
					</span>
					<span class="like"><a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-file-text-o"></i></a></span> 
				</div>				
				<?php if ( $product->is_on_sale() ) : ?>
				<div class="sale">
					<?php echo apply_filters( 'woocommerce_sale_flash', '<span>' . esc_html__( 'On Sale!', 'neomag' ) . '</span>', $post, $product ); ?>
				</div>
				<?php endif; ?>
				<?php echo get_the_post_thumbnail($post->ID, array(260,300));?>
			</div>
			<div class="pro-content">
				<div class="rate"><?php echo esc_attr($currency);?><?php if($sale_price <> ''){echo esc_attr($sale_price);}else{echo esc_attr($regular_price);}?></div>
				<h3><?php echo esc_attr(get_the_title());?></h3>
				<div class="bottom-row woocommerce">
					<?php if ( $rating_html = $product->get_rating_html() ) : ?>
						<?php echo html_entity_decode($rating_html); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php
	}
	
	//Print Simple WooCommerce Product Grid
	function cp_simple_grid($product='',$post=''){
		$regular_price = get_post_meta($post->ID, '_regular_price', true);
		if($regular_price == ''){
			$regular_price = get_post_meta($post->ID, '_max_variation_regular_price', true);
		}
		$sale_price = get_post_meta($post->ID, '_sale_price', true);
		if($sale_price == ''){
			$sale_price = get_post_meta($post->ID, '_min_variation_sale_price', true);
		}
		$sku_num = get_post_meta($post->ID, '_sku', true);
		$currency = get_woocommerce_currency_symbol(); ?>
                   <li class="col-md-3 col-sm-6">
                    <div class="pro-list">
                      <div class="thumb"><a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_post_thumbnail($post->ID, array(250,264));?></a></div>
                      <div class="text">
                        <div class="pro-name">
                          <h4><?php echo esc_attr(get_the_title());?></h4>
                          <p class="price"><?php echo esc_attr($currency);?><?php if($sale_price <> ''){echo esc_attr($sale_price);}else{echo esc_attr($regular_price);}?></p>
                        </div>
                        <p><?php echo esc_attr(substr(get_the_content(),0,35));?>...</p>
                      </div>
                      <div class="cart-options"> 
                      <ul>
                      <li>
                      <a><i class="fa fa-heart-o"></i></a> 
                      <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                      </li>
                      <li>
                <?php
				echo apply_filters( 'woocommerce_loop_add_to_cart_link',
				sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="cart button %s product_type_%s"><i class="fa fa-shopping-cart"></i>Cart</a>',
						esc_url( $product->add_to_cart_url() ),
						esc_attr( $product->id ),
						esc_attr( $product->get_sku() ),
						$product->is_purchasable() ? 'add_to_cart_button' : '',
						esc_attr( $product->product_type ),
						esc_html( $product->add_to_cart_text() )
					),
				$product );
				?>
                </li>
                <li>
                      <a href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-search"></i><?php esc_html_e('Details','neomag');?></a> 
                </li>
                <li>
                
                
                
                
      <div class="dropdown">
         <a class="share-dropdown-toggle dLabel" role="button" data-toggle="dropdown" data-target="#"><i class="fa fa-share-alt"></i><?php esc_html_e('Share','neomag');?></a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								 <?php cp_include_social_shares();?> 
								</ul>
        </div>
                         </li>
                        </ul> 
                    <div class="rating"> 
                        <?php if ( $rating_html = $product->get_rating_html() ) : ?>
							<?php echo html_entity_decode($rating_html); ?>
						<?php endif; ?>
                         </div>

                      </div>
                    </div>
                  </li>
	<?php 
	}