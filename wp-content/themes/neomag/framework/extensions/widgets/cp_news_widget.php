<?php
class recent_news_show extends WP_Widget
{
  function recent_news_show()
  {
    $widget_ops = array('classname' => 'widget-holder', 'description' => 'Blog/News Post Widget' );
    parent::__construct('recent_news_show', 'CrunchPress : Latest News', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
	$wid_class = isset( $instance['wid_class'] ) ? ( $instance['wid_class'] ) : '';
    $title = $instance['title'];
	$recent_post_category = isset( $instance['recent_post_category'] ) ? ( $instance['recent_post_category'] ) : '';
	$news_layout = isset( $instance['news_layout'] ) ? ( $instance['news_layout'] ) : '';
	$number_of_news = isset( $instance['number_of_news'] ) ? ( $instance['number_of_news'] ) : '';
?>
 <p>
 
 <label for="<?php echo esc_attr($this->get_field_id('news_layout')); ?>">
	  <?php esc_html_e('Select Layout:','neomag');?>
	  <select id="<?php echo ($this->get_field_id('news_layout')); ?>" name="<?php echo ($this->get_field_name('news_layout')); ?>" class="widefat">
             <option <?php if($news_layout == 'Slider'){echo ('selected');}?> value="Slider" ><?php esc_html_e('Slider','neomag');?> </option>						
			 <option <?php if($news_layout == 'List'){echo ('selected');}?> value="List" ><?php esc_html_e('List','neomag');?> </option>						
      </select>
  </label>
  </p>
 <p>
  <label for="<?php echo ($this->get_field_id('title')); ?>">
	 <?php esc_html_e('Title:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('title')); ?>" name="<?php echo ($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('recent_post_category')); ?>">
	  <?php esc_html_e('Select Category:','neomag');?>
	  <select id="<?php echo ($this->get_field_id('recent_post_category')); ?>" name="<?php echo ($this->get_field_name('recent_post_category')); ?>" class="widefat">
		<?php
				foreach ( get_category_list_array('category') as $category){ ?>
                    <option <?php if(($recent_post_category) == $category->slug){echo 'selected';}?> value="<?php echo esc_attr($category->slug);?>" >
	                    <?php echo (substr(($category->name), 0, 20));	if ( strlen($category->name) > 20 ) echo "...";?>
                    </option>						
			<?php }?>
      </select>
  </label>
  </p>  
  <p>
  <label for="<?php echo ($this->get_field_id('number_of_news')); ?>">
	  <?php esc_html_e('Number of News','neomag');?>
	<input class="widefat" size="5" id="<?php echo ($this->get_field_id('number_of_news')); ?>" name="<?php echo ($this->get_field_name('number_of_news')); ?>" type="text" value="<?php echo esc_attr($number_of_news); ?>" />
  </label>
  </p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
	$instance['wid_class'] = $new_instance['wid_class'];
    $instance['title'] = $new_instance['title'];
    $instance['recent_post_category'] = $new_instance['recent_post_category'];	
	 $instance['news_layout'] = $new_instance['news_layout'];	
	$instance['number_of_news'] = $new_instance['number_of_news'];	
    return $instance;
  }
 
	function widget($args, $instance)
	{
		
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$wid_class = empty($instance['wid_class']) ? ' ' : apply_filters('widget_title', $instance['wid_class']);
		$recent_post_category = isset( $instance['recent_post_category'] ) ? ( $instance['recent_post_category'] ) : '';		
		$news_layout = isset( $instance['news_layout'] ) ? ( $instance['news_layout'] ) : '';		
		$number_of_news = isset( $instance['number_of_news'] ) ? ( $instance['number_of_news'] ) : '';				
		echo html_entity_decode($before_widget);	
		// WIDGET display CODE Start
		if (!empty($title))
			echo html_entity_decode($before_title); 
			echo esc_attr($title);
			echo html_entity_decode($after_title);
			global $wpdb, $post;
			
			
			  $category_array = get_term_by('slug', $recent_post_category, 'category');
				global $post, $wp_query;
				$neomag_class_odd = '';
					$args = array(
						'posts_per_page'			=> $number_of_news,
						'post_type'					=> 'post',
						'category'					=> $recent_post_category,
						'post_status'				=> 'publish',
						'order'						=> 'DESC',
						);
					query_posts($args);
					if ( have_posts() ) {
						if ($news_layout == 'List'){?>
							<ul class="footer-blog">
							<?php
							$counter_news = 0;		
								while ( have_posts() ): the_post();
								$counter_news++; ?>
								
								<li class="<?php if(get_the_post_thumbnail($post->ID, array(150,100)) <> ''){ echo 'image_found';}else{echo 'no_image';}?>">
									<div class="thumbnail">
										<a class="img_class_cp" href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_post_thumbnail($post->ID, array(150,100));?></a>
										<div class="caption">
											<a href="<?php echo esc_url(get_permalink());?>">
													<?php  
													$title = get_the_title();
													if (strlen($title) < 20){ 
														echo (get_the_title());
													}else {
														echo substr((get_the_title()),0,20);
														echo '...';
													}	
													?>
											</a>
											<p><?php echo (strip_tags(mb_substr(get_the_content(),0,35))); ?>...</p>
											<small><?php echo (get_the_date()); ?></small>
										</div>
									</div>
								</li>
								<?php 
								endwhile;
								wp_reset_postdata();
								?>
							</ul>
						<?php } else { // Slider Case ?>		
							<div class="news1-slider">
								<script type="text/javascript">
									jQuery(document).ready(function ($) {
										"use strict";
										if ($('.banner-slider').length) {
											$('.banner-slider').bxSlider({
												// minSlides: 1,
												// maxSlides: 1,
												auto:true,
												controls:false
											});
										}
									});
								</script>
								<ul class="footer-blog banner-slider">
								<?php
								$counter_news = 0;		
								while ( have_posts() ): the_post();
								$counter_news++; ?>
									<li><?php echo get_the_post_thumbnail($post->ID, array(180,180));?></li>
								<?php endwhile;
								wp_reset_postdata();
								?>
								</ul>
							</div>
					<?php }
					}  wp_reset_query();
	
	
	echo html_entity_decode($after_widget);
		}
		
	}
add_action( 'widgets_init', create_function('', 'return register_widget("recent_news_show");') );?>