<?php 
/*	
*	CrunchPress Headers File
*	---------------------------------------------------------------------
* 	@version	1.0
* 	@author		CrunchPress
* 	@link		http://crunchpress.com
* 	@copyright	Copyright (c) CrunchPress
*	---------------------------------------------------------------------
*	This file Contain all the custom Built in function 
*	Developer Note: do not update this file.
*	---------------------------------------------------------------------
*/

	function cp_header_style_1(){ ?>
  <!--HEADER START-->
  <header id="header" class="inner-header">
    <div class="search">
		<?php get_search_form( ); ?>
    </div>
    <!--HEADER TOPBAR START-->
    <div class="cp-top-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="left-box">
            	<?php echo neo_mag_get_latest_post(); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="right-box">
              <div class="cp-header-search-box">
                <div class="burger"> <i class="fa fa-search"></i> </div>
              </div>
              <?php 
					$topsocial_icon = neomag_cp_get_themeoption_value('topsocial_icon','general_settings'); 
					if(esc_attr($topsocial_icon) == 'enable'){ 
						 cp_social_icons_list('social'); 
					} 
				?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--HEADER TOPBAR END--> 
    
    <!--NAVIGATION SECTION START-->
    <div class="cp-navigation-section">
      <nav class="navbar navbar-inverse">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only"><?php esc_html_e('Toggle navigation','neomag');?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <strong class="cp-logo-style-2"><?php neo_mag_cp_default_logo(); ?></strong> </div>
          <div id="navbar" class="collapse navbar-collapse">
			<?php cp_bootstrap_menu('header-menu','menu'); ?>
          </div>
        </div>
      </nav>
    </div>
    <!--NAVIGATION SECTION START--> 
    
  </header>
  <!--HEADER END--> 
	<?php 
	} 
	
	
	//Header Function 2 Start
function cp_header_style_2(){ ?>
      <!--HEADER START-->
      <header id="header" class="header2">
        <div class="search">
			<?php get_search_form( ); ?>
        </div>
        <!--HEADER TOPBAR START-->
        <div class="cp-top-bar">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="left-box">
					<?php echo neo_mag_get_latest_post(); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="right-box">
                
                
                
                  <div class="cp-header-search-box">
    <div class="burger">
      <i class="fa fa-search"></i>
      
    </div>
                  </div>
              <?php 
					$topsocial_icon = neomag_cp_get_themeoption_value('topsocial_icon','general_settings'); 
					if(esc_attr($topsocial_icon) == 'enable'){ 
						 cp_social_icons_list('social'); 
					} 
				?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--HEADER TOPBAR END--> 
        
        <!--NAVIGATION SECTION START-->
        <div class="cp-navigation-section">
          <nav class="navbar navbar-inverse">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only"><?php esc_html_e('Toggle navigation','neomag');?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                	<strong class="cp-logo-style-1"><?php neo_mag_cp_default_logo(); ?></strong> 
                </div>
              <div id="navbar" class="collapse navbar-collapse">
					<?php cp_bootstrap_menu('header-menu','menu'); ?>
              </div>
            </div>
          </nav>
        </div>
        <!--NAVIGATION SECTION START--> 
      </header>
      <!--HEADER END--> 
	  <?php 
	} 
  
	function cp_header_style_3(){ ?>
    <!--WRAPPER START-->
    <?php  
		global $post,$post_id;
		$neomag_slider_off = get_post_meta ( $post->ID, "page-option-top-slider-on", true ); 
	
	$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings'); 
	if($neomag_maintenance_mode <> 'disable'){ } else { ?>

	<?php } ?>
  <!--HEADER START-->
  <header id="header"  <?php if($neomag_slider_off == 'No'){ echo 'class="static-header"'; } ?>>
    <div class="search">
		<?php get_search_form( ); ?>
    </div>
    <!--HEADER TOPBAR START-->
    <div class="cp-top-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="left-box">
				<?php echo neo_mag_get_latest_post(); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="right-box">
            
              <div class="cp-header-search-box">
				<div class="burger">
				  <i class="fa fa-search"></i>
				</div>
              </div>
              <?php 
					$topsocial_icon = neomag_cp_get_themeoption_value('topsocial_icon','general_settings'); 
					if(($topsocial_icon) == 'enable'){ 
						 cp_social_icons_list('social'); 
					} 
				?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--HEADER TOPBAR END--> 
    
    <!--NAVIGATION SECTION START-->
    <div class="cp-navigation-section">
      <nav class="navbar navbar-inverse">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only"><?php esc_html_e('Toggle navigation','neomag');?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            	<strong class="cp-logo-style-1"><?php cp_default_logo(); ?></strong>
             </div>
          <div id="navbar" class="collapse navbar-collapse">
				<?php cp_bootstrap_menu('header-menu','menu'); ?>
          </div>
        </div>
      </nav>
    </div>
    <!--NAVIGATION SECTION START--> 
  </header>
  <!--HEADER END--> 
	<?php }
	
	
	function cp_header_style_4(){ 
		global $post,$post_id;
		$neomag_slider_off = get_post_meta ( $post->ID, "page-option-top-slider-on", true ); 
	?>
      <!--HEADER START-->
      <header id="header" class="header3 <?php if($neomag_slider_off == 'No'){ echo 'static-header'; } ?>">
        <div class="search">
			<?php get_search_form( ); ?>
        </div>
        <!--HEADER TOPBAR START-->
        <div class="cp-top-bar">
          <div class="container">
            <div class="row">
              <div class="col-md-5 col-sm-4 col-xs-12">
                <div class="left-box">
					<?php echo neo_mag_get_latest_post(); ?>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <strong class="cp-logo-style-1"><?php cp_default_logo(); ?></strong> 
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="right-box">
                
                
                
                  <div class="cp-header-search-box">
    <div class="burger">
      <i class="fa fa-search"></i>
      
    </div>
                  </div>
              <?php 
					$topsocial_icon = neomag_cp_get_themeoption_value('topsocial_icon','general_settings'); 
					if(($topsocial_icon) == 'enable'){ 
						 cp_social_icons_list('social'); 
					} 
				?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--HEADER TOPBAR END--> 
        
        <!--NAVIGATION SECTION START-->
        <div class="cp-navigation-section">
          <nav class="navbar navbar-inverse">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only"><?php esc_html_e('Toggle navigation','neomag');?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>
              <div id="navbar" class="collapse navbar-collapse">
				<?php cp_bootstrap_menu('header-menu','menu'); ?>
              </div>
            </div>
          </nav>
        </div>
        <!--NAVIGATION SECTION START--> 
      </header>
      <!--HEADER END-->
	<?php }

	function cp_header_style_5(){ ?>
		<!-- Header Start -->
		<header class="bg-travel-wrapper" id="cp_header2">
		</header>
		<!--Header End-->
	<?php }
   
	function cp_header_style_6(){ ?>
	
			<header id="cp_header1">
		</header>
		
	
	<?php }
  
  //Header Function 7
	function cp_header_style_7(){ ?>
  <?php 
	} 
	function cp_header_style_8(){ ?>
	<?php }
	//Header Function 9
	function cp_header_style_9(){ ?>
  <?php 
	} 
	//Header Function 5
	function cp_header_style_10(){ ?>
  <?php }
	function cp_header_style_11(){ ?>
  <?php 
  }
	function cp_header_style_12(){ ?>
	<?php }
	function cp_header_style_13(){ ?>
	<?php }
	function cp_header_style_14(){ ?>	
	<?php }
	function cp_header_style_15(){ ?>
	<?php }
	function cp_header_style_16(){ ?>
	<?php }
	function cp_header_style_17(){ ?>
	<?php 
	}
	function cp_header_style_18(){ ?>
	<?php }
	function cp_header_style_19(){ ?>
	<?php }
	function cp_header_style_20(){ ?>
	<?php }
  
	 //Header Function html
	function cp_print_header_html($header=""){
		
		$header_style_apply = neomag_cp_get_themeoption_value('header_style_apply','general_settings');
		if($header_style_apply == 'enable'){$header = 'enable';}else{}
		if($header == 'Style 1'){
			cp_header_style_1();
		}else if($header == 'Style 2'){
			cp_header_style_2();
		}else if($header == 'Style 3'){
			cp_header_style_3();
		}else if($header == 'Style 4'){
			cp_header_style_4();
		}else if($header == 'Style 5'){
			cp_header_style_5();
		}else if($header == 'Style 6'){
			cp_header_style_6();
		}else if($header == 'Style 7'){
			cp_header_style_7();
		}else if($header == 'Style 8'){
			cp_header_style_8();
		}else if($header == 'Style 9'){
			cp_header_style_9();
		}else if($header == 'Style 10'){
			cp_header_style_10();
		}else if($header == 'Style 11'){
			cp_header_style_11();
		}else if($header == 'Style 12'){
			cp_header_style_12();
		}else if($header == 'Style 13'){
			cp_header_style_13();
		}else if($header == 'Style 14'){
			cp_header_style_14();
		}else if($header == 'Style 15'){
			cp_header_style_15();
		}else if($header == 'Style 16'){
			cp_header_style_16();
		}else if($header == 'Style 17'){
			cp_header_style_17();
		}else if($header == 'Style 18'){
			cp_header_style_18();
		}else if($header == 'Style 19'){
			cp_header_style_19();
		}else if($header == 'Style 20'){
			cp_header_style_20();
		}else{
			$select_header_cp = neomag_cp_get_themeoption_value('select_header_cp','general_settings');
			if($select_header_cp == 'Style 1'){
				cp_header_style_1();
			}else if($select_header_cp == 'Style 2'){
				cp_header_style_2();
			}else if($select_header_cp == 'Style 3'){
				cp_header_style_3();
			}else if($select_header_cp == 'Style 4'){
				cp_header_style_4();
			}else if($select_header_cp == 'Style 5'){
				cp_header_style_5();
			}else if($select_header_cp == 'Style 6'){
				cp_header_style_6();
			}else if($select_header_cp == 'Style 7'){
				cp_header_style_7();
			}else if($select_header_cp == 'Style 8'){
				cp_header_style_8();
			}else if($select_header_cp == 'Style 9'){
				cp_header_style_9();
			}else if($select_header_cp == 'Style 10'){
				cp_header_style_10();
			}else if($select_header_cp == 'Style 11'){
				cp_header_style_11();
			}else if($select_header_cp == 'Style 12'){
				cp_header_style_12();
			}else if($select_header_cp == 'Style 13'){
				cp_header_style_13();
			}else if($select_header_cp == 'Style 14'){
				cp_header_style_14();
			}else if($select_header_cp == 'Style 15'){
				cp_header_style_15();
			}else if($select_header_cp == 'Style 16'){
				cp_header_style_16();
			}else if($select_header_cp == 'Style 17'){
				cp_header_style_17();
			}else if($select_header_cp == 'Style 18'){
				cp_header_style_18();
			}else if($select_header_cp == 'Style 19'){
				cp_header_style_19();
			}else if($select_header_cp == 'Style 20'){
				cp_header_style_20();
			}else{
				cp_header_style_11();
			}
		}
	}
	
	 //Header Function html
	function cp_print_header_html_val($header=""){
		$header_style_apply = neomag_cp_get_themeoption_value('header_style_apply','general_settings');
		
		if($header_style_apply == 'enable'){$header = 'enable';}else{}
		if(($header) == 'enable'){
			$select_header_cp = neomag_cp_get_themeoption_value('select_header_cp','general_settings');
			return ($select_header_cp);
		}else{
			return ($header);
		}
	}
	
	//print header style
	function cp_print_header_class($header=""){
		$banner_class = '';
		$header_style_apply = neomag_cp_get_themeoption_value('header_style_apply','general_settings');
		if(($header_style_apply) == 'enable'){$header = 'enable';}else{}
		if(($header) == 'Style 1'){
			$banner_class = 'banner-inner';
			
		}else if(($header) == 'Style 2'){
			$banner_class = 'banner banner-inner';
		}else if(($header) == 'Style 3'){
			$banner_class = 'banner banner-inner';
		}else if(($header) == 'Style 4'){
			$banner_class = 'banner banner-inner';
		}else if(($header) == 'Style 5'){
			$banner_class = '';
		}else if(($header) == 'Style 6'){
			$banner_class = '';
		}else if(($header) == 'Style 7'){
				$banner_class = '';
		}else if(($header) == 'Style 8'){
			$banner_class = '';
		}else if(($header) == 'Style 9'){
			$banner_class = '';
		}else if(($header) == 'Style 10'){
			$banner_class = '';
		}else if(($header) == 'Style 11'){
			$banner_class = '';
		}else if(($header) == 'Style 12'){
			$banner_class = '';
		}else if(($header) == 'Style 13'){
			$banner_class = '';
		}else if(($header) == 'Style 14'){
			$banner_class = '';
		}else if(($header) == 'Style 15'){
			$banner_class = '';
		}else if(($header) == 'Style 16'){
			$banner_class = '';
		}else if(($header) == 'Style 17'){
			$banner_class = '';
		}else if(($header) == 'Style 18'){
			$banner_class = '';
		}else if(($header) == 'Style 19'){
			$banner_class = '';
		}else if(($header) == 'Style 20'){
			$banner_class = '';
		}else{
			$select_header_cp = neomag_cp_get_themeoption_value('select_header_cp','general_settings');
			if(($select_header_cp) == 'Style 1'){
				$banner_class = 'banner-inner';
			}else if(($select_header_cp) == 'Style 2'){
				$banner_class = 'banner banner-inner';
			}else if(($select_header_cp) == 'Style 3'){
				$banner_class = 'banner banner-inner';
			}else if(($select_header_cp) == 'Style 4'){
				$banner_class = 'banner banner-inner';
			}else if(($select_header_cp) == 'Style 5'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 6'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 7'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 8'){
				$banner_class = '';				
			}else if(($select_header_cp) == 'Style 9'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 10'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 11'){
				$banner_class = '';				
			}else if(($select_header_cp) == 'Style 12'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 13'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 14'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 15'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 16'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 17'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 18'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 19'){
				$banner_class = '';
			}else if(($select_header_cp) == 'Style 20'){
				$banner_class = '';
			}else{
				$banner_class = 'banner-inner';
			}
		}
		return $banner_class;
	}
	
	//header background
	function cp_add_header_bg($header=''){
		$header_style_apply = neomag_cp_get_themeoption_value('header_style_apply','general_settings');
		if(esc_attr($header_style_apply) == 'enable'){ $header = 'enable';}else{}
		global $post;
		if(esc_attr($header) == 'Style 2' || esc_attr($header) == 'Style 3'){
			$thumbnail_id = get_post_thumbnail_id( $post->ID );
			if(esc_attr($thumbnail_id) <> ''){
				$thumbnail = wp_get_attachment_image_src( $thumbnail_id , 'full' );
				$html_thumb = 'style="background-image:url('.esc_url($thumbnail[0]).') !important"';
				echo '<style>.banner-inner{height:262px;}</style><section class="banner banner-inner" '.esc_attr($html_thumb).'></section>';
			}else{
				$html_thumb = '';
			}
		}else{
		}
	}
	
	//Bootstrap Menu
	function cp_bootstrap_menu($location='',$neomag_class=''){ ?>
				<?php 
				// Menu parameters		
				$defaults = array(
				'theme_location'  => $location,
				'menu'            => '', 
				'container'       => '', 
				'container_class' => 'menu-{menu slug}-container', 
				'container_id'    => 'navbar',
				'menu_class'      => 'nav navbar-nav ', 
				'menu_id'         => 'nav',
				'echo'            => true,
				'fallback_cb'     => '',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => '');				
				if(has_nav_menu($location)){ ?>
					<?php wp_nav_menu( $defaults);?>
				<?php }else{
				$args = array(
				'sort_column' => 'menu_order, post_title',
				'include'     => '',
				'exclude'     => '',
				'echo'        => true,
				'show_home'   => false,
				'menu'            => '', 
				'container'       => '', 
				'link_before' => '',
				'link_after'  => '' );?>
				<div id="navbarCollapse" class="collapse navbar-collapse <?php echo esc_attr($neomag_class);?>">
					<div id="navbar" class="nav navbar-nav">
						<?php wp_page_menu( $args ); ?>                
					</div>
				</div>	
				<?php } ?>
	<?php }
	
	//Social Networking Icons
	function cp_social_icons_list($neomag_class=''){
		
		$cp_social_settings = get_option('social_settings');
		if($cp_social_settings <> ''){
			$cp_social = new DOMDocument ();
			$cp_social->loadXML ( $cp_social_settings );
			//Social Networking Values
			$facebook_network = neomag_cp_get_themeoption_value('facebook_network','social_settings');
			$twitter_network = neomag_cp_get_themeoption_value('twitter_network','social_settings');
			$delicious_network = neomag_cp_get_themeoption_value('delicious_network','social_settings');
			$neomag_google_plus_network = neomag_cp_get_themeoption_value('google_plus_network','social_settings');
			$linked_in_network = neomag_cp_get_themeoption_value('linked_in_network','social_settings');
			$youtube_network = neomag_cp_get_themeoption_value('youtube_network','social_settings');
			$flickr_network = neomag_cp_get_themeoption_value('flickr_network','social_settings');
			$vimeo_network = neomag_cp_get_themeoption_value('vimeo_network','social_settings');
			$pinterest_network = neomag_cp_get_themeoption_value('pinterest_network','social_settings');
			$Instagram_network = neomag_cp_get_themeoption_value('Instagram_network','social_settings'); 
			$github_network = neomag_cp_get_themeoption_value('github_network','social_settings'); 
			$skype_network = neomag_cp_get_themeoption_value('skype_network','social_settings');
		}
		?>
	<ul class="cp-top-bar-social">
<?php if(($facebook_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($facebook_network);?>" title="Facebook"><i class="fa fa-facebook"></i></a></li><?php }?>
<?php if(($twitter_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($twitter_network);?>" title="Twitter"><i class="fa fa-twitter"></i></a></li><?php }?>
<?php if(($delicious_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($delicious_network);?>" title="Dribbble"><i class="fa fa-dribbble"></i></a></li><?php }?>
<?php if(($neomag_google_plus_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($neomag_google_plus_network);?>" title="Google Plus"><i class="fa fa-google-plus"></i></a></li><?php }?>
<?php if(($linked_in_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($linked_in_network);?>" title="Linkedin"><i class="fa fa-linkedin"></i></a></li><?php }?>
<?php if(($youtube_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($youtube_network);?>" title="Youtube"><i class="fa fa-youtube"></i></a></li><?php }?> 
<?php if(($flickr_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($flickr_network);?>" title="Flickr"><i class="fa fa-flickr"></i></a></li><?php }?>
<?php if(($vimeo_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($vimeo_network);?>" title="Vimeo"><i class="fa fa-vimeo"></i></a></li><?php }?>
<?php if(($pinterest_network) <> ''){?><li><a data-rel='tooltip' href="<?php echo esc_url($pinterest_network);?>" title="Pinterest"><i class="fa fa-pinterest"></i></a></li><?php }?>
<?php if(($Instagram_network) <> ''){?><li><a data-rel='tooltip' href="<?php echo esc_url($Instagram_network);?>" title="Instagram"><i class="fa fa-instagram"></i></a></li><?php }?>
<?php if(($github_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($github_network);?>" title="github"><i class="fa fa-github"></i></a></li><?php }?>
<?php if(($skype_network) <> ''){ ?><li><a data-rel='tooltip' href="<?php echo esc_url($skype_network);?>" title="Skype"><i class="fa fa-skype"></i></a></li><?php }?>
	</ul>	
	<?php 
	} 
	function cp_social_icons_anchor($neomag_class='',$data_placement=''){
		$social_networking = neomag_cp_get_themeoption_value('social_networking','general_settings');
		if($social_networking == 'enable'){ 
			$facebook_network = neomag_cp_get_themeoption_value('facebook_network','social_settings');
			$twitter_network = neomag_cp_get_themeoption_value('twitter_network','social_settings');
			$delicious_network = neomag_cp_get_themeoption_value('delicious_network','social_settings');
			$neomag_google_plus_network = neomag_cp_get_themeoption_value('google_plus_network','social_settings');
			$linked_in_network = neomag_cp_get_themeoption_value('linked_in_network','social_settings');
			$youtube_network = neomag_cp_get_themeoption_value('youtube_network','social_settings');
			$flickr_network = neomag_cp_get_themeoption_value('flickr_network','social_settings');
			$vimeo_network = neomag_cp_get_themeoption_value('vimeo_network','social_settings');
			$pinterest_network = neomag_cp_get_themeoption_value('pinterest_network','social_settings');
			$Instagram_network = neomag_cp_get_themeoption_value('Instagram_network','social_settings'); 
			$github_network = neomag_cp_get_themeoption_value('github_network','social_settings'); 
			$skype_network = neomag_cp_get_themeoption_value('skype_network','social_settings'); ?>
			<div class="<?php echo esc_attr($neomag_class);?>"> 
				<?php if(($facebook_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($facebook_network);?>" title="Facebook"><i class="fa fa-facebook-square"></i></a><?php }?>
				<?php if(($twitter_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($twitter_network);?>" title="Twitter"><i class="fa fa-twitter-square"></i></a><?php }?>
				<?php if(($delicious_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($delicious_network);?>" title="Delicious"><i class="fa fa-delicious"></i></a><?php }?>
				<?php if(($neomag_google_plus_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($neomag_google_plus_network);?>" title="Google Plus"><i class="fa fa-google-plus"></i></a><?php }?>
				<?php if(($linked_in_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($linked_in_network);?>" title="Linkedin"><i class="fa fa-linkedin"></i></a><?php }?>
				<?php if(($youtube_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($youtube_network);?>" title="Youtube"><i class="fa fa-youtube"></i></a><?php }?> 
				<?php if(($flickr_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($flickr_network);?>" title="Flickr"><i class="fa fa-flickr"></i></a><?php }?>
				<?php if(($vimeo_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($vimeo_network);?>" title="Vimeo"><i class="fa fa-vimeo-square"></i></a><?php }?>
				<?php if(($pinterest_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($pinterest_network);?>" title="Pinterest"><i class="fa fa-pinterest-square"></i></a><?php }?>
				<?php if(($Instagram_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($Instagram_network);?>" title="Instagram"><i class="fa fa-instagram"></i></a><?php }?>
				<?php if(($github_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($github_network);?>" title="Github"><i class="fa fa-github"></i></a><?php }?>
				<?php if(($skype_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($skype_network);?>" title="Skype"><i class="fa fa-skype"></i></a><?php }?>
			</div>
		<?php }
	}
	
	function cp_social_icons_anchor_header($neomag_class='',$data_placement=''){
		$topsocial_icon = neomag_cp_get_themeoption_value('topsocial_icon','general_settings');
		if($topsocial_icon == 'enable'){ 
			$facebook_network = neomag_cp_get_themeoption_value('facebook_network','social_settings');
			$twitter_network = neomag_cp_get_themeoption_value('twitter_network','social_settings');
			$delicious_network = neomag_cp_get_themeoption_value('delicious_network','social_settings');
			$neomag_google_plus_network = neomag_cp_get_themeoption_value('google_plus_network','social_settings');
			$linked_in_network = neomag_cp_get_themeoption_value('linked_in_network','social_settings');
			$youtube_network = neomag_cp_get_themeoption_value('youtube_network','social_settings');
			$flickr_network = neomag_cp_get_themeoption_value('flickr_network','social_settings');
			$vimeo_network = neomag_cp_get_themeoption_value('vimeo_network','social_settings');
			$pinterest_network = neomag_cp_get_themeoption_value('pinterest_network','social_settings');
			$Instagram_network = neomag_cp_get_themeoption_value('Instagram_network','social_settings'); 
			$github_network = neomag_cp_get_themeoption_value('github_network','social_settings'); 
			$skype_network = neomag_cp_get_themeoption_value('skype_network','social_settings'); ?>
			<div class="<?php echo esc_attr($neomag_class);?>"> 
				<?php if(($facebook_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($facebook_network);?>" title="Facebook"><i class="fa fa-facebook-square"></i></a><?php }?>
				<?php if(($twitter_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($twitter_network);?>" title="Twitter"><i class="fa fa-twitter-square"></i></a><?php }?>
				<?php if(($delicious_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($delicious_network);?>" title="Delicious"><i class="fa fa-delicious"></i></a><?php }?>
				<?php if(($neomag_google_plus_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($neomag_google_plus_network);?>" title="Google Plus"><i class="fa fa-google-plus"></i></a><?php }?>
				<?php if(($linked_in_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($linked_in_network);?>" title="Linkedin"><i class="fa fa-linkedin"></i></a><?php }?>
				<?php if(($youtube_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($youtube_network);?>" title="Youtube"><i class="fa fa-youtube"></i></a><?php }?> 
				<?php if(($flickr_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($flickr_network);?>" title="Flickr"><i class="fa fa-flickr"></i></a><?php }?>
				<?php if(($vimeo_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($vimeo_network);?>" title="Vimeo"><i class="fa fa-vimeo-square"></i></a><?php }?>
				<?php if(($pinterest_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($pinterest_network);?>" title="Pinterest"><i class="fa fa-pinterest-square"></i></a><?php }?>
				<?php if(($Instagram_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($Instagram_network);?>" title="Instagram"><i class="fa fa-instagram"></i></a><?php }?>
				<?php if(($github_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($github_network);?>" title="Github"><i class="fa fa-github"></i></a><?php }?>
				<?php if(($skype_network) <> ''){ ?><a data-placement="<?php echo esc_attr($data_placement);?>" data-rel='tooltip' href="<?php echo esc_url($skype_network);?>" title="Skype"><i class="fa fa-skype"></i></a><?php }?>
			</div>
		<?php }
	}
	
	function cp_footer_logo_int(){
		$footer_logo = neomag_cp_get_themeoption_value('footer_logo','general_settings');
		$footer_link = neomag_cp_get_themeoption_value('footer_link','general_settings');
		$footer_logo_width = neomag_cp_get_themeoption_value('footer_logo_width','general_settings');
		$footer_logo_height = neomag_cp_get_themeoption_value('footer_logo_height','general_settings');
		if(esc_attr($footer_logo_width) == '' || esc_attr($footer_logo_width) == ' '){ $footer_logo_width = '191';}
		if(esc_attr($footer_logo_height) == '' || esc_attr($footer_logo_height) == ' '){ $footer_logo_height = '46';}
		?>
		<a class="ft-logo" title="<?php esc_attr(bloginfo('name')); ?><?php wp_title( ' - ', true, 'left' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<?php
			if(!empty($footer_logo)){ 
				$image_src_head = wp_get_attachment_image_src( $footer_logo, 'full' );
				$image_src_head = (empty($image_src_head))? '': $image_src_head[0];
				$thumb_src_preview = wp_get_attachment_image_src( $footer_logo, 'full');
				if(esc_url($thumb_src_preview[0]) <> ''){
					echo '<img width="'.esc_attr($footer_logo_width).'" height="'.esc_attr($footer_logo_height).'" src="'.esc_url($thumb_src_preview[0]).'" alt="footer logo" />';
				}else{
					echo '<img src="'.NEOMAG_PATH_URL.'/images/footer-logo.png" alt="footer logo" />';
				}
			}else{
				echo '<img src="'.NEOMAG_PATH_URL.'/images/footer-logo.png" alt="footer logo" />';
			}
			?>
		</a>
	<?php 
	}
	function neo_mag_cp_default_logo($logo_url =''){ 
			if(esc_attr($logo_url) == '' || esc_attr($logo_url) == ' '){
				$logo_url = 'logo-black.png';
			} 
			$header_logo_btn = neomag_cp_get_themeoption_value('header_logo_btn','general_settings');
			if(esc_attr($header_logo_btn) == 'enable'){ 
				$header_logo = neomag_cp_get_themeoption_value('header_logo2','general_settings');

				$logo_width = neomag_cp_get_themeoption_value('logo_width','general_settings');
				$logo_height = neomag_cp_get_themeoption_value('logo_height','general_settings');
				//Print Logo
				$image_src = '';
				if(!empty($header_logo)){ 
					if(is_numeric($header_logo)){ 
					$neomag_image_url = wp_get_attachment_image_src($header_logo, 'Full'); 
					$image_src = $neomag_image_url[0];
					} else {
					$image_src = $header_logo; } 		
				} ?>
				<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img class="logo_img" width="<?php if(esc_attr($logo_width) == '' or esc_attr($logo_width) == ' '){ echo '200'; }else{echo esc_attr($logo_width);}?>" height="<?php if(esc_attr($logo_height) == '' or esc_attr($logo_height) == ' '){ echo ''; }else{echo esc_attr($logo_height);}?>" src="<?php if($image_src <> ''){echo esc_url($image_src); }else{echo esc_url(NEOMAG_PATH_URL.'/images/'.esc_attr($logo_url).''); }?>" alt="<?php echo esc_attr(bloginfo( 'name' ));?>">
				</a>
			<?php }else{ 
				$logo_text_cp = neomag_cp_get_themeoption_value('logo_text_cp','general_settings');
				$logo_subtext = neomag_cp_get_themeoption_value('logo_subtext','general_settings'); ?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<h1><?php echo esc_attr($logo_text_cp);?></h1>
					<strong><?php echo esc_attr($logo_subtext);?></strong>
				</a>
			<?php }?>
	<?php }
		
	function cp_default_logo($logo_url =''){ 
			if(esc_attr($logo_url) == '' || esc_attr($logo_url) == ' '){
				$logo_url = 'logo.png';
			}
			$header_logo_btn = neomag_cp_get_themeoption_value('header_logo_btn','general_settings');
			if(esc_attr($header_logo_btn) == 'enable'){
				$header_logo = neomag_cp_get_themeoption_value('header_logo','general_settings');

				$logo_width = neomag_cp_get_themeoption_value('logo_width','general_settings');
				$logo_height = neomag_cp_get_themeoption_value('logo_height','general_settings');
				//Print Logo
				$image_src = '';
				if(!empty($header_logo)){ 
					$image_src = wp_get_attachment_image_src( $header_logo, 'full' );
					$image_src = (empty($image_src))? '': esc_url($image_src[0]);			
				} ?>
				<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img class="logo_img" width="<?php if(esc_attr($logo_width) == '' or esc_attr($logo_width) == ' '){ echo '200'; }else{echo esc_attr($logo_width);}?>" height="<?php if(esc_attr($logo_height) == '' or esc_attr($logo_height) == ' '){ echo ''; }else{echo esc_attr($logo_height);}?>" src="<?php if(esc_url($image_src) <> ''){echo esc_url($image_src);}else{echo esc_url(NEOMAG_PATH_URL.'/images/'.esc_attr($logo_url).'');}?>" alt="<?php echo esc_attr(bloginfo( 'name' ));?>">
				</a>
			<?php }else{
				$logo_text_cp = neomag_cp_get_themeoption_value('logo_text_cp','general_settings');
				$logo_subtext = neomag_cp_get_themeoption_value('logo_subtext','general_settings'); ?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<h1><?php echo esc_attr($logo_text_cp);?></h1>
					<strong><?php echo esc_attr($logo_subtext);?></strong>
				</a>
			<?php }?>
	<?php }
	
	//CP Mega menu
	function cp_mega_menu($location=''){
		if(has_nav_menu($location)){
			echo '<div class="navigation-bar"><div class="container">';
				$defaults = array(
				  'theme_location'  => $location,
				  'menu'            => '', 
				  'container'       => '', 
				  'container_class' => 'menu-{menu slug}-container', 
				  'container_id'    => 'navbar',
				  'menu_class'      => '', 
				  'menu_id'         => 'nav',
				  'echo'            => true,
				  'fallback_cb'     => '',
				  'before'          => '',
				  'after'           => '',
				  'link_before'     => '',
				  'link_after'      => '',
				  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				  'depth'           => 0,
				  'walker'          => '');		
				echo '<div id="custom_mega" class="cp_mega_plugin">';
					wp_nav_menu( $defaults);
				echo '</div>';
			echo '</div></div>';	
		}else if(has_nav_menu('mega_main_sidebar_menu')){
			echo '<div class="side-nav-container"><a id="no-show-menu-cp" class="menu_show_cp"><i class="fa fa-bars"></i></a>';
				$defaults = array(
				  'theme_location'  => 'mega_main_sidebar_menu',
				  'menu'            => '', 
				  'container'       => '', 
				  'container_class' => 'menu-{menu slug}-container', 
				  'container_id'    => 'navbar',
				  'menu_class'      => '', 
				  'menu_id'         => 'nav',
				  'echo'            => true,
				  'fallback_cb'     => '',
				  'before'          => '',
				  'after'           => '',
				  'link_before'     => '',
				  'link_after'      => '',
				  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				  'depth'           => 0,
				  'walker'          => '');		
				echo '<div id="custom_mega" class="cp_mega_plugin">';
					wp_nav_menu( $defaults);
				echo '</div>';
			echo '</div>';		
		}
	}
	
	function cp_search_html($icon_show=''){
		if(esc_attr($icon_show) == false){ ?>											
			<a href="#" id="no-active-btn" class="search cp_search_animate"><span class="hsearch"><i class="fa fa-search"></i></span></a>
		<?php }?>	
			<div id="cp_search" class="cp_search">
				<form class="cp_search-form" action="<?php  echo esc_url( home_url( '/' ) ); ?>/">
					<input name="s" class="cp_search-input" value="<?php esc_attr(the_search_query()); ?>" type="search" placeholder="Search..." />
					<button class="cp_search-submit" type="submit"><i class="fa fa-search"></i></button>
				</form>
				<span class="cp_search-close"></span>
			</div><!-- /cp_search -->
	<?php }
	
	function woo_commerce_cart($shop_text='',$shop_icon='',$shop_val=''){
		$cart_html = '';
		if(class_exists("Woocommerce")){
			$shop_html = '';
			if($shop_val == 'icon'){
				$shop_html = $shop_icon;
			}else if($shop_val == 'text'){
				if($shop_text <> ''){
					$shop_html = $shop_text;
				}else{
					$shop_html = 'Shopping Cart';
				}
			}
			
			global $post,$post_id,$product,$woocommerce;	
			$currency = get_woocommerce_currency_symbol();
			
			if($woocommerce->cart->cart_contents_count <> 0){ 
					$shopping_cart_div = '<div class="widget_shopping_cart_content"></div>';
			}else{ 
				$shopping_cart_div = '<div class="hide_cart_widget_if_empty"></div>';
			}
				
			$cart_html = '<li class="cart-item">
				<a href="#" class="btn-login" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Shopping" id="no-active-btn-shopping">
					'.html_entity_decode($shop_html).'</a>' .html_entity_decode($shopping_cart_div) .'</li>';		
		}
		
		return $cart_html;
	}