<?php
/**
 * Minha Casa Solar functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Minha_Casa_Solar
 */

if ( ! function_exists( 'minhacasasolar_blog_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function minhacasasolar_blog_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Minha Casa Solar, use a find and replace
		 * to change 'minhacasasolar_blog' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'minhacasasolar_blog', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'minhacasasolar_blog' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'minhacasasolar_blog_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'minhacasasolar_blog_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function minhacasasolar_blog_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'minhacasasolar_blog_content_width', 640 );
}
add_action( 'after_setup_theme', 'minhacasasolar_blog_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function minhacasasolar_blog_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'minhacasasolar_blog' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'minhacasasolar_blog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'minhacasasolar_blog_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function minhacasasolar_blog_scripts() {
	
	//FONTS
	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');
	wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css');

	//CSS
	wp_enqueue_style( 'minhacasasolar-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( 'minhacasasolar-owlcarousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( 'minhacasasolar-animate', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style( 'minhacasasolar-hover', get_template_directory_uri() . '/css/hover.css');
	wp_enqueue_style( 'minhacasasolar-style', get_stylesheet_uri() );

	//JAVA SCRIPT
	wp_enqueue_script( 'minhacasasolar-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );
	wp_enqueue_script( 'minhacasasolar-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'minhacasasolar-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );
	wp_enqueue_script( 'minhacasasolar-geral', get_template_directory_uri() . '/js/geral.js' );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'minhacasasolar_blog_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/********************************************
			FUNCÕES EXTRAS
*********************************************/


	//REDUX FRAMEWORK
	if (class_exists('ReduxFramework')) {
		require_once (get_template_directory() . '/redux/sample-config.php');
	}

	//REGISTRAR SVG
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

	//PAGINAÇÃO
	function pagination($pages = '', $range = 4){
	    $showitems = ($range * 2)+1;
	    global $paged;
	    if(empty($paged)) $paged = 1;

	    if($pages == ''){
	        global $wp_query;
	        $pages = $wp_query->max_num_pages;
	        if(!$pages){
	            $pages = 1;
	        }
	    }

	    if(1 != $pages){

	        echo "<div class=\"paginador\"><ul>";
	        //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
	        //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
			$htmlPaginas = "";
	        for ($i=1; $i <= $pages; $i++){
	            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
	                $htmlPaginas .= ($paged == $i)? '<li><a href="' . get_pagenum_link($i) . '" class="numero selecionado">' . $i . '</a></li>' : '<li><a href="' . get_pagenum_link($i) . '" class="numero">' . $i . '</a></li>';
	            }

	        }

	        if ($paged < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '" class="esquerda"><i class="fa fa-chevron-left" aria-hidden="true"></a></i></a>';
	        	echo $htmlPaginas;

			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($pages) . '" class="direita"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';

	        	echo "</ul></div>\n";

		}

	}

	//FUNÇÃO DE ABREVIAÇÃO DE CARACTER
	function customExcerpt($qtdCaracteres) {
	  $excerpt = get_the_excerpt();
	  $qtdCaracteres++;
	  if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {
	    $subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );
	    $exwords = explode( ' ', $subex );
	    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
	    if ( $excut < 0 ) {
	      echo mb_substr( $subex, 0, $excut );
	    } else {
	      echo $subex;
	    }

	    echo '...';
	  }else{
	    echo $excerpt;
	  }

	}

	//FUNÇÃO PARA REGISTRAR MENU
	function register_my_menus() {
		register_nav_menus(
			array(
				'minhacasaSolar' => __('Menu Minha Casa Solar'),
			)
		);
	}
	add_action( 'init', 'register_my_menus' );

	// VERSIONAMENTO DE FOLHAS DE ESTILO
	function my_wp_default_styles($styles){
		$styles->default_version = "18072018";
	}
	
	add_action("wp_default_styles", "my_wp_default_styles");

 	// VERSIONAMENTO DE FOLHAS DE ESTILO
	function my_wp_default_scripts($scripts){
		$scripts->default_version = "18072018";
	}
	add_action("wp_default_scripts", "my_wp_default_scripts");

	function disqus_count($disqus_shortname) {
	    wp_enqueue_script('disqus_count','http://'.$disqus_shortname.'.disqus.com/count.js');
	    echo '<a class="comentariosPost" href="'. get_permalink() .'#disqus_thread"></a>';
	}
