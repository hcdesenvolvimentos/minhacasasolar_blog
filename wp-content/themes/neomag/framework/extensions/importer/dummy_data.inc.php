<?php
/** 
     * @author Roy Stone
     * @copyright roshi[www.themeforest.net/user/crunchpress]
     * @version 2013
     */

if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

require_once ABSPATH . 'wp-admin/includes/import.php';

$import_filepath = get_template_directory()."/framework/extensions/importer/dummy_data";
$errors = false;
if ( !class_exists( 'WP_Importer' ) ) {
	$neomag_class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
	if ( file_exists( $neomag_class_wp_importer ) )
	{
		require_once($neomag_class_wp_importer);
	}
	else
	{
		$errors = true;
	}
}
if ( !class_exists( 'WP_Import' ) ) {
	$wp_importer = NEOMAG_FW. '/extensions/importer/wordpress-importer.php';
	if ( file_exists( $wp_importer ) )
	{
		require_once($wp_importer);
	}
	else
	{
		$errors = true;
	}
}

if($errors){
   echo "Errors while loading classes. Please use the standart wordpress importer."; 
}else{
    
    
	include_once('default_dummy_data.inc.php');
	if(!is_file($import_filepath.'_1.xml'))
	{
		echo "Problem with dummy data file. Please check the permisions of the xml file";
	}
	else
	{  
	   if(class_exists( 'WP_Import' )){
	       global $wp_version;
			$our_class = new themeple_dummy_data();
			$our_class->fetch_attachments = true;
			$our_class->import($import_filepath.'_1.xml');
		
$widget_recent_news_show = array (
  2 => 
  array (
    'wid_class' => NULL,
    'title' => 'Latest Blog',
    'recent_post_category' => 'english-primier-league',
    'news_layout' => 'List',
    'number_of_news' => '3',
  ),
  3 => 
  array (
    'wid_class' => NULL,
    'title' => 'Latest News',
    'recent_post_category' => 'english-primier-league',
    'news_layout' => 'Slider',
    'number_of_news' => '5',
  ),
  4 => 
  array (
    'wid_class' => NULL,
    'title' => 'Latest News',
    'recent_post_category' => 'english-primier-league',
    'news_layout' => 'List',
    'number_of_news' => '3',
  ),
  '_multiwidget' => 1,
);$widget_text = array (
  2 => 
  array (
    'title' => '',
    'text' => '<div class="span4"> <img src="http://themeink.com/dummy/sports/default/wp-content/uploads/2015/03/footer-mag.jpg" alt=""> </div>',
    'filter' => false,
  ),
  3 => 
  array (
    'title' => 'Contact Us',
    'text' => '[contact-form-7 id="438" title="Footer Contact Form"]',
    'filter' => false,
  ),
  4 => 
  array (
    'title' => 'Contact Information',
    'text' => '<div class="span3">

<div class="contact-info">

<p>Lorem LTD<br>

United Kingdom<br>

Oxford Street 48/188<br>

Working Days: Mon. - Sun.<br>

Working Hours: 9.00AM - 8.00PM</p>

</div>

<h3>Join Now</h3>

<ul class="footer-social">

<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>

<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>

<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>

<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>

</ul>

</div>',
    'filter' => false,
  ),
  '_multiwidget' => 1,
);$widget_woocommerce_product_categories = array (
  3 => 
  array (
    'title' => 'Accesiories',
    'orderby' => 'name',
    'dropdown' => 0,
    'count' => 0,
    'hierarchical' => '1',
    'show_children_only' => 0,
  ),
  '_multiwidget' => 1,
);$widget_categories = array (
  3 => 
  array (
    'title' => 'Our Offers',
    'count' => 0,
    'hierarchical' => 1,
    'dropdown' => 0,
  ),
  '_multiwidget' => 1,
);$widget_twitter_widget = array (
  2 => 
  array (
    'title' => 'Latest Tweets',
    'consumer_key' => '1iUu8muQcbDfv4UAp58rXw',
    'consumer_secret' => 'am535ByNUMFFo8vHQtpkVpgdJz9QgcW4FpWaGDvH5Xw',
    'user_token' => '88209931-h16p4dNkvaXe0UQTRAx8zzPcWgl3L7rXj2XDOT5c2',
    'user_secret' => '3ugAtUwyrCcXK99xleZssr2OkiwVymoB2ceFwknYwLk',
    'username_widget' => 'neomag',
    'num_of_tweets' => '1-',
  ),
  '_multiwidget' => 1,
);$widget_em_widget = array (
  2 => 
  array (
    'title' => 'Events',
    'limit' => '5',
    'scope' => 'future',
    'orderby' => 'event_start_date,event_start_time,event_name',
    'order' => 'ASC',
    'category' => '0',
    'all_events_text' => 'all events',
    'format' => '<li>#_EVENTLINK<ul><li>#_EVENTDATES</li><li>#_LOCATIONTOWN</li></ul></li>',
    'no_events_text' => '<li>No events</li>',
    'nolistwrap' => false,
    'all_events' => 0,
  ),
  '_multiwidget' => 1,
);$widget_search = array (
  2 => 
  array (
    'title' => 'Search',
  ),
  '_multiwidget' => 1,
);$widget_popular_post = array (
  2 => 
  array (
    'title' => 'Popular Posts',
    'get_cate_posts' => NULL,
    'nop' => '5',
  ),
  '_multiwidget' => 1,
);$widget_recent = false;$widget_newsletter_widget = array (
  2 => 
  array (
    'title' => 'Newsletter',
    'show_name' => NULL,
    'news_letter_des' => '',
  ),
  '_multiwidget' => 1,
);$widget_flickr_widget = array (
  2 => 
  array (
    'title' => ' Flikr Gallery Widget',
    'type' => 'user',
    'flickr_id' => '  wajidali_14_56@yahoo.com',
    'count' => ' 5',
    'display' => 'latest',
    'size' => 'latest',
    'copyright' => NULL,
  ),
  '_multiwidget' => 1,
);$widget_gallery_image_show = array (
  2 => 
  array (
    'wid_class' => '',
    'title' => 'Gallery',
    'select_gallery' => '424',
    'nofimages' => '3',
    'externallink' => NULL,
  ),
  '_multiwidget' => 1,
);$widget_nav_menu = array (
  2 => 
  array (
    'title' => 'Custom Menu',
  ),
  '_multiwidget' => 1,
);$sidebars_widgets=array (
  'wp_inactive_widgets' => 
  array (
  ),
  'sidebar-footer' => 
  array (
    0 => 'recent_news_show-2',
    1 => 'text-2',
    2 => 'text-3',
  ),
  'sidebar-upper-footer' => 
  array (
    0 => 'woocommerce_product_categories-3',
    1 => 'categories-3',
    2 => 'twitter_widget-2',
    3 => 'text-4',
  ),
  'custom-sidebar0' => 
  array (
    0 => 'em_widget-2',
    1 => 'recent_news_show-3',
  ),
  'custom-sidebar1' => 
  array (
    0 => 'search-2',
    1 => 'popular_post-2',
    2 => 'recent-posts-2',
  ),
  'custom-sidebar2' => 
  array (
  ),
  'custom-sidebar3' => 
  array (
    0 => 'newsletter_widget-2',
  ),
  'custom-sidebar4' => 
  array (
    0 => 'flickr_widget-2',
    1 => 'gallery_image_show-2',
    2 => 'recent_news_show-4',
    3 => 'nav_menu-2',
  ),
  'custom-sidebar5' => 
  array (
  ),
  'array_version' => 3,
);
$show_on_front = 'page';
$page_on_front = 285;
$theme_mods_sports = array (
  0 => false,
  'nav_menu_locations' => 
  array (
    'header-menu' => 7,
  ),
);




		//Default Widgets
			save_option('widget_recent_news_show','', $widget_recent_news_show);
			save_option('widget_text','', $widget_text);	
			save_option('widget_woocommerce_product_categories','', $widget_woocommerce_product_categories);			
			save_option('widget_categories','', $widget_categories);			
			save_option('widget_twitter_widget','', $widget_twitter_widget);			
			save_option('widget_em_widget','', $widget_em_widget);	
			save_option('widget_search','', $widget_search);	
			save_option('widget_popular_post','', $widget_popular_post);
			save_option('widget_recent','', $widget_recent);
			save_option('widget_newsletter_widget','', $widget_newsletter_widget);
			save_option('widget_flickr_widget','', $widget_flickr_widget);
			save_option('widget_gallery_image_show','', $widget_gallery_image_show);
			save_option('widget_nav_menu','', $widget_nav_menu);
			save_option('sidebars_widgets','', $sidebars_widgets);
				
			//Default Widgets
			save_option('show_on_front','', $show_on_front);
			save_option('page_on_front','', $page_on_front);
			save_option('theme_mods_sports','', $theme_mods_sports);			
		

        }
	}    
}


?>