<?php 
/*	
*	CrunchPress Pagination File
*	---------------------------------------------------------------------
* 	@version	1.0
* 	@author		CrunchPress
* 	@link		http://crunchpress.com
* 	@copyright	Copyright (c) CrunchPress
*	---------------------------------------------------------------------
*	This file return the Pagination to the selected post_type
*	---------------------------------------------------------------------
*/
	
	if( !function_exists('pagination') ){
		function cp_pagination($pages = '', $range = 4)
		{
			
			// Don't print empty markup if there's only one page.
			if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
				return;
			}

			$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
			$pagenum_link = html_entity_decode( get_pagenum_link() );
			$query_args   = array();
			$url_parts    = explode( '?', $pagenum_link );

			if ( isset( $url_parts[1] ) ) {
				wp_parse_str( $url_parts[1], $query_args );
			}

			$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
			$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

			$neomag_format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
			$neomag_format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

			// Set up paginated links.
			$links = paginate_links( array(
				'base'     => $pagenum_link,
				'format'   => $neomag_format,
				'total'    => $GLOBALS['wp_query']->max_num_pages,
				'current'  => $paged,
				'mid_size' => 1,
				'add_args' => array_map( 'urlencode', $query_args ),
				'prev_text' => esc_html__( '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>','neomag' ),
				'next_text' => esc_html__( '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>','neomag' ),
			) );

			if ( $links ) :

			?>
            
 			<div class="paging">           
              <ul class="pagination">
                <li>
	                <?php echo html_entity_decode($links); ?>
                </li>
              </ul>
         	</div><!-- .pagination -->        

			<?php
			endif;

		}
	}
	
	
	if( !function_exists('cp_post_nav') ){
		function cp_post_nav() {
			// Don't print empty markup if there's nowhere to navigate.
			$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
			$next     = get_adjacent_post( false, '', false );

			if ( ! $next && ! $previous ) {
				return;
			}

			?>			
			<div class="nav-links">
				<?php
				if ( is_attachment() ) :
					previous_post_link( '%link', esc_html__( '<span class="meta-nav">Publicado em</span>%title','neomag' ) );
				else :
					previous_post_link( '%link', esc_html__( '<span class="meta-nav">Post Anterior</span>%title','neomag' ) );
					next_post_link( '%link', esc_html__( '<span class="meta-nav">Próximo Post</span>%title','neomag' ) );
				endif;
				?>
			</div><!-- .nav-links -->			
			<?php
		}
	}
	
	
	if( !function_exists('cp_post_next') ){
		function cp_post_next() {
			// Don't print empty markup if there's nowhere to navigate.
			$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
			$next     = get_adjacent_post( false, '', false );

			if ( ! $next && ! $previous ) {
				return;
			}

				if ( is_attachment() ) :
					echo '<div class="portfolio-thumb">';
					previous_post_link( '%link', esc_html__( '<span class="meta-nav">Publicado em</span>%title','neomag' ) );
					echo '</div>';
				else :
					echo '<div class="portfolio-thumb">';
					previous_post_link( '%link', esc_html__( '<span class="meta-nav">Próximo Post</span>%title','neomag' ) );
					next_post_link( '%link', esc_html__( '<span class="meta-nav">Próximo Post</span>%title','neomag' ) );
					echo '</div>';
				endif;
		}
	}
	
	if( !function_exists('pagination_crunch') ){
		function pagination_crunch($pages = '', $range = 4)
		{
			 $showitems = ($range * 2)+1;  
		 
			 global $paged;
			 if(empty($paged)) $paged = 1;
	
			 if($pages == '')
			 {
				 global $wp_query;
				 
				 $pages = $wp_query->max_num_pages;
				 
				 if(!$pages)
				 {
					 $pages = 1;
				 }
			 }   
		 
			 if(1 != $pages)
			 {		
				echo '<div class="cp-pagination pd-tp50"><nav>';
				  echo "<ul class='pagination'>";  
				 if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo; Primeiro</a></li>";
				 if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Próximo</a></li>";
		 
				 for ($i=1; $i <= $pages; $i++)
				 {
					 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
					 {
						 echo ($paged == $i)? '<li class="active"><span>'.$i.' <span class="sr-only">(current)</span></span></li>':"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
					 }
				 }
		 
				 if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\">Próximo &rsaquo;</a></li>";
				 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>Último &raquo;</a></li>";
				 echo "</ul>\n";
				 if(class_exists("Woocommerce")){ if(is_shop()){} else { echo '</nav></div>'; } } else { echo '</nav></div>'; }
			 }
		}
	}
	
	if( !function_exists('neo_mag_pagination') ){
		function neo_mag_pagination($pages = '', $range = 4)
		{
			 $showitems = ($range * 2)+1;  
		 
			 global $paged;
			 if(empty($paged)) $paged = 1;
	
			 if($pages == '')
			 {
				 global $wp_query;
				 
				 $pages = $wp_query->max_num_pages;
				 
				 if(!$pages)
				 {
					 $pages = 1;
				 }
			 }   
		 
			 if(1 != $pages)
			 {		
				echo '<div class="more-btn">';
				 if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>Posts Anteriores</a>";
				 if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>Posts Anteriores</a>";
		 
				 for ($i=1; $i <= $pages; $i++)
				 {
					 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
					 {
						 echo ($paged == $i)? '<span class="sr-only">(current)</span>':"<a href='".get_pagenum_link($i)."' class=\"inactive\">Posts Anteriores</a>";
					 }
				 }
		 
				 if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Posts Anteriores</a>";
				 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Posts Anteriores</a>";
			 }
		}
	}
	
?>