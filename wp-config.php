<?php



/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_minhacasasolar_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


define('WP_MEMORY_LIMIT', '64M');

// define('WP_HOME','http://minhacasasolar.handgran.com.br/');
// define('WP_SITEURL','http://minhacasasolar.handgran.com.br/');
// define('RELOCATE',true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's`XhV~[8!=.vl!@Mb2!?U69V3-;42`%;WztEf2QNaOg =Q?<1m%Y;&%k}|S `cbk');
define('SECURE_AUTH_KEY',  'lVt4xg)!{)e3Jr+R7^Qc8VmEZasU2ynj!k~.e~BjYYy8HUHWcT S-BX=2b%kQ]1B');
define('LOGGED_IN_KEY',    ']A)+l,9-f^?l%`C{,_*/AMM0%49<dh+;<c<:sNbA0O>mMNYO,QPwd=+b:cy/zJ[Y');
define('NONCE_KEY',        'ZcPbi[L+erZ]|AD-yz!gM7[/].<~7G[fWW`vo>I6wwMwXV|*ObAUT&Gf.;Gg^Mj0');
define('AUTH_SALT',        'N<iU;F+Sn@ygs/uc9[_*)=f)z+ Y F+#,NcF]Yiuz-hx Ml bomI/z9IL0{-w2)j');
define('SECURE_AUTH_SALT', 'ht[)e<Hw?k`>-w*_-wHbR8m`Wnih`{uVRo=G7|qKV0:`t7qyAAO4n2IvZ4$#LclY');
define('LOGGED_IN_SALT',   'ffBlD6N$D+%gF[}o]A{4%h$H_hc>+nJdekB{BZ0-_u*9yJYI$cYd}LDK9<;a7*|q');
define('NONCE_SALT',       'P#YL}z:DY{0HI&g] V;4dx<trQ[qcTw|+I@AsI^E^1dn8gpC)Ha0&r<noEhMQ.&w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mcs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
