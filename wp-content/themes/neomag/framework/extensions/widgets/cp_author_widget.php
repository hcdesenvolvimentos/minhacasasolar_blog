<?php
class author_widget extends WP_Widget
{
  function author_widget()
  {
    $widget_ops = array('classname' => 'widget_text footer-box-1', 'description' => 'author-widget widget' );
    parent::__construct('author_widget', 'CrunchPress : Author Widget', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
	$title = $instance['title'];
	$show_name = isset( $instance['show_name'] ) ? ( $instance['show_name'] ) : '';	
	$author_image = isset( $instance['author_image'] ) ? ( $instance['author_image'] ) : '';	
	$author_text = isset( $instance['author_text'] ) ? ( $instance['author_text'] ) : '';	
?>
  <p>
  <label for="<?php echo ($this->get_field_id('title')); ?>">
	 <?php esc_html_e('Title:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('title')); ?>" name="<?php echo ($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </label>
  </p> 
  <p>
  <label for="<?php echo ($this->get_field_id('author_image')); ?>">
	 <?php esc_html_e('Author image url:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('author_image')); ?>" name="<?php echo ($this->get_field_name('author_image')); ?>" type="text" value="<?php echo esc_attr($author_image); ?>" />
  </label>
  </p> 
  <p>
  <label for="<?php echo ($this->get_field_id('author_text')); ?>">
	 <?php esc_html_e('Author Text:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('author_text')); ?>" name="<?php echo ($this->get_field_name('author_text')); ?>" type="text" value="<?php echo esc_attr($author_text); ?>" />
  </label>
  </p> 
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['show_name'] = $new_instance['show_name'];
		$instance['author_image'] = $new_instance['author_image'];
		$instance['author_text'] = $new_instance['author_text'];
    return $instance;
  }
 
	function widget($args, $instance)
	{
		
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$show_name = isset( $instance['show_name'] ) ? ( $instance['show_name'] ) : '';
		$author_image = isset( $instance['author_image'] ) ? ( $instance['author_image'] ) : '';		
		$author_text = isset( $instance['author_text'] ) ? ( $instance['author_text'] ) : '';		
		
		echo html_entity_decode($before_widget);	
		// WIDGET display CODE Start
			echo '<h3>'.($title).'</h3>';
			$image = aq_resize( $author_image, 228, 280, true );
			?>
              <div class="widget-content"> 
              <?php if($author_image <> ''){ ?>
              <img src="<?php echo esc_url($image); ?>" alt="authorImage">
              <?php } ?>
              <?php if($author_text <> ''){ ?>
                <p><?php echo esc_attr($author_text); ?></p>
              <?php } ?>
              </div>
			<?php
	
	echo html_entity_decode($after_widget);
	}
		
}
add_action( 'widgets_init', create_function('', 'return register_widget("author_widget");') );?>