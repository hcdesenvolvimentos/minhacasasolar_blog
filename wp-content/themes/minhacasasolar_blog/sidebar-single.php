<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Minha_Casa_Solar
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<!-- OUTROS POSTS -->
<aside>

	<!-- POSTS MAIS LIDOS -->
	<div class="postsMaislidos">
		<span>Posts mais lidos</span>

		<?php dynamic_sidebar( 'sidebar-1' ); ?>

		
	</div>

	<!-- BANNERS -->
	<div class="bannersSidebar">
		<a href="<?php echo $configuracao['link_banner'];  ?>" target="_blank">
			<img src="<?php echo $configuracao['bannerPropaganda']['url'];  ?>" alt="Banner">
		</a>
	</div>

	<!-- NEWSLLATER -->
	<div class="areaNewsllater">
		<figure>
			<img src="<?php echo get_bloginfo("template_url"); ?>/img/if_a_9_2578125.png" alt="">
		</figure>
		
		<p>Receba conteúdo em primeira mão</p>
			<div class="form">

			
			<!--START Scripts : this is the script part you can add to the header of your theme-->
			<script type="text/javascript" src="<?php echo get_site_url() ?>/wp-includes/js/jquery/jquery.js?ver=2.8.2"></script>
			<script type="text/javascript" src="<?php echo get_site_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.8.2"></script>
			<script type="text/javascript" src="<?php echo get_site_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.2"></script>
			<script type="text/javascript" src="<?php echo get_site_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.2"></script>
			<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_site_url() ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
			</script><script type="text/javascript" src="<?php echo get_site_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.2"></script>
			<!--END Scripts-->


			<div class="widget_wysija_cont html_wysija">
				<div id="msg-form-wysija-html5b3cda2db852d-1" class="wysija-msg ajax"></div>
				<form id="form-wysija-html5b3cda2db852d-1" method="post" action="#wysija" class="widget_wysija html_wysija">
  
				    <small>
			    		
				   		<input type="text" name="wysija[user][firstname]" class="validate[required]" title="Nome" placeholder="Nome" value="" />
    
				    </small>
				    <span class="abs-req">
				        <input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
				    </span>
				
					<small>
						<input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email" placeholder="Email" value="" />
    
					</small>

				    <span class="abs-req">
				        
				        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
				    </span>

					<input class="wysija-submit wysija-submit-field" type="submit" value="Assinar!" />

					<input type="hidden" name="form_id" value="1" />
					<input type="hidden" name="action" value="save" />
					<input type="hidden" name="controller" value="subscribers" />
					<input type="hidden" value="1" name="wysija-page" />


					<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
				</form> 
			</div>
		</div>
	</div>

</aside>
