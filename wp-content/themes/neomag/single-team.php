<?php get_header();  ?>
<?php if ( have_posts() ){ while (have_posts()){ the_post();
	global $post;
	setPostViews(get_the_ID());
	
	// Get Post Meta Elements detail 
	$neomag_post_social = '';
	$neomag_sidebar = '';
	$neomag_right_sidebar = '';
	$neomag_left_sidebar = '';
	$neomag_thumbnail_types = '';
	$neomag_cp_page_caption = '';
	$neomag_post_loc = '';
	$neomag_designation = '';
	$neomag_post_slider_id = '';
	$neomag_get_designation = get_post_custom();
	if(isset($neomag_get_designation['designation_text'])){ $neomag_designation = $neomag_get_designation['designation_text']; }
	
	$neomag_post_format = get_post_meta($post->ID, 'post_format', true);
	$neomag_team_detail_xml = get_post_meta($post->ID, 'team_detail_xml', true);
	if($neomag_team_detail_xml <> ''){
		$neomag_team_xml = new DOMDocument ();
		$neomag_team_xml->loadXML ( preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $neomag_team_detail_xml) );
		$neomag_team_social = cp_find_xml_value($neomag_team_xml->documentElement,'team_social');
		$neomag_sidebar = cp_find_xml_value($neomag_team_xml->documentElement,'sidebar_team');
		$neomag_left_sidebar = cp_find_xml_value($neomag_team_xml->documentElement,'left_sidebar_team');
		$neomag_right_sidebar = cp_find_xml_value($neomag_team_xml->documentElement,'right_sidebar_team');
		$neomag_team_designation = cp_find_xml_value($neomag_team_xml->documentElement,'team_designation');
		$neomag_team_facebook = cp_find_xml_value($neomag_team_xml->documentElement,'team_facebook');
		$neomag_team_caption = cp_find_xml_value($neomag_team_xml->documentElement,'team_caption');
		
		$neomag_team_linkedin = cp_find_xml_value($neomag_team_xml->documentElement,'team_linkedin');
		$neomag_google_plus = cp_find_xml_value($neomag_team_xml->documentElement,'team_google');
		$neomag_team_twitter = cp_find_xml_value($neomag_team_xml->documentElement,'team_twitter');
	}
	$neomag_select_layout_cp = '';
	$neomag_cp_general_settings = get_option('general_settings');
	if($neomag_cp_general_settings <> ''){
		$neomag_cp_logo = new DOMDocument ();
		$neomag_cp_logo->loadXML ( $neomag_cp_general_settings );
		$neomag_select_layout_cp = cp_find_xml_value($neomag_cp_logo->documentElement,'select_layout_cp');
	}
	
	$neomag_sidebar_class = '';
	$neomag_content_class = '';
	$neomag_get_post_cp = get_post($post);
	
	//Get Sidebar for page
	$neomag_sidebar_class = cp_sidebar_func($neomag_sidebar);
	$neomag_header_style = '';
	$neomag_html_class_banner = '';
	$neomag_html_class = cp_print_header_class($neomag_header_style);
	if($neomag_html_class <> ''){$neomag_html_class_banner = 'banner';}
	$neomag_breadcrumbs = '';
	$neomag_breadcrumbs = neomag_cp_get_themeoption_value('breadcrumbs','general_settings');
	$neomag_header_style = '';
	//Print Style 6
	if(cp_print_header_html_val($neomag_header_style) == 'Style 7'){
		cp_print_header_html($neomag_header_style);
	} 
	$neomag_post_format = get_post_format($post->ID);
	$neomag_check_post_std = get_post_format();
	if ( false === $neomag_check_post_std ) {
					if($neomag_thumbnail_types == 'Slider'){ $neomag_post_format = 'slider post'; } else
						{ $neomag_post_format = 'standard post'; }
					}
					$neomag_post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
					if($neomag_post_detail_xml <> ''){ 
						$neomag_cp_post_xml = new DOMDocument ();
						$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml ); 
						$neomag_thumbnail_types = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_thumbnail');
						$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
						$neomag_select_slider_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'select_slider_type');	
						$neomag_audio_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'audio_url_type');
					}
					$neomag_thumbnail_id = get_post_thumbnail_id( $post->ID );
					$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, 'neomag_std_post');
	?>
		<!--Inner Pages Heading Area Start-->
        <div class="cp-page-title">
              <h2><?php echo esc_attr($neomag_post_format); ?></h2>
        
                <?php if($neomag_breadcrumbs == 'enable'){
					if(!is_front_page()){
						echo cp_breadcrumbs(); 
					}
                 } ?>
    	</div>
		<!--Inner Pages Heading Area End--> 		
	<?php if($neomag_post_slider_id <> ''){ ?>
          <!--BANNER START-->
          <div class="cp-post-carousel">
            <div id="cp-post-carousel" class="owl-carousel">
          <?php    	query_posts(
					array( 
					'post_type' => 'post',
					'posts_per_page' => -1,
					'ignore_sticky_posts' => true,
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'terms' => $neomag_post_slider_id,
							'field' => 'term_id',
						)
					),
					'orderby' => 'date',
					'order' => 'DESC' )
				);
			if(have_posts()){
					while( have_posts() ){
					the_post(); 
					$neomag_thumbnail_id_post = get_post_thumbnail_id( $post->ID );
					$neomag_image_thumb_post = wp_get_attachment_image_src($neomag_thumbnail_id_post, 'neomag_std_post');
					$neomag_format = get_post_format(); 
					if ( false === $neomag_format ) {
						$neomag_format = 'standard';
					}
					$neomag_author = get_the_author();
					?>
					
              <div class="item"><img class="lazyOwl" src="<?php echo esc_url($neomag_image_thumb_post[0]); ?>" alt="<?php echo 'post_slider_'.$post->ID; ?>">
                <div class="cp-slider-content"> <strong><?php esc_html_e('Written ','neomag'); echo esc_attr($neomag_author); ?></strong>
                  <h2><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></h2>
                  <ul class="cp-post-meta">
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj ); 
                              ?>
                    <li><a href="<?php echo get_category_link( $neomag_category_name->cat_ID ); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                    <li><a><?php echo esc_attr($neomag_format); ?></a></li>
                  </ul>
                </div>
              </div>
					
					
			<?php		} //end while
					wp_reset_query();
					}
			?>
            
            </div>
          </div>
          <!--BANNER END--> 
	<?php } ?>
  <!--CONTENT START-->
  <div id="cp-content-wrap" class="cp-content-wrap">
    <div class="container">
      <div class="row"> 
        <!--SIDEBAR START-->
			<?php
			if($neomag_sidebar == "left-sidebar" || $neomag_sidebar == "both-sidebar" || $neomag_sidebar == "both-sidebar-left"){?>
				<div id="block_first" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
					<?php dynamic_sidebar( $neomag_left_sidebar ); ?>
				</div>
			<?php
				}
			if($neomag_sidebar == 'both-sidebar-left'){?>
				<div id="block_first_left" class="sidebar side-bar <?php echo esc_attr($neomag_sidebar_class[0]);?>">
					<?php dynamic_sidebar( $neomag_right_sidebar );?>
				</div>
			<?php } ?>
        <!--SIDEBAR END--> 
        <!--LEFT CONTENT START-->
        <div class="col-md-9">
          <div class="cp-posts-style-1">
            <ul class="cp-posts-list cp-post-details">
              
              <!--Post Start-->
              <li class="cp-post">
              
                  <?php if($neomag_post_format == 'link'){ 
                // Get the text & url from the first link in the content
                $neomag_content = get_the_content();
                $neomag_link_string = neo_mag_extract_link('<a href=', '/a>', $neomag_content);
                $neomag_link_bits = explode('"', $neomag_link_string);
                foreach( $neomag_link_bits as $neomag_bit ) {
                    if( substr($neomag_bit, 0, 1) == '>') $neomag_link_text = substr($neomag_bit, 1, strlen($neomag_bit)-2);
                    if( substr($neomag_bit, 0, 4) == 'http') $neomag_link_url = $neomag_bit;
                } ?>
                    <div class="cp-thumb link-post"> 
                        <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                             <div class="cp-link-post-link">
                                    <i class="fa fa-link"></i> <a href="<?php echo esc_url($neomag_link_url); ?>" target="_blank"><?php echo esc_attr($neomag_link_text); ?></a>
                             </div>
                    </div>
                  <?php } ?>                  
                  <?php if(($neomag_post_format == 'standard post')||($neomag_post_format == 'chat')){ 
				  			if($neomag_image_thumb[0] <> ''){
				  ?>
                <div class="cp-thumb">
	                <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                </div>
                  <?php } } ?>                  
                  <?php if($neomag_post_format == 'video'){ ?>
                <div class="cp-thumb">
                  <iframe src="<?php echo esc_url($neomag_video_url_type); ?>"></iframe> 
                </div>
                  <?php } ?>
                  <?php if($neomag_post_format == 'slider post'){ ?>
                    <div class="cp-thumb">
					<?php echo cp_print_blog_thumbnail($post->ID, 849); ?>
                    </div>
                  <?php } ?>                  
                  <?php if($neomag_post_format == 'audio'){ ?>
                    <div class="cp-thumb">
					<?php echo cp_print_blog_thumbnail($post->ID, 849); ?>
                    </div>
                  <?php } ?>                  
                <div class="cp-post-base">
                  
                  <!--Post Content Start-->
                  <div class="cp-post-content">
                    <a href="<?php echo get_permalink();?>"><h2><?php echo get_the_title(); ?></h2></a>
                    <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = wp_get_post_terms( $post->ID, 'team-category' ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_term_link($neomag_category_name->term_id, 'team-category');
									
								if($neomag_category_name <> ''){
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                              <?php } ?>
                      <li>
                           <?php
						   global $allowedposttags;
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<i class="fa fa-comment-o"></i> Leave a Comment','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                      
                      </li>
                    </ul>
                    
                    <div class="cp-content">
                        <ul class="cp_social-links">
                              <?php if(isset($neomag_team_facebook) AND $neomag_team_facebook <> ''){?>
                                <li><a href="<?php echo esc_url($neomag_team_facebook); ?>"><i class="fa fa-facebook"></i></a></li>
                              <?php }?>
                              <?php if(isset($neomag_team_twitter) AND $neomag_team_twitter <> ''){?>
                                <li><a href="<?php echo esc_url($neomag_team_twitter); ?>"><i class="fa fa-twitter"></i></a></li>
                              <?php }?>
                              <?php if(isset($team_google) AND $team_google <> ''){?>
                                <li><a href="<?php echo esc_url($team_google); ?>"><i class="fa fa-google-plus"></i></a></li>
                              <?php }?>
                              <?php if(isset($neomag_team_linkedin) AND $neomag_team_linkedin <> ''){?>
                                <li><a href="<?php echo esc_url($neomag_team_linkedin); ?>"><i class="fa fa-linkedin"></i></a></li>
                              <?php }?>
                        </ul>
                    </div>
					<?php the_content();?>
                  </div>
                  <!--Post Content End-->
                  
                  <!--Share Start-->
                  <div class="cp-post-share">
                    <?php cp_include_social_shares();?>
                  </div>
                  <!--Share End-->
                  <!--Related Posts-->
                  <div class="cp-related-posts">
				<?php
                $neomag_related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
				?>
                <?php if( $neomag_related ){ ?>
                    <h4><?php esc_html_e('You May Also Like','neomag'); ?></h4>
                    <ul class="cp-posts" id="related-post-heading">
                <?php } ?>
                <?php if( $neomag_related ) foreach( $neomag_related as $post ) {
                setup_postdata($post); 
					$neomag_thumbnail_id = get_post_thumbnail_id( $post->ID );
					$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, 'full');
					$neomag_image = aq_resize( $neomag_image_thumb[0], 217, 134, true ); 
				
				?>
                      <li class="cp-post">
                        <div class="cp-thumb-r">
                        <?php if($neomag_image <> ''){ ?>
                        <img src="<?php echo esc_url($neomag_image); ?>" alt="<?php if($neomag_image <> ''){ echo esc_attr($post->ID); } ?>">
                        <?php } ?>
                        </div>
                        <div class="cp-post-text">
                          <h4><a href="<?php the_permalink() ?>"><?php echo substr(get_the_title(),0,20); if(strlen(get_the_title()) > 20 ){ echo '...'; } ?></a></h4>
                          <strong><?php echo get_the_date('F');?> <?php echo get_the_date('j, Y');?></strong> 
                        </div>
                      </li>
                <?php }
                  if( $neomag_related ){ ?>
                    </ul>
                <?php } ?>
                  </div>
                  <!--Related Posts End-->
                  <!--Comments Start-->
                  <div class="cp-post-comments-form">
                  <div class="row">
                  
                  <?php comments_template(); ?>
            <?php if(is_user_logged_in()){ ?>
                  <!--Comments End-->
              </li>
              <!--Post End-->
            </ul>
        <?php } else{  echo '</div></div></div></li></ul></div>'; }?>
        </div>
        <!--LEFT CONTENT END--> 
        
        <!--SIDEBAR START-->
			<?php
				if($neomag_sidebar == "both-sidebar-right"){?>
					<div class="<?php echo esc_attr($neomag_sidebar_class[0]);?> side-bar">
						<?php dynamic_sidebar( $neomag_left_sidebar ); ?>
					</div>
			<?php
					}
				if($neomag_sidebar == 'both-sidebar-right' || $neomag_sidebar == "right-sidebar" || $neomag_sidebar == "both-sidebar"){?>
					<div class="<?php echo esc_attr($neomag_sidebar_class[0]);?> side-bar">
						<?php dynamic_sidebar( $neomag_right_sidebar );?>
					</div>
			<?php } ?>				
        <!--SIDEBAR END--> 
        
      </div>
    </div>
  </div>
  <!--CONTENT END--> 
<?php 
	}
}
?>
<div class="clear"></div>
<?php get_footer(); ?>