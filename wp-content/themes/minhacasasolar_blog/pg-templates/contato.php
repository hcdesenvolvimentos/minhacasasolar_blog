<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package Minha_Casa_Solar
 */
	$fotoContato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoContato = $fotoContato[0];
get_header(); ?>
<!-- PAGINA DE CONTATO-->
<div class="pg pg-contato">
	
	<!-- BANNER PAGINA CONTATOS -->
	<figure class="bannerPost" style="background: url(<?php echo $fotoContato ?>)">
	</figure>

	<div class="containerLagura">
		<div class="row">
			
			<div class="col-sm-6">
				<!-- ARTICLE PARA DEFINIAR A ESTRUTURA DO TEXTO -->
				<article>
					<?php while ( have_posts() ):the_post(); echo the_content();  endwhile; ?>
				</article>
				
				<!-- DIV QUE REPRESENTA A AREA DE CONTATO -->
				<div class="areaContato">
					<div class="col-sm-6">
						<!-- DIV ENDEREÇO -->
						<div class="endereco">
							<span>Endereço</span>
							<a href="#" ><?php echo $configuracao['opt_endereco'] ?></a>
						</div>
					</div>

					<div class="col-sm-6">
						<!-- DIV CONTATO -->
						<div class="contato">	
							<span>Contatos</span>
							<?php 
								foreach ($configuracao['opt_contatos'] as $configuracao['opt_contatos']):
									$configuracao['opt_contatos'] = $configuracao['opt_contatos'];
									$opt_contatos = explode(",", $configuracao['opt_contatos']); 
							?>
							<a href="tel:<?php echo $opt_contatos[1] ?>" target="_blank" ><?php echo $opt_contatos[0] ?> <?php echo $opt_contatos[1] ?></a>
						<?php endforeach?>
						</div>
					</div>
				</div>

			</div>

			<div class="col-sm-6">
				<div class="formContato">
					<!-- NEWSLLATER -->
					<div class="areaNewsllater">
						<figure>
							<img src="<?php echo get_bloginfo("template_url"); ?>/img/if_misc-_message_1276855.png" alt="">
						</figure>
						
						<p>Envie uma mensagem</p>
						<div class="form">
							<?php echo do_shortcode('[contact-form-7 id="5" title="Formulário de contato"]'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>			

	</div>
	<!-- MAPA -->	
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3751.5242502613105!2d-44.00758725717501!3d-19.90230111956245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa696bb9edcda79%3A0x8ed918f5a0779c21!2sR.+Dep.+Cl%C3%A1udio+Pinheiro+Lima%2C+1133+-+Gl%C3%B3ria%2C+Belo+Horizonte+-+MG%2C+30870-020!5e0!3m2!1spt-BR!2sbr!4v1527613089018" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<?php get_footer(); ?>