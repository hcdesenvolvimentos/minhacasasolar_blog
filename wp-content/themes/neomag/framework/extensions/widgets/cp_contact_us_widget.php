<?php
class contactus_widget extends WP_Widget
{
  function contactus_widget()
  {
    $widget_ops = array('classname' => 'widget_text footer-box-1', 'description' => 'Custom Contact Us widget to put Address' );
    parent::__construct('contactus_widget', 'CrunchPress : Contact Us Widget', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
	$title = $instance['title'];
	$show_name = isset( $instance['show_name'] ) ? ( $instance['show_name'] ) : '';	
	$addr = isset( $instance['addr'] ) ? ( $instance['addr'] ) : '';	
	$phone = isset( $instance['phone'] ) ? ( $instance['phone'] ) : '';	
	$fax = isset( $instance['fax'] ) ? ( $instance['fax'] ) : '';	
	$email = isset( $instance['email'] ) ? ( $instance['email'] ) : '';	
?>
  <p>
  <label for="<?php echo ($this->get_field_id('title')); ?>">
	 <?php esc_html_e('Title:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('title')); ?>" name="<?php echo ($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </label>
  </p> 
  <p>
  <label for="<?php echo ($this->get_field_id('addr')); ?>">
	 <?php esc_html_e('Address:','neomag');?>
	  <input class="widefat"  id="<?php echo ($this->get_field_id('addr')); ?>" name="<?php echo ($this->get_field_name('addr')); ?>" type="text" value="<?php echo esc_attr($addr); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('phone')); ?>">
	 <?php esc_html_e('Phone:','neomag');?>
	  <input class="widefat"  id="<?php echo ($this->get_field_id('phone')); ?>" name="<?php echo ($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('fax')); ?>">
	 <?php esc_html_e('Fax:','neomag');?>
	  <input class="widefat"  id="<?php echo ($this->get_field_id('fax')); ?>" name="<?php echo ($this->get_field_name('fax')); ?>" type="text" value="<?php echo esc_attr($fax); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('email')); ?>">
	 <?php esc_html_e('email:','neomag');?>
	  <input class="widefat"  id="<?php echo ($this->get_field_id('email')); ?>" name="<?php echo ($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
  </label>
  </p>      
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['show_name'] = $new_instance['show_name'];
		$instance['addr'] = $new_instance['addr'];
		$instance['phone'] = $new_instance['phone'];
		$instance['fax'] = $new_instance['fax'];
		$instance['email'] = $new_instance['email'];
    return $instance;
  }
 
	function widget($args, $instance)
	{
		
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$show_name = isset( $instance['show_name'] ) ? ( $instance['show_name'] ) : '';
		$addr = isset( $instance['addr'] ) ? ( $instance['addr'] ) : '';		
		$phone = isset( $instance['phone'] ) ? ( $instance['phone'] ) : '';		
		$fax = isset( $instance['fax'] ) ? ( $instance['fax'] ) : '';		
		$email = isset( $instance['email'] ) ? ( $instance['email'] ) : '';		
		
		echo html_entity_decode($before_widget);	
		// WIDGET display CODE Start
			echo '<h3>'.($title).'</h3>';
			?>
              <address>
			<?php 
				if($addr != ''){ 
					echo '<p>'.($addr).'</p>'; 
				}
			?>
              <p>
			<?php 
				if($phone != ''){ 
					echo esc_attr($phone); 
				}
			?>
              <br>
			<?php 
				if($fax != ''){ 
					echo esc_attr($fax); 
				}
			?>
			</p>
			<?php 
				if($email != ''){ 
					echo '<p><a href="'.($email).'">'.$email.'</a></p>'; 
				}
			?>
              </address>
	<?php 
	
	echo html_entity_decode($after_widget);
	}
		
}
add_action( 'widgets_init', create_function('', 'return register_widget("contactus_widget");') );?>