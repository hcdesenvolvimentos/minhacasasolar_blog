<?php
	/**
	 * Extend Recent Posts Widget 
	 *
	 * Adds different formatting to the default WordPress Recent Posts Widget
	 */
	 
	Class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts {
	 
		function widget($args, $instance) {
		
			extract( $args ); 
			
			$title = apply_filters('widget_title', empty($instance['title']) ? esc_html__('Recent Posts' ,'neomag') : $instance['title'], $instance, $this->id_base);
					
			if( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
				$number = 10;
					
			$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) ); 	
			if( $r->have_posts() ) :
				
				echo html_entity_decode($before_widget);
				if( $title ) echo html_entity_decode($before_title) . ($title) . html_entity_decode($after_title); ?>
				<ul class="posts">
					<?php while( $r->have_posts() ) : $r->the_post(); ?>
                    	<li>
                      		<div class="thumb"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( $r->ID, array( 131, 131) ); ?></a></div>
                    	</li>
					<?php endwhile; ?>
				</ul>
				 
				<?php
				echo html_entity_decode($after_widget);
			
			wp_reset_postdata();
			
			endif;
		}
	}
	//add_action( 'widgets_init', function(){ register_widget( 'My_Recent_Posts_Widget' ); });