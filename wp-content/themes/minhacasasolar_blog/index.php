<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Minha_Casa_Solar
 */

	get_header();
?>
<!-- PG INICIAL -->
<div class="pg pg-inicial">
	
	<!-- DEFININDO CONTAINER -->
	<div class="containerLagura">
		
		<!-- DEFININDO COLUNAS -->
		<div class="row">

			<div class="col-sm-8">
				
				<!-- SESSÃO CARROSSEL DE DESTAQUE -->
				<section class="carrosselDestaque">
					<h6 class="hidden"> Sessão de posts destaques</h6>
					<?php 
						//LOOP DE POST CATEGORIA DESTAQUE				
						$postsCategory = new WP_Query(array(
							'post_type'     => 'post',
							'posts_per_page'   => 5,
							'tax_query'     => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => 'destaque',
									)
								)
							)
						);

					?>
					<div id="carrosselDestaque" class="owl-Carousel">
						<!-- ITEM / BACKGROUND FUNDO-->
						<?php 
							while ($postsCategory->have_posts()) : $postsCategory->the_post(); 

							//FOTO DESTACADA
							$fotoPostCategory = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPostCategory = $fotoPostCategory[0];

							global $post;
							$categories = get_the_category();

						?>
						<figure class="item" style="background: url(<?php echo $fotoPostCategory ?>)">
							<!-- LINK DO POST -->
							<a href="<?php echo get_permalink() ?> ">
								<div class="tituloDestaque">
									<!-- CATEGORIA DO POST -->
									<?php 
										foreach ($categories as $categories){
											if ($categories->name != "Destaque"){
												$nomeCategoria = $categories->name;
											}
										} 
									?>
									<h3><?php echo $nomeCategoria;  ?></h3>
									<!-- TÍTULO DO POST -->
									<h2><?php echo get_the_title(); ?></h2>
									<!-- DATA DO POST -->
									<strong><?php echo  get_the_date('j F, Y'); ?></strong>
								</div>
							</a>
						</figure>
						<?php  endwhile; wp_reset_query();  ?>
					</div>

				</section>

				<!-- SESSÃO DE POSTS -->
				<section class="sessaoPosts">
					<h6 class="hidden">Sessão de Posts </h6>
					
					<!-- LISTA DE POSTS -->
					<ul>
						<?php 
					            	
							if ( have_posts() ) : while( have_posts() ) : the_post();
							$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoBlog = $fotoBlog[0];
							global $post;
							$categories = get_the_category();
						?>
						<!-- POST -->
						<li>
							
							<a href="<?php echo get_permalink() ?>" class="linkPost">
								<div class="row">

									<!-- DESCRÇÃO DE POST -->
									<div class="col-sm-7">
										<!-- CATEGORIA DO POST -->
										<?php 
											foreach ($categories as $categories){
												if ($categories->name != "Destaque"){
													$nomeCategoria = $categories->name;
												}
											} 
										?>
										<!-- CATEGORIA -->
										<h3><?php echo $nomeCategoria;  ?></h3>
										<!-- TÍTULO DO POST -->
										<h2><?php echo get_the_title() ?></h2>
										<!-- DATA -->
										<strong><?php echo  get_the_date('j F, Y'); ?></strong>
										<!-- BREVE DESCRIÇÃO -->
										<p><?php customExcerpt(130); ?></p>
										
									</div>

									<!-- IMAGEM DESTACADA -->
									<div class="col-sm-5">
										<figure>
											<img src="<?php echo $fotoBlog  ?>" alt="<?php echo get_the_title() ?> ">
											<span style="background: url(<?php echo $fotoBlog  ?>)"></span>
										</figure>
									</div>

								</div>
							</a>
							<?php disqus_count('myshortcode'); ?>	
						</li>
						<?php endwhile;endif; wp_reset_query(); ?>
					</ul>

				</section>

				<!-- PÁGINADOR -->
				<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			</div>

			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>

		</div>

	</div>	
</div>
	

<?php
get_footer();
