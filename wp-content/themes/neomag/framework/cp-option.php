<?php

	/*	
	*	CrunchPress Options File
	*	---------------------------------------------------------------------
	* 	@version	1.0
	* 	@author		CrunchPress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) CrunchPress
	*	---------------------------------------------------------------------
	*	This file contains the CrunchPress panel elements and create the 
	*	CrunchPress panel at the back-end of the framework
	*	---------------------------------------------------------------------
	*/
	
	
	// add action to embeded the panel in to dashboard
	add_action('admin_menu','cp_add_crunchpress_panel');
	function cp_add_crunchpress_panel(){
	
		add_theme_page( 'CrunchPress Option', NEOMAG_FULL, 'administrator', 'cp_general_options', 'cp_general_options', '' );
			add_theme_page( 'general_options', 'Typography Settings', 'administrator','cp_typography_settings', 'cp_typography_settings' );
			add_theme_page( 'general_options', 'Slider Settings', 'administrator','cp_slider_settings', 'cp_slider_settings' );
			add_theme_page( 'general_options', 'Social Network', 'administrator','cp_social_settings', 'cp_social_settings' );
			add_theme_page( 'general_options', 'Sidebar Settings', 'administrator','cp_sidebar_settings', 'cp_sidebar_settings' );
			add_theme_page( 'general_options', 'Default Pages Settings', 'administrator','cp_default_pages_settings', 'cp_default_pages_settings' );
			add_theme_page( 'general_options', 'Newsletter Settings', 'administrator','cp_newsletter_settings', 'cp_newsletter_settings' );
			add_theme_page( 'general_options', 'Import Dummy Data', 'administrator','cp_dummydata_import', 'cp_dummydata_import' );
	}
	
		
add_action('wp_ajax_general_options','cp_general_options');
function cp_general_options(){
		
	foreach ($_REQUEST as $keys=>$values) {
		$$keys = trim($values);
	}

	$return_data = array('success'=>'-1', 'alert'=>'Save option failed, please try contacting your host provider to increase the post_max_size and suhosin.post.max_vars varialble on the server.');?>
<div class="cp-wrapper bootstrap_admin cp-margin-left"> 

    <!--content area start -->	  
	<div class="hbg top_navigation row-fluid">
		<div class="cp-logo span2">
			<img src="<?php echo esc_url(NEOMAG_PATH_URL.'/framework/images/logo.png');?>" class="logo" alt="logo" />
		</div>
		<div class="sidebar span10">
			<?php echo esc_attr(cp_top_navigation_html_tooltip());?>
		</div>
	
	
	</div>
	<div class="content-area-main row-fluid"> 
	 
      <!--sidebar start -->
      <div class="sidebar-wraper span2">
        <div class="sidebar-sublinks">
          <ul id="wp_t_o_right_menu">
            <li id="active_tab" class="logo" >
              <?php esc_html_e('Logo Settings','neomag'); ?>
            </li>
            <li class="color_style">
              <?php esc_html_e('Style & Color Scheme','neomag'); ?>
              </li>
            <li class="hr_settings">
              <?php esc_html_e('Header Settings','neomag'); ?>
              </li>
            <li class="ft_settings">
              <?php esc_html_e('Footer Settings','neomag'); ?>
              </li>
            <li class="misc_settings">
              <?php esc_html_e('MISC Settings','neomag'); ?>
              </li>
			  <li class="maintenance_mode_settings">
              <?php esc_html_e('Maintenance Mode Settings','neomag'); ?>
              </li>
			  
            <?php if(!class_exists( 'Envato_WordPress_Theme_Upgrader' )){}else{?>
            <li class="envato_api">
              <?php esc_html_e('User API Settings','neomag'); ?>
              </li>
            <?php }?>
          </ul>
        </div>
      </div>
      <!--sidebar end --> 
      <!--content start -->
      <div class="content-area span10">
        <form id="options-panel-form" name="cp-panel-form">
          <div class="panel-elements" id="panel-elements">
            <div class="panel-element" id="panel-element-save-complete">
              <div class="panel-element-save-text">
                <?php esc_html_e('Save Options Complete','neomag'); ?>
                .</div>
              <div class="panel-element-save-arrow"></div>
            </div>
            <div class="panel-element">
              <?php 
							if(isset($action) AND $action == 'general_options'){
								$general_logo_xml = '<general_settings>';
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_logo_btn',esc_attr($header_logo_btn));
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_logo_bg',esc_html($header_logo_bg));
								$general_logo_xml = $general_logo_xml . create_xml_tag('logo_text_cp',esc_attr($logo_text_cp));
								$general_logo_xml = $general_logo_xml . create_xml_tag('logo_subtext',esc_attr($logo_subtext));								 
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_logo',esc_html($header_logo));
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_logo2',esc_html($header_logo2));
								$general_logo_xml = $general_logo_xml . create_xml_tag('logo_width',esc_attr($logo_width));
								$general_logo_xml = $general_logo_xml . create_xml_tag('logo_height',esc_attr($logo_height));
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_favicon',esc_html($neomag_header_favicon));
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_fav_link',esc_html($neomag_header_fav_link));
								$general_logo_xml = $general_logo_xml . create_xml_tag('topcart_icon',esc_attr($topcart_icon));
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('select_layout_cp',esc_attr($select_layout_cp));
								$general_logo_xml = $general_logo_xml . create_xml_tag('boxed_scheme',esc_attr($boxed_scheme));
								$general_logo_xml = $general_logo_xml . create_xml_tag('color_scheme',esc_attr($color_scheme));
								$general_logo_xml = $general_logo_xml . create_xml_tag('body_color',esc_attr($body_color));
								$general_logo_xml = $general_logo_xml . create_xml_tag('heading_color',esc_attr($heading_color));							
								$general_logo_xml = $general_logo_xml . create_xml_tag('select_bg_pat',esc_attr($select_background_patren));
								$general_logo_xml = $general_logo_xml . create_xml_tag('bg_scheme',esc_attr($bg_scheme));
								$general_logo_xml = $general_logo_xml . create_xml_tag('body_patren',esc_html($body_patren));
								$general_logo_xml = $general_logo_xml . create_xml_tag('color_patren',esc_attr($color_patren));
								$general_logo_xml = $general_logo_xml . create_xml_tag('body_image',esc_html($body_image));
								$general_logo_xml = $general_logo_xml . create_xml_tag('position_image_layout',esc_html($position_image_layout));
								$general_logo_xml = $general_logo_xml . create_xml_tag('image_repeat_layout',esc_html($image_repeat_layout));
								$general_logo_xml = $general_logo_xml . create_xml_tag('image_attachment_layout',esc_html($image_attachment_layout));
								$general_logo_xml = $general_logo_xml . create_xml_tag('contact_us_code',esc_attr($contact_us_code));
								$general_logo_xml = $general_logo_xml . create_xml_tag('select_header_cp',esc_attr($select_header_cp));
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_style_apply',esc_attr($header_style_apply));
								$general_logo_xml = $general_logo_xml . create_xml_tag('header_css_code',esc_attr($header_css_code));
								$general_logo_xml = $general_logo_xml . create_xml_tag('cp_javascript_code',esc_attr($cp_javascript_code));
								$general_logo_xml = $general_logo_xml . create_xml_tag('topbutton_icon',esc_attr($topbutton_icon));
								$general_logo_xml = $general_logo_xml . create_xml_tag('topsocial_icon',esc_attr($topsocial_icon));
								$general_logo_xml = $general_logo_xml . create_xml_tag('topsign_icon',esc_attr($topsign_icon));
								$general_logo_xml = $general_logo_xml . create_xml_tag('resv_button',esc_attr($neomag_resv_button));
								$general_logo_xml = $general_logo_xml . create_xml_tag('resv_text',esc_attr($neomag_resv_text));
								$general_logo_xml = $general_logo_xml . create_xml_tag('resv_short',esc_attr($neomag_resv_short));
								


								$general_logo_xml = $general_logo_xml . create_xml_tag('select_footer_cp',esc_attr($select_footer_cp));
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_style_apply',esc_attr($neomag_footer_style_apply));
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_upper_layout',esc_attr($footer_upper_layout));
								$general_logo_xml = $general_logo_xml . create_xml_tag('copyright_code',esc_attr($copyright_code));
								$general_logo_xml = $general_logo_xml . create_xml_tag('social_networking',esc_attr($social_networking));
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('twitter_feed',esc_attr($twitter_feed));							
								$general_logo_xml = $general_logo_xml . create_xml_tag('twitter_home_button',esc_attr($twitter_home_button));
								$general_logo_xml = $general_logo_xml . create_xml_tag('twitter_id',esc_attr($twitter_id));
								$general_logo_xml = $general_logo_xml . create_xml_tag('consumer_key',esc_attr($consumer_key));
								$general_logo_xml = $general_logo_xml . create_xml_tag('consumer_secret',esc_attr($consumer_secret));
								$general_logo_xml = $general_logo_xml . create_xml_tag('access_token',esc_attr($access_token));
								$general_logo_xml = $general_logo_xml . create_xml_tag('access_secret_token',esc_attr($access_secret_token));
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_col_layout',esc_attr($footer_col_layout));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_logo',esc_html($footer_logo));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_link',esc_attr($footer_link));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_logo_width',esc_attr($footer_logo_width));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('footer_logo_height',esc_attr($footer_logo_height));	

								$general_logo_xml = $general_logo_xml . create_xml_tag('breadcrumbs',esc_attr($breadcrumbs));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('rtl_layout',esc_attr($rtl_layout));
								$general_logo_xml = $general_logo_xml . create_xml_tag('site_loader',esc_attr($site_loader));
								$general_logo_xml = $general_logo_xml . create_xml_tag('element_loader',esc_attr($element_loader));
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('maintenance_mode',esc_attr($maintenance_mode));
								$general_logo_xml = $general_logo_xml . create_xml_tag('maintenace_title',esc_attr($maintenace_title));
								$general_logo_xml = $general_logo_xml . create_xml_tag('countdown_time',esc_attr($countdown_time));
								$general_logo_xml = $general_logo_xml . create_xml_tag('email_mainte',esc_attr($email_mainte));
								$general_logo_xml = $general_logo_xml . create_xml_tag('mainte_description',esc_attr($mainte_description));
								$general_logo_xml = $general_logo_xml . create_xml_tag('cp_comming_soon',esc_attr($cp_comming_soon));
								
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('social_icons_mainte',esc_attr($neomag_social_icons_mainte));
								$general_logo_xml = $general_logo_xml . create_xml_tag('donation_button',esc_attr($donation_button));
								$general_logo_xml = $general_logo_xml . create_xml_tag('donate_btn_text',esc_attr($donate_btn_text));
								$general_logo_xml = $general_logo_xml . create_xml_tag('donation_page_id',esc_attr($donation_page_id));
								$general_logo_xml = $general_logo_xml . create_xml_tag('donate_email_id',esc_attr($donate_email_id));
								$general_logo_xml = $general_logo_xml . create_xml_tag('donate_title',esc_attr($donate_title));
								$general_logo_xml = $general_logo_xml . create_xml_tag('donation_currency',esc_attr($donation_currency));
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('safari_banner',esc_attr($safari_banner));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('safari_banner_link',esc_attr($neomag_safari_banner_link));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('safari_banner_width',esc_attr($safari_banner_width));	
								$general_logo_xml = $general_logo_xml . create_xml_tag('safari_banner_height',esc_attr($safari_banner_height));	
								
								$general_logo_xml = $general_logo_xml . create_xml_tag('tf_username',esc_attr($tf_username));
								$general_logo_xml = $general_logo_xml . create_xml_tag('tf_sec_api',esc_attr($tf_sec_api));
								$general_logo_xml = $general_logo_xml . '</general_settings>';

								if(!cp_save_option('general_settings', get_option('general_settings'), $general_logo_xml)){
								
									die( json_encode($return_data) );
									
								}
								
								die( json_encode( array('success'=>'0') ) );
								
							}?>
            </div>
            <?php
				$header_logo_btn = '';
				$logo_text_cp = '';
				$logo_subtext = '';
				$header_logo_upload = '';
				$logo_width = '';
				$logo_height = '';
				$neomag_header_favicon = '';
				$neomag_header_fav_link = '';
				$select_layout_cp = '';
				$boxed_scheme = '';
				$select_bg_pat = '';
				$scheme_color_scheme = '';
				$color_scheme = '';
				$select_bg_pat = '';
				$scheme_color_scheme = '';
				$color_scheme = '';
				$header_logo = '';
				$header_logo_bg = '';
				$neomag_footer_style_apply = '';
				$select_footer_cp = '';
				$body_color = '';
				$heading_color = '';
				$border_color = '';
				$button_color = '';
				$button_hover_color = '';
				$color_patren = '';
				$bg_scheme = '';
				$body_patren = '';
				$body_image = '';
				$position_image_layout = '';
				$image_repeat_layout = '';
				$image_attachment_layout = '';
				$contact_us_code = '';
				$neomag_resv_button = '';
				$neomag_resv_text = '';
				$neomag_resv_short = '';
				$header_css_code = '';
				$cp_javascript_code = '';
				$topbutton_icon = '';
				$footer_upper_layout = '';
				$select_header_cp = '';
				$header_style_apply = '';
				$about_header = '';
				$topcart_icon = '';
				$topcounter_circle = '';
				$countd_event_category = '';
				$topsocial_icon = '';
				$topsign_icon = '';
				$topsearch_icon = '';
				$copyright_code = '';
				$footer_banner = '';
				$footer_col_layout = '';
				$social_networking = '';
				$twitter_feed = '';
				$twitter_home_button = '';
				$twitter_id = '';
				$consumer_key = '';
				$consumer_secret = '';
				$access_token = '';
				$access_secret_token = '';
				$top_count_header = '';
				
				$footer_link = '';
				$footer_logo = '';
				$footer_logo_width = '';
				$footer_logo_height = '';
				$footer_layout = '';
				$breadcrumbs = '';			
				$rtl_layout = '';
				$site_loader = '';
				$element_loader = '';
				
				$maintenance_mode = '';
				$maintenace_title = '';
				$countdown_time = '';
				$email_mainte = '';
				$mainte_description = '';
				$neomag_social_icons_mainte = '';
				$cp_comming_soon = '';

				$donation_button = '';
				$donate_btn_text = '';
				$donation_page_id = '';
				$donate_email_id = '';
				$donate_title = '';
				$donation_currency = '';
				
				$neomag_safari_banner_link = '';
				$safari_banner = '';
				$safari_banner_width = '';
				$safari_banner_height = '';
				
				$tf_username = '';
				$tf_sec_api = '';
				$cp_general_settings = get_option('general_settings');
				if($cp_general_settings <> ''){
					$cp_logo = new DOMDocument ();
					$cp_logo->loadXML ( $cp_general_settings );
					$header_logo_btn = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_logo_btn'));
					$header_logo_bg = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_logo_bg'));
					$logo_text_cp = esc_html(cp_find_xml_value($cp_logo->documentElement,'logo_text_cp'));
					$logo_subtext = esc_html(cp_find_xml_value($cp_logo->documentElement,'logo_subtext'));
					$header_logo = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_logo'));
					$header_logo2 = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_logo2'));
					$logo_width = esc_html(cp_find_xml_value($cp_logo->documentElement,'logo_width'));
					$logo_height = esc_html(cp_find_xml_value($cp_logo->documentElement,'logo_height'));
					$neomag_header_favicon = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_favicon'));
					$neomag_header_fav_link = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_fav_link'));									
					
					
					$select_layout_cp = esc_html(cp_find_xml_value($cp_logo->documentElement,'select_layout_cp'));
					$boxed_scheme = esc_html(cp_find_xml_value($cp_logo->documentElement,'boxed_scheme'));
					$color_scheme = esc_html(cp_find_xml_value($cp_logo->documentElement,'color_scheme'));					
					$select_bg_pat = esc_html(cp_find_xml_value($cp_logo->documentElement,'select_bg_pat'));
					$bg_scheme = esc_html(cp_find_xml_value($cp_logo->documentElement,'bg_scheme'));				
					$body_color = esc_html(cp_find_xml_value($cp_logo->documentElement,'body_color'));
					$heading_color = esc_html(cp_find_xml_value($cp_logo->documentElement,'heading_color'));
					
					
					$body_patren = esc_html(cp_find_xml_value($cp_logo->documentElement,'body_patren'));
					$color_patren = esc_html(cp_find_xml_value($cp_logo->documentElement,'color_patren'));
					$body_image = esc_html(cp_find_xml_value($cp_logo->documentElement,'body_image'));
					$position_image_layout = esc_html(cp_find_xml_value($cp_logo->documentElement,'position_image_layout'));
					$image_repeat_layout = esc_html(cp_find_xml_value($cp_logo->documentElement,'image_repeat_layout'));
					$image_attachment_layout = esc_html(cp_find_xml_value($cp_logo->documentElement,'image_attachment_layout'));
					$select_header_cp = esc_html(cp_find_xml_value($cp_logo->documentElement,'select_header_cp'));
					$header_style_apply = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_style_apply'));
					
					$contact_us_code = esc_html(cp_find_xml_value($cp_logo->documentElement,'contact_us_code'));
					
					
					$header_css_code = esc_html(cp_find_xml_value($cp_logo->documentElement,'header_css_code'));
					$cp_javascript_code = esc_html(cp_find_xml_value($cp_logo->documentElement,'cp_javascript_code'));
					$topbutton_icon = esc_html(cp_find_xml_value($cp_logo->documentElement,'topbutton_icon'));
					$topcart_icon = esc_html(cp_find_xml_value($cp_logo->documentElement,'topcart_icon'));
					$topsocial_icon = esc_html(cp_find_xml_value($cp_logo->documentElement,'topsocial_icon'));
					$topsign_icon = esc_html(cp_find_xml_value($cp_logo->documentElement,'topsign_icon'));
					
					$neomag_resv_button = esc_html(cp_find_xml_value($cp_logo->documentElement,'resv_button'));
					$neomag_resv_text = esc_html(cp_find_xml_value($cp_logo->documentElement,'resv_text'));
					$neomag_resv_short = esc_html(cp_find_xml_value($cp_logo->documentElement,'resv_short'));
					
					$select_footer_cp = esc_html(cp_find_xml_value($cp_logo->documentElement,'select_footer_cp'));
					$neomag_footer_style_apply = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_style_apply'));
					$footer_upper_layout = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_upper_layout'));
					$copyright_code = esc_html(cp_find_xml_value($cp_logo->documentElement,'copyright_code'));
					$footer_banner = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_banner'));
					$footer_col_layout = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_col_layout'));
					$footer_logo = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_logo'));
					$footer_link = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_link'));
					$footer_logo_width = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_logo_width'));
					$footer_logo_height = esc_html(cp_find_xml_value($cp_logo->documentElement,'footer_logo_height'));
					$social_networking = esc_html(cp_find_xml_value($cp_logo->documentElement,'social_networking'));
					$twitter_feed = esc_html(cp_find_xml_value($cp_logo->documentElement,'twitter_feed'));
					$twitter_home_button = esc_html(cp_find_xml_value($cp_logo->documentElement,'twitter_home_button'));
					$twitter_id = esc_html(cp_find_xml_value($cp_logo->documentElement,'twitter_id'));
					$consumer_key = esc_html(cp_find_xml_value($cp_logo->documentElement,'consumer_key'));
					$consumer_secret = esc_html(cp_find_xml_value($cp_logo->documentElement,'consumer_secret'));
					$access_token = esc_html(cp_find_xml_value($cp_logo->documentElement,'access_token'));
					$access_secret_token = esc_html(cp_find_xml_value($cp_logo->documentElement,'access_secret_token'));
					$breadcrumbs = esc_html(cp_find_xml_value($cp_logo->documentElement,'breadcrumbs'));
					$rtl_layout = esc_html(cp_find_xml_value($cp_logo->documentElement,'rtl_layout'));
					$site_loader = esc_html(cp_find_xml_value($cp_logo->documentElement,'site_loader'));
					$element_loader = esc_html(cp_find_xml_value($cp_logo->documentElement,'element_loader'));
					
					$maintenance_mode = esc_html(cp_find_xml_value($cp_logo->documentElement,'maintenance_mode'));
					$maintenace_title = esc_html(cp_find_xml_value($cp_logo->documentElement,'maintenace_title'));
					$countdown_time = esc_html(cp_find_xml_value($cp_logo->documentElement,'countdown_time'));
					$email_mainte = esc_html(cp_find_xml_value($cp_logo->documentElement,'email_mainte'));
					$mainte_description = esc_html(cp_find_xml_value($cp_logo->documentElement,'mainte_description'));
					$cp_comming_soon = esc_html(cp_find_xml_value($cp_logo->documentElement,'cp_comming_soon'));
					
					
					$donation_button = esc_html(cp_find_xml_value($cp_logo->documentElement,'donation_button'));
					$donate_btn_text = esc_html(cp_find_xml_value($cp_logo->documentElement,'donate_btn_text'));
					$donation_page_id = esc_html(cp_find_xml_value($cp_logo->documentElement,'donation_page_id'));
					$donate_email_id = esc_html(cp_find_xml_value($cp_logo->documentElement,'donate_email_id'));
					$donate_title = esc_html(cp_find_xml_value($cp_logo->documentElement,'donate_title'));
					$donation_currency = esc_html(cp_find_xml_value($cp_logo->documentElement,'donation_currency'));
					
					$safari_banner = esc_html(cp_find_xml_value($cp_logo->documentElement,'safari_banner'));
					$neomag_safari_banner_link = esc_html(cp_find_xml_value($cp_logo->documentElement,'safari_banner_link'));
					$safari_banner_width = esc_url(cp_find_xml_value($cp_logo->documentElement,'safari_banner_width'));
					$safari_banner_height = esc_html(cp_find_xml_value($cp_logo->documentElement,'safari_banner_height'));
					
					$neomag_social_icons_mainte = esc_html(cp_find_xml_value($cp_logo->documentElement,'social_icons_mainte'));
					
					$tf_username = esc_html(cp_find_xml_value($cp_logo->documentElement,'tf_username'));
					$tf_sec_api = esc_html(cp_find_xml_value($cp_logo->documentElement,'tf_sec_api'));
				}
				
			
			?>
            <ul class="logo_tab">
              <li id="logo" class="logo_dimenstion active_tab">
                <div id="header_logo_cp" class="row-fluid">
					<ul class="panel-body recipe_class span4 header_logo_btn">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3><?php esc_html_e('HEADER LOGO','neomag'); ?></h3>
							</span>
							<label for="header_logo_btn">
								<div class="checkbox-switch
									<?php echo (esc_attr($header_logo_btn) == 'enable' || (esc_attr($header_logo_btn) == '' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off'; ?>">
								</div>
							</label>
							<input type="checkbox" name="header_logo_btn" class="checkbox-switch" value="disable" checked>
							<input type="checkbox" name="header_logo_btn" id="header_logo_btn" class="checkbox-switch" value="enable" <?php 

							echo (esc_attr($header_logo_btn) =='enable' || (esc_attr($header_logo_btn)=='' && empty($default)))? 'checked': ''; 

							?>>
							<div class="clear"></div>
							<p> <?php esc_html_e('You can switch between header logo image and header logo text, turning it on it will show logo as image, turning it off it will disable image and show text which you have entered in wordpress settings.','neomag');?></p>
						</li>
					</ul>
					<ul class="panel-body recipe_class span4 cp_logo_text">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('LOGO TEXT','neomag'); ?>
								</h3>
							</span>
							<input type="text" name="logo_text_cp" id="logo_text_cp" value="<?php echo (esc_attr($logo_text_cp) == '')? esc_attr($logo_text_cp): esc_attr($logo_text_cp);?>" />
							<div class="clear"></div>
							<p><?php esc_html_e('Please paste logo text.','neomag');?></p>
						</li>
					</ul>
					<ul class="panel-body recipe_class span4 cp_logo_text">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('LOGO SUBTEXT','neomag'); ?>
								</h3>
							</span>
							<input type="text" name="logo_subtext" id="logo_subtext" value="<?php echo (esc_attr($logo_subtext) == '')? esc_attr($logo_subtext): esc_attr($logo_subtext);?>" />
							<div class="clear"></div>
							<p><?php esc_html_e('Please paste logo subtext.','neomag');?></p>
						</li>
					</ul>
				</div>
				<ul class="panel-body recipe_class logo_upload row-fluid cp_logo">
                  <?php 
					$image_src_head = '';
					if(!empty($header_logo)){ 
						$image_src_head = wp_get_attachment_image_src( $header_logo, 'full' );
						$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
					}
					?>
					<li class="panel-input span8 eql_height">
						<span class="panel-title">
							<h3 for="header_logo" >
							  <?php esc_html_e('Logo','neomag'); ?>
							</h3>
						</span>
						<div class="content_con">
							<input name="header_logo" type="hidden" class="clearme" id="upload_image_attachment_id" value="<?php echo esc_attr($header_logo); ?>" />
							<input name="header_link" id="upload_image_text" class="clearme upload_image_text" type="text" value="<?php echo esc_url($image_src_head); ?>" />
							<input class="upload_image_button" type="button" value="Upload" />
						</div>
						<p> <?php esc_html_e('Upload logo image here, PNG, Gif, JPEG, JPG format supported only.','neomag');?> </p>  
					</li>
					<li class="panel-right-box span4 eql_height">
						<div class="admin-logo-image">
						  <?php 
							if(!empty($header_logo)){ 
								$image_src_head = wp_get_attachment_image_src( $header_logo, 'full' );
								$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
								$thumb_src_preview = wp_get_attachment_image_src( $header_logo, array(150,150)); ?>
									<img class="clearme img-class" src="<?php if(!empty($image_src_head)){echo esc_url($thumb_src_preview[0]);}?>" alt="logo" />
									<span class="close-me"></span>
						  <?php } ?>
						</div>
					</li>
                </ul>
                <div class="clear"></div>
				<ul class="panel-body recipe_class logo_upload row-fluid cp_logo">
                  <?php 
					$image_src_head2 = '';
					if(!empty($header_logo2)){ 
						$image_src_head2 = wp_get_attachment_image_src( $header_logo2, 'full' );
						$image_src_head2 = (empty($image_src_head2))? '': esc_url($image_src_head2[0]);
					}
					?>
					<li class="panel-input span8 eql_height">
						<span class="panel-title">
							<h3 for="header_logo" >
							  <?php esc_html_e('Logo for inner pages','neomag'); ?>
							</h3>
						</span>
						<div class="content_con">
							<input name="header_logo2" type="hidden" class="clearme" id="upload_image_attachment_id" value="<?php echo esc_attr($header_logo2); ?>" />
							<input name="header_link" id="upload_image_text" class="clearme upload_image_text" type="text" value="<?php echo esc_url($image_src_head2); ?>" />
							<input class="upload_image_button" type="button" value="Upload" />
						</div>
						<p> <?php esc_html_e('Upload logo image here, PNG, Gif, JPEG, JPG format supported only.','neomag');?> </p>  
					</li>
					<li class="panel-right-box span4 eql_height">
						<div class="admin-logo-image">
						  <?php 
							if(!empty($header_logo2)){ 
								$image_src_head2 = wp_get_attachment_image_src( $header_logo2, 'full' );
								$image_src_head2 = (empty($image_src_head2))? '': esc_url($image_src_head2[0]);
								$thumb_src_preview2 = wp_get_attachment_image_src( $header_logo2, array(150,150)); ?>
									<img class="clearme img-class" src="<?php if(!empty($image_src_head2)){echo esc_url($thumb_src_preview2[0]);}?>" alt="logo" />
									<span class="close-me"></span>
						  <?php } ?>
						</div>
					</li>
                </ul>
                <div class="clear"></div>
                
                <ul class="panel-body recipe_class row-fluid cp_logo">
                  <li class="panel-input span8">
					  <span class="panel-title">
						<h3 for="logo_width" >
						  <?php esc_html_e('Width','neomag'); ?>
						</h3>
					  </span>
                    <div id="logo_width" class="sliderbar" rel="logo_bar"></div>
                    <input type="hidden" name="logo_width" value="<?php echo esc_attr($logo_width);?>">
					<p> <?php esc_html_e('Please scroll Left to Right to adjust logo image width, you can also use Arrow keys UP,Down - Left,Right.','neomag');?> </p>                  
                  </li>
                  <li class="span4 right-box-sec" id="slidertext"><?php echo esc_attr($logo_width);?> <?php esc_html_e('px','neomag');?> </li>
                </ul>
                <div class="clear"></div>
                <ul class="panel-body recipe_class row-fluid cp_logo">
                  <li class="panel-input span8">
					  <span class="panel-title">
						<h3 for="logo_height" >
						  <?php esc_html_e('Height','neomag'); ?>
						</h3>
					  </span>
                    <div id="logo_height" class="sliderbar" rel="logo_bar"></div>
                    <input type="hidden" name="logo_height" value="<?php echo esc_attr($logo_height);?>">
					<p> <?php esc_html_e('Please scroll Left to Right to adjust logo image height, you can also use Arrow keys UP,Down - Left,Right.','neomag');?> </p>  
                  </li>
				  <li class="span4 right-box-sec" id="slidertext"><?php echo esc_attr($logo_height);?> <?php esc_html_e('px','neomag');?> </li>
                </ul>
				<ul class="panel-body recipe_class favi_upload row-fluid">
                  <?php 
					$image_src_head = '';
					if(!empty($neomag_header_favicon)){ 
						$image_src_head = wp_get_attachment_image_src( $neomag_header_favicon, 'full' );
						$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
					}
					?>
					<li class="panel-input span8 eql_height">
						<span class="panel-title">
							<h3>
							  <?php esc_html_e('Upload Favicon','neomag'); ?>
							</h3>
						</span>
						<div class="content_con">
							<input name="header_favicon" type="hidden" class="clearme" id="upload_image_attachment_id" value="<?php echo esc_attr($neomag_header_favicon); ?>" />
							<input name="header_fav_link" id="upload_image_text" class="clearme upload_image_text" type="text" value="<?php echo esc_url($image_src_head); ?>" />
							<input class="upload_image_button" type="button" value="Upload" />
						</div>
						<p> <?php esc_html_e('Upload Favicon image here, PNG, Gif, JPEG, JPG format supported only.','neomag');?> </p>  
					</li>
					<li class="panel-right-box span4 eql_height">
						<div class="admin-logo-image">
						  <?php 
							if(!empty($neomag_header_favicon)){ 
								$image_src_head = wp_get_attachment_image_src( $neomag_header_favicon, 'full' );
								$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
								$thumb_src_preview = wp_get_attachment_image_src( $neomag_header_favicon, array(150,150)); ?>
									<img class="clearme img-class" src="<?php if(!empty($image_src_head)){echo esc_url($thumb_src_preview[0]);}?>" alt="logo" />
									<span class="close-me"></span>
						  <?php } ?>
						</div>
					</li>
                </ul>
              </li>
              <li id="color_style" class="style_color_scheme">
                <ul class="panel-body recipe_class row-fluid">
                  <li class="panel-input span8">
					<span class="panel-title">
						<h3 for="select_layout_cp">
						  <?php esc_html_e('SELECT LAYOUT','neomag'); ?>
						</h3>
					</span>
                    <div class="combobox">
                      <select name="select_layout_cp" class="select_layout_cp" id="select_layout_cp">
                        <option <?php if(esc_attr($select_layout_cp) == 'full_layout'){echo 'selected';}?> value="full_layout" class="full_layout">Full Layout</option>
                      </select>
                    </div>
					<p> <?php esc_html_e('Please select website layout.','neomag');?> </p>
                  </li>
                  <li class="span4 logo_upload">
					<div class="admin-logo-image">
						<img src="<?php echo esc_url(NEOMAG_PATH_URL.'/images/full_version.jpg');?>" class="full_v" />
					</div>	
				  </li>
                </ul>
                <div class="clear"></div>
				 <ul id="boxed_layout" class="panel-body recipe_class row-fluid">
                   <li class="panel-input span8">
					<span class="panel-title">
						<h3>
						  <?php esc_html_e('BOXED LAYOUT BACKGROUND','neomag'); ?>
						</h3>
					</span>
					<div class="color-picker-container">
						<input type="text" name="boxed_scheme" class="color-picker" value="<?php if(esc_attr($boxed_scheme) <> ''){echo esc_attr($boxed_scheme);}?>" />
					</div>
					<p> <?php esc_html_e('Please select any color from color palette to use as color scheme, leaving blank color scheme will be auto selected as default.','neomag');?> </p>
                  </li>
                  <li class="span4 right-box-sec"> </li>
                </ul>
                <div class="clear"></div>
                
				<div class="row-fluid">
					<ul class="recipe_class span4">
						<li class="panel-radioimage panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('COLOR SCHEME','neomag'); ?>
								</h3>
							</span>

							<div class="color-picker-container">
								<input type="text" name="color_scheme" class="color-picker" value="<?php if(esc_attr($color_scheme) <> ''){echo esc_attr($color_scheme);}?>" />
							</div>
							<p> <?php esc_html_e('Please select any color from color palette to use as color scheme (it will effect on all headings and anchors), leaving blank color scheme will be auto selected as default.','neomag');?> </p>
						</li>
					</ul>
					<ul class="recipe_class span4">
						<li class="panel-radioimage panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('SECONDARY COLOR','neomag'); ?>
								</h3>
							</span>

							<div class="color-picker-container">
								<input type="text" name="body_color" class="color-picker" value="<?php if(esc_attr($body_color) <> ''){echo esc_attr($body_color);}?>" />
							</div>
							<p> <?php esc_html_e('Please select any color from color palette to use as color scheme (it will effect on all headings and anchors), leaving blank color scheme will be auto selected as default.','neomag');?> </p>
						</li>
					</ul>
					
				</div>
				<div class="clear"></div>
                <ul class="panel-body recipe_class row-fluid"> 
                  <li class="panel-input span8">
				  <span class="panel-title">
                    <h3>
                      <?php esc_html_e('SELECT BACKGROUND TYPE','neomag'); ?>
                    </h3>
                  </span>
                    <div class="combobox">
                      <select name="select_background_patren" class="select_background_patren" id="select_background_patren">
                        <option <?php if(esc_attr($select_bg_pat) == 'Background-Patren'){echo sanitize_text_field('selected');}?> value="Background-Patren" class="select_bg_patren"> <?php esc_html_e('Background Pattern','neomag');?> </option>
                        <option <?php if(esc_attr($select_bg_pat) == 'Background-Color'){echo sanitize_text_field('selected');}?> value="Background-Color" class="select_bg_color"> <?php esc_html_e('Background Color','neomag');?> </option>
                        <option <?php if(esc_attr($select_bg_pat) == 'Background-Image'){echo sanitize_text_field('selected');}?> value="Background-Image" class="select_bg_image"> <?php esc_html_e('Background Image','neomag');?> </option>
                      </select>
                    </div>
					<p> <?php esc_html_e('Please select background pattern or background color.','neomag');?> </p>
                  </li>
                  <li id="select_bg_patren" class="span4 pattern-container">
				  <?php 
								$options = array(
									'1'=>array('value'=>'1', 'image'=>'/framework/images/pattern/pattern-1.png'),
									'2'=>array('value'=>'2','image'=>'/framework/images/pattern/pattern-2.png'),
									'3'=>array('value'=>'3','image'=>'/framework/images/pattern/pattern-3.png'),
									'4'=>array('value'=>'4','image'=>'/framework/images/pattern/pattern-4.png'),
									'5'=>array('value'=>'5','image'=>'/framework/images/pattern/pattern-5.png'),
									'6'=>array('value'=>'6','image'=>'/framework/images/pattern/pattern-6.png'),
									'7'=>array('value'=>'7','image'=>'/framework/images/pattern/pattern-7.png'),
									'8'=>array('value'=>'8','image'=>'/framework/images/pattern/pattern-8.png'),
									'9'=>array('value'=>'9','image'=>'/framework/images/pattern/pattern-9.png'),
									'10'=>array('value'=>'10','image'=>'/framework/images/pattern/pattern-10.png'),
									'11'=>array('value'=>'11','image'=>'/framework/images/pattern/pattern-11.png'),
									'12'=>array('value'=>'12','image'=>'/framework/images/pattern/pattern-12.png'),
									'13'=>array('value'=>'13','image'=>'/framework/images/pattern/pattern-13.png'),
									'14'=>array('value'=>'14','image'=>'/framework/images/pattern/pattern-14.png'),
									'15'=>array('value'=>'15','image'=>'/framework/images/pattern/pattern-15.png'),
									'16'=>array('value'=>'16','image'=>'/framework/images/pattern/pattern-16.png'),
									'17'=>array('value'=>'17','image'=>'/framework/images/pattern/pattern-17.png'),
									'18'=>array('value'=>'18','image'=>'/framework/images/pattern/pattern-18.png'),
									'19'=>array('value'=>'19','image'=>'/framework/images/pattern/pattern-19.png'),
									'20'=>array('value'=>'20','image'=>'/framework/images/pattern/pattern-20.png'),
									'21'=>array('value'=>'21','image'=>'/framework/images/pattern/pattern-21.png'),
									'22'=>array('value'=>'22','image'=>'/framework/images/pattern/pattern-22.png'),
									'23'=>array('value'=>'23','image'=>'/framework/images/pattern/pattern-45.png'),
								);
								$value = '';
								$default = '';
								foreach( $options as $option ){ 
								?>
                    <div class='radio-image-wrapper'>
                      <label for="<?php echo esc_attr($option['value']); ?>">
                      <img src=<?php echo esc_url(NEOMAG_PATH_URL.$option['image'])?> class="color_patren" alt="color_patren">
                      <div id="check-list"></div>
                      </label>
                      <input type="radio" class="checkbox_class" name="color_patren" value="<?php echo esc_attr($option['image']); ?>" <?php 
											if(esc_attr($color_patren) == esc_attr($option['image'])){
												echo sanitize_text_field('checked');
											}else if(esc_attr($color_patren) == '' && $default == esc_attr($option['image'])){
												echo sanitize_text_field('checked');
											}
										?> id="<?php echo esc_attr($option['value']); ?>" class=""
										>
                    </div>
                    <?php } ?> 
				  </li>
                </ul>
                <div class="clear"></div>
                <ul id="select_bg_color" class="panel-body recipe_class row-fluid">
                  
                  <li class="panel-input span8">
				  <span class="panel-title">
                    <h3 for="bg_scheme" >
                      <?php esc_html_e('BACKGROUND COLOR','neomag'); ?>
                    </h3>
                  </span>
                    <div class="color-picker-container">
						<input type="text" name="bg_scheme" class="color-picker" value="<?php if(esc_attr($bg_scheme) <> ''){echo esc_attr($bg_scheme);}?>"/>
					</div>
					<p> <?php esc_html_e('Please select any color from color palette to use as background color, leaving blank background will be auto selected as default background.','neomag');?> </p>
                  </li>
                  <li class="span4 right-box-sec"></li>
                </ul>
                <div class="clear"></div>
                <ul id="bg_upload_id" class="recipe_class logo_upload row-fluid">
                  <li class="panel-input span8 ">
					  <span class="panel-title">
						<h3 for="body_patren" >
						  <?php esc_html_e('Upload Background Pattern','neomag'); ?>
						</h3>
					  </span>
					  <?php
					  $image_src_head = '';
								
								if(!empty($header_logo)){ 
								
									$image_src_head = wp_get_attachment_image_src( $body_patren, 'full' );
									$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);

									
								} 
					  ?>
					<div class="content_con">
						<input name="body_patren" class="emptyme" type="hidden" id="upload_image_attachment_id" value="<?php echo esc_attr($body_patren); ?>" />
						<input id="upload_image_text" class="emptyme upload_image_text" type="text" value="<?php echo esc_url($image_src_head); ?>" />
						<input class="upload_image_button" type="button" value="Upload" />
					</div>
					<p> <?php esc_html_e('Upload background pattern for your theme this option provide you access to put your own image to use as background pattern.','neomag');?> </p>
                  </li>
                  
				   <li class="panel-right-box span4">
					   <div class="admin-logo-image">
						  <?php 
							if(!empty($body_patren)){ 
								$image_src_head = wp_get_attachment_image_src( $body_patren, 'full' );
								$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
								$thumb_src_preview = wp_get_attachment_image_src( $body_patren, array(150,150));?>
						  <img class="clearme img-class" src="<?php if(!empty($image_src_head)){echo esc_url($thumb_src_preview[0]);}?>" /><span class="close-me"></span>
						  <?php } ?>
						</div>
					</li>
                </ul>
                <ul id="image_upload_id" class="recipe_class logo_upload row-fluid">
                 
                  <li class="panel-input span8">
					   <span class="panel-title">
						<h3 for="body_image" >
						  <?php esc_html_e('Upload Background Image','neomag'); ?>
						</h3>
					  </span>
					  <?php
					   $image_src_head = '';
								
								if(!empty($body_image)){ 
								
									$image_src_head = wp_get_attachment_image_src( $body_image, 'full' );
									$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
									
								}
						
					  ?>
                    <div class="content_con">
						<input name="body_image" class="clearme" type="hidden" id="upload_image_attachment_id" value="<?php echo esc_attr($body_image); ?>" />
						<input id="upload_image_text" class="clearme upload_image_text" type="text" value="<?php echo esc_url($image_src_head); ?>" />
						<input class="upload_image_button" type="button" value="Upload" />
					</div>
					<p> <?php esc_html_e('Upload background Image for your theme this option provide you access to put your own image to use as background Image.','neomag');?> </p>
                  </li>
				   <li class="span4 description" >
					   <div class="admin-logo-image">
						  <?php 
							if(!empty($body_image)){ 
								$image_src_head = wp_get_attachment_image_src( $body_image, 'full' );
								$image_src_head = (empty($image_src_head))? '': esc_url($image_src_head[0]);
								$thumb_src_preview = wp_get_attachment_image_src( $body_image, array(150,150));?>
						  <img class="clearme img-class" src="<?php if(!empty($image_src_head)){echo esc_url($thumb_src_preview[0]);}?>" /><span class="close-me"></span>
						  <?php } ?>
						</div>
					</li>
					
                </ul>
                <div class="clear"></div>
				<div class="row-fluid">
                <ul class="recipe_class image_upload_options span4">
                 
                  <li class="panel-radioimage panel-input full-width">
				   <span class="panel-title">
                    <h3 for="position_image_layout">
                      <?php esc_html_e('Image Position','neomag'); ?>
                    </h3>
                  </span>
				  <div class="combobox cp-select-wrap">
					<select name="position_image_layout" class="position_image_layout" id="position_image_layout">
							<?php 
								$value = '';
								$options = array(
									
									'1'=>array('value'=>'top','title'=>'Position Top'),
									'2'=>array('value'=>'right','title'=>'Position Right'),
									'3'=>array('value'=>'left','title'=>'Position Left'),
									'4'=>array('value'=>'bottom','title'=>'Position Bottom'),
									'5'=>array('value'=>'center','title'=>'Position Center'),
									
								);
								foreach( $options as $option ){ ?>
									<option <?php if(esc_attr($position_image_layout) == esc_attr($option['value'])){echo sanitize_text_field('selected');}?> value="<?php echo esc_attr($option['value']);?>" class="position_image_layout"><?php echo esc_attr($option['title']);?></option>
								<?php } ?>
                    </select>
					</div>
					<p> <?php esc_html_e('You can manage background image position in this area.','neomag');?> </p>
                  </li>
				  
                </ul>
                <ul class="panel-body recipe_class image_upload_options span4">
                  <li class="panel-input full-width">
				  <span class="panel-title">
                    <h3 for="image_repeat_layout">
                      <?php esc_html_e('SELECT BACKGROUND TYPE','neomag'); ?>
                    </h3>
                  </span>
                    <div class="combobox cp-select-wrap">
                      <select name="image_repeat_layout" class="image_repeat_layout" id="image_repeat_layout">
					  			<?php
								$value = '';
								$options = array(
									'1'=>array('value'=>'no-repeat','title'=>'No Repeat'),
									'2'=>array('value'=>'repeat-x','title'=>'Repeat Horizontal'),
									'3'=>array('value'=>'repeat-y','title'=>'Repeat Vertical'),
									'4'=>array('value'=>'repeat','title'=>'Repeat'),
								);
								foreach( $options as $option ){ ?>
							<option <?php if(esc_attr($image_repeat_layout) == esc_attr($option['value'])){echo sanitize_text_field('selected');}?> value="<?php echo esc_attr($option['value']);?>" class="select_bg_patren"><?php echo esc_attr($option['title']);?></option>
						<?php }?>
                      </select>
                    </div>
					<p> <?php esc_html_e('You can manage your image repeat whether its repeated horizontal verticle or both.','neomag');?> </p>
                  </li>
                </ul>
                <ul class="recipe_class image_upload_options span4">
                  
                  <li class="panel-radioimage panel-input full-width">
				  <span class="panel-title ">
                    <h3 for="image_attachment_layout">
                      <?php esc_html_e('Image Attachment','neomag'); ?>
                    </h3>
                  </span>
				  <div class="combobox cp-select-wrap">
				   <select name="image_attachment_layout" class="image_attachment_layout" id="image_attachment_layout">
						<?php 
						$value = '';
						$options = array(
							'1'=>array('value'=>'fixed','title'=>'Fixed'),
							'2'=>array('value'=>'scroll','title'=>'Scroll'),
						);
						foreach( $options as $option ){ ?>
							<option <?php if(esc_attr($image_attachment_layout) == esc_attr($option['value'])){echo sanitize_text_field('selected');}?> value="<?php echo esc_attr($option['value']);?>" class="image_attachment_layout"><?php echo esc_attr($option['title']);?></option>                   
						<?php } ?>
					</select>
					</div>
					<p> <?php esc_html_e('You can manage your background image attachment fixed or scroll.','neomag');?> </p>
                  </li>
				 
                </ul>
				</div>
              </li>
              <li id="hr_settings" class="logo_dimenstion">
				<div class="row-fluid">
					<ul class="panel-body recipe_class span12">
						<li class="panel-input full-width">
							<?php
								$images = array(
									'1'=>array('value'=>'header_1', 'image'=>'/frontend/header/header_1.jpg'),
									'2'=>array('value'=>'header_2','image'=>'/frontend/header/header_2.jpg'),
									'3'=>array('value'=>'header_3','image'=>'/frontend/header/header_3.jpg'),
									'4'=>array('value'=>'header_4','image'=>'/frontend/header/header_4.jpg'),
								);							
								echo '<div class="select_header_img">';
									foreach($images as $keys=>$val){
										echo '<div class="header_image_cp" id="'.esc_attr($val['value']).'"><img src="'.esc_url(NEOMAG_PATH_URL.$val['image']).'" atl=""></div>';
									}
								echo '</div>';
							?>
							
							<p><?php esc_html_e('Please select website default header style from dropdown below.','neomag');?></p>
						</li>
					</ul>    
				</div>
			<div class="row-fluid">
					<ul class="panel-body recipe_class span8">
						 <li class="panel-input full-width">
							<span class="panel-title">
								<h3 for="select_header_cp">
								  <?php esc_html_e('SELECT HEADER LAYOUT','neomag'); ?>
								</h3>
							</span>
							<div class="combobox">
							  <select name="select_header_cp" class="select_header_cp" id="select_header_cp">
								<option <?php if(esc_attr($select_header_cp) == 'Style 1'){echo sanitize_text_field('selected');}?> value="Style 1" class="header_1">Style 1</option>
								<option <?php if(esc_attr($select_header_cp) == 'Style 2'){echo sanitize_text_field('selected');}?> value="Style 2" class="header_2">Style 2</option>
								<option <?php if(esc_attr($select_header_cp) == 'Style 3'){echo sanitize_text_field('selected');}?> value="Style 3" class="header_3">Style 3</option>
								<option <?php if(esc_attr($select_header_cp) == 'Style 4'){echo sanitize_text_field('selected');}?> value="Style 4" class="header_4">Style 4</option>
							  </select>
							</div>
							<p> <?php esc_html_e('Please select website default header style from dropdown.','neomag');?> </p>
						  </li>
					</ul>    
					<ul class="panel-body recipe_class span4">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3 for="" >
									<?php esc_html_e('Apply Header Style On All Pages','neomag'); ?>
								</h3>
							</span>
							<label for="header_style_apply">
								<div class="checkbox-switch <?php echo (esc_attr($header_style_apply)=='enable' || (esc_attr($header_style_apply) =='' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off'; ?>"></div>
							</label>
							<input type="checkbox" name="header_style_apply" class="checkbox-switch" value="disable" checked>
							<input type="checkbox" name="header_style_apply" id="header_style_apply" class="checkbox-switch" value="enable" 
							<?php echo (esc_attr($header_style_apply) =='enable' || (esc_attr($header_style_apply) =='' && empty($default)))? 'checked': ''; ?>>
							<p><?php esc_html_e('You can turn On/Off Top Bar. Note: Turning it off Search and Top Social Networking Icons will be activated.','neomag');?></p>
							<p><?php esc_html_e('You can turn On/Off to add above header style apply on all pages turning it off page settings will be apply on each page.','neomag');?></p>
						</li>
					</ul>
				</div>
				<div class="row-fluid">
					<ul class="panel-body recipe_class span6">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3 for="header_css_code" >
									<?php esc_html_e('HEADER CSS CODE','neomag'); ?>
								</h3>
							</span>
							<textarea name="header_css_code" id="header_css_code" ><?php echo (esc_attr($header_css_code) == '')? esc_textarea($header_css_code): esc_textarea($header_css_code);?></textarea>
							<p><?php esc_html_e('Please write css code for you theme if you want to put some extra code in css for styling purpose only.','neomag');?></p>
						</li>				 
					</ul>
					<ul class="panel-body recipe_class span6">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3 for="cp_javascript_code" >
									<?php esc_html_e('HEADER JAVASCRIPT CODE','neomag'); ?>
								</h3>
							</span>
							<textarea name="cp_javascript_code" id="cp_javascript_code" ><?php if(esc_attr($cp_javascript_code) <> '') { echo esc_textarea($cp_javascript_code);}?></textarea>
							<p><?php esc_html_e('Please write js code for you theme if you want to put some extra code in your theme.','neomag');?></p>
						</li> 
					</ul>    
				</div>
				<div class="row-fluid">
				<!--Sign In Sign Out Button -->
					<ul class="panel-body recipe_class span4">
					  <li class="panel-input full-width">
						<span class="panel-title">
							<h3 for="" >
								<?php esc_html_e('TOP SOCIAL NETWORK ICONS','neomag'); ?>
							</h3>
						</span>
						<label for="topsocial_icon">
							<div class="checkbox-switch <?php echo (esc_attr($topsocial_icon) =='enable' || (esc_attr($topsocial_icon) =='' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off';?>"></div>
						</label>
						<input type="checkbox" name="topsocial_icon" class="checkbox-switch" value="disable" checked>
						<input type="checkbox" name="topsocial_icon" id="topsocial_icon" class="checkbox-switch" value="enable" <?php echo (esc_attr($topsocial_icon) =='enable' || (esc_attr($topsocial_icon) =='' && empty($default)))? 'checked': ''; ?>>
						<p><?php esc_html_e('You can turn On/Off Top social network icons from main menu.','neomag');?></p>
					  </li>
					</ul>
				</div>
				<!--Sign In Sign Out Button -->
			
              </li>
            <li id="ft_settings" class="logo_dimenstion">
				<div class="row-fluid">
					<ul class="panel-body recipe_class span12">
						<li class="panel-input full-width">
							<?php
								$images = array(
									'1'=>array('value'=>'footer_1', 'image'=>'/frontend/footer/footer_1.jpg'),
								);							
								echo '<div class="select_footer_img">';
									foreach($images as $keys=>$val){
										echo '<div class="footer_image_cp" id="'.esc_attr($val['value']).'"><img src="'.esc_url(NEOMAG_PATH_URL.$val['image']).'" atl=""></div>';
									}
								echo '</div>';
							?>
							
							
						</li>
					</ul>    
				</div>
				<div class="row-fluid">
					<ul class="panel-body recipe_class span8">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
								  <?php esc_html_e('SELECT FOOTER LAYOUT','neomag'); ?>
								</h3>
							</span>
							<div class="combobox">
								<select name="select_footer_cp" class="select_footer_cp" id="select_footer_cp">
									<option <?php if(esc_attr($select_footer_cp) == 'Style 1'){echo sanitize_text_field('selected');}?> value="Style 1" class="footer_1">Style 1</option>
								</select>
							</div>
							<p><?php esc_html_e('Please select website default footer style from dropdown.','neomag');?></p>
						</li>
					</ul>    
					<ul class="panel-body recipe_class span4">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('APPLY FOOTER STYLE ON ALL PAGES','neomag'); ?>
								</h3>
							</span>
							<label for="footer_style_apply">
								<div class="checkbox-switch <?php echo (esc_attr($neomag_footer_style_apply) =='enable' || (esc_attr($neomag_footer_style_apply) =='' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off'; ?>"></div>
							</label>
							<input type="checkbox" name="footer_style_apply" class="checkbox-switch" value="disable" checked>
							<input type="checkbox" name="footer_style_apply" id="footer_style_apply" class="checkbox-switch" value="enable" 
							<?php echo (esc_attr($neomag_footer_style_apply) =='enable' || (esc_attr($neomag_footer_style_apply) =='' && empty($default)))? 'checked': ''; ?>>
							<p><?php esc_html_e('You can turn On/Off to add above footer style apply on all pages turning it off page settings will be apply on each page.','neomag');?></p>
						</li>
					</ul>
				</div>
                <div class="clear"></div>
                <ul class="panel-body recipe_class row-fluid">
                  <li class="panel-input span8">
					  <span class="panel-title">
						<h3>
						  <?php esc_html_e('Width','neomag'); ?>
						</h3>
					  </span>
                    <div id="footer_logo_width" class="sliderbar" rel="logo_bar"></div>
                    <input type="hidden" name="footer_logo_width" value="<?php echo esc_attr($footer_logo_width);?>">
					<p> <?php esc_html_e('Please scroll Left to Right to adjust logo image width, you can also use Arrow keys UP,Down - Left,Right.','neomag');?> </p>                  
                  </li>
                  <li class="span4 right-box-sec" id="slidertext"><?php echo esc_attr($footer_logo_width);?> <?php esc_html_e('px','neomag');?> </li>
                </ul>
                <div class="clear"></div>
                <ul class="panel-body recipe_class row-fluid">
                  <li class="panel-input span8">
					  <span class="panel-title">
						<h3>
						  <?php esc_html_e('Height','neomag'); ?>
						</h3>
					  </span>
                    <div id="footer_logo_height" class="sliderbar" rel="logo_bar"></div>
                    <input type="hidden" name="footer_logo_height" value="<?php echo esc_attr($footer_logo_height);?>">
					<p> <?php esc_html_e('Please scroll Left to Right to adjust logo image height, you can also use Arrow keys UP,Down - Left,Right.','neomag');?> </p>  
                  </li>
				  <li class="span4 right-box-sec" id="slidertext"><?php echo esc_attr($footer_logo_height);?> <?php esc_html_e('px','neomag');?> </li>
                </ul>				
				<div class="row-fluid">
					<ul class="panel-body recipe_class span6">
					  <li class="panel-input full-width">
					  <span class="panel-title">
						<h3 for="" >
						  <?php esc_html_e('SOCIAL ICONS','neomag'); ?>
						</h3>
					  </span>
						<label for="social_networking">
						<div class="checkbox-switch <?php
									
									echo esc_attr((esc_attr($social_networking) =='enable' || (esc_attr($social_networking) =='' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off'); 

								?>"></div>
						</label>
						<input type="checkbox" name="social_networking" class="checkbox-switch" value="disable" checked>
						<input type="checkbox" name="social_networking" id="social_networking" class="checkbox-switch" value="enable" <?php 
									
									echo esc_attr((esc_attr($social_networking) =='enable' || (esc_attr($social_networking) =='' && empty($default)))? 'checked': ''); 
								
								?>>
								<div class="clear"></div>
							<p> <?php esc_html_e('You can turn On/Off footer social networking profile icons.','neomag');?></p>
					  </li>
					</ul>
					<ul class="panel-body recipe_class span6">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3 for="copyright_code" >
									<?php esc_html_e('COPY RIGHT TEXT','neomag'); ?>
								</h3>
							</span>
							<input type="text" name="copyright_code" id="copyright_code" value="<?php echo (esc_attr($copyright_code) == '')? esc_attr($copyright_code): esc_attr($copyright_code);?>" />
							<div class="clear"></div>
							<p><?php esc_html_e('Please paste here your copy right text.','neomag');?></p>
						</li>
					</ul>
				</div>
				<div class="row-fluid">
					<ul class="recipe_class span6 footer_widget">
						<li class="panel-radioimage panel-input full-width">
							<span class="panel-title">
								<h3 for="">
								  <?php esc_html_e('FOOTER WIDGET LAYOUT','neomag'); ?>
								</h3>
							</span>
							<?php 
								$value = '';
								$options = array(
								'1'=>array('value'=>'footer-style1','image'=>'/framework/images/footer-style1.png'),
								'6'=>array('value'=>'footer-style6','image'=>'/framework/images/footer-style6.png'),
								);
								foreach( $options as $option ){ ?>
							<div class='radio-image-wrapper'>
								<label for="<?php echo esc_attr($option['value']); ?>">
								  <img src=<?php echo esc_url(NEOMAG_PATH_URL.$option['image'])?> class="footer_col_layout" alt="footer_col_layout" />
								  <div id="check-list"></div>
								</label>
								<input type="radio" name="footer_col_layout" value="<?php echo esc_attr($option['value']); ?>" id="<?php echo esc_attr($option['value']); ?>" class="dd"
								<?php if(esc_attr($footer_col_layout) == esc_attr($option['value'])){ echo sanitize_text_field('checked');}?>>
							</div>
							<?php } ?>
							<div class="clear"></div>
							<p> <?php esc_html_e('Please select home page layout style.','neomag');?></p>
						</li>
					</ul>
				</div>
            </li>
              <li id="misc_settings">
				<div class="row-fluid">
					<ul class="panel-body recipe_class span6">
					  <li class="panel-input full-width">
					   <span class="panel-title">
						<h3 for="" >
						  <?php esc_html_e('BREADCRUMBS','neomag'); ?>
						</h3>
					  </span>
						<label for="breadcrumbs">
						<div class="checkbox-switch <?php
									
									echo (esc_attr($breadcrumbs) =='enable' || (esc_attr($breadcrumbs) =='' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off'; 

								?>"></div>
						</label>
						<input type="checkbox" name="breadcrumbs" class="checkbox-switch" value="disable" checked>
						<input type="checkbox" name="breadcrumbs" id="breadcrumbs" class="checkbox-switch" value="enable" <?php 
									
									if(esc_attr($breadcrumbs) =='enable' ){echo '';} echo (esc_attr($breadcrumbs) =='enable' || (esc_attr($breadcrumbs) =='' && empty($default)))? 'checked': ''; 
								
								?>>
						<p> <?php esc_html_e('You can turn On/Off BreadCrumbs from Top of the page.','neomag');?></p>
					  </li>
					  
					</ul>
				</div>
              </li>

			  <li id="maintenance_mode_settings">
				<div class="row-fluid">
					<ul class="panel-body recipe_class span3">
						<li class="panel-input full-width">
						   <span class="panel-title">
							<h3 for="" >
							  <?php esc_html_e('Maintenance Mode','neomag'); ?>
							</h3>
						  </span>
							<label for="maintenance_mode">
							<div class="checkbox-switch <?php
										
										echo (esc_attr($maintenance_mode) =='enable' || (esc_attr($maintenance_mode) =='' && empty($default)))? 'checkbox-switch-on': 'checkbox-switch-off'; 

									?>"></div>
							</label>
							<input type="checkbox" name="maintenance_mode" class="checkbox-switch" value="disable" checked>
							<input type="checkbox" name="maintenance_mode" id="maintenance_mode" class="checkbox-switch" value="enable" 
							<?php if(esc_attr($maintenance_mode) =='enable' ){echo '';} echo (esc_attr($maintenance_mode) =='enable' || (esc_attr($maintenance_mode) =='' && empty($default)))? 'checked': ''; ?>>
							<p><?php esc_html_e('You can turn On/Off Maintenance mode from here.','neomag');?></p>
						</li>
					</ul>
					<ul class="panel-body recipe_class span3">
						 <li class="panel-input full-width">
							<span class="panel-title">
								<h3>
								  <?php esc_html_e('SELECT COMMING SOON LAYOUT','neomag'); ?>
								</h3>
							</span>
							<div class="combobox">
								<select name="cp_comming_soon" class="cp_comming_soon" id="cp_comming_soon">
									<option <?php if('Style 1' == esc_attr($cp_comming_soon)){echo 'selected';}?> value="<?php esc_html_e('Style 1','neomag')?>"><?php esc_html_e('Style 1','neomag');?></option>
								</select>
							</div>
							<p> <?php esc_html_e('Please select coming soon style of your choice.','neomag');?> </p>
						  </li>
					</ul>
					<ul class="panel-body recipe_class span3">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('Maintenance Title','neomag'); ?>
								</h3>
							</span>
							<input type="text" name="maintenace_title" id="maintenace_title" value="<?php echo (esc_attr($maintenace_title) == '')? esc_attr($maintenace_title): esc_attr($maintenace_title);?>" />
							<div class="clear"></div>
							<p><?php esc_html_e('Please add title on maintenance page.','neomag');?></p>
						</li>
					</ul>
					<ul class="panel-body recipe_class span3">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('Countdown Time','neomag'); ?>
								</h3>
							</span>
							<input type="text" name="countdown_time" id="countdown_time" value="<?php echo (esc_attr($countdown_time) == '')? esc_attr($countdown_time): esc_attr($countdown_time);?>" />
							<div class="clear"></div>
							<p><?php esc_html_e('Please select time span when your site will be live and counter will countdown seconds to mints and days till that on maintenance page.','neomag');?></p>
						</li>
					</ul>
					
				</div>
				<div class="row-fluid">
					<ul class="panel-body recipe_class span4">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3>
									<?php esc_html_e('Email','neomag'); ?>
								</h3>
							</span>
							<input type="text" name="email_mainte" id="email_mainte" value="<?php echo (esc_attr($email_mainte) == '')? esc_attr($email_mainte): esc_attr($email_mainte);?>" />
							<div class="clear"></div>
							<p><?php esc_html_e('Add email where you want to post subscriptions.','neomag');?></p>
						</li>
					</ul>
					
					<ul class="panel-body recipe_class span4">
						<li class="panel-input full-width">
							<span class="panel-title">
								<h3 for="mainte_description" >
									<?php esc_html_e('Description','neomag'); ?>
								</h3>
							</span>
							<textarea name="mainte_description" id="mainte_description" ><?php if(esc_attr($mainte_description) <> '') { echo esc_textarea(esc_attr($mainte_description));}?></textarea>
							<p><?php esc_html_e('Please add description text for your website maintenance.','neomag');?></p>
						</li> 
					</ul>    
				</div>	
              </li>
			 
              <?php if(!class_exists( 'Envato_WordPress_Theme_Upgrader' )){}else{?>
              <li id="envato_api" class="envato_api">
                <ul class="panel-body recipe_class">
                 
                  <li class="panel-input">
					   <span class="panel-title">
						<h3 for="tf_username" >
						  <?php esc_html_e('Username','neomag'); ?>
						</h3>
					  </span>
                    <input type="text" name="tf_username" id="tf_username" value="<?php echo (esc_attr($tf_username) == '')? esc_attr($tf_username): esc_attr($tf_username);?>" />
					<span><?php esc_html_e('Please enter your Theme Forest username.','neomag');?>  <br />
                    <p><?php esc_html_e('For example: denonstudio','neomag');?>  </p></span>
                  </li>
                  
                </ul>
                <ul class="panel-body recipe_class">
                  
                  <li class="panel-input">
				  <span class="panel-title">
                    <h3 for="tf_sec_api" >
                      <?php esc_html_e('API Key','neomag'); ?>
                    </h3>
                  </span>
                    <input type="text" name="tf_sec_api" id="tf_sec_api" value="<?php echo (esc_attr($tf_sec_api) == '')? esc_attr($tf_sec_api): esc_attr($tf_sec_api);?>" />
                  </li>
                  <span class="right-box-sec"> <?php esc_html_e('Please paste here your theme forest Secret API Key.','neomag');?>  <br />
                     <p><?php esc_html_e('For example: xxxxxxxav7hny3p1ptm7xxxxxxxx','neomag');?> <p></span>
                </ul>
              </li>
              <?php }?>
            </ul>
            <div class="clear"></div>
            <div class="panel-element-tail">
              <div class="tail-save-changes">
                <div class="loading-save-changes"></div>
                <input type="submit" value="<?php echo esc_html_e('Save Changes','neomag') ?>">
                <input type="hidden" name="action" value="general_options">
                
              </div>
            </div>
          </div>
        </form>
      </div>
      <!--content End --> 
    </div>
    <!--content area end --> 
  </div>
<?php
	}
?>