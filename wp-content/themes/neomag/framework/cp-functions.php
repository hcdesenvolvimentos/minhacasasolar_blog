<?php

	/*	
	*	Crunchpress Function Registered File
	*	---------------------------------------------------------------------
	* 	@version	1.0
	* 	@author		Crunchpress
	* 	@link		http://crunchpress.com
	* 	@copyright	Copyright (c) Crunchpress
	*	---------------------------------------------------------------------
	*	This file use to register the wordpress function to the framework,
	*	and also use filter to hook some necessary events.
	*	---------------------------------------------------------------------
	*/
	
	if (function_exists('register_sidebar')){	
	
		// default sidebar array
		$sidebar_attr = array(
			'name' => '',
			'description' => '',
			'before_widget' => '<div class="widget sidebar-recent-post sidebar_section %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		);

		$footer_col_layout = '';
		$footer_col_layout = neomag_cp_get_themeoption_value('footer_col_layout','general_settings');
		
		$sidebar_id = 0;
		$cp_sidebar = array();
		//Print Footer Widget Areas
		$select_footer_cp = neomag_cp_get_themeoption_value('select_footer_cp','general_settings');
		if($select_footer_cp == 'Style 1'){
			$cp_sidebar = array("Footer");
			$cp_sidebar_upper = array("Upper-Footer");
		}else if($select_footer_cp == 'Style 6'){
			$cp_sidebar = array();
			$cp_sidebar_upper = array();
		}else{
			$cp_sidebar = array("Footer");
			$cp_sidebar_upper = array();
		}
		//Home Page Layout		
		if($footer_col_layout == 'footer-style1'){
			
			foreach( $cp_sidebar as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name;
				$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
				$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug ;
				$sidebar_attr['before_widget'] = '<div class="col-md-3"><div class="widget box-1 %2$s">' ;
				$sidebar_attr['before_title'] = '<h4>' ;
				$sidebar_attr['after_widget'] = '</div></div>' ;
				$sidebar_attr['after_title'] = '</h4>' ;
				$sidebar_attr['description'] = 'Please place widget here' ;				
				register_sidebar($sidebar_attr);
			}
		}else{
			foreach( $cp_sidebar as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name;
				$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
				$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug ;
				$sidebar_attr['before_widget'] = '<div class="col-md-4"><div class="widget box-1 %2$s">' ;
				$sidebar_attr['after_widget'] = '</div></div>' ;
				$sidebar_attr['before_title'] = '<h2>';
				$sidebar_attr['after_title'] = '<span class="h-line"></span></h2>' ;
				$sidebar_attr['description'] = 'Please place widget here' ;
				register_sidebar($sidebar_attr);
			}
		}

		$footer_upper_layout = neomag_cp_get_themeoption_value('footer_upper_layout','general_settings');
		//Home Page Layout		
		if($footer_upper_layout == 'footer-style-upper-1'){
			
			foreach( $cp_sidebar_upper as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name;
				$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
				$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug ;
				$sidebar_attr['before_widget'] = '<div class="col-md-3"><div class="widget %2$s">' ;
				$sidebar_attr['before_title'] = '<h2>';
				$sidebar_attr['after_widget'] = '</div></div>' ;
				$sidebar_attr['after_title'] = '<span class="h-line"></span></h2>' ;
				$sidebar_attr['description'] = 'Please place widget here' ;				
				register_sidebar($sidebar_attr);
			}
		}else{
			
			foreach( $cp_sidebar_upper as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name;
				$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
				$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug ;
				$sidebar_attr['before_widget'] = '<div class="%2$s">' ;
				$sidebar_attr['after_widget'] = '</div>' ;
				$sidebar_attr['before_title'] = '<h3>';
				$sidebar_attr['after_title'] = '</h3>' ;
				$sidebar_attr['description'] = 'Please place widget here' ;
				register_sidebar($sidebar_attr);
			}
		}			
		
		$cp_sidebar = '';
		$cp_sidebar = get_option('sidebar_settings');
		
		if(!empty($cp_sidebar)){
			$xml = new DOMDocument();
			$xml->loadXML($cp_sidebar);
			foreach( $xml->documentElement->childNodes as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name->nodeValue;
				$sidebar_attr['id'] = 'custom-sidebar' . $sidebar_id++ ;
				$sidebar_attr['before_widget'] = '<div class="widget sidebar_section sidebar-recent-post %2$s">' ;
				$sidebar_attr['after_widget'] = '</div>' ;
				$sidebar_attr['before_title'] = '<h3>' ;
				$sidebar_attr['after_title'] = '</h3>' ;
				register_sidebar($sidebar_attr);
			}
		}
	}
	
	//Add Theme Support
	if(function_exists('add_theme_support')){
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list',) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		
		// enable featured image
		add_theme_support('post-thumbnails');
		
		// enable editor style
		add_editor_style('editor-style.css');
		
		// enable navigation menu
		add_theme_support('menus');
		register_nav_menus(array('header-menu'=>'Header Menu'));
		
		add_theme_support( 'title-tag' );
	}

class neo_mag_archives extends WP_Widget_Archives{

	public function __construct() {
		$widget_ops = array('classname' => 'archives-widget', 'description' => esc_html__( 'A monthly archive of your site&#8217;s Posts.','neomag') );
		parent::__construct('archives', esc_html__('Archives','neomag'), $widget_ops);
	}
	public function widget( $args, $instance ) {
		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$d = ! empty( $instance['dropdown'] ) ? '1' : '0';

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? esc_html__( 'Archives','neomag' ) : $instance['title'], $instance, $this->id_base );
		echo html_entity_decode($args['before_widget']);
		if ( $title ) {
			echo html_entity_decode($args['before_title']) . $title . $args['after_title'];
		}
		if ( $d ) {
			$dropdown_id = "{$this->id_base}-dropdown-{$this->number}";
?>
              
<div class="widget-content">
		<label class="screen-reader-text" for="<?php echo esc_attr( $dropdown_id ); ?>"><?php echo esc_attr($title); ?></label>
		<select id="<?php echo esc_attr( $dropdown_id ); ?>" name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
			<?php
			$dropdown_args = apply_filters( 'widget_archives_dropdown_args', array(
				'type'            => 'monthly',
				'format'          => 'option',
				'show_post_count' => $c
			) );

			switch ( $dropdown_args['type'] ) {
				case 'yearly':
					$label = esc_html__( 'Select Year' ,'neomag');
					break;
				case 'monthly':
					$label = esc_html__( 'Select Month' ,'neomag');
					break;
				case 'daily':
					$label = esc_html__( 'Select Day' ,'neomag');
					break;
				case 'weekly':
					$label = esc_html__( 'Select Week' ,'neomag');
					break;
				default:
					$label = esc_html__( 'Select Post' ,'neomag');
					break;
			}
			?>
			<option value=""><?php echo esc_attr( $label ); ?></option>
			<?php wp_get_archives( $dropdown_args ); ?>
	</select>
              </div>
<?php
		} else {
?>
	<div class="widget-content">
		<ul>
<?php
		wp_get_archives( apply_filters( 'widget_archives_args', array(
			'type'            => 'monthly',
			'show_post_count' => $c
		) ) );
?>
		</ul>
     </div>   
<?php
		}
		echo html_entity_decode($args['after_widget']);
	}
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args( (array) $new_instance, array( 'title' => '', 'count' => 0, 'dropdown' => '') );
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = $new_instance['count'] ? 1 : 0;
		$instance['dropdown'] = $new_instance['dropdown'] ? 1 : 0;

		return $instance;
	}
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'count' => 0, 'dropdown' => '') );
		$title = strip_tags($instance['title']);
		$count = $instance['count'] ? 'checked="checked"' : '';
		$dropdown = $instance['dropdown'] ? 'checked="checked"' : '';
?>
		<p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:','neomag'); ?></label> <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo esc_attr($dropdown); ?> id="<?php echo esc_attr($this->get_field_id('dropdown')); ?>" name="<?php echo esc_attr($this->get_field_name('dropdown')); ?>" /> <label for="<?php echo esc_attr($this->get_field_id('dropdown')); ?>"><?php esc_html_e('Display as dropdown','neomag'); ?></label>
			<br/>
			<input class="checkbox" type="checkbox" <?php echo esc_attr($count); ?> id="<?php echo esc_attr($this->get_field_id('count')); ?>" name="<?php echo esc_attr($this->get_field_name('count')); ?>" /> <label for="<?php echo esc_attr($this->get_field_id('count')); ?>"><?php esc_html_e('Show post counts','neomag'); ?></label>
		</p>
<?php
	}
}
// add_action( 'widgets_init', function(){
//      register_widget( 'neo_mag_archives' );
// });

	class Neomag_Customize {
	   public static function register ( $wp_customize ) {
		  $wp_customize->add_section( 'mytheme_options', 
			 array(
				'title' => esc_html__( 'MyTheme Options','neomag' ), //Visible title of section
				'priority' => 35, //Determines what order this appears in
				'capability' => 'edit_theme_options', //Capability needed to tweak
				'description' => esc_html__('Allows you to customize some example settings for crunchpress.','neomag'), //Descriptive tooltip
			 ) 
		  );
		  
		  $wp_customize->add_setting( 'mytheme_options[link_textcolor]', //Give it a SERIALIZED name (so all theme settings can live under one db record)
			 array(
				'default' => '#2BA6CB', //Default setting/value to save
				'type' => 'option', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
				'sanitize_callback' => 'example_sanitize_text',
			 ) 
		  );      
				
		  $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
			 $wp_customize, //Pass the $wp_customize object (required)
			 'mytheme_link_textcolor', //Set a unique ID for the control
			 array(
				'label' => esc_html__( 'Link Color & Button Color','neomag' ), //Admin-visible name of the control
				'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
				'settings' => 'mytheme_options[link_textcolor]', //Which setting to load and manipulate (serialized is okay)
				'priority' => 10, //Determines the order this control appears in for the specified section
			 ) 
		  ) );
		  
		  $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
		  $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
		  $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
		  $wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
	   }
	   public static function header_output() {
		  ?>
		  <!--Customizer CSS--> 
		  <style type="text/css">
			   <?php self::generate_css('#site-title a', 'color', 'header_textcolor', '#'); ?> 
			   <?php self::generate_css('body', 'background-color', 'background_color', '#'); ?> 
			   <?php self::generate_css('a', 'color', 'mytheme_options[link_textcolor]'); ?>
		  </style> 
		  <!--/Customizer CSS-->
		  <?php
	   }
	   public static function live_preview() {
		  wp_enqueue_script( 
			   'mytheme-themecustomizer', // Give the script a unique ID
			   get_template_directory_uri() . '/frontend/js/theme-customizer.js', // Define the path to the JS file
			   array(  'jquery', 'customize-preview' ), // Define dependencies
			   '', // Define a version (optional) 
			   true // Specify whether to put in footer (leave this true)
		  );
	   }
		public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
		  $return = '';
		  $mod = get_theme_mod($mod_name);
		  if ( ! empty( $mod ) ) {
			 $return = sprintf('%s { %s:%s; }',
				$selector,
				$style,
				$prefix.$mod.$postfix
			 );
			 if ( $echo ) {
				echo esc_attr($return);
			 }
		  }
		  return $return;
		}
	}

	// Setup the Theme Customizer settings and controls...
	add_action( 'customize_register' , array( 'Neomag_Customize' , 'register' ) );
	// Output custom CSS to live site
	add_action( 'wp_head' , array( 'Neomag_Customize' , 'header_output' ) );
	// Enqueue live preview javascript in Theme Customizer admin screen
	add_action( 'customize_preview_init' , array( 'Neomag_Customize' , 'live_preview' ) );

class neo_mag_tag_cloud extends WP_Widget_Tag_Cloud{

	public function __construct() {
		$widget_ops = array('classname' => 'tags-widget',  'description' => esc_html__( "A cloud of your most used tags.",'neomag') );
		parent::__construct('tag_cloud', esc_html__('Tag Cloud','neomag'), $widget_ops);
	}
	public function widget( $args, $instance ) {
		$current_taxonomy = $this->_get_current_taxonomy($instance);
		if ( !empty($instance['title']) ) {
			$title = $instance['title'];
		} else {
			if ( 'post_tag' == $current_taxonomy ) {
				$title = esc_html__('Tags','neomag');
			} else {
				$tax = get_taxonomy($current_taxonomy);
				$title = $tax->labels->name;
			}
		}
		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo html_entity_decode($args['before_widget']);
		if ( $title ) {
			echo html_entity_decode($args['before_title']) . $title . $args['after_title'];
		}
		echo '<div class="widget-content tags-widget">';

		wp_tag_cloud( apply_filters( 'widget_tag_cloud_args', array(
			'taxonomy' => $current_taxonomy
		) ) );
		echo "</div>\n";
		echo html_entity_decode($args['after_widget']);
	}
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags(stripslashes($new_instance['title']));
		$instance['taxonomy'] = stripslashes($new_instance['taxonomy']);
		return $instance;
	}
	public function form( $instance ) {
		$current_taxonomy = $this->_get_current_taxonomy($instance);
?>
	<p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:','neomag') ?></label>
	<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php if (isset ( $instance['title'])) {echo esc_attr( $instance['title'] );} ?>" /></p>
	<p><label for="<?php echo esc_attr($this->get_field_id('taxonomy')); ?>"><?php esc_html_e('Taxonomy:','neomag') ?></label>
	<select class="widefat" id="<?php echo esc_attr($this->get_field_id('taxonomy')); ?>" name="<?php echo esc_attr($this->get_field_name('taxonomy')); ?>">
	<?php foreach ( get_taxonomies() as $taxonomy ) :
				$tax = get_taxonomy($taxonomy);
				if ( !$tax->show_tagcloud || empty($tax->labels->name) )
					continue;
	?>
		<option value="<?php echo esc_attr($taxonomy) ?>" <?php selected($taxonomy, $current_taxonomy) ?>><?php echo esc_attr($tax->labels->name); ?></option>
	<?php endforeach; ?>
	</select></p><?php
	}
	public function _get_current_taxonomy($instance) {
		if ( !empty($instance['taxonomy']) && taxonomy_exists($instance['taxonomy']) )
			return $instance['taxonomy'];

		return 'post_tag';
	}
}
// add_action( 'widgets_init', function(){
//      register_widget( 'neo_mag_tag_cloud' );
// });

	add_filter( 'wpcf7_support_html5_fallback', '__return_true' ); 


	function getPostViews($postID){
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
			return "0 View";
		}
		return $count.' Views';
	}
	
	function setPostViews($postID) {
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}


/* Filter the content of chat posts. */
add_filter( 'the_content', 'neo_mag_chat_content' );

function neo_mag_chat_content( $neomag_content ) {
	global $_post_format_chat_ids;

	if ( !has_post_format( 'chat' ) )
		return $neomag_content;
	$_post_format_chat_ids = array();
	$separator = apply_filters( 'neo_mag_format_chat_separator', ':' );
	$chat_output = "\n\t\t\t" . '<div id="chat-transcript-' . esc_attr( get_the_ID() ) . '" class="cp-chat-content"><ul>';
	$chat_rows = preg_split( "/(\r?\n)+|(<br\s*\/?>\s*)+/", $neomag_content );
	foreach ( $chat_rows as $chat_row ) {

		if ( strpos( $chat_row, $separator ) ) {
			$chat_row_split = explode( $separator, trim( $chat_row ), 2 );
			$chat_author = strtolower(strip_tags( trim( $chat_row_split[0] ) ));
			$chat_text = trim( $chat_row_split[1] );
			$speaker_id = neo_mag_chat_row_id( $chat_author );
			$chat_output .= "\n\t\t\t\t" . '<li class="' . sanitize_html_class( "user-{$speaker_id}" ) . '">';
			/* Add the chat row author. */
			$chat_output .= "\n\t\t\t\t\t" . '<div class="chat-user ' . sanitize_html_class( strtolower( "chat-author-{$chat_author}" ) ) . ' vcard">';
			if($speaker_id == 1){
			$chat_output .= '<img alt="'.$speaker_id.'" src="'.NEOMAG_PATH_URL.'/images/chatuser1.jpg"><strong>' . $chat_author . '' . $separator . '</strong></div>';
			} else {
			$chat_output .= '<img alt="'.$speaker_id.'" src="'.NEOMAG_PATH_URL.'/images/chatuser2.jpg"><strong>' . $chat_author . '' . $separator . '</strong></div>';
				}
			/* Add the chat row text. */
			if($chat_text <> ''){
			$chat_output .= "\n\t\t\t\t\t" . '<div class="chat-text"><p>' . str_replace( array( "\r", "\n", "\t" ), '', apply_filters( 'neo_mag_format_chat_text', $chat_text, $chat_author, $speaker_id ) ) . '</div>'; }
			/* Close the chat row. */
			$chat_output .= "\n\t\t\t\t" . '</li><!-- .chat-row -->';
		}
		else {
			/* Make sure we have text. */
			if ( !empty( $chat_row ) ) {
				/* Open the chat row. */
				$chat_output .= "\n\t\t\t\t" . '<li class="chat-row ' . sanitize_html_class( "user-{$speaker_id}" ) . '">';
				/* Don't add a chat row author.  The label for the previous row should suffice. */
				/* Add the chat row text. */
				$chat_output .= "\n\t\t\t\t\t" . '<div class="chat-text"><p>' . str_replace( array( "\r", "\n", "\t" ), '', apply_filters( 'neo_mag_format_chat_text', $chat_row, $chat_author, $speaker_id ) ) . '</p></div>';
				/* Close the chat row. */
				$chat_output .= "\n\t\t\t</li><!-- .chat-row -->";
			}
		}
	}
	/* Close the chat transcript div. */
	$chat_output .= "\n\t\t\t</ul></div><!-- .chat-transcript -->\n";
	/* Return the chat content and apply filters for developers. */
	return apply_filters( 'neo_mag_format_chat_content', $chat_output );
}

function neo_mag_chat_row_id( $chat_author ) {
	global $_post_format_chat_ids;

	/* Let's sanitize the chat author to avoid craziness and differences like "John" and "john". */
	$chat_author = strtolower( strip_tags( $chat_author ) );

	/* Add the chat author to the array. */
	$_post_format_chat_ids[] = $chat_author;

	/* Make sure the array only holds unique values. */
	$_post_format_chat_ids = array_unique( $_post_format_chat_ids );

	/* Return the array key for the chat author and add "1" to avoid an ID of "0". */
	return absint( array_search( $chat_author, $_post_format_chat_ids ) ) + 1;
}

	
	// add filter to hook when user press "insert into post" to include the attachment id
	add_filter('media_send_to_editor', 'cp_add_para_media_to_editor', 20, 2);
	function cp_add_para_media_to_editor($html, $id){

		if(strpos($html, 'href')){
			$pos = strpos($html, '<a') + 2;
			$html = substr($html, 0, $pos) . ' attid="' . $id . '" ' . substr($html, $pos);
		}
		return $html ;
	}
	
	// enable theme to support the localization
	add_action('init', 'cp_word_translation');
	function cp_word_translation(){
		load_theme_textdomain( 'neomag', get_template_directory() . '/languages/' );		
	}

	// excerpt filter
	add_filter('excerpt_length','cp_excerpt_length');
	function cp_excerpt_length(){
		return 1000;
	}

	add_action('wp_footer', 'cp_add_javascript_code');
	// Javascript Code
	function cp_add_javascript_code(){
		$cp_javascript_code = '';
		//Get Options
		$cp_general_settings = get_option('general_settings');
		if($cp_general_settings <> ''){
			$cp_logo = new DOMDocument ();
			$cp_logo->loadXML ( $cp_general_settings );
			$cp_javascript_code = cp_find_xml_value($cp_logo->documentElement,'cp_javascript_code');
			}
		echo '<script>'.$cp_javascript_code.'</script>';
	}

/**
 * Adds a dropdown to the main column on the Page edit screen.
 */
function post_categories_dropdown() {

	$screens = array( 'page' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'myplugin_sectionid',
			esc_html__( 'Select Post category','neomag' ),
			'post_categories_dropdown_callback',
			$screen, 'side'
		);
	}
}
add_action( 'add_meta_boxes', 'post_categories_dropdown' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function post_categories_dropdown_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'post_categories_dropdown_save_meta_box_data', 'post_categories_dropdown_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, 'select_name', true ); 

wp_dropdown_categories(array('hide_empty' => 0, 'name' => 'select_name', 'selected' => $value, 'hierarchical' => true));
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function post_categories_dropdown_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['post_categories_dropdown_meta_box_nonce'] ) ) {
		return;
	}
	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['post_categories_dropdown_meta_box_nonce'], 'post_categories_dropdown_save_meta_box_data' ) ) {
		return;
	}
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}
	/* OK, it's safe for us to save the data now. */
	// Make sure that it is set.
	if ( ! isset( $_POST['select_name'] ) ) {
		return;
	}
	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['select_name'] );
	// Update the meta field in the database.
	update_post_meta( $post_id, 'select_name', $my_data );
}
add_action( 'save_post', 'post_categories_dropdown_save_meta_box_data' );

/**
 * Adds a dropdown to the main column on the Post edit screen.
 */
function post_size_dropdown() {

	$screens = array( 'post' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'homepage4',
			esc_html__( 'Select Post Width size','neomag' ),
			'post_size_dropdown_callback',
			$screen, 'side'
		);
	}
}
add_action( 'add_meta_boxes', 'post_size_dropdown' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function post_size_dropdown_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'post_size_dropdown_save_meta_box_data', 'post_size_dropdown_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */

$value = get_post_meta( $post->ID, 'homepage4_post_size', true ); 

	$neomag_post_size_dropdown_html = '<select class="postform" id="homepage4_post_size" name="homepage4_post_size">';
	$neomag_post_size_dropdown_html .= '<option value="full_width" ';
	if($value == 'full_width'){ $neomag_post_size_dropdown_html .= ' selected="selected" '; }
	$neomag_post_size_dropdown_html .= ' class="level-0">Full Width</option>';
	$neomag_post_size_dropdown_html .= '<option value="half_width" ';
	if($value == 'half_width'){ $neomag_post_size_dropdown_html .= ' selected="selected" '; }
	$neomag_post_size_dropdown_html .= ' class="level-0">Half Width</option>';
	$neomag_post_size_dropdown_html .= '<option value="small_width" ';
	if($value == 'small_width'){ $neomag_post_size_dropdown_html .= ' selected="selected" '; }
	$neomag_post_size_dropdown_html .= ' class="level-0">Small Width</option>';
	$neomag_post_size_dropdown_html .= '</select>';
	echo html_entity_decode($neomag_post_size_dropdown_html);
}

/**
 * When the post is saved, saves our custom data.
 */
function post_size_dropdown_save_meta_box_data( $post_id ) {

	// Check if our nonce is set.
	if ( ! isset( $_POST['post_size_dropdown_meta_box_nonce'] ) ) {
		return;
	}
	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['post_size_dropdown_meta_box_nonce'], 'post_size_dropdown_save_meta_box_data' ) ) {
		return;
	}
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}
	} else {
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}
	/* OK, it's safe for us to save the data now. */
	// Make sure that it is set.
	if ( ! isset( $_POST['homepage4_post_size'] ) ) {
		return;
	}
	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['homepage4_post_size'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, 'homepage4_post_size', $my_data );
}
add_action( 'save_post', 'post_size_dropdown_save_meta_box_data' );




	
	add_action('wp_footer', 'cp_add_header_code');
	// Header Style or Script
	function cp_add_header_code(){
		$header_css_code = '';
		//Get Options
		$cp_general_settings = get_option('general_settings');
		if($cp_general_settings <> ''){
			$cp_logo = new DOMDocument ();
			$cp_logo->loadXML ( $cp_general_settings );
			$header_css_code = cp_find_xml_value($cp_logo->documentElement,'header_css_code');
		}
		if($header_css_code <> ''){	echo '<style>'.$header_css_code.'</style>'; }
	}
	
	add_action('wp_footer', 'cp_add_typekit_code');
	// Google Analytics
	function cp_add_typekit_code(){
		$embed_typekit_code = '';
		$cp_typography_settings = get_option('typography_settings');
		if($cp_typography_settings <> ''){
			$cp_typo = new DOMDocument ();
			$cp_typo->loadXML ( $cp_typography_settings );
			$embed_typekit_code = cp_find_xml_value($cp_typo->documentElement,'embed_typekit_code');
		}
		echo esc_attr($embed_typekit_code);
	}
	
	// Custom Post type Feed
	add_filter('request', 'cp_myfeed_request');
	function cp_myfeed_request($qv) {
		if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('post', 'portfolio');
		return $qv;
	}

	// Translate the wpml shortcode
	function cp_webtreats_lang_test( $atts, $content = null ) {
		extract(shortcode_atts(array( 'lang' => '' ), $atts));
		
		$lang_active = ICL_LANGUAGE_CODE;
		
		if($lang == $lang_active){
			return $content;
		}
	}


		//Theme Dummy Installation
		add_action('wp_ajax_themeple_ajax_dummy_data', 'cp_themeple_ajax_dummy_data');
		
		//Dummy Importer
		add_action('wp_ajax_cp_dummy_import', 'cp_dummy_import');
	//Theme Dummy Data Installation	
	function cp_themeple_ajax_dummy_data(){
		require_once BIGCARE_FW . '/extensions/importer/dummy_data.inc.php';
		die('themeple_dummy');
	}
	
	//Theme Dummy Data Installation	
	function cp_dummy_import(){
		foreach ($_REQUEST as $keys=>$values) {
			$$keys = trim($values);
		}
		$cp_layout = $layout;
		if(wp_verify_nonce( $cp_nonce_dummy, 'cp_nonce_dummy' )){
			require_once BIGCARE_FW . '/extensions/importer/dummy_data.inc.php';
			die('dummy_import');
		}else{
			die('Not Loaded');
		}
	}


	
	// Add Another theme support
	add_filter('widget_text', 'do_shortcode');
	add_theme_support( 'automatic-feed-links' );	
	
	if ( ! isset( $content_width ) ){ $content_width = 980; }
	
	// update the option if new value is exists and not equal to old one 
	function cp_save_option($name, $old_value, $new_value){
	
		if(empty($new_value) && !empty($old_value)){
			if(!delete_option($name)){
				return false;
			}
		}else if($old_value != $new_value){
			if(!update_option($name, $new_value)){
				return false;
			}
		}
		return true;
	}
	
	function neomag_cp_get_themeoption_value($para_val='',$get_option=''){ 
		//Fetch Data from Theme Options
		$cp_general_settings = get_option($get_option); 
		if($cp_general_settings <> ''){
			$cp_logo = new DOMDocument ();
			$cp_logo->loadXML ( $cp_general_settings ); 
			return cp_find_xml_value($cp_logo->documentElement,$para_val);
		}else{
			return $para_val;
		}
	}



	//Add Newsletter Table
	function add_newsletter_table() {
		global $wpdb;
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."cp_newsletter` (
			  `name` varchar(100) NOT NULL,
			  `email` varchar(100) NOT NULL,
			  `ip` varchar(16) NOT NULL,
			  `date_time` datetime NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		");
	}
	
	/* Flush rewrite rules for custom post types. */
		global $pagenow, $wp_rewrite;
		if ( 'themes.php' == $pagenow && isset( $_GET['activated'] ) ){
			add_action('init', 'add_newsletter_table');	
			
			if(get_option('default_pages_settings') == ''){$default_pages_xml = "<default_pages_settings><sidebar_default>no-sidebar</sidebar_default><right_sidebar_default>Right Sidebar</right_sidebar_default><left_sidebar_default>Left Sidebar</left_sidebar_default><default_excerpt></default_excerpt></default_pages_settings>";save_option('default_pages_settings', get_option('default_pages_settings'),$default_pages_xml);}if(get_option('general_settings') == ''){$general_settings = "<general_settings><header_logo_btn>enable</header_logo_btn><header_logo_bg>696</header_logo_bg><logo_text_cp></logo_text_cp><logo_subtext></logo_subtext><header_logo>12</header_logo><header_logo2>161</header_logo2><logo_width>204</logo_width><logo_height>71</logo_height><header_favicon>13</header_favicon><header_fav_link>http://localhost/neomag/wp-content/uploads/2015/11/favicon.png</header_fav_link><topcart_icon>disable</topcart_icon><select_layout_cp>full_layout</select_layout_cp><boxed_scheme></boxed_scheme><color_scheme>#666666</color_scheme><body_color>#6d3b3b</body_color><heading_color></heading_color><select_bg_pat>Background-Color</select_bg_pat><bg_scheme>#f1f1f1</bg_scheme><body_patren></body_patren><color_patren>/framework/images/pattern/pattern-9.png</color_patren><body_image></body_image><position_image_layout>top</position_image_layout><image_repeat_layout>no-repeat</image_repeat_layout><image_attachment_layout>fixed</image_attachment_layout><contact_us_code></contact_us_code><select_header_cp>Style 1</select_header_cp><header_style_apply>disable</header_style_apply><header_css_code></header_css_code><cp_javascript_code></cp_javascript_code><topbutton_icon></topbutton_icon><topsocial_icon>enable</topsocial_icon><topsign_icon>disable</topsign_icon><resv_button></resv_button><resv_text></resv_text><resv_short></resv_short><select_footer_cp>Style 1</select_footer_cp><footer_style_apply>enable</footer_style_apply><footer_upper_layout></footer_upper_layout><copyright_code>Copyright 2015 NeoMag by CrunchPress Themes</copyright_code><social_networking>disable</social_networking><twitter_feed></twitter_feed><twitter_home_button></twitter_home_button><twitter_id></twitter_id><consumer_key></consumer_key><consumer_secret></consumer_secret><access_token></access_token><access_secret_token></access_secret_token><footer_col_layout>footer-style1</footer_col_layout><footer_logo>524</footer_logo><footer_link></footer_link><footer_logo_width>311</footer_logo_width><footer_logo_height>55</footer_logo_height><breadcrumbs>enable</breadcrumbs><rtl_layout></rtl_layout><site_loader></site_loader><element_loader></element_loader><maintenance_mode>disable</maintenance_mode><maintenace_title>Under construction</maintenace_title><countdown_time>11/17/2016</countdown_time><email_mainte>support@crunchpress.com</email_mainte><mainte_description>We are Comming Soon</mainte_description><cp_comming_soon>Style 1</cp_comming_soon><social_icons_mainte></social_icons_mainte><donation_button></donation_button><donate_btn_text></donate_btn_text><donation_page_id></donation_page_id><donate_email_id></donate_email_id><donate_title></donate_title><donation_currency></donation_currency><safari_banner></safari_banner><safari_banner_link></safari_banner_link><safari_banner_width></safari_banner_width><safari_banner_height></safari_banner_height><tf_username></tf_username><tf_sec_api></tf_sec_api></general_settings>";save_option('general_settings', get_option('general_settings'),$general_settings);}if(get_option('typography_settings') == ''){$typography_settings = "<typography_settings><font_google>Lato</font_google><font_size_normal>14</font_size_normal><font_google_heading>Domine</font_google_heading><menu_font_google>Lato</menu_font_google><heading_h1>36</heading_h1><heading_h2>24</heading_h2><heading_h3>24</heading_h3><heading_h4>24</heading_h4><heading_h5>18</heading_h5><heading_h6>16</heading_h6><embed_typekit_code></embed_typekit_code></typography_settings>";save_option('typography_settings', get_option('typography_settings'),$typography_settings);}if(get_option('slider_settings') == ''){$slider_settings = "<slider_settings><select_slider>default</select_slider><bx_slider_settings><slide_order_bx>slide</slide_order_bx><auto_play_bx>enable</auto_play_bx><pause_on_bx>enable</pause_on_bx><animation_speed_bx>2000</animation_speed_bx><show_bullets>enable</show_bullets><show_arrow>disable</show_arrow><video_slider_on_off>enable</video_slider_on_off><video_banner_url></video_banner_url><video_banner_caption></video_banner_caption><video_banner_title></video_banner_title><safari_banner></safari_banner><safari_banner_link></safari_banner_link><safari_banner_width></safari_banner_width><safari_banner_height></safari_banner_height></bx_slider_settings></slider_settings>";save_option('slider_settings', get_option('slider_settings'),$slider_settings);}if(get_option('social_settings') == ''){$social_settings = "<social_settings><facebook_network>http://facebook.com/</facebook_network><twitter_network>https://twitter.com/</twitter_network><delicious_network>https://dribbble.com</delicious_network><google_plus_network>https://plus.google.com</google_plus_network><linked_in_network>https://www.linkedin.com</linked_in_network><youtube_network></youtube_network><flickr_network></flickr_network><vimeo_network></vimeo_network><pinterest_network>https://pinterest.com</pinterest_network><Instagram_network></Instagram_network><github_network></github_network><skype_network></skype_network><facebook_sharing>enable</facebook_sharing><twitter_sharing>enable</twitter_sharing><stumble_sharing>disable</stumble_sharing><delicious_sharing>disable</delicious_sharing><googleplus_sharing>enable</googleplus_sharing><digg_sharing>disable</digg_sharing><myspace_sharing>disable</myspace_sharing><reddit_sharing>disable</reddit_sharing></social_settings>";save_option('social_settings', get_option('social_settings'),$social_settings);}if(get_option('sidebar_settings') == ''){$sidebar_settings = "<sidebar_settings><sidebar_name>Right Sidebar</sidebar_name><sidebar_name>Left Sidebar</sidebar_name><sidebar_name>Dual Sidebar Left</sidebar_name><sidebar_name>Dual Sidebar Right</sidebar_name><sidebar_name>Contact Us Sidebar</sidebar_name><sidebar_name>Events Sidebar</sidebar_name></sidebar_settings>";save_option('sidebar_settings', get_option('sidebar_settings'),$sidebar_settings);}
		}

		//Custom background Support	
		$args = array(
			'default-color'          => '',
			'default-image'          => '',
			'wp-head-callback'       => '_custom_background_cb',
			'admin-head-callback'    => '',
			'admin-preview-callback' => ''
		);

		//Custom Header Support	
		$defaults = array(
			'default-image'          => '',
			'random-default'         => false,
			'width'                  => 950,
			'height'                 => 200,
			'flex-height'            => false,
			'flex-width'             => false,
			'default-text-color'     => '',
			'header-text'            => true,
			'uploads'                => true,
			'wp-head-callback'       => '',
			'admin-head-callback'    => '',
			'admin-preview-callback' => '',
		);
		global $wp_version;
		if ( version_compare( $wp_version, '3.4', '>=' ) ){ 
			add_theme_support( 'custom-background', $args );
			add_theme_support( 'custom-header', $defaults );
		}
	
	add_image_size('Team',360,340, true);
	add_image_size('Testimonial',231,231, true);
	add_image_size('home_3',945,342, true);
	add_image_size('about',359, 212, true);

 	function maintenance_mode(){
	}		
	
	function cp_ajax_login(){

		// First check the nonce, if it fails the function will break
		check_ajax_referer( 'ajax-login-nonce', 'security' );

		// Nonce is checked, get the POST data and sign user on
		$info = array();
		$info['user_login'] = $_POST['username'];
		$info['user_password'] = $_POST['password'];
		$info['remember'] = true;

		$user_signon = wp_signon( $info, false );
		if ( is_wp_error($user_signon) ){
			echo json_encode(array('loggedin'=>false, 'message'=> esc_html__('Wrong username or password.','neomag')));
		} else {
			echo json_encode(array('loggedin'=>true, 'message'=> esc_html__('Login successful, Now Redirecting...','neomag')));
		}
		die();
	}	
	
	function cp_ajax_login_init(){

		wp_register_script('ajax-login-script', NEOMAG_PATH_URL.'/frontend/js/ajax-login-script.js', array('jquery') ); 
		wp_enqueue_script('ajax-login-script');

		wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'redirecturl' => home_url('/'),
			'loadingmessage' => esc_html__('Sending user info, please wait...','neomag')
		));

		// Enable the user with no privileges to run ajax_login() in AJAX
		add_action( 'wp_ajax_nopriv_ajaxlogin', 'cp_ajax_login' );
	}	
	
	// Execute the action only if the user isn't logged in
	if (!is_user_logged_in()) {
		add_action('init', 'cp_ajax_login_init');		
	}
	
	function cp_ajax_signup(){
		
		// First check the nonce, if it fails the function will break
		// Nonce is checked, get the POST data and sign user on
		foreach ($_REQUEST as $keys=>$values) {
			$$keys = $values;
		}
		$default_role = get_option('default_role');
		$nickname = $_POST['nickname'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$user_email = $_POST['user_email'];
		$user_pass = $_POST['user_pass'];
		$captcha_code = $_POST['captcha_code'];
		$ajax_captcha = $_POST['ajax_captcha'];
		
		$userdata = array(
			'user_login'    => $nickname,
			'first_name'  => $first_name,
			'last_name'  => $last_name,
			'user_email'  => $user_email,
			'user_pass'  => $user_pass,
			'role' => $default_role
		);
		$user_signup = wp_insert_user( $userdata );
		$exists = email_exists($user_email);
		if ( !$exists ){
			if(strtolower($captcha_code) == strtolower($ajax_captcha)){
				if ( is_wp_error($user_signup) ){
					echo json_encode(array('signup'=>false, 'message'=> esc_html__('Please verify the details you are providing.','neomag')));
				} else {
					echo json_encode(array('signup'=>true, 'message'=> esc_html__('Your request submitted successfully, Redirecting you to login page!','neomag')));
				}
			}else{
				echo json_encode(array('signup'=>false, 'message'=> esc_html__('Notice: Invalid Captcha','neomag')));
			}
		}else{
			echo json_encode(array('signup'=>false, 'message'=> esc_html__('Notice: Email already exists!','neomag')));
		}
		die();
	}	
	
	function cp_ajax_signup_init(){

		wp_register_script('ajax-signup-script', NEOMAG_PATH_URL.'/frontend/js/ajax-signup-script.js', array('jquery') ); 
		wp_enqueue_script('ajax-signup-script');

		wp_localize_script( 'ajax-signup-script', 'ajax_signup_object', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'redirecturl' => home_url('/'),
			'loadingmessage' => esc_html__('Sending user info, please wait...','neomag')
		));
		
		// Enable the user with no privileges to run ajax_login() in AJAX
		add_action('wp_ajax_ajaxsignup', 'cp_ajax_signup');
		add_action( 'wp_ajax_nopriv_ajaxsignup', 'cp_ajax_signup' );
	}
	add_action('init', 'cp_ajax_signup_init');	

	function CP_SIGN_UP(){ ?>
		<div id="signup" class="modal fade signup" tabindex="-1" role="dialog" aria-labelledby="signup" aria-hidden="true">		
			<div class="modal-dialog modal-sm">
				<?php
				$users_can_register = get_option('users_can_register');
				if($users_can_register <> 1){ ?>
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#42;</button>
							<h3><?php esc_html_e('Sign up not allowed by admin.','neomag');?></h3>
						</div>
						<div class="modal-body">
							<p><?php esc_html_e('Please contact admin for registration.','neomag');?></p>
						</div>
						<div class="modal-footer">
						</div>
					</div>	
				<?php }else{ 
				//Start Session for Captcha
				$session_variable = '';
				$_SESSION = array();
				include_once(NEOMAG_FW. '/extensions/captcha/default/cp_default_captcha.php'); // Custom Facebook Widget 
				$_SESSION['captcha'] = simple_php_captcha();
				if(isset($_SESSION['captcha'])){
					$session_variable = $_SESSION['captcha']['image_src'];
				}
				?>
				<form id="sing-up" action="../../neomag/framework/signup" method="post">					
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#42;</button>
							<h3><?php esc_html_e('SIGN UP','neomag');?></h3>
						</div>
						<div class="modal-body">
							
							<label><?php esc_html_e('First Name','neomag');?></label>
							<input name="first_name" id="first_name" type="text" class="input-block-level" />	
							
							<label><?php esc_html_e('Last Name','neomag');?></label>
							<input name="last_name" id="last_name" type="text" class="input-block-level" />	
							
							<label><?php esc_html_e('Email Address','neomag');?></label>
							<input name="user_email" id="user_email" type="text" class="input-block-level" />	
							
							<label><?php esc_html_e('Username','neomag');?></label>
							<input name="nickname" id="nickname" type="text" class="input-block-level" />	
							
							<label><?php esc_html_e('Password','neomag');?></label>
							<input name="user_pass" id="user_pass" type="password" class="input-block-level" />	
							
							<div class="clear clearfix"></div>
							<img src="<?php echo esc_url($session_variable);?>" alt="CAPTCHA CODE" />
							<label><?php esc_html_e('Enter Captcha Code','neomag');?></label>
							<input name="captcha_code" id="captcha_code" type="text" class="input-block-level" />
							
							<?php wp_nonce_field( 'ajax-signup-nonce', 'security' ); ?>
							<input class="btn-style" type="submit" value="<?php esc_html_e('Sign Up','neomag');?>" name="submit">
							<input type="hidden" id="ajax_captcha" name="ajax_captcha" value="<?php echo esc_attr($_SESSION['captcha']['code']);?>"/>
						</div>
						<div class="modal-footer">
							<p class="status"></p>
						</div>
					</div>
				</form>	
				<?php }?>
			</div>
		</div>	
	<?php
	}
	
	function CP_SIGN_IN(){ ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
		<!--LOGIN BOX START-->
				<?php if (is_user_logged_in()) { ?>
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#42;</button>
							<h3><?php esc_html_e('You are logged in','neomag');?></h3>
						</div>
						<div class="modal-body">
							<p><?php esc_html_e('For logout click on following logout link.','neomag');?></p>
						</div>
						<div class="modal-footer">
							<a class="btn-style login_button" href="<?php echo esc_url(wp_logout_url( home_url('/') )); ?>"><?php esc_html_e('Logout','neomag');?></a>
						</div>
					</div>	
				<?php } else { ?>
				<form action="../../neomag/framework/login" method="post">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php esc_html_e('Sign In','neomag');?></h4>
                      </div>
                      <div class="modal-body">
                        <p>
                          <input class="form-control" name="username" placeholder="<?php esc_html_e('User Name','neomag');?>" type="text">
                        </p>
                        <p>
                          <input class="form-control" name="password" placeholder="<?php esc_html_e('Password','neomag');?>" type="password">
                        </p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close','neomag');?></button>
                        <input class="btn-style" type="submit" value="<?php esc_html_e('Sign In','neomag');?>" name="submit">
                        <?php wp_nonce_field( 'ajax-login-nonce', 'security2' ); ?>
                      </div>
                    </div>
				</form> 
				<?php }?>
			</div>
		</div>
		<!--LOGIN BOX END-->
   <?php }	