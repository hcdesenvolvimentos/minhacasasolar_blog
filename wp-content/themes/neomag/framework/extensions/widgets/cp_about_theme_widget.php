<?php
class about_theme_widget extends WP_Widget
{
  function about_theme_widget()
  {
    $widget_ops = array('classname' => 'about-widget', 'description' => 'About Theme Widget' );
    parent::__construct('about_theme_widget', 'CrunchPress : About Theme Widget', $widget_ops);
  }
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
	$text_caption = isset( $instance['text_caption'] ) ? ( $instance['text_caption'] ) : '';
	$text = isset( $instance['text'] ) ? ( $instance['text'] ) : '';
	$link = isset( $instance['link'] ) ? ( $instance['link'] ) : '';
	$link_text = isset( $instance['link_text'] ) ? ( $instance['link_text'] ) : '';
?>
  <p>
  <label for="<?php echo ($this->get_field_id('title')); ?>">
	 <?php esc_html_e('Section Heading:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('title')); ?>" name="<?php echo ($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('text_caption')); ?>">
	 <?php esc_html_e('Text Title:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('text_caption')); ?>" name="<?php echo ($this->get_field_name('text_caption')); ?>" type="text" value="<?php echo esc_attr($text_caption); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('text')); ?>">
	 <?php esc_html_e('Enter Text:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('text')); ?>" name="<?php echo ($this->get_field_name('text')); ?>" type="text" value="<?php echo esc_attr($text); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('link')); ?>">
	 <?php esc_html_e('Url:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('link')); ?>" name="<?php echo ($this->get_field_name('link')); ?>" type="text" value="<?php echo esc_attr($link); ?>" />
  </label>
  </p>
  <p>
  <label for="<?php echo ($this->get_field_id('link_text')); ?>">
	 <?php esc_html_e('Link url word:','neomag');?>  
	  <input class="widefat"  id="<?php echo ($this->get_field_id('link_text')); ?>" name="<?php echo ($this->get_field_name('link_text')); ?>" type="text" value="<?php echo esc_attr($link_text); ?>" />
  </label>
  </p>

<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    $instance['text_caption'] = $new_instance['text_caption'];	
    $instance['text'] = $new_instance['text'];	
    $instance['link'] = $new_instance['link'];	
    $instance['link_text'] = $new_instance['link_text'];	
	
    return $instance;
  }
 
	function widget($args, $instance)
	{
		
		extract($args, EXTR_SKIP);
		
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$text_caption = isset( $instance['text_caption'] ) ? ( $instance['text_caption'] ) : '';		
		$text = isset( $instance['text'] ) ? ( $instance['text'] ) : '';		
		$link = isset( $instance['link'] ) ? ( $instance['link'] ) : '';		
		$link_text = isset( $instance['link_text'] ) ? ( $instance['link_text'] ) : '';		
		
		echo html_entity_decode($before_widget);
		if (!empty($title)){
			echo html_entity_decode($before_title);
			echo esc_attr($title);
			echo html_entity_decode($after_title);
		}	
		
	?>
              <div class="about-content"> 
              <?php if (!empty($title)){ echo '<strong>'.($text_caption).'</strong>'; } ?>
              <?php if (!empty($text)){ echo '<p>'.($text).'</p>'; } ?>
              <?php if (!empty($link)){ echo '<a href="'.($link).'">'.($link_text).'</a>'; } ?>
               </div>
	<?php

	echo html_entity_decode($after_widget);
		}
		
	}
add_action( 'widgets_init', create_function('', 'return register_widget("about_theme_widget");') );?>