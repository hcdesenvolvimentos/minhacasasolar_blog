<?php
	/* settings */
	session_start();
	$number_of_posts = 5; //5 at a time
	$_SESSION['posts_start'] = $_SESSION['posts_start'] ? $_SESSION['posts_start'] : $number_of_posts;

	/* loading of stuff */
	if(isset($_GET['start'])) {
		/* spit out the posts within the desired range */
		echo get_posts($_GET['start'],$_GET['desiredPosts']);
		/* save the user's "spot", so to speak */
		$_SESSION['posts_start']+= $_GET['desiredPosts'];
		/* kill the page */
		die();
	}
	
	/* grab stuff */
	function get_posts($start = 0, $number_of_posts = 5) {
		/* connect to the db */
		$connection = mysql_connect('localhost','username','password');
		mysql_select_db('my_database',$connection);
		$posts = array();
		/* get the posts */
		$query = "SELECT post_title, post_content, post_name, ID FROM wp_posts WHERE post_status = 'publish' ORDER BY post_date DESC LIMIT $start,$number_of_posts";
		$result = mysql_query($query);
		while($row = mysql_fetch_assoc($result)) {
			preg_match("/<p>(.*)<\/p>/",$row['post_content'],$matches);
			$row['post_content'] = strip_tags($matches[1]);
			$posts[] = $row;
		}
		/* return the posts in the JSON format */
		return json_encode($posts);
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr">
<head>
	<style type="text/css">
		#posts-container			{ width:400px; border:1px solid #ccc; -webkit-border-radius:10px; -moz-border-radius:10px; }
		.post						{ padding:5px 10px 5px 100px; min-height:65px; border-bottom:1px solid #ccc; background:url(dwloadmore.png) 5px 5px no-repeat; cursor:pointer;  }
		.post:hover					{ background-color:lightblue; }
		a.post-title 				{ font-weight:bold; font-size:12px; text-decoration:none; }
		a.post-title:hover			{ text-decoration:underline; color:#900; }
		a.post-more					{ color:#900; }
		p.post-content				{ font-size:10px; line-height:17px; padding-bottom:0; }
		#load-more					{ background-color:#eee; color:#999; font-weight:bold; text-align:center; padding:10px 0; cursor:pointer; }
		#load-more:hover			{ color:#666; }
		.activate					{ background:url(loadmorespinner.gif) 140px 9px no-repeat #eee; }
	</style>
	<script type="text/javascript" src="mootools.1.2.3.js"></script>
	<script type="text/javascript">
	//safety closure
	(function($) {
		//domready event
		window.addEvent('domready',function() {
			//settings on top
			var domain = 'http://davidwalsh.name/';
			var initialPosts = <?php echo get_posts(0,$_SESSION['posts_start']);  ?>;
			
			//function that creates the posts
			var postHandler = function(postsJSON) {
				postsJSON.each(function(post,i) {
					//post url
					var postURL = '' + domain + post.post_name;
					//create the HTML
					var postDiv = new Element('div',{
						'class': 'post',
						events: {
							click: function() {
								window.location = postURL;
							}
						},
						id: 'post-' + post.ID,
						html: '<a href="' + postURL + '" class="post-title">' + post.post_title + '</a><p class="post-content">' + post.post_content + '<br /><a href="' + postURL + '" class="post-more">Read more...</a></p>'
					});
					//inject into the container
					postDiv.inject($('posts'));
					//create the Fx Slider
					var fx = new Fx.Slide(postDiv).hide().slideIn();
					//scroll to first NEW item
					if(i == 0) {
						var scroll = function() {
							new Fx.Scroll(window).toElement($('post-' + post.ID));
						};
						scroll.delay(300); //give time so scrolling can happen
					}
				});
				
			};
			
			//place the initial posts in the page
			postHandler(initialPosts);
			
			//a few more variables
			var start = <?php echo esc_js($_SESSION['posts_start']); ?>;
			var desiredPosts = <?php echo esc_js($number_of_posts); ?>;
			var loadMore = $('load-more');
			var request = new Request.JSON({
				url: 'mootools-version.php', //ajax script -- same page
				method: 'get',
				link: 'cancel',
				noCache: true,
				onRequest: function() {
					//add the activate class and change the message
					loadMore.addClass('activate').set('text','Loading...');
				},
				onSuccess: function(responseJSON) {
					//reset the message
					loadMore.set('text','Load More');
					//increment the current status
					start += desiredPosts;
					//add in the new posts
					postHandler(responseJSON);
				},
				onFailure: function() {
					//reset the message
					loadMore.set('text','Oops! Try Again.');
				},
				onComplete: function() {
					//remove the spinner
					loadMore.removeClass('activate');
				}
			});
			//add the "Load More" click event
			loadMore.addEvent('click',function(){
				//begin the ajax attempt
				request.send({
					data: {
						'start': start,
						'desiredPosts': desiredPosts
					},
				});
			});
		});
	})(document.id);
	</script>
</head>
<body>
	
	<!-- Widget XHTML Starts Here -->
	<div id="posts-container">
		<!-- Posts go inside this DIV -->
		<div id="posts"></div>
		<!-- Load More "Link" -->
		<div id="load-more">Load More</div>
	</div>
	<!-- Widget XHTML Ends Here -->
	
</body>
</html>