<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php esc_html(bloginfo( 'charset' )); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	

	<link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png"/>
	<link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {

		$neomag_header_favicon = neomag_cp_get_themeoption_value('header_favicon','general_settings');
		$neomag_header_fav_link = neomag_cp_get_themeoption_value('header_fav_link','general_settings');
		if($neomag_header_favicon <> '' && $neomag_header_fav_link <> ''){ 
			echo '<link rel="shortcut icon" href="'.esc_url($neomag_header_fav_link).'" />';
		}
}
		$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings'); 
		wp_head();
		?>
		<?php if(is_page(39)){ ?>
		<style>
			.bx-pager {display: none!important;}
			.bx-wrapper .bx-prev{
				margin-left: -10px;
				background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/passador_left.jpg);
				background-position: 0px 0px!important;
			}
			.bx-wrapper .bx-prev:hover{
				background-position: 0px 0px!important;
			}
			.bx-wrapper .bx-next{
				margin-right: -10px;
				background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/passador_right.jpg);
				background-position: 0px 0px!important;
			}
			.bx-wrapper .bx-next:hover{
				background-position: 0px 0px!important;
			}
		</style>
		<?php } ?>
</head>
	<?php if(is_page_template( 'homev2.php' ) || is_single() || is_404() || ($neomag_maintenance_mode <> 'disable') ){ ?>
    <body <?php body_class('inner-page'); ?>>
		<?php } elseif(is_page_template( 'homev3.php' )||is_page_template( 'homev4.php' )||is_page_template( 'homev5.php' )||is_page_template( 'homev1.php' )) { ?>
        <body <?php body_class(); ?>>
			<?php } elseif(is_search()){ ?>
            <body class="search-results  inner-page cp_full_width inner_page_cp ">
				<?php } else { ?>
                <body <?php body_class('inner-page'); ?>>
<?php } ?>
    <!--WRAPPER START-->
	<div id="wrapper"> 
		<!--LOGIN BOX START-->
		<?php CP_SIGN_UP();?>
		<!--LOGIN BOX END-->
		<!--SIGN UP BOX START-->
		<?php CP_SIGN_IN();
		//Print Header
		if(is_search() || is_404()){
			$neomag_header_style = '';
		}else{
			$neomag_header_style = neomag_get_header_style();
		}
		$neomag_maintenance_mode = neomag_cp_get_themeoption_value('maintenance_mode','general_settings'); 
		if($neomag_maintenance_mode <> 'disable'){
			$neomag_header_style = 'Style 1';
			}
		if(cp_print_header_html_val($neomag_header_style) <> 'Style 7'){
			
			cp_print_header_html($neomag_header_style);
		}
?>