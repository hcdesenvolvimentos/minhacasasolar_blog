<?php
/**
 * The template for displaying featured content
 *
 * @package CrunchPress
 * @subpackage Neomag
 */
?>
<li class="cp-post">
		<div id="<?php the_ID(); ?>" <?php post_class('sticky'); ?>>
	<?php
		$neomag_thumbnail_types = '';
		$neomag_counter_posts = 1;
		$neomag_featured_posts = neomag_cp_get_featured_posts();
		foreach ( (array) $neomag_featured_posts as $order => $post ) :
			setup_postdata( $post ); 
			$neomag_thumbnail_types = '';
			$neomag_post_detail_xml = get_post_meta($post->ID, 'post_detail_xml', true);
			if($neomag_post_detail_xml <> ''){
				$neomag_cp_post_xml = new DOMDocument ();
				$neomag_cp_post_xml->loadXML ( $neomag_post_detail_xml );
				$neomag_post_social = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_social');
				$neomag_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'sidebar_post');
				$neomag_right_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'right_sidebar_post');
				$neomag_left_sidebar = cp_find_xml_value($neomag_cp_post_xml->documentElement,'left_sidebar_post');
				$neomag_thumbnail_types = cp_find_xml_value($neomag_cp_post_xml->documentElement,'post_thumbnail');
				$neomag_video_url_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'video_url_type');
				$neomag_select_slider_type = cp_find_xml_value($neomag_cp_post_xml->documentElement,'select_slider_type');	
			}
				$neomag_thumbnail_id = get_post_thumbnail_id( $post->ID );
				$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, array(1170,350));
				$neomag_image_thumb = wp_get_attachment_image_src($neomag_thumbnail_id, 'full');
				$neomag_mask_html = '';
				$neomag_no_image_class = 'no-image';
				if(get_the_post_thumbnail($post->ID, array(1170,350)) <> ''){
					$neomag_mask_html = '<div class="mask">
						<a href="'.get_permalink().'#comments" class="anchor"><span> </span> <i class="fa fa-comment"></i></a>
						<a href="'.get_permalink().'" class="anchor"> <i class="fa fa-link"></i></a>
					</div>';
					$neomag_no_image_class = 'image-exists';
				}			

				$neomag_get_post_cp = get_post($post);
			?>
				<!--BLOG LIST ITEM START-->
                 <div class="cp-thumb"> 
                        <?php if($neomag_image_thumb[0] <> ''){ ?>
                        <img src="<?php echo esc_url($neomag_image_thumb[0]); ?>" alt="<?php echo esc_attr($post->ID); ?>">
                          <div class="cp-post-hover"> <a href="<?php echo get_permalink();?>"><i class="fa fa-link"></i></a> 
                          <a href="<?php echo get_permalink();?>"><i class="fa fa-search"></i></a> </div>
                        <?php } ?>
                        </div>
                        <div class="cp-post-base">
                          <div class="cp-post-content">
                            <a href="<?php echo get_permalink();?>"><h2><?php echo get_the_title(); ?></h2></a>
                            <ul class="cp-post-meta">
                              <li><a href="<?php echo get_permalink();?>"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ); ?> 
                              <?php esc_html_e('ago','neomag'); ?></a></li>
                              <?php
                                    $neomag_get_category_obj = get_the_category ( $post->ID ); 
                                    $neomag_category_name = array_shift( $neomag_get_category_obj );
									$neomag_catlink = get_category_link( $neomag_category_name->term_id ); 
                              ?>
                              <li><a href="<?php echo esc_url($neomag_catlink); ?>"><?php echo esc_attr($neomag_category_name->name); ?></a></li>
                            </ul>
                            <?php $neomag_post_content_image = neo_mag_catch_that_image(); 
								if($neomag_post_content_image <> ''){
									echo '<div class="content-img"><img src="'.esc_url($neomag_post_content_image).'" alt="'.$post->ID.'">';
									} else {
							?>
                            <p><?php echo html_entity_decode(mb_substr(get_the_content(),0, 400));?></p>
                            <?php } ?>
                            <a href="<?php echo get_permalink();?>" class="read-more"><?php esc_html_e('Read More','neomag'); ?></a> 
                           <?php
						   global $allowedposttags;
                                    //Get Post Comment 
                                    comments_popup_link(wp_kses( __('<i class="fa fa-comment-o"></i> Leave a Comment','neomag'),$allowedposttags),
                                    esc_html__('1 Comment','neomag'),
                                    esc_html__('% Comments','neomag'), '',
                                    esc_html__('Comments are off','neomag') );
                              ?>
                            </div>
                        </div>
			<?php endforeach; ?>
	</div><!-- .featured-content-inner -->
 </li>